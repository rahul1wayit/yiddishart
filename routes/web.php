<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Auth::routes();
Auth::routes(['verify' => true]);

Route::get('/', function () {
  return view('welcome');
});
Route::get('/sellers', 'Auth\RegisterController@showRegistrationForm')->name('seller');
Route::get('/confirm-account/{id}', 'Auth\RegisterController@confirmAccount')->name('confirm-account');
Route::get('/verify/{id}', 'Auth\RegisterController@verifyPage')->name('verify');
Route::get('/resend-register-email/{id}', 'Auth\RegisterController@resendRegisterEmail')->name('resend-register-email');

Route::get('/home', 'HomeController@index')->name('home');
Route::any('/front-search', 'HomeController@frontSearch');
Route::any('/sort-search', 'HomeController@searchShort');
Route::any('/template/{id}', 'HomeController@template');

Route::any('/contact-us', 'HomeController@contactUs')->name('contact-us');
Route::any('/support-center', 'HomeController@supportCenter')->name('support-center');
Route::get('/support-center/{id}/{question}', 'HomeController@supportCatoryWise');
Route::get('/support-center-answer/{id}/{question}', 'HomeController@supportCenterAnswer');

//Route::any('/support-search', 'HomeController@supportSearch');
Route::get('/faq', 'HomeController@faq')->name('faq');
Route::get('/about-us', 'HomeController@aboutUs')->name('about-us');
Route::get('/events', 'HomeController@events')->name('events');
Route::get('/careers', 'HomeController@careers')->name('careers');
Route::get('/categories', 'HomeController@categories')->name('categories');
Route::get('/category/{slug}', 'HomeController@categoryWise')->name('category-wise');

Route::get('/payment-view/{id}', 'PaymentController@paymentView')->name('payment-view');

if(!function_exists('adminRoutes')) {
  function adminRoutes() {
    Route::group(['namespace' => 'Admin','middleware' => 'disablepreventback'], function()
    {
      Route::resource('dashboard', 'DashboardController');
      Route::get('settings', 'DashboardController@settings');
      Route::post('postCredentials', 'DashboardController@postCredentials');
      Route::post('updateimage', 'DashboardController@updateImage');
      Route::any('uploadSingleImage', 'DashboardController@uploadSingleImage');
      Route::post('deleteSingleImage', 'DashboardController@deleteSingleImage');
      Route::resource('sellers', 'SellersController');
      Route::any('search-sellers', 'SellersController@searchSellers');
      Route::post('change-status', 'SellersController@changeStatus');
      Route::any('entriesperpage', 'SellersController@entriesperpage');
      Route::post('deactivate-seller', 'SellersController@deactivateSeller');
      Route::resource('buyers', 'BuyersController');
      Route::any('search-buyers', 'BuyersController@searchBuyers');
      Route::any('entriesperpage', 'BuyersController@entriesperpage');
      Route::post('deactivate-buyer', 'BuyersController@deactivateBuyer');
      Route::resource('subscription_plan', 'SubscriptionplanController');
      Route::get('subscription', 'SubscriptionplanController@subscription');
      Route::get('new-subscription', 'SubscriptionplanController@newSubscription');
      Route::any('seller-design/{id}', 'SellersController@sellerDesign');
      Route::any('seller-logs/{id}/{designid}', 'SellersController@sellerLogs');

      Route::resource('events', 'EventController');

      Route::resource('orders', 'OrderController');
      Route::any('search-orders', 'OrderController@searchOrders');
      Route::get('order-details/{id}', 'OrderController@orderDetail');

      Route::get('transactions', 'TransactionController@index');
      Route::any('search-transaction', 'TransactionController@searchTransaction');

      Route::resource('faq', 'FaqController');
      Route::post('edit-modal', 'FaqController@editModal');
      // support center
      Route::resource('support-center', 'SupportCenterController');
      Route::post('edit-support-modal', 'SupportCenterController@editModal');

      Route::resource('support-category', 'SupportCategoryController');
      Route::post('support-category-modal', 'SupportCategoryController@supportCategoryModal');
    });
  }
}
if(!function_exists('buyerRoutes')) {
  function buyerRoutes() {
    Route::group(['namespace' => 'Buyer','middleware' => 'disablepreventback'], function()
    {
      Route::resource('/', 'DashboardController');
      //Route::resource('/dashboard', 'DashboardController');
      Route::resource('/dashboard', 'AccountSettingsController');
      Route::resource('/account-settings', 'AccountSettingsController');
      // Route::get('/resend-register-email/{id}', 'AccountSettingsController@resendRegisterEmail');
      Route::post('/changePassword','AccountSettingsController@changePassword')->name('changePassword');
      Route::post('/changeGenralInfo','AccountSettingsController@changeGenralInfo')->name('changeGenralInfoBuyer');
      Route::post('/addRatingFaviorite','DashboardController@addRatingFaviorite')->name('addRatingFaviorite');
      Route::resource('/favourite','FavoriteController');
      Route::get('/downloads','FavoriteController@downloads');
      Route::get('/edit-template/{id}', 'FavoriteController@editTemplate');
      Route::any('search-downloads', 'FavoriteController@searchDownload');
      Route::any('uploadSingleImage', 'FavoriteController@uploadSingleImage');
      Route::any('/add-to-cart', 'CartController@addToCart');
      Route::any('/cart', 'CartController@index');
      Route::any('/cart-list', 'CartController@cartList');
      Route::any('/delete-update-cart-item', 'CartController@removeUpdateFromCart');
      Route::get('/checkout', 'CheckoutController@index')->name('checkout');
      Route::any('/payment-with-paypal', 'CheckoutController@paypalWithPayment')->name('payment.paypal');
      Route::get('/paypal-success', 'CheckoutController@paypalSuccess')->name('payment.paypalSuccess');

      Route::resource('orders', 'OrderController');
      Route::get('order-details/{id}', 'OrderController@orderDetail');
      Route::post('addRatingReview', 'FavoriteController@addRatingReview');
      Route::any('getRatingReview', 'FavoriteController@getRatingReview');


    });
  }
}
if(!function_exists('sellerRoutes')) {
  function sellerRoutes() {
    Route::group(['namespace' => 'Seller','middleware' => 'disablepreventback'], function() {
      //Route::group(['middleware' => 'plan'], function() {
      Route::group([], function() {
        Route::resource('/', 'DashboardController');
        Route::resource('dashboard', 'DashboardController');
        Route::resource('account-settings', 'AccountSettingsController');
        Route::post('/changePassword','AccountSettingsController@changePassword')->name('changePassword');
        Route::resource('portfolio', 'DesignController');
        Route::post('geteditdesignmodal', 'DesignController@getEditDesignModal');
        Route::any('uploadSingleImage', 'DesignController@uploadSingleImage');

        Route::get('uploads', 'DesignController@getUploads')->name('uploads');
        Route::any('search-uploads', 'DesignController@searchUploads');
        Route::post('addFont', 'DesignController@addFont');

        Route::post('/changeGenralInfo','AccountSettingsController@changeGenralInfo')->name('changeGenralInfo');
        Route::any('/getImageEditor', 'DesignController@getImageEditor');

        Route::resource('earnings', 'EarningController');
        Route::any('search-earnings', 'EarningController@searchEarnings');
        Route::any('get_earning_details', 'EarningController@get_earning_details');
        Route::any('download-csv', 'EarningController@earningsExport');
      });

      Route::resource('choose_plan', 'ChoosePlanController');
    });
  }
}
Route::group(['prefix' => 'admin','middleware' => ['auth','role:admin','design']], function()
{
  adminRoutes();
});
Route::group(['prefix' => 'buyer','middleware' => ['auth','role:buyer']], function()
{
  buyerRoutes();
});
Route::group(['prefix' => 'seller','middleware' => ['auth','role:seller','verified']], function()
{
  sellerRoutes();
});
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
