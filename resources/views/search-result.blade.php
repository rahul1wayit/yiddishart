<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="search_head">
        <!-- <h4>< Search Term > <span class="searchNameOn"></span></h4> -->
        <h4>Search results for <span class="searchCategoryNameOn">{{ $searchCatByName }}</span></h4>
      </div>
      <div class="search_items">
        <div class="search_filter">
          <ul>
            <li>Sort by</li>
            @php
             $asc= "Uparrow.png";
             $dsc= "Downarrow.png";
            @endphp
            <li><span class="searchShort" ref="design" data-type="price" rel="{{ $order }}">Price
              @if($column =='price')
                @if($order == 'asc')

                  <img src="{{ asset('assets/images/'.$asc) }}"/>
                @else
                  <img src="{{ asset('assets/images/'.$dsc) }}"/>
                @endif
              @else
                  <img src="{{ asset('assets/images/'.$asc) }}"/>
              @endif
            </span></li>
            <li><span class="searchShort" ref="design" data-type="id" rel="{{ $order }}">
              @if($column =='id')
                @if($order == 'asc')
                Latest
                  <img src="{{ asset('assets/images/'.$asc) }}"/>
                @else
                  Oldest
                  <img src="{{ asset('assets/images/'.$dsc) }}"/>
                @endif
              @else
              Latest
                  <img src="{{ asset('assets/images/'.$asc) }}"/>
              @endif
             </span></li>
        <!--     <li><span class="searchShort" ref="design" data-type="id" rel="{{ $order }}">Oldest
              @if($column =='id')
                @if($order == 'asc')
                  <img src="{{ asset('assets/images/'.$asc) }}"/>
                @else
                  <img src="{{ asset('assets/images/'.$dsc) }}"/>
                @endif
              @endif
            </span></li> -->
          </ul>
        </div>
        <div class="row">
          @if(!$result->isEmpty())
          @foreach ($result as $list)
          <div class="col-md-3">
            <div class="search_item">
              <div class="item_img">
                <img src="{{ asset("assets/images/seller-design/".$list->designimage) }}" alt="{{ $list->name }}" />
              </div>
              <div class="item_cont">
                <h5><a href="{{ URL('/template/'.Crypt::encrypt($list->id)) }}" >{{ ucfirst($list->name) }}</a>
                  @php
                  if(isset($list->ratings))
                  $fav = $list->ratings->favourite;
                  else
                  $fav = '';

                  if(isset($list->totalRating))
                  $rate = $list->totalRating->avg('rating');
                  else
                  $rate = '';

                  @endphp
                  @if(Auth::check())
                  @if(Auth::user()->roles()->first()->name == 'buyer')
                  <a class="favourite favouriteBtn @if($fav == 1) active @endif" data-id="{{ $list->id }}" data-encid="{{ Crypt::encrypt($list->id) }}" href="javascript:void(0);"><i class="fa @if($fav == 1) fa-heart @else fa-heart-o @endif"></i></a>
                  @endif
                  @else
                  <a class="favourite favouriteBtn redirectTo" data-id="{{ $list->id }}" data-encid="{{ Crypt::encrypt($list->id) }}" href="javascript:void(0);"><i class="fa fa-heart-o"></i></a>

                  @endif
                </h5>
                <h4>{{ Helper::numberFormat($list->price) }}</h4>
                <div class='rating-stars'>
                  <ul class='stars'>
                    @if($rate != '')
                    @for($i = 1; $i <= $rate; $i++)
                    <li class='star' data-value=''>
                      <i class='fa fa-star fa-fw'></i>
                    </li>
                    @endfor
                    @for($j = $rate; $j < 5; $j++)
                    <li class='star' data-value=''>
                      <i class='fa fa-star-o fa-fw'></i>
                    </li>
                    @endfor
                    @endif
                  </ul>
                </div>
                <p>{{ $list->business_category->name }}</p>
              </div>
            </div>
          </div>
          @endforeach
          @else
          <span class="text-center"> No record found.</span>
          @endif
        </div>
      </div>
    </div>
  </div>
</div>
