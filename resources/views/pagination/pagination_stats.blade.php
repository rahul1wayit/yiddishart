@extends('layouts.frontend')
@section('content')
<section class="main_content_wrapper">
      <div class="banner text-center">
        <h1>Send your love<br/>with design you love</h1>
        <div class="banner_search">
          <div class="container">
            <div class="input-group" id="adv-search">
                <input type="text" class="form-control" placeholder="Search for snippets" />
                <div class="input-group-btn">
                    <div class="btn-group" role="group">
                        <div class="dropdown dropdown-lg">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">in Category <span class="caret"></span></button>
                            <div class="dropdown-menu dropdown-menu-right" role="menu">
                                <form class="form-horizontal" role="form">
                                  <div class="form-group">
                                    <label for="filter">Filter by</label>
                                    <select class="form-control">
                                        <option value="0" selected>All Snippets</option>
                                        <option value="1">Featured</option>
                                        <option value="2">Most popular</option>
                                        <option value="3">Top rated</option>
                                        <option value="4">Most commented</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="contain">Author</label>
                                    <input class="form-control" type="text" />
                                  </div>
                                  <div class="form-group">
                                    <label for="contain">Contains the words</label>
                                    <input class="form-control" type="text" />
                                  </div>
                                  <button type="submit" class="btn btn-primary">Search</button>
                                </form>
                            </div>
                        </div>
                        <button type="button" class="btn btn-primary">Search</button>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
      <div class="Featured_categories">
        <div class="container">
          <div class="Featured_panel">
            <div class="row">
              <div class="col-md-6">
                <h4 class="section_head">Favourite Categories</h4>
              </div>
              <div class="col-md-6 text-right">
                <a href="">Browse all ></a>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="Feature_box">
                  <div class="img_box">
                    <img src="{{ asset('assets/images/f_img1.jpg') }}"/>
                  </div>
                  <div class="image_title">
                    <h5>School</h5>
                  </div>
                </div>
                <div class="Feature_box">
                  <div class="img_box">
                    <img src="{{ asset('assets/images/f_img2.jpg')}}"/>
                  </div>
                  <div class="image_title">
                    <h5>Simcha</h5>
                  </div>
                </div>
                <div class="Feature_box">
                  <div class="img_box">
                    <img src="{{ asset('assets/images/f_img3.jpg')}}"/>
                  </div>
                  <div class="image_title">
                    <h5>Annual</h5>
                  </div>
                </div>
                <div class="Feature_box">
                  <div class="img_box">
                    <img src="{{ asset('assets/images/f_img4.jpg')}}"/>
                  </div>
                  <div class="image_title">
                    <h5>Holiday</h5>
                  </div>
                </div>
                <div class="Feature_box">
                  <div class="img_box">
                    <img src="{{ asset('assets/images/f_img5.jpg')}}"/>
                  </div>
                  <div class="image_title">
                    <h5>Shul</h5>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="spacer20"></div>
      <div class="Design_process">
        <div class="container">
            <h4 class="section_head">How to make your custom design</h4>
            <div class="row text-center">
              <div class="col-md-3">
                  <img src="{{ asset('assets/images/process1.jpg')}}"/>
                  <h5>Search and find <br/>the design you like</h5>
              </div>
              <div class="col-md-3">
                  <img src="{{ asset('assets/images/process2.jpg')}}"/>
                  <h5>Modify the text<br/>to make it your own</h5>
              </div>
              <div class="col-md-3">
                  <img src="{{ asset('assets/images/process3.jpg')}}"/>
                  <h5>Send to printer<br/> or download</h5>
              </div>
              <div class="col-md-3">
                  <img src="{{ asset('assets/images/process4.jpg')}}"/>
                  <h5>Enjoy</h5>
              </div>
            </div>
        </div>
      </div>
      <div class="spacer20"></div>
      <div class="special_schools">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="special_school_panel">
                <div class="row">
                  <div class="col-md-4">&nbsp;
                  </div>
                  <div class="col-md-8">
                    <div class="row">
                      <div class="col-md-8">
                        <h3>SPECIAL <span>for schools</span></h3>
                        <p>Sign up to become member and have access to unlimited downloads for a annual price</p>
                      </div>
                      <div class="col-md-4">
                        <a href="#" class="btn btn-primary">Sign up</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
@endsection
