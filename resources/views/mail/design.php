<!DOCTYPE html>
<body>

  <table border="0" width="100%" style="padding:30px; border:1px solid #ccc;">
    <tr>
      <td style="text-align:center;"><img src="<?php echo URL('/assets/images/logo.png'); ?>" style="width:150px;"/></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><b style="font-size:17px;">Hello Admin,</b></td>
    </tr>
    <tr>
      <td><?php echo $msg; ?></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><b style="font-size:17px;">Design Detail</b></td>
    </tr>
    <tr>
      <td>
        <table style="width:100%;">
          <tr>
            <td style="width:30%;">Uploaded Date</td>
            <td style="width:70%;"><?php echo date('m/d/Y'); ?></td>
          </tr>
          <tr>
          <td style="width:30%;">Design</td>
          <td style="width:70%;"><?php echo $data['name']; ?></td>
          </tr>
          <tr>
          <td style="width:30%;">Price</td>
          <td style="width:70%;"><?php echo '$ '.$data['price']; ?></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>Sincerely, </td>
    </tr>
    <tr>
      <td>Support <b>Yiddishart</b> </td>
    </tr>
  </table>
</body>
</html>
