<!DOCTYPE html>

<body style="padding:10px;">
	<table border="0" width="100%">
		<tbody>
			<tr>
				<td>
					<table align="center" border="0" cellpadding="0" cellspacing="0" style="background:#fff; border: 1px solid #ddd; box-shadow:0 0 10px #ccc;" width="600">
						<tbody>
							<tr>
								<td align="center" height="100" style="{{ asset('assets/images/logo.png') }} 0 0 repeat-x; border-top: 6px solid #566369; border-bottom:solid 1px #f5f5f5; background:#eee;" valign="middle"><img src="{{ asset('assets/images/logo.png') }}"> </td>
							</tr>
							<tr>
								<td>
									<table cellpadding="0" cellspacing="0" width="100%">
										<tbody>
											<tr>
												<td width="15">&nbsp;</td>
												<td width="570">
													<table cellpadding="0" cellspacing="0" width="100%">
														<tbody>
															<tr>
																<td height="15">&nbsp;</td>
															</tr>
															<tr>
																<td width="530">
																	<table cellpadding="0" cellspacing="0" width="100%">
																		<tbody>
																			<tr>
																				<td style="font-family:Arial, Helvetica, sans-serif; padding:10px; font-size:13px; color:#333;">
																					<table border="0" cellpadding="0" cellspacing="0" width="100%">
																						<tbody>
																							<tr>
																								<td colspan="2" align="left" height="30" style="font-size:18px; font-family:Verdana, Geneva, sans-serif; color:#1f7cae; padding-bottom:22px;" valign="middle"><strong>Hi Admin,</strong></td>
																							</tr>
																							<tr>
																								<td colspan="2" align="left" style="color: #fff;background-color:#1f7cae; border-bottom:solid 1px #8dbfd9;font-size: 17px;height: 40px;line-height: 27px;text-align: center;margin-bottom:22px;" valign="middle">New Message Received</td>
																							</tr>
																							<tr>
																								<td style="padding:5px; font-size:15px;">
																									<strong>Name</strong>
																								</td>
																								<td style="padding:5px; font-size:15px;">
																									{{ $data->name }}
																								</td>
																							</tr>
																							<tr>
																								<td style="padding:5px; font-size:15px;">
																									<strong>Email</strong>
																								</td>
																								<td style="padding:5px; font-size:15px;">
																									{{ $data->email }}
																								</td>
																							</tr>
																							<tr>
																								<td style="padding:5px; font-size:15px;">
																									<strong>Phone Number</strong>
																								</td>
																								<td style="padding:5px; font-size:15px;">
																									{{ $data->phone }}
																								</td>
																							</tr>
																							<tr>
																								<td style="padding:5px; font-size:15px;">
																									<strong>Message</strong>
																								</td>
																								<td style="padding:5px; font-size:15px;">
																									{{ $data->message }}
																								</td>
																							</tr>
																							<tr><td colspan="2">&nbsp;</td></tr>
																							<tr>
																								<td colspan="2" align="left" style="font-family:Arial, Helvetica, sans-serif; font-size:17px;color:#1f7cae;" valign="top">Regards,<br />
																									Yiddishart Team</td>
																								</tr>
																							</tbody>
																						</table>
																					</td>
																				</tr>
																			</tbody>
																		</table>
																	</td>
																</tr>
																<tr>
																	<td height="15" style="padding-bottom:15px;">
																		<p>*This email account is not monitored. Please do not reply to this email as we will not be able to read and respond to your messages.</p>
																	</td>
																</tr>
															</tbody>
														</table>
													</td>
													<td width="15">&nbsp;</td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
								<tr>
									<td align="center" style="padding:5px; background: none repeat scroll 0 0 #333; border-top: 1px solid #CCCCCC;color:#fff;" valign="top">
										<p>You have received this message by auto generated e-mail.</p>

										<center> </center>
									</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</tbody>
		</table>


	</body>
	</html>


</body>
</html>
