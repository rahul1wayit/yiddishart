@extends('layouts.frontend')
@section('content')
<section class="main_content_wrapper searchSection">
	<div class="container">
		<div class="row">
			<div class="col-md-12" id="errorrr">
				<div class="title_heading text-center">
					<h2>Login</h2>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="signup-form">
					<div class="row">
						<div class="col-md-5">
							@if (\Session::has('success'))
							<div class="alert alert-success">
								<ul>
									<li>{!! \Session::get('success') !!}</li>
								</ul>
							</div>
							@endif
							@if (\Session::has('error'))
							<div class="alert alert-warning">
								<ul>
									<li>{!! \Session::get('error') !!}</li>
								</ul>
							</div>
							@endif
							<form method="POST" action="{{ route('login') }}" id="login-form">
								@csrf
								<div class="form-group">
									<input id="email" type="email" class="form-control" name="email" placeholder="E-mail" >
									@if (Request::has('previous'))
									<input type="hidden" name="previous" value="{{ Request::get('previous') }}">
									<input type="hidden" name="design_id" value="{{ Request::get('design_id') }}">
									@endif
								</div>
								<div class="form-group">
									<input id="password" type="password" class="form-control" name="password" placeholder="Password">
								</div>
								<div class="form-group">
									<div class="checkbox_rem d-inline text-left">
										<label><input type="radio" name="login_as" value="1"> Login as a Seller</label>
									</div><br>
									<div class="checkbox_rem d-inline text-left">
										<label><input type="radio" name="login_as" value="2"> Login as a Buyer</label>
									 </div>
								</div>
								<div class="form-group">
									<button type="submit" class="btn btn-primary">Login</button>
								</div>
								<div class="form-group text-right">

										@if (Route::has('password.request'))
												<a class="forgot" href="{{ route('password.request') }}">
														{{ __('Forgot Password?') }}
												</a>
										@endif
								</div>
							</form>
						</div>
						<div class="col-md-7">
							<img src="{{asset('assets/images/signup-img.png')}}" class="img-responsive">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
