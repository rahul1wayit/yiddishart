@extends('layouts.frontend')
@section('content')
<section class="main_content_wrapper">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="title_heading text-center">
          <h2>Complete Registration</h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="Complete_Registration">
          <div class="row">
            <div class="col-md-6">
              <h4>Email have been sent to<br/> <strong>{{ $data['0']['email'] }}</strong></h4>
              <div class="spacer15"></div>
              @php
              $redirectUrl = url('/').'/resend-register-email'.'/'.Crypt::encrypt($data['0']['id'])
              @endphp
              <p>To complete registration, click on verification link in email that we just sent you. Thank You. </p>
              <p> {{ __('If you did not receive the email') }}, <a href="{{ $redirectUrl }}" class="hrefColor">{{ __('click here to request another') }}</a>.</p>
            </div>
            <div class="col-md-6">
              <img src="{{ asset('assets/images/Complete_Registration.png') }}" class="img-responsive"/>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
