@extends('layouts.frontend')
@if (Request::path() == 'sellers')
    @include('auth.seller')
@else
    @include('auth.default')
@endif
