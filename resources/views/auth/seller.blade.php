@section('content')
<section class="main_content_wrapper searchSection">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="title_heading text-center">
          <h2>Become Seller</h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="become_seller-panel">
          <form method="POST" id="seller-register-form1" action="{{ route('register') }}" enctype="multipart/form-data">
            @csrf
            <div class="row">
              <div class="col-md-3">
                <div class="upload_img">
                  <a href="javascript:void(0);" class="image-upload">
                    <span>
                      <img src="{{ asset('assets/images/user.png') }}" class="view-profile"/>
                    </span>
                    <div class="clearfix"></div>
                    <i class="fa fa-plus"></i>
                    <em>upload image</em>
                  </a>
                  <input type="file" style="display:none;" class="profile-img" name="profileimage">
                </div>
                <span class="error-image" style="color: #ff0000;"></span>
              </div>
              <div class="col-md-6">
                <h5>Registration Process</h5>
                <div class="form-group">
                  <input id="first_name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old('first_name') }}" placeholder="First Name" autofocus>
                  @if ($errors->has('first_name'))
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('first_name') }}</strong>
                  </span>
                  @endif
                </div>
                <div class="form-group">
                  <input id="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ old('last_name') }}" placeholder="Last Name" autofocus>
                  @if ($errors->has('last_name'))
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('last_name') }}</strong>
                  </span>
                  @endif
                </div>
                <div class="form-group">
                  <input id="user_name" type="text" class="form-control{{ $errors->has('user_name') ? ' is-invalid' : '' }}" name="user_name" value="{{ old('user_name') }}" placeholder="User Name" autofocus>
                  @if ($errors->has('user_name'))
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('user_name') }}</strong>
                  </span>
                  @endif
                </div>
                <div class="form-group">
                  <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email">
                  @if ($errors->has('email'))
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                  </span>
                  @endif
                </div>
                <div class="form-group">
                  <input type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }} numberonly" placeholder="Mobile Number" name="phone" value="{{ old('phone') }}"/>
                  @if ($errors->has('phone'))
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('phone') }}</strong>
                  </span>
                  @endif
                </div>
                <div class="form-group">
                  <input type="hidden" name="role" value="3">
                  <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password">
                  @if ($errors->has('password'))
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('password') }}</strong>
                  </span>
                  @endif
                </div>
                <div class="form-group">
                  <input id="password_confirmation" type="password" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" name="password_confirmation" placeholder="Confirm Password">
                  @if ($errors->has('password_confirmation'))
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                  </span>
                  @endif
                </div>
                <div class="spacer15"></div>
                <h5>Business Information</h5>
                <div class="form-group">
                  <input id="businessname" type="text" class="form-control{{ $errors->has('businessname') ? ' is-invalid' : '' }}" placeholder="Business Name" name="businessname" value="{{ old('businessname') }}"/>
                  @if ($errors->has('businessname'))
                  <span class="invalid-feedback" role="alert">
                    <strong>The business name field is required.</strong>
                  </span>
                  @endif
                </div>
                <div class="form-group">
                  @php
                  $oldd = old('cat');
                  $business_categories = Helper::getTablaData('business_categories');
                  @endphp
                  <select class="form-control{{ $errors->has('cat') ? ' is-invalid' : '' }} business_categories" name="cat">
                    <option value="">Select Category for Business</option>
                    @if(!empty($business_categories))
                    @foreach($business_categories as $category)
                    <option @if($oldd == $category->id) selected @endif value="{{ $category->id }}">{{ $category->name }}</option>
                    @endforeach
                    <option @if($oldd == 'other') selected @endif value="other">Other</option>
                    @endif
                  </select>
                  @if ($errors->has('cat'))
                  <span class="invalid-feedback" role="alert">
                    <strong> The category field is required. </strong>
                  </span>
                  @endif
                </div>
                <div class="form-group business_categories_other" style="display:{{ ($oldd == 'other') ? 'block' : 'none' }}">
                  <input id="business_categories_other" type="text" class="form-control{{ $errors->has('business_categories_other') ? ' is-invalid' : '' }}" placeholder="Other category Name" name="business_categories_other" value="{{ old('business_categories_other') }}"/>
                  @if ($errors->has('business_categories_other'))
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('business_categories_other') }}</strong>
                  </span>
                  @endif
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-6">
                      <input type="text" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" placeholder="City" name="city" value="{{ old('city') }}"/>
                      @if ($errors->has('city'))
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('city') }}</strong>
                      </span>
                      @endif
                    </div>
                    <div class="col-md-6">
                      <input type="text" class="form-control{{ $errors->has('state') ? ' is-invalid' : '' }}" placeholder="State" name="state" value="{{ old('state') }}"/>
                      @if ($errors->has('state'))
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('state') }}</strong>
                      </span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-6">
                      <input type="text" class="form-control{{ $errors->has('zip') ? ' is-invalid' : '' }}" placeholder="Zip code" name="zip" value="{{ old('zip') }}"/>
                      @if ($errors->has('zip'))
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('zip') }}</strong>
                      </span>
                      @endif
                    </div>
                    <div class="col-md-6">
                      <input type="text" class="form-control{{ $errors->has('contact') ? ' is-invalid' : '' }} numberonly" placeholder="Contact no" name="contact" value="{{ old('contact') }}"/>
                      @if ($errors->has('contact'))
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('contact') }}</strong>
                      </span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <textarea class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" placeholder="Tell us about your business" name="description">{{ old('description') }}</textarea>
                  @if ($errors->has('description'))
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('description') }}</strong>
                  </span>
                  @endif
                </div>
                <div class="form-group">
                  <button class="btn btn-primary">Create Account</button>
                </div>
              </div>
              {{--
              <div class="col-md-3">
                <div class="gender">
                  <h5>Gender</h5>
                  @php
                  $olddd = old('gender');
                  @endphp
                  <label class="radio">Male
                    <input @if($olddd == 'Male') checked @endif type="radio" checked="checked" name="gender" value="Male">
                    <span class="checkround"></span>
                  </label>
                  <label class="radio">Female
                    <input @if($olddd == 'Female') checked @endif type="radio" name="gender" value="Female">
                    <span class="checkround"></span>
                  </label>
                  <label class="check" style="display:none;">Buying
                    <input type="checkbox" checked="checked" name="is_name">
                    <span class="checkmark"></span>
                  </label>
                </div>
              </div>
              --}}
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="clearfix"></div>
@endsection
