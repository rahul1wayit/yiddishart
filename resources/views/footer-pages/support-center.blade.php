@extends('layouts.frontend')
@section('content')
<section class="main_content_wrapper searchSection">
  <div class="Support_Banner text-center">
    <div class="banner_search">
      <div class="container">
        <h4>Support Center</h4>
          {{ Form::open(array('action' => 'HomeController@supportCenter', 'id' => 'supportSearch','method'=>'GET')) }}
        <div class="input-group" id="adv-search">
          <input type="text" class="form-control" name="search" placeholder="Have a question? Ask or enter a search term here" value="@if(isset($search)){{$search}}@else  @endif"/>
          <div class="input-group-btn">
            <div class="btn-group" role="group">
              {{ Form::submit('Search', array('class' => 'btn btn-primary')) }}
              <!-- <button type="button" class="btn btn-primary">Search</button> -->
            </div>
          </div>
        </div>
        {{ Form::close() }}
      </div>
    </div>
  </div>
  <div class="Support_cont" id="support-data">
    @include('includes/support-data')
    <div class="spacer15"></div>
  </section>
  @endsection
