@extends('layouts.frontend')
@section('content')
<section class="main_content_wrapper searchSection">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="title_heading text-center">
          <h2>Categories</h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="category_page_panel">
          <div class="row">
            @if(!$categories->isEmpty())
            @foreach($categories as $index => $category)
            <div class="col col-xs-12 category_wise_design" rel="{{ $category->id }}">
              <div class="Category_bx">
                <div class="cat_img">
                  <img src="{{ asset('assets/images/'.$category->image) }}"/>
                </div>
                <div class="cat_name">
                  <h4>{{ $category->name }}</h4>
                </div>
              </div>
            </div>
            @endforeach
            @endif
            <div class="col col-xs-12">
              <div class="Category_bx">
                <div class="cat_img">
                  <img src="{{ asset('assets/images/category_default.png') }}"/>
                </div>
                <div class="cat_name">
                  <h4>Category</h4>
                </div>
              </div>
            </div>
            <div class="col col-xs-12">
              <div class="Category_bx">
                <div class="cat_img">
                  <img src="{{ asset('assets/images/category_default.png') }}"/>
                </div>
                <div class="cat_name">
                  <h4>Category</h4>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col col-xs-12">
              <div class="Category_bx">
                <div class="cat_img">
                  <img src="{{ asset('assets/images/category_default.png') }}"/>
                </div>
                <div class="cat_name">
                  <h4>Category</h4>
                </div>
              </div>
            </div>
            <div class="col col-xs-12">
              <div class="Category_bx">
                <div class="cat_img">
                  <img src="{{ asset('assets/images/category_default.png') }}"/>
                </div>
                <div class="cat_name">
                  <h4>Category</h4>
                </div>
              </div>
            </div>
            <div class="col col-xs-12">
              <div class="Category_bx">
                <div class="cat_img">
                  <img src="{{ asset('assets/images/category_default.png') }}"/>
                </div>
                <div class="cat_name">
                  <h4>Category</h4>
                </div>
              </div>
            </div>
            <div class="col col-xs-12">
              <div class="Category_bx">
                <div class="cat_img">
                  <img src="{{ asset('assets/images/category_default.png') }}"/>
                </div>
                <div class="cat_name">
                  <h4>Category</h4>
                </div>
              </div>
            </div>
            <div class="col col-xs-12">
              <div class="Category_bx">
                <div class="cat_img">
                  <img src="{{ asset('assets/images/category_default.png') }}"/>
                </div>
                <div class="cat_name">
                  <h4>Category</h4>
                </div>
              </div>
            </div>
            <div class="col col-xs-12">
              <div class="Category_bx">
                <div class="cat_img">
                  <img src="{{ asset('assets/images/category_default.png') }}"/>
                </div>
                <div class="cat_name">
                  <h4>Category</h4>
                </div>
              </div>
            </div>
            <div class="col col-xs-12">
              <div class="Category_bx">
                <div class="cat_img">
                  <img src="{{ asset('assets/images/category_default.png') }}"/>
                </div>
                <div class="cat_name">
                  <h4>Category</h4>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col col-xs-12">
              <div class="Category_bx">
                <div class="cat_img">
                  <img src="{{ asset('assets/images/category_default.png') }}"/>
                </div>
                <div class="cat_name">
                  <h4>Category</h4>
                </div>
              </div>
            </div>
            <div class="col col-xs-12">
              <div class="Category_bx">
                <div class="cat_img">
                  <img src="{{ asset('assets/images/category_default.png') }}"/>
                </div>
                <div class="cat_name">
                  <h4>Category</h4>
                </div>
              </div>
            </div>
            <div class="col col-xs-12">
              <div class="Category_bx">
                <div class="cat_img">
                  <img src="{{ asset('assets/images/category_default.png') }}"/>
                </div>
                <div class="cat_name">
                  <h4>Category</h4>
                </div>
              </div>
            </div>
            <div class="col col-xs-12">
              <div class="Category_bx">
                <div class="cat_img">
                  <img src="{{ asset('assets/images/category_default.png') }}"/>
                </div>
                <div class="cat_name">
                  <h4>Category</h4>
                </div>
              </div>
            </div>
            <div class="col col-xs-12">
              <div class="Category_bx">
                <div class="cat_img">
                  <img src="{{ asset('assets/images/category_default.png') }}"/>
                </div>
                <div class="cat_name">
                  <h4>Category</h4>
                </div>
              </div>
            </div>
            <div class="col col-xs-12">
              <div class="Category_bx">
                <div class="cat_img">
                  <img src="{{ asset('assets/images/category_default.png') }}"/>
                </div>
                <div class="cat_name">
                  <h4>Category</h4>
                </div>
              </div>
            </div>
            <div class="col col-xs-12">
              <div class="Category_bx">
                <div class="cat_img">
                  <img src="{{ asset('assets/images/category_default.png') }}"/>
                </div>
                <div class="cat_name">
                  <h4>Category</h4>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
