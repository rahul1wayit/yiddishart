@extends('layouts.frontend')
@section('content')
<section class="main_content_wrapper searchSection">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="title_heading text-center">
          <h2>Contact Us</h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="signup-form contact_page">
          {!! Form::model('',['route' => 'contact-us', 'method' => 'post','id'=>'contact-form']) !!}
            <div class="form-group">
              <div class="row">
                <label class="col-md-3">Name*</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="name" placeholder="Name"/>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="row">
                <label class="col-md-3">Phone Number*</label>
                <div class="col-md-9">
                  <input type="text" class="form-control validNumber" name="phone" placeholder="Phone Number"/>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="row">
                <label class="col-md-3">Email*</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="email" placeholder="Email"/>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="row">
                <label class="col-md-3">Message*</label>
                <div class="col-md-9">
                  <textarea type="text" class="form-control" name="message" placeholder="Message"/></textarea>
                </div>
              </div>
            </div>
            <div class="form-group text-right">
              {{ Form::submit('Send Message', array_merge(['class' => 'btn btn-primary', 'id' => 'submit'])) }}
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
