@extends('layouts.frontend')
@section('content')
<section class="main_content_wrapper searchSection">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="title_heading text-center">
          <h2>Career</h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="About_page_panel">
          <div class="row">
            <div class="col-md-12">
              <div class="about_page text-center">
                <div class="about_image">
                  <img src="{{ asset('assets/images/careers.png') }}" alt=""/>
                </div>
                <div class="about_content">
                  <h5>Come work with us! Join our team and start building your career.</h5>
                  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
