@extends('layouts.frontend')
@section('content')
<section class="main_content_wrapper searchSection">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="title_heading">
          @if( isset($supportData) && sizeOf($supportData) > 0 )
            
          <h2>{{ucwords($supportData->question)}}</h2>
          @else
          <h2>Question not found</h2>
          @endif  
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="FAQ">
          @if( isset($supportData) && sizeOf($supportData) > 0 )
            {!! ucwords($supportData->answer) !!}
          @else
            Answer not found
          @endif
        </div>
      </div>
    </div>
  </div>
</section>
  @endsection
