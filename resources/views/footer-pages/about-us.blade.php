@extends('layouts.frontend')
@section('content')
<section class="main_content_wrapper searchSection">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="title_heading text-center">
          <h2>About Us</h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="About_page_panel">
          <div class="row">
            <div class="col-md-12">
              <div class="about_page">
                <div class="about_image">
                  <img src="{{ asset('assets/images/about.png') }}" alt=""/>
                </div>
                <div class="about_content">
                  <p>Yiddish Art was founded in 2018. We started out with two people, and today we’re a company of about 50 spread out across 32 different cities around the world. Our headquarters is in New York, but everyone at here is free to live and work wherever they want. Many of us love working remotely – we literally wrote the book on remote working!</p>
                  <p>We’re designers, programmers, tinkerers, writers, speakers, bikers, engineers, runners, developers, chefs, analysts, campers, musicians, filmmakers, knitters, hikers, authors, photographers, pilots, race car drivers, readers, travelers, gardeners, volunteers, parents, and hard workers.</p>
                  <p>With such a diverse group of people, from so many different places, we bring a unique perspective to everything we do. We’re big into sharing, so we even wrote a NYT best-selling book called ART that details how we run a happy and healthy business.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
