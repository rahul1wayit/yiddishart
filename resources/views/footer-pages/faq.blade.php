@extends('layouts.frontend')
@section('content')
<section class="main_content_wrapper searchSection">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="title_heading">
          <h2>FAQ</h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="FAQ" id="faq_panel">
            <div class="accordion_container">
                @if(!empty($getQues))
                    @foreach($getQues as $index => $list)
                        <div class="accordion_head">{{ ucwords($list["question"]) }}<span class="plusminus">@if($index==0)-@else+@endif</span>
                        </div>
                        <div class="accordion_body" style="display: @if($index==0) block @else none @endif;">
                            <p>{{ str_limit(strip_tags(ucwords($list["answer"])), $limit = 10000, $end = '...') }}</p>
                        </div>
                    @endforeach
                @else
                    No Record Found
                @endif
            </div>
        </div>
      </div>
    </div>
  </div>
</section>

@endsection
