@extends('layouts.frontend')
@section('content')
<section class="main_content_wrapper searchSection">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="title_heading text-center">
          <h2>Events</h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="Event_page">
          @if(!$events->isEmpty())
          @foreach($events as $index => $event)
          <div class="event_box">
            <div class="row">
              <div class="col-md-2">
                <div class="event_date">{{ Helper::dateFormat($event->date,'d.m.Y') }}</div>
              </div>
              <div class="col-md-7">
                <div class="event_cont">
                  <h4>{{ $event->name }}</h4>
                  <p>{{ $event->description }}</p>
                </div>
              </div>
              <div class="col-md-3">
                <div class="event_img">
                  @if($event->image != '')
                  <img class="img-fluid" src="<?php echo asset('assets/images/event/'); ?>/{{ $event->image }}">
                  @else
                  <img class="img-fluid" src="{{ asset('assets/images/category_default.png') }}">
                  @endif
                </div>
              </div>
            </div>
          </div>
          @endforeach
          @else
          No Event yet.
          @endif

        </div>
      </div>
    </div>
  </div>
</section>
@endsection
