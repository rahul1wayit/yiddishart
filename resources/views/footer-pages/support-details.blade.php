@extends('layouts.frontend')
@section('content')
<section class="main_content_wrapper searchSection">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="title_heading">
          <h2>@if( isset($supportData) && sizeOf($supportData) > 0 )
                {{$supportData[0]->name}}

              @endif
          </h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="FAQ" id="faq_panel">
            <div class="accordion_container">
              @if( isset($supportData) && sizeOf($supportData) > 0 )
                    @foreach($supportData as $index => $list)
                    <?php //print_r($list);?>
                      @foreach($list->support_qus_ans as $qa_key =>$qa_value )
                        <div class="accordion_head">{{ ucwords($qa_value->question) }}<span class="plusminus">@if($qa_key==0)-@else+@endif</span>
                        </div>
                        <div class="accordion_body" style="display: @if($qa_key==0) block @else none @endif;">
                            <p>{!! $qa_value->answer !!}</p>
                        </div>
                      @endforeach

                    @endforeach
                @else
                    No Record Found
                @endif
            </div>
        </div>
      </div>
    </div>
  </div>
</section>

@endsection
