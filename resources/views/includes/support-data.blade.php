    <div class="container">
      <div class="row">
         <div class="col-md-8">
            <div class="row">
          @if( isset($supportData) && sizeOf($supportData) > 0 )
            @foreach($supportData as $key=>$value )

        <div class="col-md-6">
            <div class="Support_feature">
              <h5>@if( isset($value->name) )
                <a href="{{url('support-center/'.$value->id.'/'. Helper::changeSupportUrl($value->name) )}}">{{ ucwords(ucwords($value->name)) }}</a>
              @else {{ ""}} @endif</h5>
                <p>
                  @if( isset($value->description) && !empty($value->description) )

                    {{ucwords($value->description)}}
                  @else
                    {{"No description"}}
                  @endif
                </p>
            </div>
          </div>
            @endforeach
          @else
          <div class="Support_feature">
            <h5>No Record Found </h5>
          </div>
          @endif
        </div></div>
        <div class="col-md-4">
          <div class="Common_questions">
            <h5>Common questions</h5>
            <ul>
              @if( isset($supportData) && sizeOf($supportData) > 0 )

                  @foreach($supportData as $key=>$value )
                    @foreach($value->support_qus_ans as $qa_key =>$qa_value )
                      @if( isset($qa_value->common_status) && $qa_value->common_status == 1)
                        <li>
                          <a href="{{url('support-center-answer/'.$qa_value->id.'/'. Helper::changeSupportUrl($qa_value->question) )}}">{{ ucwords($qa_value->question) }}</a>
                        </li>
                      @endif
                    @endforeach
                  @endforeach
              @else
                  <li>No common questions found</li>
              @endif
            </ul>
          </div>
        </div>
      </div>
    </div>
