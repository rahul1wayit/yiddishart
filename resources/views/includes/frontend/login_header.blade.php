<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <link rel="icon" href="{{ asset('assets/images/fav-icon1.png') }}" sizes="16x16 32x32" type="image/png">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Yiddishart</title>
  <!-- Bootstrap core CSS -->
  <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet"/>
  <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet"/>
  <link href="{{ asset('assets/css/style-custom.css') }}" rel="stylesheet"/>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
  <link rel="stylesheet" href="{{ asset('assets/css/jquery.toast.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/css/star-rating.css') }}">
</head>
<body>
  <header>
    <input id="site_url" type="hidden" value="{{ URL('/') }}">
    <div class="container">
      <div class="row">
        <div class="col-md-5">
          @guest
          <a href="{{ url('/') }}"><img src="{{ asset('assets/images/logo.jpg')}}" alt="" title="logo"/></a>
          @else
          <a href="{{ url('/') }}"><img src="{{ asset('assets/images/logo.jpg')}}" alt="" title="logo"/></a>
          @endguest
        </div>
        <div class="col-md-7 pull-right">
          <div class="search_top_link">
            <div class="row">
              <div class="col-md-6">
                <div id="custom-search-input">
                  <div class="input-group">
                    <input type="text" class="form-control input-lg searchName" rel="{{ Request::segment(1) }}" ref="@if(Auth::check())@php echo Auth::user()->roles()->first()->name @endphp@endif" placeholder="Search for snippets" />
                    <span class="input-group-btn">
                      <button class="btn btn-info btn-lg frontSearch" rel="{{ Request::segment(1) }}" ref="@if(Auth::check())@php echo Auth::user()->roles()->first()->name @endphp@endif" type="button">
                        <i class="fa fa-search" aria-hidden="true"></i>
                      </button>
                    </span>
                  </div>
                </div>
              </div>
              <div class="col-md-6 text-right">
                <div class="Top_links">
                  @guest
                  <a href="{{ route('login') }}">Login</a>
                  <a href="{{ route('register') }}" class="active">Sign up</a>
                  <a href="{{ route('seller') }}" class="active">Become a Seller</a>
                  @else
                  <div class="dropdown">
                    <div class="user_img_box">
                      @if(Auth::user()->gender == 'Male')
                      <img src="{{ asset('assets/images/dummy-male.png') }}" class="view-profile"/>
                      @else
                      <img src="{{ asset('assets/images/dummy-female.png') }}" class="view-profile"/>
                      @endif
                    </div>
                    <a class="dropdown-toggle" data-toggle="dropdown">
                      Hello, {{ Auth::user()->name }} <i class="fa fa-angle-down"></i>
                    </a>
                    <div class="dropdown-menu">
                      <!-- <div class="drpDwnAdd"></div> -->
                      @if(Auth::user()->roles()->first()->name == 'seller')
                      <a href="{{ url('/'.Auth::user()->roles()->first()->name.'/dashboard') }}" class="dropdown-item">Home</a>
                      <a href="{{ url('/'.Auth::user()->roles()->first()->name.'/portfolio') }}" class="dropdown-item">Portfolio</a>
                      <a href="{{ url('/'.Auth::user()->roles()->first()->name.'/uploads') }}" class="dropdown-item">Uploads</a>
                      <a href="{{ url('/'.Auth::user()->roles()->first()->name.'/earnings') }}" class="dropdown-item">Earnings</a>
                      @elseif(Auth::user()->roles()->first()->name == 'buyer')
                      <a href="{{ url('/'.Auth::user()->roles()->first()->name.'/dashboard') }}" class="dropdown-item">Profile</a>
                      <a href="{{ url('/'.Auth::user()->roles()->first()->name.'/downloads') }}" class="dropdown-item">Downloads</a>
                      <a href="{{ url('/'.Auth::user()->roles()->first()->name.'/favourite') }}" class="dropdown-item"> Favorite</a>
                      <a href="{{ url('/'.Auth::user()->roles()->first()->name.'/cart') }}" class="dropdown-item">Cart</a>
                      <a href="{{ url('/'.Auth::user()->roles()->first()->name.'/orders') }}" class="dropdown-item">My Orders</a>
                      @endif
                      @if(Auth::user()->roles()->first()->name == 'admin')
                      <a href="{{ url('/'.Auth::user()->roles()->first()->name.'/dashboard') }}" class="dropdown-item">Home</a>
                      <a href="{{ url('/'.Auth::user()->roles()->first()->name.'/settings') }}" class="dropdown-item">Settings</a>
                      @else
                      <a href="{{ url('/'.Auth::user()->roles()->first()->name.'/account-settings') }}" class="dropdown-item">Settings</a>
                      @endif
                      <a class="dropdown-item" href="{{ route('logout') }}"
                      onclick="event.preventDefault();
                      document.getElementById('logout-form').submit();">
                      {{ __('Logout') }}
                      </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                    </form>
                  </div>
                </div>
                @endguest
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>
