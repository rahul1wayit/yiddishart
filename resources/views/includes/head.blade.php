<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Yiddishart</title>
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/responsive.css')}}">
    <!-- <link href="css/bootstrap.min.css" rel="stylesheet"/>
      <link href="css/style.css" rel="stylesheet"/> -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
  </head>
  <body>
    <header>
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <a href="{{ url('/') }}"><img src="{{ asset('assets/images/logo.jpg')}}" alt="" title="logo"/></a>
            <!-- <a href="index.html"><img src="images/logo.jpg" alt="" title="logo"/></a> -->
          </div>
          <!-- <div class="col-md-6 pull-right">
              <div class="search_top_link">
                <div class="row">
                  <div class="col-md-7">
                    <div id="custom-search-input">
                        <div class="input-group">
                            <input type="text" class="form-control input-lg" placeholder="Buscar" />
                            <span class="input-group-btn">
                                <button class="btn btn-info btn-lg" type="button">
                                  <i class="fa fa-search" aria-hidden="true"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                  </div>
                  <div class="col-md-5 text-right">
                    <div class="Top_links">
                      <a href="#">Login</a>
                      <a href="#" class="active">Sign up</a>
                    </div>
                  </div>
                </div>
              </div>
          </div> -->
        </div>
      </div>
    </header>
