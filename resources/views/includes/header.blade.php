<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <link rel="icon" href="{{ asset('assets/images/fav-icon1.png') }}" sizes="16x16 32x32" type="image/png">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Yiddishart</title>
  <!-- Bootstrap core CSS -->
  <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet"/>
  <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet"/>
  <link href="{{ asset('assets/css/style-custom.css') }}" rel="stylesheet"/>
  <link href="{{ asset('assets/css/jquery.toast.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/css/owl.theme.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/css/owl.carousel.min.css') }}" rel="stylesheet">
  <!-- Keyboard start -->
  <link href="{{ asset('assets/keyboard-master/css/jquery-ui.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/keyboard-master/css/keyboard.css') }}" rel="stylesheet">
  <!-- Keyboard end -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
  <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
</head>
<script>
var url = "<?php echo url(request()->route()->getPrefix())?>";
</script>
<body>
  <input type="hidden" id="site_url" value="{{ URL('/') }}" />
  <header>
    <div class="container">
      <div class="row">
        <div class="col-md-5">
          <a href="{{ url('/') }}"><img src="{{ asset('assets/images/logo.jpg')}}" alt="" title="logo"/></a>
        </div>
        <div class="col-md-7 pull-right">
          <div class="search_top_link">
            <div class="row">
              <div class="col-md-6">
                <div id="custom-search-input">
                  <div class="input-group">
                    <input type="text"  autocomplete="off" class="form-control input-lg searchName" rel="{{ Request::segment(1) }}" ref="@if(Auth::check())@php echo Auth::user()->roles()->first()->name @endphp@endif" placeholder="Search for snippets"/>
                    <span class="input-group-btn">
                      <button class="btn btn-info btn-lg frontSearch" rel="{{ Request::segment(1) }}" ref="@if(Auth::check())@php echo Auth::user()->roles()->first()->name @endphp@endif" type="button">
                        <i class="fa fa-search" aria-hidden="true"></i>
                      </button>
                    </span>
                  </div>
                </div>
              </div>
              <div class="col-md-6 text-right">
                <div class="Top_links">
                  <div class="dropdown">
                    <div class="user_img_box">
                      @if(Auth::user()->image != '')
                      <img src="{{ asset('assets/images/profile/'.Auth::user()->image) }}" alt=""/>
                      @else

                      @if(Auth::user()->gender == 'Male')
                      <img src="{{ asset('assets/images/dummy-male.png') }}" class="view-profile"/>
                      @else
                      <img src="{{ asset('assets/images/dummy-female.png') }}" class="view-profile"/>
                      @endif
                      @endif
                    </div>
                    <a class="dropdown-toggle" data-toggle="dropdown">
                      Hello, {{ Auth::user()->name }} <i class="fa fa-angle-down"></i>
                    </a>
                    <div class="dropdown-menu">
                      <!-- <div class="drpDwnAdd"></div> -->
                          @php
                      $role = Auth::user()->roles->first()->name;
                      @endphp
                      @if($role == 'buyer')
                      <!-- <a href="{{ URL('/'.$role.'/dashboard') }}" class="dropdown-item">Profile</a> -->
                      <a href="{{ URL('/'.$role.'/downloads') }}" class="dropdown-item">Downloads</a>
                      <a href="{{ URL('/'.$role.'/favourite') }}" class="dropdown-item">Favorite</a>
                      <a href="{{ URL('/'.$role.'/cart') }}" class="dropdown-item">Cart</a>
                      <a href="{{ URL('/'.$role.'/orders') }}" class="dropdown-item">My Orders</a>
                      <a href="{{ URL('/'.$role.'/dashboard') }}" class="dropdown-item">Settings</a>
                      @endif
                      <a class="dropdown-item" href="{{ route('logout') }}"
                      onclick="event.preventDefault();
                      document.getElementById('logout-form').submit();">
                      {{ __('Logout') }}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>
