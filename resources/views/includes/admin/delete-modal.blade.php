<div class="modal designModal" id="deleteBuyersModal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">
					<span class="header-text deleteHead"> Delete <span class="sellerHeadName">Buyer</span> </span>
					<span class="header-text deactivateHead" style="display:none;"> Deactivate <span class="sellerHeadName">Buyer</span> </span>
					<span class="header-text activateHead" style="display:none;"> Activate <span class="sellerHeadName">Buyer</span> </span>
				</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<span class="body-text deleteText">Are you sure you want to delete this <span class="sellerText">buyer</span>?</span>
					<span class="body-text deactivateText" style="display:none;">Are you sure you want to deactivate this <span class="sellerText">buyer</span>?</span>
					<span class="body-text activateText" style="display:none;">Are you sure you want to activate this <span class="sellerText">buyer</span>?</span>
				</div>
				<!-- <button type="submit" class="btn btn-primary pull-right" id="btnclose" data-dismiss="modal">No</button> -->
				<!-- <button type="submit" class="btn btn-primary pull-right deleteBuyers">Yes</button> -->
				@csrf
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
					<button type="button" class="btn btn-primary deleteBuyers">Yes</button>
					<input type="hidden" name="buyerId" id="buyerId">
				</div>
			</div>
		</div>
	</div>
</div>
<!-- The image view Modal -->
<div id="zoomImg" class="modal">
	<div class="modal-content ">
		<span class="closeModal" >&times;</span>
		<img class="img01" >
	</div>
</div>
<!-- The image view Modal -->
<div id="addFontModel" class="modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-titleS">Upload Font</h4>
				<span class="closeFontModel">&times;</span>
			</div>
			<div class="modal-body">
				<form method="post" id="add_font_form" action="{{ url('seller/addFont') }}">
					<div class="form-group">
						<label for="inputAddress">Font Name</label>
						<input type="text" class="form-control success2 font_name" name="font_name" placeholder="Enter Font Name">
					</div>
					<div class="form-group">
						<button class="btn btn-primary" type="submit">Upload</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
