<?php $uri = Request::segment(2); ?>
<div id="sidebar">
	<ul>
		<li class="@if($uri == 'dashboard')active @endif"><a href="{{ $url.'/dashboard' }}"><img src="{{ asset('assets/images/Home_icon.png') }}"/>Home<img class="hover_img" src="{{ asset('assets/images/Home_icon_hover.png') }}"/></a></li>
		<li class="@if($uri == 'sellers' || $uri == 'seller-design' || $uri == 'seller-logs')active @endif"><a href="{{ $url.'/sellers' }}"><img src="{{ asset('assets/images/Sellers_icon.png') }}"/>Sellers<img class="hover_img" src="{{ asset('assets/images/Sellers_icon_hover.png') }}"/></a></li>
		<li class="@if($uri == 'buyers')active @endif"><a href="{{ $url.'/buyers' }}"><img src="{{ asset('assets/images/Buyers_icon.png') }}"/>Buyers<img class="hover_img" src="{{ asset('assets/images/Buyers_icon_hover.png') }}"/></a></li>
		<li class="@if($uri == 'orders' || $uri == 'order-details')active @endif"><a href="{{ $url.'/orders' }}"><img src="{{ asset('assets/images/Order_icon.png') }}"/>Orders<img class="hover_img" src="{{ asset('assets/images/Order_icon_hover.png') }}"/></a></li>
		<li class="@if($uri == 'settings')active @endif"><a href="{{ $url.'/settings' }}"><img src="{{ asset('assets/images/Settings_icon.png') }}"/>Settings<img class="hover_img" src="{{ asset('assets/images/Settings_icon_hover.png') }}"/></a></li>
		<li class="@if($uri == 'transactions')active @endif"><a href="{{ $url.'/transactions' }}"><img src="{{ asset('assets/images/Transactions_icon.png') }}"/>Transactions<img class="hover_img" src="{{ asset('assets/images/Transactions_icon_hover.png') }}"/></a></li>
		<li class="@if($uri == 'subscription')active @endif"><a href="{{ $url.'/subscription_plan' }}"><img src="{{ asset('assets/images/Subscription_icon.png') }}"/>Subscription plans<img class="hover_img" src="{{ asset('assets/images/Subscription_icon_hover.png') }}"/></a></li>
		<li class="@if($uri == 'events')active @endif"><a href="{{ $url.'/events' }}"><img src="{{ asset('assets/images/event.png') }}"/>Events<img class="hover_img" src="{{ asset('assets/images/event_hover.png') }}"/></a></li>
		<li class="@if($uri == 'faq')active @endif"><a href="{{ $url.'/faq' }}"><img src="{{ asset('assets/images/faq.png') }}"/>FAQ<img class="hover_img" src="{{ asset('assets/images/faq_active.png') }}"/></a></li>
		<li class="@if($uri == 'support-center')active @endif"><a href="{{ $url.'/support-center' }}"><img src="{{ asset('assets/images/support.png') }}"/>Support Center<img class="hover_img" src="{{ asset('assets/images/support_active.png') }}"/></a></li>
		<li class="@if($uri == 'support-category')active @endif"><a href="{{ $url.'/support-category' }}"><img src="{{ asset('assets/images/support-category.png') }}"/>Support Category<img class="hover_img" src="{{ asset('assets/images/support-category_active.png') }}"/></a></li>
	</ul>
	<div class="side_bg_img">
		<img src="{{ asset('assets/images/side_logo.png') }}"/>
	</div>
	<div class="footer">
		<p><?php echo date('Y') ?> Copyright Yiddish art </p>
	</div>
</div>
