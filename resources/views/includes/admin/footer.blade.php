<div class="se-pre-con loaderSec" style="display: none;"></div>
<!-- Bootstrap core JavaScript -->
<!-- <script type="text/javascript" src="{{ asset('assets/js/jquery-3.2.1.slim.min.js') }}"></script> -->
<script type="text/javascript" src="{{ asset('assets/js/jquery.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery-ui.js') }}"></script>

<!-- <script type="text/javascript" src="{{ asset('assets/js/jquery.min.js') }}"></script> -->
<script type="text/javascript" src="{{ asset('assets/js/popper.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/lib/jquery/toastr.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.toast.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/lib/jquery/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/additional-methods.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/form-validate.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/common.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/custom.js') }}"></script>
<script src="{{ asset('assets/js/jquery.minicolors.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/admin-custom.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/bootbox.min.js') }}"></script>
<div id="zoomImg" class="modal">
	<div class="modal-content ">
		<span class="closeModal" >&times;</span>
		<img class="img01">
	</div>
</div>

<script>
$( document ).ready(function() {
  setTimeout(function(){   $(".se-pre-con").hide(); }, 500);
  $( "#datepicker" ).datepicker({
    inline: true,
    dateFormat: 'dd.mm.yy',
    minDate: 0
  });
});
</script>
