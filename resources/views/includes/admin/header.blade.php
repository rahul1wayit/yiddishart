<input type="hidden" id="site_url" value="{{ URL('/') }}" />
<header id="header">
<!-- check  -->
		<div class="user_name"><span class="user_img"><img src="{{ asset('assets/images/profile/'.Auth::user()->image) }}"/></span>Hello, {{ Auth::user()->name }}</div>
		<div class="logo_box">
			<a href="{{ $url.'/dashboard' }}"><img src="{{ asset('assets/images/logo.png') }}"/></a>
		</div>
		<a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar">
			<i class="fa fa-bars"></i>
		</a>
  <div class="header_actions">
    <!-- Right Side Of Navbar -->
    <ul class="navbar-nav ml-auto">
      <!-- Authentication Links -->
      @guest
      <li class="nav-item">
        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
      </li>
      @if (Route::has('register'))
      <li class="nav-item">
        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
      </li>
      @endif
      @else
			<li class="nav-item dropdown notification_menu">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fa fa-bell"></i>
					<em>{{sizeOf($notification) ?? 0}}</em>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
					@forelse($notification as $key =>$value )
          	<a class="dropdown-item" href="{{url('admin/seller-design/'.\Crypt::encrypt($value->added_by ?? ''))}}">{{$value->name ?? ''}}</a>
					@empty
						<a class="dropdown-item" >Empty Notification</a>
					@endforelse
        </div>

      </li>

      <li class="nav-item dropdown">
				<a class="dropdown-item" href="{{ route('logout') }}"
				onclick="event.preventDefault();
				document.getElementById('logout-form').submit();">
				Sign out
			</a>
			<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
				@csrf
			</form>
    </li>
    @endguest
  </ul>
</div>
</header>
