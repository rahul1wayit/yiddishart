<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>{{ $title }} | Yiddishart</title>
  <link rel="icon" href="{{ asset('assets/images/fav-icon1.png') }}" sizes="16x16 32x32" type="image/png">
  <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet"/>
  <link href="{{ asset('assets/css/style-admin.css') }}" rel="stylesheet"/>
  <link href="{{ asset('assets/css/style-custom.css') }}" rel="stylesheet"/>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css"/>
  <link rel="stylesheet" href="{{ asset('assets/css/jquery.minicolors.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/css/jquery.toast.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/css/jquery-ui.css') }}">
</head>
