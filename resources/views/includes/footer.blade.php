<div class="clearfix"></div>
<!-- Footer -->
<footer class="footer">
  <div class="container">
    <div class="row">
      <div class="col-md-7">
        <div class="row">
          <div class="col-md-4">
            <h3>Categories</h3>
            <ul>
              @php
              $categories = Helper::getTablaDataOrderBy('business_categories','id','asc');
              @endphp
              @if(!$categories->isEmpty())
              @foreach($categories as $index => $category)
              <li><a href="{{url('category/'.$category->slug)}}">{{ $category->name }}</a></li>
              @endforeach
              @endif
            </ul>
          </div>
          <div class="col-md-4">
            <h3>Support</h3>
            <ul>
              <li><a href="{{ URL('/contact-us') }}">Contact us</a></li>
              <li><a href="{{ URL('/support-center') }}">Support center</a></li>
              <li><a href="{{ URL('/faq') }}">FAQ</a></li>
            </ul>
          </div>
          <div class="col-md-4">
            <h3>Yiddish art</h3>
            <ul>
              <li><a href="{{ URL('/about-us') }}">About us</a></li>
              <li><a href="{{ URL('/events') }}">Events</a></li>
              <li><a href="{{ URL('/careers') }}">Careers</a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-md-5 text-right">
        <ul class="social_links">
          <li><a href="" class="facebook_link"></a></li>
          <li><a href="" class="twitter_link"></a></li>
          <li><a href="" class="pinterest_link"></a></li>
          <li><a href="" class="instagram_link"></a></li>
        </ul>
        <p>&copy;  2019 Yiddish art. All rights reserved</p>
      </div>
    </div>
  </div>
</footer>
<div class="se-pre-con loaderSec" style="display: none;"></div>
<!-- Bootstrap core JavaScript -->
<!-- <script src="{{ asset('assets/js/jquery-3.2.1.slim.min.js') }}"></script> -->
<script type="text/javascript" src="{{ asset('assets/js/popper.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/lib/jquery/toastr.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.toast.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/lib/jquery/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/form-validate.js') }}"></script>
<!-- Keyboard start -->
<script type="text/javascript" src="{{ asset('assets/keyboard-master/js/jquery-ui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/keyboard-master/js/jquery.keyboard.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/keyboard-master/js/jquery.keyboard.extension-altkeyspopup.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/keyboard-master/js/jquery.keyboard.extension-previewkeyset.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/keyboard-master/js/jquery.keyboard.extension-typing.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/keyboard-master/js/jquery.jui_theme_switch.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/keyboard-master/layouts/keyboard-layouts-combined.js') }}"></script>
<!-- Keyboard end -->
<script type="text/javascript" src="{{ asset('assets/jCanvas/jcanvas.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/jCanvas/jcanvas-handles.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/common.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/custom.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/bootbox.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>
<div id="zoomImg" class="modal">
	<div class="modal-content ">
		<span class="closeModal" >&times;</span>
		<img class="img01" >
	</div>
</div>
<script>
jQuery("#news-slider10").owlCarousel({
  items : 4,
  itemsDesktop:[1199,3],
  itemsDesktopSmall:[980,2],
  itemsMobile : [600,1],
  navigation:true,
  navigationText:["",""],
  pagination:true,
  autoPlay:true
});
</script>
@include('seller.script')
@include('includes.chat.zendesk-chat')
</body>
</html>
