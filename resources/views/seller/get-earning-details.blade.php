
  <div class="modal-content">
    <div class="modal-header">
      <h4 class="modal-title">{{ $result->name }}</h4>
    </div>
      <div class="row">
        <div class="col-md-6">
          <div class="View_Earnings_image">
            <img src="{{ asset("assets/images/seller-design/".$result->designimage) }}" alt="{{ $result->name }}" title="{{ $result->name }}"/>
          </div>
        </div>
        <div class="col-md-6">
          <div class="Earnings_Price_Cont">
            <h4>{{ Helper::numberFormat($result->price) }}</h4>
            <p>{{ $result->description }}</p>
          </div>
          <div class="Earnings_Category">
            <h5><span>Category</span>{{ $result->business_category->name }}</h5>
            <h5><span>Downloads</span><em>{{ $result->orderItems->sum('qty') }}</em></h5>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="customer_list_panel">
            <h4>Last customer details</h4>
            <div class="row">
              <div class="col-md-12">
                @if(count($last_order) == 1)
                <div class="detail_box">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="customer_img">
                        @if($last_order->order->buyer->image != '')
                        <img src="{{ asset('assets/images/profile/').'/'.$last_order->order->buyer->image }}"/>
                        @else
                        <img src="{{ asset('assets/images/dummy-male.png') }}"/>
                        @endif
                      </div>
                      <div class="address">{{ $last_order->order->order_details->full_name }}<br/>{{ $last_order->order->order_details->shipping_address.' '.$last_order->order->order_details->shipping_town.' '.$last_order->order->order_details->shipping_state }}<br/>{{ $last_order->order->order_details->shipping_email }}</div>
                    </div>
                    <div class="col-md-6">
                      <div class="paid_unpaid">
                        <i class="fa fa-check"></i>
                        <span class="check">Paid</span>
                      </div>
                    </div>
                  </div>
                </div>
                @else
                No record found.
                @endif
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="customer_list_panel">
            <h4>Customer list <a href="#" class="link-text">View entire list > </a></h4>
            <div class="table-responsive">
              <table class="table">
                @if(!$all_order->isEmpty())
                @foreach ($all_order as $user)
                <tr>
                  <td class="user_img">
                    <span>
                      @if($user->buyer->image != '')
                      <img src="{{ asset('assets/images/profile/').'/'.$user->buyer->image }}"/>
                      @else
                      <img src="{{ asset('assets/images/dummy-male.png') }}"/>
                      @endif
                    </span>
                  </td>
                  <td class="username">{{ $user->buyer->name }}</td>
                  <td class="text-right"><a href="#" class="link-text">details</a></td>
                </tr>
                @endforeach
                @else
                No record found.
                @endif
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
