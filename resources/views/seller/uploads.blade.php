@extends('layouts.seller')
@section('content')
<section class="main_content_wrapper">
    <div class="container">
        @include('seller.menu')
        <div class="designer_uploads_section">
            <div class="row">
                <div class="col-md-12">
                    <div class="upload_cont text-center">
                        <p>Uploaded designs are in approval state by our platform. Once we confirm that quality is in satisfied level, uploaded design will be accesible for buying.</p>
                    </div>
                </div>
            </div>
            <div class="uploads_panel">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th style="width:130px;">Design</th>
                                        <th style="font-size:0;width: 161px;">Name</th>
                                        <th>Price</th>
                                        <th>Uploaded</th>
                                        <th style="width:200px;">Category</th>
                                        <th>Status</th>
                                        <th style="font-size:0;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td><img src="images/added_img1.jpg"/></td>
                                        <td>Women in red striped socks</td>
                                        <td>$75</td>
                                        <td>23.12.2018.</td>
                                        <td>school/art</td>
                                        <td>waiting for approval</td>
                                        <td><a class="edit" data-toggle="modal" data-target="#Edit_Design"><i class="fa fa-edit"></i></a></td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td><img src="images/added_img1.jpg"/></td>
                                        <td>Women in red striped socks</td>
                                        <td>$75</td>
                                        <td>23.12.2018.</td>
                                        <td>art/decoration</td>
                                        <td>waiting for approval</td>
                                        <td><a class="edit" data-toggle="modal" data-target="#Edit_Design"><i class="fa fa-edit"></i></a></td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td><img src="images/added_img1.jpg"/></td>
                                        <td>Women in red striped socks</td>
                                        <td>$75</td>
                                        <td>23.12.2018.</td>
                                        <td>school/art</td>
                                        <td>waiting for approval</td>
                                        <td><a class="edit" data-toggle="modal" data-target="#Edit_Design"><i class="fa fa-edit"></i></a></td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td><img src="images/added_img1.jpg"/></td>
                                        <td>Women in red striped socks</td>
                                        <td>$75</td>
                                        <td>23.12.2018.</td>
                                        <td>school/art</td>
                                        <td>waiting for approval</td>
                                        <td><a class="edit" data-toggle="modal" data-target="#Edit_Design"><i class="fa fa-edit"></i></a></td>
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td><img src="images/added_img1.jpg"/></td>
                                        <td>Women in red striped socks</td>
                                        <td>$75</td>
                                        <td>23.12.2018.</td>
                                        <td>school/art</td>
                                        <td>waiting for approval</td>
                                        <td><a class="edit" data-toggle="modal" data-target="#Edit_Design"><i class="fa fa-edit"></i></a></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal" id="Edit_Design">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit design</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-7">
                        <div class="form-group">
                            <input class="form-control" placeholder="Name your design"/>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <div class="custom-file">
                                <input id="logo" type="file" class="custom-file-input">
                                <label for="logo" class="custom-file-label text-truncate">Choose your file</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="" class="col-md-4 col-form-label">Add price <em class="asterisk">*</em></label>
                            <div class="col-md-5">
                                <input class="form-control" type="text" value="$" id="example-text-input">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <select class="form-control">
                                <option>Select category</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="" class="col-md-4 col-form-label">Offer price</label>
                            <div class="col-md-5">
                                <input class="form-control" type="text" value="$" id="example-text-input">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <select class="form-control">
                                <option>Select size</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <textarea class="form-control" placeholder="Design description"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="action_design">
                            <h5>Select actions for design</h5>
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="check">Select language
                                        <input type="checkbox" checked="checked" name="is_name">
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="check">Create space
                                        <input type="checkbox" name="space">
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="check">Font selection
                                        <input type="checkbox" name="Font">
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="check">Spacing tracking
                                        <input type="checkbox" name="Spacing">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="col-md-5">
                                    <label class="check"><img src="images/text-align-img.jpg"/>
                                        <input type="checkbox" name="is_name">
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="check"> <img src="images/color-img.jpg"/>
                                        <input type="checkbox" checked="checked" name="space">
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="check">Allow description <span class="limit">40 words limit</span>
                                        <input type="checkbox" name="Font">
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="check"> <img src="images/font-style-img.jpg"/>
                                        <input type="checkbox" name="Spacing">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group upload_img_btn">
                                        <a href="#" class="btn btn-primary">Upload image</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
