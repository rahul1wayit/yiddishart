@extends('layouts.seller')
@section('content')
<section class="main_content_wrapper">
  <?php // echo "<pre>"; print_r($getAllSubscriptionplan); echo "</pre>"; ?>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="title_heading text-center">
          <h2>Choose subscription plan</h2>
        </div>
      </div>
    </div>
    <div class="row">
      @foreach ($getAllSubscriptionplan as $plan)
      <div class="col-md-3 padding_0">
        <div class="subscribe_panel {{ str_replace(' ', '-', strtolower($plan['title'])) }}">
          <div class="plan_head">
            <img src="{{ asset('assets/images') }}/{{ $plan['image'] }}"/>
            <h4>{!! str_replace(' ', '<br>', $plan['title']) !!}</h4>
          </div>
          <div class="plan_content">
            <ul>
              <li>{{ $plan['uploads'] }}<span>@if($plan['price']<=0) uploads available @else uploads/month @endif</span></li>
              <li>{{ $plan['storage'] }}{{ $plan['storagelimit'] }}<span>storage</span></li>
            </ul>
            @if($plan['price']<=0)
            <h3>free</h3>
            @else
            <h3>${{ $plan['price'] }}</h3>
            @endif
          </div>
          <div class="plan_button">
            <a href="{{ route('payment-view', ['id' => encrypt($plan['id']) ]) }}" class="btn">Choose</a>
          </div>
        </div>
      </div>
      @endforeach



      <!--div class="col-md-3 padding_0">
      <div class="subscribe_panel entry-level">
      <div class="plan_head">
      <img src="{{ asset('assets/images/entry-level.png') }}"/>
      <h4>Entry <br/>Level</h4>
    </div>
    <div class="plan_content">
    <ul>
    <li>50<span>uploads/month</span></li>
    <li>100MB<span>storage</span></li>
  </ul>
  <h3>$49</h3>
</div>
<div class="plan_button">
<a href="#" class="btn">Choose</a>
</div>
</div>
</div>
<div class="col-md-3 padding_0">
<div class="subscribe_panel intermediate-plan">
<div class="plan_head">
<img src="{{ asset('assets/images/intermediate-plan.png') }}"/>
<h4>Intermediate <br/>Plan</h4>
</div>
<div class="plan_content">
<ul>
<li>150<span>uploads/month</span></li>
<li>500MB<span>storage</span></li>
</ul>
<h3>$79</h3>
</div>
<div class="plan_button">
<a href="#" class="btn">Choose</a>
</div>
</div>
</div>
<div class="col-md-3 padding_0">
<div class="subscribe_panel expert-level">
<div class="plan_head">
<img src="{{ asset('assets/images/expert-level.png') }}"/>
<h4>Expert <br/>Level</h4>
</div>
<div class="plan_content">
<ul>
<li>250<span>uploads/month</span></li>
<li>1GB<span>storage</span></li>
</ul>
<h3>$99</h3>
</div>
<div class="plan_button">
<a href="#" class="btn">Choose</a>
</div>
</div>
</div-->

</div>
</div>
</section>
@endsection
