<table class="table">
  <thead>
    <tr>
      <th>No.</th>
      <th style="width:130px;">Design</th>
      <th style="font-size:0;width: 161px;">Name</th>
      <th>Price</th>
      <th>Uploaded</th>
      <th style="width:200px;">Category</th>
      <th>Status</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
    @if(!empty($result))
    @foreach ($result as $list)
    <tr>
      <td>{{ ++$i }}</td>
      <td><img src="{{ asset("assets/images/seller-design/".$list->designimage) }}" alt="{{ $list->name }}" /></td>
      <td>{{ ucfirst($list->name) }}</td>
      <td>{{ Helper::numberFormat($list->price) }}</td>
      <td>{{ Helper::dateFormat($list->created_at,"d.m.Y") }}</td>
      <td>{{ $list->business_category->name }}</td>
      <td>
        @if($list->status==1)
        Approved
        @elseif($list->status==2)
        Rejected
        @else
        Waiting for approval
        @endif
      </td>
      <td><a class="edit edit_Design_btn" data-designid="{{ encrypt($list->id) }}" data-toggle="modal" data-target="#Edit_Design"><i class="fa fa-edit"></i></a></td>
    </tr>
    @endforeach
    @else
    No Record Found
    @endif
  </tbody>
</table>
<div class="pull-right">
  {{ $result->links('pagination.pagination_links') }}
  @if($result->count() > 10)
  <div class="per_Page">
    Entries per page <select class="entriesperpage">
      <option value="10" selected>10</option>
      <option value="15">15</option>
      <option value="20">20</option>
      <option value="100">100</option>
    </select>
  </div>
  @endif
</div>
