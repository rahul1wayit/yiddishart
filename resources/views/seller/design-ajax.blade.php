<div class="row">
  @if(!empty($result))
  @foreach ($result as $list)
  <div class="col-md-3">
    <div class="portfolio_box">
      <div class="portfolio_img">
        <img src="{{ URL("/")."/assets/images/seller-design/".$list->designimage }}" alt="{{ $list->name }}" />
      </div>
      <div class="portfolio_content">
        <h5>{{ ucfirst($list->name) }}</h5>
        <h5><strong>{{ Helper::numberFormat($list->price) }}</strong></h5>
        <p>{{ $list->business_category->name }}</p>
      </div>
    </div>
  </div>
  @endforeach
  @else
  No Record Found
  @endif
</div>
{{ $result->links('pagination.pagination_links') }}
