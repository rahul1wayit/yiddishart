<div class="Add_design Add_designhide">
  <div class="custom-file">
    <input id="logo" type="file" name="image" class="custom-file-input imgPreviewInp">
    <label for="logo" class="custom-file-label text-truncate"></label>
  </div>
  <div class="add_file">Add your file</div>
  <span class="error-image" style="color:red; text-align:center;" ></span>
</div>

<div class="add_design_image" style="display:none">
  <div class="add_design_image_panel">
    <img class="mainImageCls" src="" alt=""/>
  </div>

  <div class="size_text_next">
    <div class="row">
      <div class="col-md-8">
        <div class="size_text">
          <p id="fileInfo"></p>
          <span>Uploaded file size:</span> <p class="sizeImg">11 x 8.5 inch / 300dpi</p>
        </div>
      </div>
      <div class="col-md-4">
        <div class="next_pre">
          <a href="javascript:void(0);" class="sec-page2-back">Prev</a>
          <a href="javascript:void(0);" class="sec-page2-next">Next</a>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="user_customization" style="display:none">
  <div class="row">
    <div class="col-md-6 tools">
      <div class="textfields_fonts">
        <div class="form-group clearfix">
          <div class="layer-trigger-wrapper textDiv">
            <div class="layer-trigger">
              <!-- <i class="deleteText fa fa-times"></i> -->
              <a href="javascript:void(0);" data-toggle="collapse" class="select_textfield collapsed" data-target="#textfields">Text field 1 <span class="icon_select"></span></a>
              <div id="textfields" class="collapse text-settings" data-name="text" data-key="2">
                <div class="xy">
                  <div class="left">
                    <div class="form-group">
                      <label>X</label>
                      <input type="number" class="form-control 1st_text_class validNumber" min="1" name="x_1st" value="150"/>
                    </div>
                  </div>
                </div>
                <div class="xy">
                  <div class="left">
                    <div class="form-group">
                      <label>Y</label>
                      <input type="number" class="form-control 1st_text_class validNumber" min="1" name="y_1st" value="150"/>
                    </div>
                  </div>
                </div>
                <div class="xy">
                  <div class="left">
                    <div class="form-group">
                      <label><img src="{{ asset('assets/images/Angle.png') }}"/></label>
                      <input type="number" class="form-control 1st_text_class validNumber angle_1st" min="0" max="360" value="0" name="angle_1st"/>
                    </div>
                  </div>
                </div>
                <div class="xy language_limit">
                  <div class="left">
                    <div class="form-group">
                      <select class="form-control 1st_text_class language" name="language_1st">
                        <option value="qwerty">English</option>
                        <option value="hebrew-qwerty">Hebrew</option>
                      </select>
                    </div>
                  </div>
                  <div class="right">
                    <div class="form-group">
                      <label>Character limit</label>
                      <input class="form-control 1st_text_class validNumber char_limit_1st" type="number" min="1" name="char_limit_1st">
                    </div>
                  </div>
                </div>
                <div class="xy Default_text">
                  <div class="form-group">
                    <input type="text" placeholder="Text field 1" class="form-control 1st_text_class text_1st keyBord1" name="text_1st" />
                    <input type="text" placeholder="Text field 1" class="form-control 1st_text_class text_1st keyBord2" style="display:none;" name="text_1st" />
                    <input type="hidden" class="languageSelectInput" name="languageName" value="qwerty" />
                  </div>
                </div>
                <div class="xy font_style">
                  <div class="form-group">
                    @php
                    $fonts = Helper::getFonts();
                    @endphp
                    <select class="font form-control 1st_text_class font_1st" name="font_1st">
                      @if(!empty($fonts))
                      @foreach($fonts as $font)
                      <option value="{{ $font->name }}">{{ $font->name }}</option>
                      @endforeach
                      @endif
                    </select>

                    <select class="px form-control 1st_text_class bold_1st" name="font_weight_1st">
                      <option value="normal">Normal</option>
                      <option value="bold">Bold</option>
                      <option value="italic">Italic</option>
                    </select>

                    <select class="px form-control 1st_text_class font_size_1st" name="font_size_1st">
                      <?php for ($i=6; $i < 50; $i++) {
                        if($i == 32)
                        {
                          $selected = 'selected';
                        }else {
                          $selected = '';
                        }
                        echo '<option '.$selected.' value="'.$i.'">'.$i.'px</option>';
                        $i = $i+1;
                      } ?>
                    </select>
                  </div>
                </div>
                <div class="xy">
                  <div class="form-group">
                    <div class="leadtrack">
                      <label>Leading</label>
                      <select class="form-control 1st_text_class" name="leading_1st">
                        <option value="1">1</option>
                        <option value="1.5">1.5</option>
                        <option value="2">2</option>
                        <option value="2.5">2.5</option>
                        <option value="3">3</option>
                        <option value="3.5">3.5</option>
                      </select>
                    </div>
                    <div class="leadtrack">
                      <label>Tracking</label>
                      <select class="form-control 1st_text_class" name="tracking_1st">
                        <option value="0">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                      </select>
                    </div>
                    <div class="leadtrack colorPicker">
                      <span>  <input type="color" class="form-control 1st_text_class" name="color_1st"/> </span>
                    </div>
                  </div>
                </div>
                <div class="xy">
                  <div class="form-group">
                    <div class="Paragraph">
                      <label>Paragraph</label>
                      <span><a href="javascript:void(0);" class="1st_text_class" ><img name="left_align" src="{{ asset('assets/images/align_left.png') }}"/></a></span>
                      <span><a href="javascript:void(0);" class="1st_text_class" ><img name="center_align" src="{{ asset('assets/images/align_center.png') }}"/></a></span>
                      <span><a href="javascript:void(0);" class="1st_text_class" ><img name="right_align" src="{{ asset('assets/images/align_right.png') }}"/></a></span>
                    </div>
                  </div>
                </div>
                <div class="xy">
                  <div class="form-group">
                      <label>Text Width</label>
                      <input class="form-control 1st_text_class validNumber text_width_set" type="number" min="1" name="text_width">
                  </div>
              </div>
              </div>
            </div>
          </div>
          <span class="add_more_text_cls"><a href="javascript:void(0);" id="add-more"><i class="fa fa-plus addNewText"></i>Add text field</a></span>

          <div class="layer-trigger-wrapper logoDiv">
            <div class="layer-trigger"> <a href="javascript:void(0);" data-toggle="collapse" class="select_textfield collapsed" data-target="#imagefields1">Logo <span class="icon_select"></span></a>
              <div id="imagefields1" class="collapse text-settings" data-name="logo" data-key="1">
                <div class="xy">
                  <div class="left">
                    <div class="form-group">
                      <label>X</label>
                      <input type="number" class="form-control 1st_text_class validNumber" min="1" name="x_1st" value="150" /> </div>
                    </div>
                    <div class="right">
                      <div class="form-group">
                        <label>Width</label>
                        <input type="number" class="form-control 1st_text_class" name="width_1st" min="1" value="200" /> </div>
                      </div>
                    </div>
                    <div class="xy">
                      <div class="left">
                        <div class="form-group">
                          <label>Y</label>
                          <input type="number" class="form-control 1st_text_class validNumber" min="1" name="y_1st" value="150" /> </div>
                        </div>
                        <div class="right">
                          <div class="form-group">
                            <label>Height</label>
                            <input type="number" class="form-control 1st_text_class" name="height_1st" min="1" value="200" /> </div>
                          </div>
                        </div>
                        <div class="xy">
                          <div class="left">
                            <div class="form-group">
                              <label><img src="{{ asset('assets/images/Angle.png') }}" /></label>
                              <input type="number" class="form-control 1st_text_class validNumber angle_1st" min="0" max="360" value="0" name="angle_1st" /> </div>
                            </div>
                            <div class="right">
                              <div class="form-group">
                                <input type="file" name="image" class="fileUp addNewImgInp">
                                <input type="hidden" class="editPatientView" value="">
                                <div class="input-group col-xs-12"> <span class="input-group-addon"><i class="glyphicon glyphicon-picture"></i></span>
                                  <input type="text" class="form-control upload_bx consent_dateClass" disabled placeholder="Choose File"> <span class="input-group-btn">    <button class="browse btn btn-primary" type="button"><i class="fa fa-upload"></i> </button>  </span></div><span class="error-image" style="color:red; text-align:center;"></span>
                                  <input type="hidden" class="imageUrl" rel="" name="imageName" />
                                </div>
                              </div>
                            </div>
                          </div>
                          <!-- <i class="deleteText fa fa-times"></i> -->
                        </div>
                      </div>
                      <span class="add_more_image_cls"><a href="javascript:void(0);" id="add-more-image"><i class="fa fa-plus addNewImage"></i>Add logo</a></span>
                    </div>
                  </div>
                </div>
                <div class="col-md-6 canvas-img">
                  <canvas class="canvasCls" id="canvas" width="570" height="150"></canvas>
                  <!-- <button type="button" name="button" id="testsave" onclick="testsave()">Test Save</button>
                  <button type="button" name="button" id="testsave" onclick="testsave22()">Test Save 22</button> -->
                </div>
              </div>
              <div class="size_text_next">
                <div class="row">
                  <div class="col-md-8">
                    <div class="Upload_font">
                      <span class="uploadFontBtn"><a href="javascript:void(0);"><i class="fa fa-plus"></i>Upload font</a></span>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="next_pre">
                      <!-- <a href="javascript:void(0);" id="fullscreenBtn"><i class="fa fa-expand"></i></a> -->
                      <a href="javascript:void(0);" class="sec-page3-back">Prev</a>
                      <a href="javascript:void(0);" class="sec-page3-next">Next</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="design_information" style="display:none">
              <form action="{{ action('Seller\DesignController@store') }}" method="POST" enctype="multipart/form-data" id="addDesignForm">
                @csrf
                <div class="row">
                  <div class="col-md-7">
                    <div class="form-group">
                      <input type="text" name="name" class="form-control success" placeholder="Name your design"/>
                    </div>
                  </div>
                  <div class="col-md-5">
                    <div class="form-group">
                      <label>Price</label>
                      <div class="price_input">
                        <input type="text" name="price" class="form-control validNumber success" placeholder="Price"/>
                        <input type="hidden" name="canvasData"  class="form-control canvasDataCls" >
                        <input type="hidden" name="designimage"  class="form-control designimage" >
                        <span>$</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      @php
                      $oldd = old('cat');
                      $business_categories = Helper::getTablaData('business_categories');
                      @endphp
                      <select name="category" class="form-control success">
                        <option value="">Select Category</option>
                        @if(!empty($business_categories))
                        @foreach($business_categories as $category)
                        <option @if($oldd == $category->id) selected @endif value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                        <!-- <option @if($oldd == 'other') selected @endif value="other">Other</option> -->
                        @endif
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <select name="size" class="form-control success">
                        <option value="">Select Size</option>
                        <option value="Small">Small</option>
                        <option value="Medium">Medium</option>
                        <option value="Large">Large</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <textarea class="form-control" name="description" placeholder="Design description"></textarea>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <input type="text" name="keyword" class="form-control" placeholder="Type keywords for your design"/>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="next_pre text-left">
                      <a href="javascript:void(0);" class="sec-page4-back">Prev</a>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <div class="Add_designBtn text-right">
                        <button type="submit" class="btn btn-primary" id="desigButton">Upload design</button>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
