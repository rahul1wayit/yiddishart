@extends('layouts.seller')
@section('content')
<section class="main_content_wrapper">
  <div class="container">
    @include('seller.menu')
    <div class="designer_uploads_section">
      <div class="row">
        <div class="col-md-12">
          <div class="upload_cont text-center">
            <p>Uploaded designs are in approval state by our platform. Once we confirm that quality is in satisfied level, uploaded design will be accesible for buying.</p>
          </div>
        </div>
      </div>
      <div class="uploads_panel">
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive" id="paginationData">
              <table class="table">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th style="width:130px;">Design</th>
                    <th style="font-size:0;width: 161px;">Name</th>
                    <th>Price</th>
                    <th>Uploaded</th>
                    <th style="width:200px;">Category</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  @if(!$result->isEmpty())
                  @foreach ($result as $list)
                  <tr>
                    <td>{{ ++$i }}</td>
                    <td><img src="{{ asset("assets/images/seller-design/".$list->designimage) }}" alt="{{ $list->name }}" /></td>
                    <td>{{ $list->name }}</td>
                    <td>{{ Helper::numberFormat($list->price) }}</td>
                    <td>{{ Helper::dateFormat($list->created_at,"d.m.Y") }}</td>
                    <td>{{ $list->business_category->name }}</td>
                    <td>
                      @if($list->status==1)
                      Approved
                      @elseif($list->status==2)
                      Rejected
                      @else
                      Waiting for approval
                      @endif
                    </td>
                    <td><a class="edit edit_Design_btn" data-designid="{{ encrypt($list->id) }}" ><i class="fa fa-edit"></i></a></td>
                  </tr>
                  @endforeach
                  @else
                  <tr>
                    <td class="text-center" colspan="8">
                      No Record Found
                    </td>
                  </tr>

                  @endif
                </tbody>
              </table>
              @if(!$result->isEmpty())
              <div class="pull-right">
                {{ $result->links('pagination.pagination_links') }}
                @if($result->count() > 10)
                <div class="per_Page">
                  Entries per page <select class="entriesperpage">
                    <option value="10" selected>10</option>
                    <option value="15">15</option>
                    <option value="20">20</option>
                    <option value="100">100</option>
                  </select>
                </div>
                @endif
              </div>
              @endif
            </div>

          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
</section>

<div id="Edit_Design" class="hidden designModal" style="display:none;">
  <div id="edit-design-body">
  </div>
</div>

<script type="text/javascript">
var searchUrl = url+"/search-uploads";
</script>

@endsection
