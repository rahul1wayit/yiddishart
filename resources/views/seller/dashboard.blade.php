@extends('layouts.seller')
@section('content')
<section class="main_content_wrapper">
  <div class="container">
    @include('seller.menu')
    <div class="last_latest_recent">
      <div class="panel">
        <div class="row">
          <div class="col-md-9">
            <h4 class="section_head">Last sold item</h4>
            @if(count($last_order) == 1)
            <div class="sold_panel">
              <div class="row">
                <div class="col-md-6">
                  <div class="last_sold_box_img">
                    <img src="{{ asset('assets/images/seller-design/').'/'.$last_order->design->designimage }}"/>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="last_sold_box_cont">
                    <div class="row">
                      <div class="col-md-8">
                        <div class="detail_box">
                          <h5>{{ $last_order->design->name }}</h5>
                          <p>{{ $last_order->design->business_category->name }}</p>
                          <p>Price <span>{{ $last_order->total_prices }}</span></p>
                        </div>
                        <div class="detail_box">
                          <h5>Customer Details</h5>
                          <div class="row">
                            <div class="col-md-5">
                              <div class="customer_img">
                                @if($last_order->order->buyer->image != '')
                                <img src="{{ asset('assets/images/profile/').'/'.$last_order->order->buyer->image }}"/>
                                @else
                                <img src="{{ asset('assets/images/dummy-male.png') }}"/>
                                @endif
                              </div>
                            </div>
                            <div class="col-md-7 padding_0">
                              <div class="address">{{ $last_order->order->order_details->full_name }}<br/>{{ $last_order->order->order_details->shipping_address.' '.$last_order->order->order_details->shipping_town.' '.$last_order->order->order_details->shipping_state }}<br/>{{ $last_order->order->order_details->shipping_email }}</div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4 padding_left_0">
                        <div class="paid_unpaid">
                          <i class="fa fa-check"></i>
                          <span class="check">Paid</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            @else
            No record found.
            @endif
          </div>
          <div class="col-md-3 text-center">
            <h6>Earnings</h6>
            <div class="total_balance">
              Total Balance<span> @if($all_order->sum('total_price')) {{ Helper::numberFormat($all_order->sum('total_price')) }} @else 0 @endif</span>
            </div>
            <div class="last_balance">
              <h6>Last Payment<span> @if(count($last_order) == 1) {{ $last_order->total_prices }} @else 0 @endif</span></h6>
            </div>
            <div class="transaction_history">
              <a href="{{ URL('/seller/earnings') }}">View transaction history</a>
            </div>
          </div>
        </div>
      </div>

      <div class="panel" id="Latest_added">
        <h4 class="section_head">Latest added items</h4>
        @if(!$result->isEmpty())
        <div class="row">
          <div class="col-md-12">
            <div class="row">
              <div id="news-slider10" class="owl-carousel">
                @foreach ($result as $list)
                <div class="post-slide10">
                  <img src="{{ URL("/")."/assets/images/seller-design/".$list->designimage }}" alt="{{ $list->name }}" />
                </div>
                @endforeach
              </div>
            </div>
          </div>
        </div>
        @else
        No record found.
        @endif
      </div>

      <div class="panel" id="Recent_activity">
        <h4 class="section_head">Recent activity</h4>
        <ul>
          @if($added_design)
          @foreach($added_design as $key=>$des)
          @if($key<= 1)
          <li><span>Uploaded 1 image in category {{ $des->design->business_category->name }}</span><a href="{{ URL('/template/').'/'.Crypt::encrypt($des->log_id) }}">details</a></li>
          @endif
          @endforeach
          @endif
          @if(count($last_order) == 1)
          <li><span>Sold {{ $last_order->qty }} item from category {{ $last_order->design->business_category->name }}</span><a href="{{ URL('/seller/earnings') }}">details</a></li>
          @endif
            @if((count($last_order) == 0) && $added_design->isEmpty() )
            No record found.
            @endif
        </ul>
      </div>
    </div>
  </div>
</div>
</section>
@endsection
