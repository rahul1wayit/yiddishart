<div class="row">
  <div class="col-md-12">
    <div class="navigation">
      <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item {{ (request()->is('seller/dashboard') || request()->is('seller')) ? 'active ': '' }}">
              <a class="nav-link" href="{{ action('Seller\DashboardController@index') }}">Home</a>
            </li>
            <li class="nav-item {{ (request()->is('seller/portfolio')) ? 'active ': '' }}">
              <a class="nav-link" href="{{ action('Seller\DesignController@index') }}">Portfolio</a>
            </li>
            <li class="nav-item {{ (request()->is('seller/uploads')) ? 'active ': '' }}">
              <a class="nav-link" href="{{ route('uploads') }}">Uploads</a>
            </li>
            <li class="nav-item {{ (request()->is('seller/earnings')) ? 'active ': '' }}">
              <a class="nav-link" href="{{ action('Seller\EarningController@index') }}">Earnings</a>
            </li>
            <li class="nav-item {{ (request()->is('seller/account-settings')) ? 'active ': '' }}">
              <a class="nav-link" href="{{ action('Seller\AccountSettingsController@index') }}">Account settings</a>
            </li>
          </ul>
        </div>
      </nav>
    </div>
  </div>
</div>
