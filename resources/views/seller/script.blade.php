<script type="text/javascript">
$(document).ready(function(){
    var site_url = $("#site_url").val();
    /*====================Start design form ================= */
    $('.custom-file-input').on('change', function() {
        $("#image_size").val(this.files[0].size);
        let fileName = $(this).val().split('\\').pop();
        $(this).parent(".custom-file").find('.custom-file-label').addClass("selected").html(fileName);
    });

    jQuery.validator.addMethod(
        "onlyimages",
        function (value, element) {
            if (this.optional(element) || !element.files || !element.files[0]) {
                return true;
            } else {
                var fileType = element.files[0].type;
                var isImage = /^(image)\//i.test(fileType);
                return isImage;
            }
        },
        'Sorry, we can only accept image files.'
    );

    jQuery.validator.addMethod('filesize', function (value, element, param) {
        $("#image_size").val(element.files[0].size);
        return this.optional(element) || (element.files[0].size <= param)
    }, 'File size must be less than {0}');

    $("#design-form").validate({
        rules:
        {
            name: {
                required: true
            },
            price: {
                required: true,
            },
            category: {
                required: true
            },
            size: {
                required: true
            },
            description: {
                required: true
            },
            designimage: {
                required: true,
                onlyimages: true
                // filesize: 5242880  //max size 5*1024*1024 byte or 5 mb
            },
        },
        messages:
        {
            name: {
                required: "Please enter your design name"
            },
            price: {
                required: "Please enter price"
            },
            category: {
                required: "Select category"
            },
            size: {
                required: "Select category"
            },
            description: {
                required: "Please enter description"
            },
            // designimage: {
            //     filesize: " file size must be less than 5 MB."
            // },
        },
        submitHandler: function (form)
        {
            formSubmit(form);
        }
    });

     $(document).on('click','.filterBy',function(event) {
        event.preventDefault();
        var APP_URL = {!! json_encode(url('/seller/portfolio')) !!}
        var filterBy = jQuery(this).attr('data-filterBy');
        if(filterBy == 'desc'){
            filterBy = 'asc';
        }else{
            filterBy = 'desc';
        }
        jQuery(this).attr('data-filterBy',filterBy);
        $.ajax({
            url:APP_URL+"?page=1&filterBy="+filterBy,
            method:'get',
            datatype:'html',
            beforeSend  : function () {
                $(".button-disabled").attr("disabled", "disabled");
                $("#loader-section").css('display','block');
            },
            complete: function () {
                $("#loader-section").css('display','none');
            },
            success:function(response) {
                $('#portfolio_panel_list').html(response);
            }

        });
     });

    // $(document).on('click','#portfolio_panel_list .pagination a',function(event) {
    //     event.preventDefault();
    //     var myurl = jQuery(this).attr('href');
    //     var filterBy = jQuery('.filterBy').attr('data-filterBy');
    //     $('li').removeClass('active');
    //     $(this).parent('li').addClass('active');
    //
    //     $.ajax({
    //         url:myurl+"&filterBy="+filterBy,
    //         method:'get',
    //         datatype:'html',
    //         beforeSend  : function () {
    //             $(".button-disabled").attr("disabled", "disabled");
    //             $("#loader-section").css('display','block');
    //         },
    //         complete: function () {
    //             $("#loader-section").css('display','none');
    //         },
    //         success:function(response) {
    //             $('#portfolio_panel_list').html(response);
    //         }
    //
    //     });
    // });

    /*====================End design form ================= */

    /*====================Start upload ================= */
    // $(document).on('click','#upload_list .pagination a',function(event) {
    //     event.preventDefault();
    //     var myurl = jQuery(this).attr('href');
    //     $('li').removeClass('active');
    //     $(this).parent('li').addClass('active');
    //
    //     $.ajax({
    //         url:myurl,
    //         method:'get',
    //         datatype:'html',
    //         beforeSend  : function () {
    //             $(".button-disabled").attr("disabled", "disabled");
    //             $("#loader-section").css('display','block');
    //         },
    //         complete: function () {
    //             $("#loader-section").css('display','none');
    //         },
    //         success:function(response) {
    //             $('#upload_list').html(response);
    //         }
    //
    //     });
    // });

    $(document).on('click','.Edit_Design',function(event) {
        event.preventDefault();
        var getId = $(this).attr("data-designid");
        $.ajax({
            url         : site_url + '/seller/geteditdesignmodal',
            type        : 'POST',
            data        : { id : getId },
            headers     : {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType    : "html",
            beforeSend  : function () {
                $("#loader-section").css('display','block');
            },
            complete: function () {
                $("#loader-section").css('display','none');
            },
            success: function (response) {
                $("#edit-design-body").html(response);
            },
            error:function(response){
                // console.log(response);
                // console.log('sdsdsds');
            }
        });
    });
    /*====================End upload ================= */

})
</script>
