<div class="user_customization">
  <div class="row">
    <div class="col-md-6">
      <div class="textfields_fonts">
        <div class="form-group">
          <?php
          $canvasArr = json_decode(unserialize($result->canvasData));
          if(!empty($canvasArr))
          {
            $textCount = 1;
            foreach ($canvasArr as $key => $value) {

              if($value->customtype == 'text')
              {
                ?>
                <div class="layer-trigger-wrapper textDiv">
                  <div class="layer-trigger">
                    <a href="javascript:void(0);" data-toggle="collapse" class="select_textfield collapsed" data-target="#textfields{{ $key }}">Text field {{ $textCount }} <span class="icon_select"></span></a>
                    <div id="textfields{{ $key }}" class="collapse text-settings" data-name="{{ $value->name }}" data-key="{{ $key }}">
                      <div class="xy">
                        <div class="left">
                          <div class="form-group">
                            <label>X</label>
                            <input type="number" class="form-control 1st_text_class validNumber" min="1" name="x_1st" value="{{ $value->x }}"/>
                          </div>
                        </div>
                      </div>
                      <div class="xy">
                        <div class="left">
                          <div class="form-group">
                            <label>Y</label>
                            <input type="number" class="form-control 1st_text_class validNumber" min="1" name="y_1st" value="{{ $value->y }}"/>
                          </div>
                        </div>
                      </div>
                      <div class="xy">
                        <div class="left">
                          <div class="form-group">
                            <label><img src="{{ asset('assets/images/Angle.png') }}"/></label>
                            <input type="number" class="form-control 1st_text_class validNumber angle_1st" min="0" max="360" value="{{ $value->rotate }}" name="angle_1st"/>
                          </div>
                        </div>
                      </div>
                      <div class="xy language_limit">
                        <div class="left">
                          <div class="form-group">
                            <select class="form-control 1st_text_class language" name="language_1st">
                              <option @if($value->language == 'qwerty') selected @endif value="qwerty">English</option>
                              <option @if($value->language == 'hebrew-qwerty') selected @endif value="hebrew-qwerty">Hebrew</option>
                            </select>
                          </div>
                        </div>
                        <div class="right">
                          <div class="form-group">
                            <label>Character limit</label>
                            <input class="form-control 1st_text_class validNumber char_limit_1st" type="number" value="{{ $value->char_limit }}" min="1" name="char_limit_1st">
                          </div>
                        </div>
                      </div>
                      <div class="xy Default_text">
                        <div class="form-group">
                          <input type="text" placeholder="Text field 1" class="form-control 1st_text_class text_1st keyBord1" style="display:@if($value->language == 'qwerty') block @else none @endif;" name="text_1st" value="{{ $value->text }}" />
                          <input type="text" placeholder="Text field 1" class="form-control 1st_text_class text_1st keyBord2" style="display:@if($value->language == 'hebrew-qwerty') block @else none @endif;" name="text_1st" value="{{ $value->text }}" />
                          <input type="hidden" class="languageSelectInput" name="languageName" value="qwerty" />
                        </div>
                      </div>
                      <div class="xy font_style">
                        <div class="form-group">

                          @php
                          $fonts = Helper::getFonts();
                          @endphp
                          <select class="font form-control 1st_text_class font_1st" name="font_1st">
                            @if(!empty($fonts))
                            @foreach($fonts as $font)
                            <option @if($value->fontFamily == $font->name) selected @endif value="{{ $font->name }}">{{ $font->name }}</option>
                            @endforeach
                            @endif
                          </select>

                          <select class="px form-control 1st_text_class bold_1st" name="font_weight_1st">
                            <option  @if($value->fontStyle == 'normal') selected @endif value="normal">Normal</option>
                            <option  @if($value->fontStyle == 'bold') selected @endif value="bold">Bold</option>
                            <option  @if($value->fontStyle == 'italic') selected @endif value="italic">Italic</option>
                          </select>

                          <select class="px form-control 1st_text_class font_size_1st" name="font_size_1st">
                            <?php for ($i=6; $i < 50; $i++) {
                              if($i == $value->fontSize)
                              {
                                $selected = 'selected';
                              }else {
                                $selected = '';
                              }
                              echo '<option '.$selected.' value="'.$i.'">'.$i.'px</option>';
                              $i = $i+1;
                            } ?>
                          </select>
                        </div>
                      </div>
                      <div class="xy">
                        <div class="form-group">
                          <div class="leadtrack">
                            <label>Leading</label>
                            <select class="form-control 1st_text_class" name="leading_1st">
                              <option @if($value->lineHeight == '1') selected @endif value="1">1</option>
                              <option @if($value->lineHeight == '1.5') selected @endif value="1.5">1.5</option>
                              <option @if($value->lineHeight == '2') selected @endif value="2">2</option>
                              <option @if($value->lineHeight == '2.5') selected @endif value="2.5">2.5</option>
                              <option @if($value->lineHeight == '3') selected @endif value="3">3</option>
                              <option @if($value->lineHeight == '3.5') selected @endif value="3.5">3.5</option>
                            </select>
                          </div>
                          <div class="leadtrack">
                            <label>Tracking</label>
                            <select class="form-control 1st_text_class" name="tracking_1st">
                              <option @if($value->letterSpacing == '0') selected @endif value="0">0</option>
                              <option @if($value->letterSpacing == '1') selected @endif value="1">1</option>
                              <option @if($value->letterSpacing == '2') selected @endif value="2">2</option>
                              <option @if($value->letterSpacing == '3') selected @endif value="3">3</option>
                              <option @if($value->letterSpacing == '4') selected @endif value="4">4</option>
                              <option @if($value->letterSpacing == '5') selected @endif value="5">5</option>
                            </select>
                          </div>
                          <div class="leadtrack colorPicker">
                            <span>  <input type="color" class="form-control 1st_text_class" value="{{ $value->fillStyle }}" name="color_1st"/> </span>
                          </div>
                        </div>
                      </div>
                      <div class="xy">
                        <div class="form-group">
                          <div class="Paragraph">
                            <label>Paragraph</label>
                            <span><a href="javascript:void(0);" class="1st_text_class" ><img name="left_align" src="{{ asset('assets/images/align_left.png') }}"/></a></span>
                            <span><a href="javascript:void(0);" class="1st_text_class" ><img name="center_align" src="{{ asset('assets/images/align_center.png') }}"/></a></span>
                            <span><a href="javascript:void(0);" class="1st_text_class" ><img name="right_align" src="{{ asset('assets/images/align_right.png') }}"/></a></span>
                          </div>
                        </div>
                      </div>
                      <div class="xy">
                        <div class="form-group">
                            <label>Text Width</label>
                            <input class="form-control 1st_text_class validNumber text_width_set" type="number" min="1" name="text_width" value="{{ $value->maxWidth }}">
                        </div>
                    </div>
                    </div>
                    <?php
                    if($value->customtype == 'text' && $value->name != 'text')
                    { ?>
                      <i class="deleteImage fa fa-times"></i>
                      <?php
                    } ?>
                  </div>
                </div>
                <?php
                $textCount++;
              }
            }
          }
          ?>
          <span class="add_more_text_cls"><a href="javascript:void(0);" id="add-more"><i class="fa fa-plus addNewText"></i>Add text field</a></span>
          <?php
          if(!empty($canvasArr))
          {
            foreach ($canvasArr as $key => $value)
            {
              if($value->name == 'logo')
              {
                ?>
                <div class="layer-trigger-wrapper logoDiv">
                  <div class="layer-trigger"> <a href="javascript:void(0);" data-toggle="collapse" class="select_textfield collapsed" data-target="#imagefields{{ $key }}">Logo <span class="icon_select"></span></a>
                    <div id="imagefields{{ $key }}" class="collapse text-settings" data-name="logo" data-key="{{ $key }}">
                      <div class="xy">
                        <div class="left">
                          <div class="form-group">
                            <label>X</label>
                            <input type="number" class="form-control 1st_text_class validNumber" min="1" name="x_1st" value="{{ ($value->x==1) ? 150 : $value->x }}" /> </div>
                          </div>
                          <div class="right">
                            <div class="form-group">
                              <label>Width</label>
                              <input type="number" class="form-control 1st_text_class" name="width_1st" min="1" value="{{ ($value->width==1) ? 200 : $value->width }}" /> </div>
                            </div>
                          </div>
                          <div class="xy">
                            <div class="left">
                              <div class="form-group">
                                <label>Y</label>
                                <input type="number" class="form-control 1st_text_class validNumber" min="1" name="y_1st" value="{{ ($value->y==1) ? 150 : $value->y }}" /> </div>
                              </div>
                              <div class="right">
                                <div class="form-group">
                                  <label>Height</label>
                                  <input type="number" class="form-control 1st_text_class" name="height_1st" min="1" value="{{ ($value->height==1) ? 200 : $value->height }}" /> </div>
                                </div>
                              </div>
                              <div class="xy">
                                <div class="left">
                                  <div class="form-group">
                                    <label><img src="{{ asset('assets/images/Angle.png') }}" /></label>
                                    <input type="number" class="form-control 1st_text_class validNumber angle_1st" min="0" max="360" value="{{ ($value->rotate==1) ? 0 : $value->rotate }}" name="angle_1st" /> </div>
                                  </div>
                                  <div class="right">
                                    <div class="form-group">
                                      <input type="file" name="image" class="fileUp addNewImgInp">
                                      <input type="hidden" class="editPatientView" value="">
                                      <div class="input-group col-xs-12"> <span class="input-group-addon"><i class="glyphicon glyphicon-picture"></i></span>
                                        <input type="text" class="form-control upload_bx consent_dateClass" disabled placeholder="Choose File"> <span class="input-group-btn">    <button class="browse btn btn-primary" type="button"><i class="fa fa-upload"></i> </button>  </span></div><span class="error-image" style="color:red; text-align:center;"></span>
                                        <input type="hidden" class="imageUrl" rel="" name="imageName" />
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <?php
                                if($value->x != 1)
                                { ?>
                                  <i class="deleteImage fa fa-times"></i>
                                  <?php
                                } ?>
                              </div>
                            </div>
                            <?php
                          }
                        }
                      } ?>
                      <?php
                      if(!empty($canvasArr))
                      {
                        $imgCount = 1;
                        foreach ($canvasArr as $key => $value)
                        {
                          if($value->customtype == 'image' && $value->name != 'logo' && $value->name != 'default-image')
                          {
                            ?>
                            <div class="layer-trigger-wrapper imageDiv">
                              <div class="layer-trigger"> <a href="javascript:void(0);" data-toggle="collapse" class="select_textfield collapsed" data-target="#imagefields{{ $key }}">Logo {{ $imgCount }} <span class="icon_select"></span></a>
                                <div id="imagefields{{ $key }}" class="collapse text-settings" data-name="{{ $value->name }}" data-key="{{ $key }}">
                                  <div class="xy">
                                    <div class="left">
                                      <div class="form-group">
                                        <label>X</label>
                                        <input type="number" class="form-control 1st_text_class validNumber" min="1" name="x_1st" value="{{ $value->x }}" /> </div>
                                      </div>
                                      <div class="right">
                                        <div class="form-group">
                                          <label>Width</label>
                                          <input type="number" class="form-control 1st_text_class" name="width_1st" min="1" value="{{ $value->width }}" /> </div>
                                        </div>
                                      </div>
                                      <div class="xy">
                                        <div class="left">
                                          <div class="form-group">
                                            <label>Y</label>
                                            <input type="number" class="form-control 1st_text_class validNumber" min="1" name="y_1st" value="{{ $value->y }}" /> </div>
                                          </div>
                                          <div class="right">
                                            <div class="form-group">
                                              <label>Height</label>
                                              <input type="number" class="form-control 1st_text_class" name="height_1st" min="1" value="{{ $value->height }}" /> </div>
                                            </div>
                                          </div>
                                          <div class="xy">
                                            <div class="left">
                                              <div class="form-group">
                                                <label><img src="{{ asset('assets/images/Angle.png') }}" /></label>
                                                <input type="number" class="form-control 1st_text_class validNumber angle_1st" min="0" max="360" value="0" name="angle_1st" /> </div>
                                              </div>
                                              <div class="right">
                                                <div class="form-group">
                                                  <input type="file" name="image" class="fileUp addNewImgInp">
                                                  <input type="hidden" class="editPatientView" value="">
                                                  <div class="input-group col-xs-12"> <span class="input-group-addon"><i class="glyphicon glyphicon-picture"></i></span>
                                                    <input type="text" class="form-control upload_bx consent_dateClass" disabled placeholder="Choose File"> <span class="input-group-btn">    <button class="browse btn btn-primary" type="button"><i class="fa fa-upload"></i> </button>  </span></div><span class="error-image" style="color:red; text-align:center;"></span>
                                                    <input type="hidden" class="imageUrl" rel="" name="imageName" />
                                                  </div>
                                                </div>
                                              </div>
                                            </div> <i class="deleteImage fa fa-times"></i> </div>
                                          </div>
                                          <?php
                                          $imgCount++;
                                        }
                                      }
                                    } ?>

                                    <span class="add_more_image_cls"><a href="javascript:void(0);" id="add-more-image"><i class="fa fa-plus addNewImage"></i>Add Logo</a></span>
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <canvas id="canvas" width="570" height="150"></canvas>
                              </div>
                            </div>
                            <div class="size_text_next">
                              <div class="row">
                                <div class="col-md-8">
                                  <!-- <div class="Upload_font">
                                    <span><a href="javascript:void(0);"><i class="fa fa-plus"></i>Upload font</a></span>
                                  </div> -->
                                </div>
                                <div class="col-md-4">
                                  <div class="next_pre">
                                    <a href="javascript:void(0);" class="sec-page3-next">Next</i></a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="design_information" style="display:none">
                            <form action="{{ action('Seller\DesignController@store') }}" method="POST" enctype="multipart/form-data" id="addDesignForm" class="design-form-first">
                              @csrf
                              <div class="row">
                                <div class="col-md-7">
                                  <div class="form-group">
                                    <input type="text" name="name" class="form-control" placeholder="Name your design" value="{{ $result->name }}"/>
                                  </div>
                                </div>
                                <div class="col-md-5">
                                  <div class="form-group">
                                    <label>Price</label>
                                    <div class="price_input">
                                      <input type="text" name="price" class="form-control validNumber" value="{{ $result->price }}"/>
                                      <input type="hidden" name="canvasData"  class="form-control canvasDataCls" value="{{ unserialize($result->canvasData) }}">
                                      <input type="hidden" name="designimage"  class="form-control designimage" value="{{ $result->designimage }}" >
                                      <input type="hidden" name="id"  class="form-control" value="{{ encrypt($result->id) }}" >
                                      <span>$</span>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-md-6">
                                  <div class="form-group">
                                    @php
                                    $oldd = old('cat');
                                    $business_categories = Helper::getTablaData('business_categories');
                                    @endphp
                                    <select name="category" class="form-control">
                                      <option value="">Select Category</option>
                                      @if(!empty($business_categories))
                                      @foreach($business_categories as $category)
                                      <option @if($result->category==$category->id) selected="selected" @endif  value="{{ $category->id }}">{{ $category->name }}</option>
                                      @endforeach
                                      @endif
                                    </select>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <select name="size" class="form-control">
                                      <option @if($result->size==0) selected="selected" @endif value="0">Select Size</option>
                                      <option @if($result->size=='Small') selected="selected" @endif value="Small">Small</option>
                                      <option @if($result->size=='Medium') selected="selected" @endif value="Medium">Medium</option>
                                      <option @if($result->size=='Large') selected="selected" @endif value="Large">Large</option>
                                    </select>
                                  </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-md-12">
                                  <div class="form-group">
                                    <textarea class="form-control" name="description" placeholder="Design description">{{ $result->description }}</textarea>
                                  </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-md-12">
                                  <div class="form-group">
                                    <input type="text" name="keyword" class="form-control" placeholder="Type keywords for your design" value="{{ $result->keyword }}"/>
                                  </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-md-6">
                                  <div class="next_pre text-left">
                                    <a href="javascript:void(0);" class="sec-page4-back">Prev</a>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <div class="Add_designBtn text-right">
                                      <button type="submit" class="btn btn-primary">Update design</button>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </form>
                          </div>

                          <script type="text/javascript">
                          var dbCanvasData = <?= json_encode(unserialize($result->canvasData)) ?>;
                          </script>
