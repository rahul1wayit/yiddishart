@extends('layouts.seller')
@section('content')
<section class="main_content_wrapper">
  <div class="container">
    @include('seller.menu')
    <div class="designer_portfolio_section">
      <div class="row">
        <div class="col-md-6">
          <div class="filter">
            <a href="javascript:void(0)" class="filterBy" data-filterBy="desc">Filter by <span>Month <i class="fa fa-arrow-down"></i></span></a>
          </div>
        </div>
        <div class="col-md-6">
          <div class="Add_designss text-right">
            <button id="dialogButton" class="btn btn-primary design_modal"><i class="fa fa-plus"></i> Add Design</button>
          </div>
        </div>
      </div>
      <div class="portfolio_panel" id="paginationData">
        <div class="row">
          @if(!$result->isEmpty())
          @foreach ($result as $list)
          <div class="col-md-3">
            <div class="portfolio_box">
              <div class="portfolio_img">
                <img src="{{ URL("/")."/assets/images/seller-design/".$list->designimage }}" alt="{{ $list->name }}" />
              </div>
              <div class="portfolio_content">
                <h5>{{ ucfirst($list->name) }}</h5>
                <h5><strong>{{ Helper::numberFormat($list->price) }}</strong></h5>
                <p>{{ $list->business_category->name }}</p>
              </div>
            </div>
          </div>
          @endforeach
          @else
          <span class="col-md-12 text-center">
            No Record Found
          </span>
        </div>
        {{ $result->links('pagination.pagination_links') }}
        @endif
      </div>
    </div>
  </div>
</section>

<div id="Add_Design" class="hidden designModal" style="display:none;">
  @php
  $designImageRequired = 1;
  @endphp
  @include('seller.designmodel')
</div>
@include('includes.admin.delete-modal')
@endsection
