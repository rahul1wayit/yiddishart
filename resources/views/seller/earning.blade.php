@extends('layouts.seller')
@section('content')
<section class="main_content_wrapper">
  <div class="container">
    @include('seller.menu')
    <div class="designer_uploads_section">
      <div class="row">
        <div class="col-md-12">
          <div class="earnings_cont text-right">
            <ul class="list-inline">
              <!-- <li class="list-inline-item"><a href="#">Change payment method</a></li> -->
              <li class="list-inline-item"><a href="{{ URL('seller/download-csv') }}"><img src="{{ asset('assets/images/download-icon.png') }}"/> Download CSV</a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="uploads_panel">
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive" id="paginationData">
              <table class="table">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th style="width:200px;">Design type</th>
                    <th style="width:130px;">Design</th>
                    <th style="font-size:0;width: 161px;">Name</th>
                    <th>Price</th>
                    <th class="text-center">No. of downloads</th>
                    <th style="font-size:0;">Action</th>
                  </tr>
                </thead>
                <tbody>
                  @if(!$result->isEmpty())
                  @foreach ($result as $index=>$list)
                  <tr>
                    <td>{{ $result->perPage() * ($result->currentPage() -1 ) + $index+1 }}</td>
                    <td>{{ $list->business_category->name }}</td>
                    <td><img class="myImg" src="{{ asset("assets/images/seller-design/".$list->designimage) }}" alt="{{ $list->name }}" /></td>
                    <td>{{ $list->name }}</td>
                    <td>{{ Helper::numberFormat($list->price) }}</td>
                    <td class="text-center"><strong>{{ $list->orderItems->sum('qty') }}</strong></td>
                    <td><a class="view_detail view_earning_detail" data-id="{{ encrypt($list->id) }}" >view details</a></td>
                  </tr>
                  @endforeach
                  @else
                  <tr>
                    <td class="text-center" colspan="8">
                      No Record Found
                    </td>
                  </tr>
                  @endif
                </tbody>
              </table>
              <div class="pull-right">
                {{ $result->links('pagination.pagination_links') }}
                @if($result->count() > 10)
                <div class="per_Page">
                  Entries per page <select class="entriesperpage">
                    <option value="10" selected>10</option>
                    <option value="15">15</option>
                    <option value="20">20</option>
                    <option value="100">100</option>
                  </select>
                </div>
                @endif
              </div>
            </div>

          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
</section>

<div id="View_Earnings" class="hidden" style="display:none;">
  <div id="get_earning_detail">
  </div>
</div>

<script type="text/javascript">
var searchUrl = url+"/search-earnings";
</script>

@endsection
