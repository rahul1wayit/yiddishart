@extends('layouts.frontend')
@section('content')
<section class="main_content_wrapper searchSection">
  <div class="banner text-center">
    <h1>Send your love<br/>with design you love</h1>
    <div class="banner_search">
      <div class="container">
        <div class="input-group" id="adv-search">
          <input type="text" class="form-control searchName" placeholder="Search for snippets" />
          <div class="input-group-btn">
            <div class="btn-group" role="group">
              @php
              $categories = Helper::getTablaDataOrderBy('business_categories','id','asc');
              @endphp
              <div class="dropdown dropdown-lg">
                <button type="button" class="btn btn-default dropdown-toggle " data-toggle="dropdown" aria-expanded="false">in <span class="selecCat">Category</span> <span class="caret"></span></button>
                <div class="dropdown-menu dropdown-menu-right" role="menu">
                  <form class="form-horizontal" role="form">
                    <div class="form-group">
                      <label for="filter">Filter by</label>
                      <select class="form-control catFilter">
                        <option value="0" selected>All Categories</option>
                        @if(!$categories->isEmpty())
                        @foreach($categories as $index => $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                        @endif
                      </select>
                    </div>
                    <button type="button" class="btn btn-primary frontCategorySearch">Search</button>
                  </form>
                </div>
              </div>
              <button type="button" class="btn btn-primary frontSearch" ref="@if(Auth::check())@php echo Auth::user()->roles()->first()->name @endphp@endif">Search</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="Featured_categories">
    <div class="container">
      <div class="Featured_panel">
        <div class="row">
          <div class="col-md-6">
            <h4 class="section_head">Categories</h4>
          </div>
          <div class="col-md-6 text-right">
            <a href="{{ URL('/categories') }}">Browse all ></a>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            @if(!$categories->isEmpty())
            @foreach($categories as $index => $category)
            <div class="Feature_box category_wise_design" rel="{{ $category->id }}">
              <div class="img_box">
                <img src="{{ asset('assets/images/'.$category->image) }}"/>
              </div>
              <div class="image_title">
                <h5>{{ $category->name }}</h5>
              </div>
            </div>
            @endforeach
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="spacer20"></div>
  <div class="special_schools">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="special_school_panel">
            <div class="row">
              <div class="col-md-4">&nbsp;
              </div>
              <div class="col-md-8">
                <div class="row">
                  <div class="col-md-8">
                    <h3>SPECIAL <span>for schools</span></h3>
                    <p>Sign up to become member and have access to unlimited downloads for a annual price</p>
                  </div>
                  <div class="col-md-4">
                    <a href="{{url('register')}}" class="btn btn-primary">Sign up</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="spacer20"></div>
  <div class="Design_process">
    <div class="container">
      <h4 class="section_head">How to make your custom design</h4>
      <div class="row text-center">
        <div class="col-md-3">
          <img src="{{ asset('assets/images/process1.jpg')}}"/>
          <h5>Search and find <br/>the design you like</h5>
        </div>
        <div class="col-md-3">
          <img src="{{ asset('assets/images/process2.jpg')}}"/>
          <h5>Modify the text<br/>to make it your own</h5>
        </div>
        <div class="col-md-3">
          <img src="{{ asset('assets/images/process3.jpg')}}"/>
          <h5>Send to printer<br/> or download</h5>
        </div>
        <div class="col-md-3">
          <img src="{{ asset('assets/images/process4.jpg')}}"/>
          <h5>Enjoy</h5>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
