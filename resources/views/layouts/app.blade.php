@include('includes.head')
<section class="main_content_wrapper searchSection">
	<div class="container">
        <main class="py-4">
            @yield('content')
        </main>
      </div>
    </section>
@include('includes.footer')
