@include('includes.admin.head')
<script type="text/javascript">
var url = "<?php //echo url(''); ?>";
</script>
<body>
  @if(Auth::check())
  <script type="text/javascript">
  url = "<?php echo url(request()->route()->getPrefix()) ?>";
</script>
@endif
<div class="main_wrapper">
    @include('includes.admin.header')
    @include('includes.admin.sidebar')
    @yield('content')
</div>
@include('includes.admin.footer')
</body>
</html>
