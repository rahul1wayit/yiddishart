@include('includes.head')
<body>
<div class="receivable">
    @include('includes.header')
    @yield('content')
</div>
@include('includes.footer')
</body>
</html>
