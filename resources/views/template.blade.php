@extends('layouts.frontend')
@section('content')
<section class="main_content_wrapper searchSection">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="ProductDetails">
          <div class="card">
            <div class="container-fliud">
              <div class="wrapper row">
                <div class="preview col-md-6">
                  <div class="preview-pic tab-content">
                    <div class="tab-pane active" id="pic-1"><img src="{{ asset("assets/images/seller-design/".$design->designimage) }}" /></div>
                    <!-- <div class="tab-pane" id="pic-2"><img src="{{ asset("assets/images/seller-design/".$design->designimage) }}" /></div>
                    <div class="tab-pane" id="pic-3"><img src="{{ asset("assets/images/seller-design/".$design->designimage) }}" /></div> -->
                  </div>
                  <!-- <ul class="preview-thumbnail nav nav-tabs">
                  <li class="active"><a data-target="#pic-1" data-toggle="tab"><img src="{{ asset("assets/images/seller-design/".$design->designimage) }}" /></a></li>
                  <li><a data-target="#pic-2" data-toggle="tab"><img src="{{ asset("assets/images/seller-design/".$design->designimage) }}" /></a></li>
                  <li><a data-target="#pic-3" data-toggle="tab"><img src="{{ asset("assets/images/seller-design/".$design->designimage) }}" /></a></li>
                </ul> -->
                @php
                if(isset($design->ratings))
                $fav = $design->ratings->favourite;
                else
                $fav = '';

                if(isset($design->ratings))
                $rate = $design->totalRating->avg('rating');
                else
                $rate = '';

                $i = '';
                $j = '';
                @endphp

              </div>
              <div class="details col-md-6">
                <h3 class="product-title">{{ $design->name }}
                  <span>
                    @if(Auth::check())
                    @if(Auth::user()->roles()->first()->name == 'buyer')
                    <a class="favourite favouriteBtn @if($fav == 1) active @endif" data-id="{{ $design->id }}" data-encid="{{ Crypt::encrypt($design->id) }}" href="javascript:void(0);"><i class="fa @if($fav == 1) fa-heart @else fa-heart-o @endif"></i></a>
                    <a href="#"><i class="fa fa-shopping-cart"></i></a>
                    @else
                    <a class="favourite favouriteBtn redirectTo" data-id="{{ $design->id }}" data-encid="{{ Crypt::encrypt($design->id) }}" href="javascript:void(0);"><i class="fa fa-heart-o"></i></a>
                    @endif
                    @endif
                  </span>
                </h3>
                <div class="cat_price">
                  <span>{{ $design->business_category->name }}</span>
                  <span>Price <b>{{ Helper::numberFormat($design->price) }}</b></span>
                </div>
                <div class='rating-stars'>
                  <ul class='stars overall_rating'>
                    @if($rate != '')
                    @for($i = 1; $i <= $rate; $i++)
                    <li class='star' data-value=''>
                      <i class='fa fa-star fa-fw'></i>
                    </li>
                    @endfor
                    @for($j = $rate; $j < 5; $j++)
                    <li class='star' data-value=''>
                      <i class='fa fa-star-o fa-fw'></i>
                    </li>
                    @endfor
                    @endif
                  </ul>
                </div>
                <p class="product-description">{{ $design->description }}</p>
                @if(Auth::check())
                @if(Auth::user()->roles()->first()->name == 'buyer')
                <div class="action_btn text-right">
                  <a class="btn btn-success" href="{{ URL('/buyer/edit-template/'.Crypt::encrypt($design->id)) }}">Buy</a>
                  <button class="btn btn-primary @if(!Auth::check()) redirectTo @endif downloadBtn" data-id="{{ $design->id }}" data-encid="{{ Crypt::encrypt($design->id) }}" href="javascript:void(0);" type="button">Download</button>
                </div>
                @endif
                @endif
              </div>
            </div>
          </div>
        </div>
        <div class="RatingReviews">
          <h2>Ratings and Reviews</h2>
          @if(!$design->totalRating->isEmpty())
          @foreach($design->totalRating as $ratingDatail)
          @if($ratingDatail->rating != '')
          <div class="Review_item">
            <div class="row">
              <div class="col-md-4">
                <div class="Review_item_info">
                  <div class="review_img">
                    @if($ratingDatail->user->image != '')
                    <img src="<?php echo asset('assets/images/profile'); ?>/{{ $ratingDatail->user->image }}">
                    @elseif($ratingDatail->user->gender != 'Male')
                    <img src="{{ asset('assets/images/dummy-female.png') }}">
                    @else
                    <img src="{{ asset('assets/images/dummy-male.png') }}">
                    @endif
                  </div>
                  <div class="Review_username">
                    <h5>{{ $ratingDatail->user->full_name }}</h5>
                    <p>{{ Helper::dateFormat($ratingDatail->updated_at,'d.m.Y. H:i') }}</p>
                    <div class="rating">
                      <div class="stars">
                        @for($i = 1; $i <= $ratingDatail->rating; $i++)
                        <i class='fa fa-star fa-fw'></i>
                        @endfor
                        @for($i = $ratingDatail->rating; $i < 5; $i++)
                        <i class='fa fa-star-o fa-fw'></i>
                        @endfor
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-8">
                <div class="ReviewDescription">
                  <p>{{ $ratingDatail->review }}</p>
                </div>
              </div>
            </div>
          </div>
          @else
          <p>No rating yet.</p>
          @endif
          @endforeach
          @else
          <p>No rating yet.</p>
          @endif
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>
</section>
@endsection
