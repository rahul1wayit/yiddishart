@extends('layouts.seller')
@section('content')
<section class="main_content_wrapper searchSection">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="title_heading text-center">
          <h2>Cart</h2>
        </div>
      </div>
    </div>
    @if ($message = Session::get('success'))
    <div class="alert alert-success alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>{{ $message }}</strong>
    </div>
    @endif

    @if ($message = Session::get('error'))
    <div class="alert alert-danger alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>{{ $message }}</strong>
    </div>
    @endif
    <div class="designer_uploads_section">
      <div class="uploads_panel">
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th style="width:130px;">Design</th>
                    <th style="font-size:0;width: 40%;">Name</th>
                    <th>Price</th>
                    <th class="text-center">Quantity</th>
                    <th>Total Price</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  @php
                  $total_price = 0;
                  @endphp

                  @if(!$result->isEmpty())
                  @foreach($result as $index => $list)

                  @php
                  $total_price += $list->price* $list->qty;
                  @endphp
                  <tr class="favTr cartTr{{ $list->id }}">
                    <td class="sr_no">{{ $result->perPage() * ($result->currentPage() -1 ) + $index+1 }}</td>
                    <td><img class="myImg" src="{{ asset("assets/images/seller-design/".$list->design_img) }}" alt="{{ $list->name }}" /></td>
                    <td><a href="javascript:void();" >{{ $list->design->name }}</a></td>
                    <td class="main_price">{{ Helper::numberFormat($list->price) }}</td>
                    <td>
                      <div id="field1" class="subadd">
                        <button type="button" id="sub" class="sub subQty">-</button>
                        <input type="number" id="1" value="{{ $list->qty }}" min="1" max="10" onkeypress="return blockSpecialChar(event)" class="qty design_qty"/>
                        <button type="button" id="add" class="add addQty">+</button>
                      </div>
                    </td>
                    <td><span class="total_price" ref="{{ $list->price }}">{{ Helper::numberFormat($list->qty*$list->price) }}</span><br>
                      <span class="remove-design" ref="{{ $list->id }}">remove</span>
                    </td>
                    <td><a class="checkout" href="{{ URL('/buyer/edit-template/'.Crypt::encrypt($list->design->id)) }}"><i class="fa fa-edit"></i> </a></td>
                  </tr>

                  @endforeach
                  <tr class="noFound1">
                    <td colspan="4">
                    </td>
                    <td class="text-center">Total Price
                    </td>
                    <td class="over_total">{{ Helper::numberFormat($total_price) }}
                    </td>
                    <td colspan="1">
                    </td>
                  </tr>
                  <tr class="noFound1">
                    <td colspan="5">
                    </td>
                    <td class="text-left"><a href="{{ URL('buyer/checkout') }}" class="btn btn-success">Check Out</a>
                    </td>
                    <td colspan="1">
                    </td>
                  </tr>
                  <tr class="noFound" style="display:none;">
                    <td colspan="7" style="text-align: center;">
                      Your Cart is empty
                    </td>
                  </tr>
                  @else
                  <tr class="noFound">
                    <td colspan="7" style="text-align: center;">
                      Your Cart is empty
                    </td>
                  </tr>
                  @endif
                </tbody>
              </table>
            </div>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
