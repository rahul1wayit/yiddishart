<div class="row">
  <div class="col-md-12">
      <div class="navigation">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item {{ (request()->is('buyer/account-settings') || request()->is('buyer/dashboard') || request()->is('buyer') ) ? 'active ': '' }}">
                <a class="nav-link" href="{{ action('Buyer\AccountSettingsController@index') }}">Account settings</a>
              </li>
<!--               <li class="nav-item {{ (request()->is('buyer/dashboard') || request()->is('buyer')) ? 'active ': '' }}">
                <a class="nav-link" href="{{ action('Buyer\DashboardController@index') }}">Profile</a>
              </li> -->
              <li class="nav-item {{ (request()->is('buyer/downloads')) ? 'active ': '' }}">
                <a class="nav-link" href="{{ action('Buyer\FavoriteController@downloads') }}">Downloads</a>
              </li>
              <li class="nav-item {{ (request()->is('buyer/favourite')) ? 'active ': '' }}">
                <a class="nav-link" href="{{ action('Buyer\FavoriteController@index') }}">Favorite</a>
              </li>
              <li class="nav-item {{ (request()->is('buyer/cart-list')) ? 'active ': '' }}">
                <a class="nav-link" href="{{ action('Buyer\CartController@cartList') }}">Cart</a>
              </li>
            </ul>
          </div>
        </nav>
      </div>
  </div>
</div>
