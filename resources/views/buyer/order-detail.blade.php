@extends('layouts.seller')
@section('content')
<section class="main_content_wrapper searchSection">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="title_heading text-center">
          <h2>Order Details</h2>
        </div>
      </div>
      <div class="col-md-6">
        <div class="title_heading text-left details_order">
          <h4>Billing Address</h4>
          <p class="name_billing">{{ $result->order_details->full_name }}</p>
          <p class="order_num">{{ $result->order_details->shipping_email }}</p>
          <p>{{ $result->order_details->shipping_address.' '.$result->order_details->shipping_town.' '.$result->order_details->shipping_state }}</p>
          <p><span>Phone: </span>{{ $result->order_details->shipping_phone }}</p>
          <p><span>Shipping method: </span>@if($result->shipping == 0) Regular @else Express @endif</p>
        </div>
      </div>
      <div class="col-md-6">
        <div class="title_heading text-right">
          <a href="{{ URL('/buyer/orders') }}" class="btn btn-primary">Back To Order</a>
        </div>
      </div>
    </div>
    <div class="designer_uploads_section">
      <div class="uploads_panel">
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive" id="paginationData">
              <table class="table">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th style="width:130px;">Design</th>
                    <th style="font-size:0;width: 161px;">Name</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th class="text-right">Total Price</th>
                  </tr>
                </thead>
                <tbody>
                  @php
                  $total_price = 0;
                  @endphp

                  @foreach($result['order_items'] as $index => $list)
                  <tr class="favTr">
                    <td>{{ $index+1 }}</td>
                    <td><img class="myImg" src="{{ asset("assets/images/seller-design/".$list['design_img']) }}" alt="{{ $list['name'] }}" /></td>
                    <td><a href="{{ URL('/template/'.Crypt::encrypt($list['design_id'])) }}" >{{ $list['design_name'] }}</a> <br/><span ref="{{ $list['design_id'] }}" class="add_rating_btn">Add Rating</span></td>
                    <td>{{ Helper::numberFormat($list['price']) }}</td>
                    <td>{{ $list['qty'] }}</td>
                    <td class="text-right">{{ Helper::numberFormat($list['total_price']) }}</td>
                  </tr>
                  @endforeach
                  <tr class="noFound1">
                    <td colspan="4">
                    </td>
                    <td class="text-center">Sub Total
                    </td>
                    <td class="over_total text-right">{{ Helper::numberFormat($result->subtotal) }}
                    </td>
                  </tr>
                  <tr class="noFound1">
                    <td colspan="4">
                    </td>
                    <td class="text-center">Shipping
                    </td>
                    <td class="over_total text-right"> @if($result->shipping == 0) FREE @else {{ Helper::numberFormat($result->shipping) }} @endif
                    </td>
                  </tr>
                  <tr class="noFound1">
                    <td colspan="4">
                    </td>
                    <td class="text-center">Total Price
                    </td>
                    <td class="over_total text-right">{{ Helper::numberFormat($result->total_amount) }}
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
</section>
<div id="Add_Rating" class="hidden" style="display:none;">
  <div class="Rate">
    <h3>Rate this design</h3>
    <div class='rating-stars'>
      <ul class='stars selected'>
        <li class='star' title='Poor' data-value='1'>
          <i class='fa fa-star fa-fw'></i>
        </li>
        <li class='star' title='Fair' data-value='2'>
          <i class='fa fa-star fa-fw'></i>
        </li>
        <li class='star' title='Good' data-value='3'>
          <i class='fa fa-star fa-fw'></i>
        </li>
        <li class='star' title='Excellent' data-value='4'>
          <i class='fa fa-star fa-fw'></i>
        </li>
        <li class='star' title='WOW!!!' data-value='5'>
          <i class='fa fa-star fa-fw'></i>
        </li>
      </ul>
    </div>
    <div class="add_rating_form">
      <input name="rating" class="rating_inpt" type="hidden">
      <input name="design_id" class="design_id_inpt" type="hidden">
      <div class="form-group">
        <label for="comment">Review:</label>
        <textarea class="form-control review_input" rows="5" name="review" name="text"></textarea>
      </div>
      <button class="btn btn-primary add_rating_submit_btn">Submit</button>
    </div>
  </div>

</div>
<script type="text/javascript">
var searchUrl = url+"/search-downloads";
</script>
@endsection
