@extends('layouts.seller')
@section('content')
<section class="main_content_wrapper searchSection">
  <div class="container">
    @include('buyer.menu')
    <div class="last_latest_recent">
      <div class="panel">
        <div class="row"><div class="col-md-12" id="General_information" role="tabpanel">
          <div class="Left_panel_tab">
            @if (session('error-acst'))
            <div class="alert alert-danger">
              {{ session('error-acst') }}
            </div>
            @endif
            @if (session('success-acst'))
            <div class="alert alert-success">
              {{ session('success-acst') }}
            </div>
            @endif
            <form method="POST" action="{{ url('buyer/changeGenralInfo') }}" enctype="multipart/form-data" id="buyer-register-form">
              @csrf
              <div class="form-group">
                <div class="row">
                  <label class="col-md-4">First Name</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control" value="{{ Auth::user()->first_name}}" name="first_name" placeholder="First Name"/>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <label class="col-md-4">Last name</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control" value="{{ Auth::user()->last_name}}" name="last_name" placeholder="Last name"/>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <label class="col-md-4">E-mail</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control" value="{{ Auth::user()->email}}" readonly name="email" placeholder="Contact"/>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <label class="col-md-4">Mobile No</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control" value="{{ Auth::user()->phone}}" name="phone" placeholder="Mobile No"/>
                  </div>
                </div>
              </div>
              <div class="form-group text-right">
                <button class="btn btn-primary">Save Changes</button>
              </div>
            </div>
            <div class="Gender_info_Img_Gender">
              <div class="upload_img">
                <a href="javascript:void(0);" class="image-upload">
                  <span>
                    @if(Auth::user()->image != '')
                    <img src="{{ asset('assets/images/profile').'/'.Auth::user()->image }}" class="view-profile"/>
                    @else
                    @if(Auth::user()->gender == 'Male')
                    <img src="{{ asset('assets/images/dummy-male.png') }}" class="view-profile"/>
                    @else
                    <img src="{{ asset('assets/images/dummy-female.png') }}" class="view-profile"/>
                    @endif
                    @endif
                  </span>
                  <div class="clearfix"></div>
                  <em>change image</em>
                </a>
                <input type="file" style="display:none;" class="profile-img" name="profileimage" />
              </div>
              <div class="gender pa_left">
                <h5>Gender</h5>
                <label class="radio">Male
                  <input type="radio" @if (Auth::user()->gender == 'Male') checked="checked" @endif name="gender" value="Male">
                  <span class="checkround"></span>
                </label>
                <label class="radio">Female
                  <input type="radio" @if (Auth::user()->gender == 'Female') checked="checked" @endif name="gender" value="Female">
                  <span class="checkround"></span>
                </label>
                <label class="check" style="display:none;">Buying
                  <input type="checkbox" checked="checked" name="is_name">
                  <span class="checkmark"></span>
                </label>
              </div>
            </div>
          </form>
        </div>
        </div>
      </div>

    </div>
  </div>
</div>
</section>
@endsection
