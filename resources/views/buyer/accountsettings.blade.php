@extends('layouts.seller')
@section('content')
<section class="main_content_wrapper searchSection">
  <div class="container">
    @include('buyer.menu')
    <div class="designer_settings_section">
      <div class="row">
        <div class="col-md-12">
          <div class="designer_settings-panel">
            <ul class="nav nav-tabs" id="Account_Settings" role="tablist">
              <li class="nav-item">
                <a class="nav-link @if (!Session::has('is-setting-save')) active @endif" data-toggle="tab" href="#General_information" role="tab" aria-controls="home"><i class="fa fa-user"></i> General information</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#Login_settings" role="tab" aria-controls="profile"><i class="fa fa-sign-in"></i> Login settings</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#Notification_settings" role="tab" aria-controls="messages"><i class="fa fa-bell"></i> Notification settings</a>
              </li>
            <!--   <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#Payment_method" role="tab" aria-controls="settings"><i class="fa fa-tag"></i> Payment method</a>
              </li> -->
            </ul>

            <div class="tab-content">
             <div class="tab-pane @if (!Session::has('is-setting-save')) active @endif" id="General_information" role="tabpanel">
                <div class="Left_panel_tab">
                  @if (session('error-acst'))
                  <div class="alert alert-danger">
                    {{ session('error-acst') }}
                  </div>
                  @endif
                  @if (session('success-acst'))
                  <div class="alert alert-success">
                    {{ session('success-acst') }}
                  </div>
                  @endif
                  <form method="POST" action="{{ route('changeGenralInfoBuyer') }}" enctype="multipart/form-data" id="buyer-register-form">
                    @csrf
                    <div class="form-group">
                      <div class="row">
                        <label class="col-md-4">First Name</label>
                        <div class="col-md-8">
                          <input type="text" class="form-control" value="{{ $data[0]['first_name']}}" name="first_name" />
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <label class="col-md-4">Last name</label>
                        <div class="col-md-8">
                          <input type="text" class="form-control" value="{{ $data[0]['last_name']}}" name="last_name" />
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <label class="col-md-4">E-mail</label>
                        <div class="col-md-8">
                          <input type="text" class="form-control" value="{{ $data[0]['email']}}" readonly name="email"/>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <label class="col-md-4">Mobile No</label>
                        <div class="col-md-8">
                          <input type="text" class="form-control" placeholder="Mobile No" value="{{ $data[0]['phone']}}" name="phone"/>
                        </div>
                      </div>
                    </div>
                
                    <div class="form-group text-right">
                      <button class="btn btn-primary">Save Changes</button>
                    </div>
                  </div>
                  <div class="Gender_info_Img_Gender">
                    <div class="upload_img">
                      <a href="javascript:void(0);" class="image-upload">
                        <span>
                          @if($data[0]['image'] != '')
                          <img src="{{ asset('assets/images/profile').'/'.$data[0]['image'] }}" class="view-profile"/>
                          @else
                          @if($data[0]['gender'] == 'Male')
                          <img src="{{ asset('assets/images/dummy-male.png') }}" class="view-profile"/>
                          @else
                          <img src="{{ asset('assets/images/dummy-female.png') }}" class="view-profile"/>
                          @endif
                          @endif
                        </span>
                        <div class="clearfix"></div>
                        <em>change image</em>
                      </a>
                      <input type="file" style="display:none;" class="profile-img" name="profileimage" />
                    </div>
                    <div class="gender">
                      <h5>Gender</h5>
                      <label class="radio">Male
                        <input type="radio" @if ($data[0]['gender'] == 'Male') checked="checked" @endif name="gender" value="Male">
                        <span class="checkround"></span>
                      </label>
                      <label class="radio">Female
                        <input type="radio" @if ($data[0]['gender'] == 'Female') checked="checked" @endif name="gender" value="Female">
                        <span class="checkround"></span>
                      </label>
                      <label class="check" style="display:none;">Buying
                        <input type="checkbox" checked="checked" name="is_name">
                        <span class="checkmark"></span>
                      </label>
                    </div>
                  </div>
                </form>
              </div>              
              <div class="tab-pane" id="Login_settings" role="tabpanel">
                <div class="Left_panel_tab">
                  <div class="form-group">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="signup-form">
                          <div class="row">
                            @if (session('error-login-tab'))
                            <div class="alert alert-danger">
                              {{ session('error-login-tab') }}
                            </div>
                            @endif
                            @if (session('success-login-tab'))
                            <div class="alert alert-success">
                              {{ session('success-login-tab') }}
                            </div>
                            @endif
                            <div class="col-md-12">
                              <div class="form-group changePwdDiv">
                                <div class="row">
                                  <label class="col-md-4">Password</label>
                                  <div class="col-md-8">
                                    <input type="password" class="form-control" value="demooooo" readonly>
                                    <a href="javascript:void(0)" class="change_pwd link-text changePwdBtn">Change password</a>
                                  </div>
                                </div>
                              </div>
                              <form method="POST" style="display:none;" autocomplete="off"  action="{{ URL('/buyer/changePassword') }}" id="changePassword-form">
                                @csrf
                                <div class="form-group">
                                  <input id="current-password" autocomplete="off" type="password" class="form-control{{ $errors->has('current-password') ? ' is-invalid' : '' }}" name="password" required placeholder="Type Old Password" >
                                  @if ($errors->has('current-password'))
                                  <span class="help-block">
                                    <strong>{{ $errors->first('current-password') }}</strong>
                                  </span>
                                  @endif
                                </div>
                                <div class="form-group">
                                  <input id="new_password" type="password" class="form-control{{ $errors->has('new-password') ? ' is-invalid' : '' }}" name="new_password" required placeholder="New Password" >
                                  @if ($errors->has('new-password'))
                                  <span class="help-block">
                                    <strong>{{ $errors->first('new-password') }}</strong>
                                  </span>
                                  @endif
                                </div>
                                <div class="form-group">
                                  <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="confirm_password" required placeholder="Retype new Password" >
                                </div>
                                <div class="form-group">
                                  <button class="btn btn-primary">Save Changes</button>
                                </div>
                              </form>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="tab-pane" id="Notification_settings" role="tabpanel">
                <div class="Left_panel_tab">
                  <div class="form-group">
                    <div class="row">
                      <div class="col-md-8">
                        <h2>Notifications</h2>
                        <p>Recieve email notifications about account change, downloads...</p>
                      </div>
                      <div class="col-md-4">
                        <label class="switch">
                          <input type="checkbox">
                          <span class="slider round"></span>
                        </label>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-md-8">
                        <h2>Newsletter</h2>
                        <p>Recieve email newsletter about site improvements, discount offers...</p>
                      </div>
                      <div class="col-md-4">
                        <label class="switch">
                          <input type="checkbox" checked>
                          <span class="slider round"></span>
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="tab-pane" id="Payment_method" role="tabpanel">
                <div class="Left_panel_tab">
                  <div class="form-group">
                    <div class="row">
                      <div class="col-md-4">
                        <a href=""><img src="{{ asset('assets/images/paypal.png') }}" alt=""/></a>
                      </div>
                      <div class="col-md-5">
                        <div class="Payment_links">
                          <a href="#" class="edit"><i class="fa fa-edit"></i></a>
                          <a href="#">Change payment method</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-3">

    </div>
  </div>
</section>
@endsection
