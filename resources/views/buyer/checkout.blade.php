@extends('layouts.seller')
@section('content')
<section class="main_content_wrapper searchSection">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="title_heading text-center">
          <h2>Checkout</h2>
        </div>
      </div>
    </div>
    @if ($message = Session::get('warning'))
    <div class="alert alert-warning alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>{{ $message }}</strong>
    </div>
    @endif
    <div class="row">
      <div class="col-md-12">
        <div class="Checkout_panel">
          {!! Form::model(null,['action' => 'Buyer\CheckoutController@paypalWithPayment', 'method' => 'post', 'files' => true,'id'=>'checkout-form']) !!}
          <div class="row">
            <div class="col-md-4">
              <h3>1. Billing adress</h3>
              <div class="form-group">
                <input type="text" class="form-control" value="{{ Auth::user()->first_name }}" name="first_name" placeholder="First name"/>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" value="{{ Auth::user()->last_name }}" name="last_name" placeholder="Last name"/>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" value="{{ Auth::user()->email }}" name="email" placeholder="Email Address"/>
              </div>
              <div class="form-group">
                <input type="text" class="form-control validNumber" value="{{ Auth::user()->phone }}" name="phone" placeholder="Telephone"/>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="address" placeholder="Billing Adress"/>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="town" placeholder="Town"/>
              </div>
              <div class="form-group">
                <select class="form-control" name="state">
                  <option value="Alabama">Alabama</option>
                  <option value="Alaska">Alaska</option>
                  <option value="Arizona">Arizona</option>
                  <option value="Arkansas">Arkansas</option>
                  <option value="California">California</option>
                  <option value="Colorado">Colorado</option>
                  <option value="Connecticut">Connecticut</option>
                  <option value="Delaware">Delaware</option>
                  <option value="Florida">Florida</option>
                  <option value="Georgia">Georgia</option>
                  <option value="Hawaii">Hawaii</option>
                  <option value="Idaho">Idaho</option>
                  <option value="Illinois">Illinois</option>
                  <option value="Indiana">Indiana</option>
                  <option value="Iowa">Iowa</option>
                  <option value="Kansas">Kansas</option>
                  <option value="Kentucky">Kentucky</option>
                  <option value="Louisiana">Louisiana</option>
                  <option value="Maine">Maine</option>
                  <option value="Maryland">Maryland</option>
                  <option value="Massachusetts">Massachusetts</option>
                  <option value="Michigan">Michigan</option>
                  <option value="Minnesota">Minnesota</option>
                  <option value="Mississippi">Mississippi</option>
                  <option value="Missouri">Missouri</option>
                  <option value="Montana">Montana</option>
                  <option value="Nebraska">Nebraska</option>
                  <option value="Nevada">Nevada</option>
                  <option value="New Hampshire">New Hampshire</option>
                  <option value="New Jersey">New Jersey</option>
                  <option value="New Mexico">New Mexico</option>
                  <option value="New York">New York</option>
                  <option value="North Carolina">North Carolina</option>
                  <option value="North Dakota">North Dakota</option>
                  <option value="Ohio">Ohio</option>
                  <option value="Oklahoma">Oklahoma</option>
                  <option value="Oregon">Oregon</option>
                  <option value="Pennsylvania">Pennsylvania</option>
                  <option value="Rhode Island">Rhode Island</option>
                  <option value="South Carolina">South Carolina</option>
                  <option value="South Dakota">South Dakota</option>
                  <option value="Tennessee">Tennessee</option>
                  <option value="Texas">Texas</option>
                  <option value="Utah">Utah</option>
                  <option value="Vermont">Vermont</option>
                  <option value="Virginia">Virginia</option>
                  <option value="Washington">Washington</option>
                  <option value="West Virginia">West Virginia</option>
                  <option value="Wisconsin">Wisconsin</option>
                  <option value="Wyoming">Wyoming</option>
                </select>
              </div>
            </div>
            <div class="col-md-4">
              <div class="shipping_method">
                <h3>2. Shipping method</h3>
                <label class="radio">Express (1-3 days, tracking)  <b>5$</b>
                  <input type="radio" class="shipping_method" checked="checked" value="express" name="shipping_method">
                  <span class="checkround"></span>
                </label>
                <label class="radio">Regular (2-8 days, no tracking)  <b>FREE</b>
                  <input type="radio" class="shipping_method" value="regular" name="shipping_method">
                  <span class="checkround"></span>
                </label>
              </div>
              <div class="shipping_method">
                <h3>3. Payment method</h3>
                <!-- <div class="row">
                <div class="col-md-5">
                <label class="radio">Credit card
                <input type="radio" checked="checked" value="credit_card" name="payment_method">
                <span class="checkround"></span>
              </label>
            </div>
            <div class="col-md-7">
            <img src="{{ asset('assets/images/credit_debit.png') }}"/>
          </div>
        </div> -->
      </div>
      <!-- <div class="form-group">
      <input type="text" class="form-control" placeholder="Credit card Number"/>
    </div>
    <div class="form-group">
    <input type="text" class="form-control" placeholder="Expiration date"/>
  </div>
  <div class="form-group">
  <input type="text" class="form-control" placeholder="Card verification number"/>
</div> -->
<label class="radio"> <img src="{{ asset('assets/images/paypal.png') }}"/>
  <input type="radio" checked="checked" value="paypal" name="payment_method">
  <span class="checkround"></span>
</label>
</div>
<div class="col-md-4">
  <h3>4. Review your order</h3>
  <div class="review_Order">
    <ul>
      @php
      $total_price = 0;
      @endphp

      @if(!$result->isEmpty())
      @foreach($result as $index => $list)

      @php
      $total_price += $list->price* $list->qty;
      @endphp
      <li><span>{{ $index+1 }}. {{ $list->design->name }}</span> <b>{{ Helper::numberFormat($list->price*$list->qty) }}</b></li>

      @endforeach
      @endif

      <li><span>{{ count($result)+1 }}. Shipping</span> <b class="shipping_class">$ 5.00 </b></li>
    </ul>
    <div class="totalSpend">
      <h5>Total <b class="total_order" ref="{{ $total_price }}">{{ Helper::numberFormat($total_price+5) }}</b></h5>
      <input type="submit" class="btn btn-success" value="Place order ">
    </div>
  </div>
</div>
</div>
</form>
</div>
</div>
</div>
</div>
</section>
<script>
$(function() {
  var $radios = $('input:radio[name=shipping_method]');
  $radios.filter('[value=express]').prop('checked', true);
});
</script>

@endsection
