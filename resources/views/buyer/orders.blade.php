@extends('layouts.seller')
@section('content')
<section class="main_content_wrapper searchSection">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="title_heading text-center">
          <h2>My Orders</h2>
        </div>
      </div>
    </div>
    @if ($message = Session::get('success'))
    <div class="alert alert-success alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>{{ $message }}</strong>
    </div>
    @endif
    <div class="designer_uploads_section">
      <div class="uploads_panel">
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive" id="paginationData">
              <table class="table">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th style="width:130px;">Order Number</th>
                    <th>Transaction Amount</th>
                    <th>Order Date</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  @if(!$result->isEmpty())
                  @foreach($result as $index => $list)
                  <tr class="favTr">
                    <td>{{ $result->perPage() * ($result->currentPage() -1 ) + $index+1 }}</td>
                    <td>{{ $list->order_number }}</td>
                    <td>{{ Helper::numberFormat($list->transaction_amount) }}</td>
                    <td>{{ Helper::dateFormat($list->created_at,'d.m.Y') }}</td>
                 <td><a class="edit" href="{{ URL('/buyer/order-details/'.Crypt::encrypt($list->id)) }}"><i class="fa fa-eye"></i></a></td>
                  </tr>
                  @endforeach
                  @else
                  <tr class="noFound">
                    <td colspan="7" style="text-align: center;">
                      No Record Found
                    </td>
                  </tr>
                  @endif
                </tbody>
              </table>
            </div>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
var searchUrl = url+"/search-downloads";
</script>
@endsection
