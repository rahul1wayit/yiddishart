@extends('layouts.seller')
@section('content')
<section class="main_content_wrapper searchSection">
  <div class="container">
    @include('buyer.menu')
    <div class="designer_uploads_section">
      <div class="uploads_panel">
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th style="font-size:0;">Favorite</th>
                    <th style="width:130px;">Design</th>
                    <th style="font-size:0;width: 161px;">Name</th>
                    <th style="width:100px;">Price</th>
                    <th>Quantity</th>
                    <th style="width:200px;">Total Price</th>
                    <th style="font-size:0;">Action</th>
                  </tr>
                </thead>
                <tbody>
                  @if(!$result->isEmpty())
                  @foreach($result as $index => $list)
                  <tr class="favTr">
                    <td>{{ $result->perPage() * ($result->currentPage() -1 ) + $index+1 }}</td>
                    <td><a href="javascript:void(0)" class="favourite"><i class="fa fa-heart"></i></a></td>
                    <td><img src="{{ asset("assets/images/seller-design/".$list->designimage) }}" alt="{{ $list->name }}" /></td>
                    <td><a href="{{ URL('/template/'.Crypt::encrypt($list->id)) }}" >{{ ucfirst($list->name) }}</a></td>
                    <td class="main_price">{{ Helper::numberFormat($list->price) }}</td>
                    <td>
                      <div id="field1" class="subadd">
                        <button type="button" id="sub" class="sub subQty" ref="fav">-</button>
                        <input type="number" id="1" value="1" min="1" max="10" class="qty" onkeypress="return blockSpecialChar(event)"/>
                        <button type="button" id="add" class="add addQty" ref="fav">+</button>
                      </div>
                    </td>
                    <td class="total_price" ref="{{ $list->price }}">{{ Helper::numberFormat($list->price) }}</td>
                    <td><a class="checkout" href="{{ URL('/buyer/edit-template/'.Crypt::encrypt($list->id)) }}"><i class="fa fa-edit"></i> </a></td>
                    <!-- <td><a class="checkout" href="#"><i class="fa fa-shopping-cart"></i> Checkout</a></td> -->
                  </tr>
                  @endforeach
                  @else
                  <tr class="noFound">
                    <td colspan="7" style="text-align: center;">
                      No Record Found
                    </td>
                  </tr>
                  @endif



                </tbody>
              </table>
            </div>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
