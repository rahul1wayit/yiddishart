@extends('layouts.seller')
@section('content')
<section class="main_content_wrapper searchSection">
  <div class="edit-template-sec">
    <div class="container">
      <div class="row">
        <div class="col-sm-7">
          <div class="celebrate-img">
            <canvas id="canvas" width="570" height="150"></canvas>


            <!-- <div class="celebrate-content">
            <h1>Please Join Us To Celebrate</h1>
            <h2>Our wedding</h2>
            <h3>Hannah + Bradley</h3>
            <i class="fa fa-heart"></i>
            <span>Saturday the twenty fourth of November</span>
            <span>two thosand and fourteen</span>
            <h4>Ceremony</h4>
            <span>Four In The Afternoon</span>
            <span>Lily White Chapel</span>
            <span>Hoboken, New Jersey</span>
          </div> -->
        </div>
      </div>
      <div class="col-sm-5">
        <div class="celebrate-right-sec">
          <h1>Edit template</h1>
          <span>Click on text boxes to edit</span>
          <div class="celebrate-form-sec">
            <?php
            $canvasArr = json_decode(unserialize($result->canvasData));
            // echo '<pre>'; print_r($canvasArr); die;
            if(!empty($canvasArr))
            {
              foreach ($canvasArr as $key => $value)
              {
                $canvasArr[$key]->key = $key;
              }
              // echo '<pre>'; print_r($canvasArr); die;
              $canvasA = array_column($canvasArr, 'y');
              array_multisort($canvasA, SORT_ASC, $canvasArr);
              $i = 1;
              foreach ($canvasArr as $key => $value)
              {
                if($value->customtype == 'text')
                {
                  if($i == 1)
                  $textName = 'text';
                  else
                  $textName = 'text'.$key;
                  ?>
                  <div class="form-group">
                    <div class="row layer-trigger2">
                      <div class="col-sm-8">
                        <input type="text" maxlength="@if($value->char_limit){{ $value->char_limit }}@endif" data-name="{{ $value->name }}" data-key="{{ $value->key }}"  class="form-control edit-text-settings text_1st keyBord1" style="display:@if($value->language == 'qwerty') block @else none @endif;" name="text_1st" value="{{ $value->text }}" />
                        <input type="text" maxlength="@if($value->char_limit){{ $value->char_limit }}@endif" data-name="{{ $value->name }}" data-key="{{ $value->key }}"  class="form-control edit-text-settings text_1st keyBord2" style="display:@if($value->language == 'hebrew-qwerty') block @else none @endif;" name="text_1st" value="{{ $value->text }}" />
                        <input type="hidden" class="languageSelectInput" name="languageName" value="qwerty" />
                      </div>
                      <div class="col-sm-4">
                        <select class="form-control 1st_text_class editLanguage" name="language_1st">
                          <option @if($value->language == 'qwerty') selected @endif value="qwerty">English</option>
                          <option @if($value->language == 'hebrew-qwerty') selected @endif value="hebrew-qwerty">Hebrew</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <?php
                  $i++;
                }
                if($value->customtype == 'image' && $value->name != 'default-image' && $value->width != '1')
                {
                  ?>
                  <div class="form-group clearfix">
                    <input type="file" name="image" data-name="{{ $value->name }}" data-key="{{ $value->key }}" class="fileUp addNewImgInp1">
                    <div class="input-group col-xs-12 edit-file">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-picture"></i></span>
                      <input type="text" class="form-control upload_bx consent_dateClass" disabled value="Upload New Image">
                      <span class="input-group-btn">
                        <button class="browse btn btn-primary" type="button"><i class="fa fa-plus-square"></i> </button>
                      </span>
                    </div>
                    <span class="error-image" style="color:red; text-align:center;"></span>
                    <input type="hidden" class="imageUrl" rel="" name="imageName" />
                  </div>
                  <?php
                }
              }
            }
            ?>
          </div>
          <div class="celebrate-btn-sec">
            <a class="btn discard-btn" href="">Discard</a>
            <a class="btn save-btn add-to-cart-btn" href="javascript:void(0);" data-id="{{ Request::segment(3) }}">Save</a>
            <a class="btn save-btn add-to-cart-btn" data-download="1" href="javascript:void(0);" data-id="{{ Request::segment(3) }}">Save & Download</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</section>
<script>
var dbCanvasData = <?= json_encode(unserialize($result->canvasData)) ?>;
var json = jQuery.parseJSON(dbCanvasData);
// console.log(json);
var canvasData = [];
setTimeout(function(){
  if(json[0] && json[0]['name'] == 'default-image')
  {
    jQuery('canvas').attr('height', json[0]['height']);
    jQuery('canvas').attr('width' , json[0]['width']);
  }
  jQuery(json).each(function (i, val) {
    val.draggable = '';
    canvasData.push(val);
    setTimeout(function(){
      if(val.customtype == 'image')
      jQuery('canvas').drawImage(val);
      if(val.customtype == 'text')
      jQuery('canvas').drawText(val);
    },100);
  });
},200);

// change text on canvas
jQuery(document).on("input change", ".edit-text-settings", function(e)
{
  var ele = e.target;
  var key = jQuery(ele).attr('data-key');
  var name = jQuery(ele).attr('data-name');
  var char_limit = jQuery(ele).attr('maxlength');
  var textVal = jQuery(this);
  if(ele.name == 'text_1st')
  {
    if(char_limit > 0)
    {
      var inputString = ele.value;
      var shortenedString = inputString.substr(0,(char_limit));
      textVal.val(shortenedString);
      canvasData[key]['text'] = ele.value;
    }else{
      canvasData[key]['text'] = ele.value;
    }
  }
  jQuery('canvas').setLayer(name,canvasData[key]).drawLayers();
  // console.log('Keyup '+key+name+JSON.stringify(canvasData[key]));
});

jQuery('body').on('change', '.editLanguage', function() {
  jQuery(this).parents('.layer-trigger2').find('.languageSelectInput').val(jQuery(this).val());
  var valu12 =  jQuery(this).parents('.layer-trigger2').find('.languageSelectInput').val();
  var keys = jQuery(this).parents('.layer-trigger2').find('.edit-text-settings').attr('data-key');
  console.log(keys);
  if(valu12 == 'hebrew-qwerty')
  {
    jQuery(this).parents('.layer-trigger2').find('.keyBord2').css('display','block');
    jQuery(this).parents('.layer-trigger2').find('.keyBord1').css('display','none');
    canvasData[keys]['language'] = 'hebrew-qwerty';
  }else {
    jQuery(this).parents('.layer-trigger2').find('.keyBord1').css('display','block');
    jQuery(this).parents('.layer-trigger2').find('.keyBord2').css('display','none');
    canvasData[keys]['language'] = 'qwerty';
  }
});

jQuery(document).on("change", ".addNewImgInp1", function()
{
  jQuery(".loaderSec").css('display','block');
  readMultiImageURL1(this);
});

jQuery(document).on("click", ".add-to-cart-btn", function()
{
  var design_id = jQuery(this).attr('data-id');
  var download = jQuery(this).attr('data-download');
  if(design_id)
  {

    var img = jQuery('canvas').getCanvasImage('jpeg');
    var formData = new FormData();
    formData.append('design_id', design_id);
    formData.append('design_data', JSON.stringify(canvasData));
    formData.append('design_img', img);
    if(download == 1){
      formData.append('download', 1);
    }
    ajaxHit('add-to-cart','POST',site_url+'/buyer/add-to-cart',formData);
  }
});

function readMultiImageURL1(input)
{
  var logoAspectRatio;
  if (input.files && input.files[0])
  {
    var formData = new FormData();
    var file = input.files[0];
    var imgName = input.files[0].name;
    formData.append("Filedata", file);
    var t = file.type.split('/').pop().toLowerCase();
    if (t != "jpeg" && t != "jpg" && t != "png" && t != "bmp" && t != "gif") {
      jQuery(".error-image").html('Please select a valid image file');
      document.getElementsByClassName("profile-img").value = '';
      return false;
    }
    var reader = new FileReader();
    reader.onload = function (e) {
      var img = new Image();
      img.src = e.target.result;
      jQuery(".error-image").html('');
      img.onload = function () {
        logoAspectRatio  = (this.width / this.height).toFixed(2);
        var imagesrc = img.src;
        var key = jQuery(input).attr('data-key');
        var name = jQuery(input).attr('data-name');
        jQuery('canvas').clearCanvas();
        jQuery.getScript(site_url+'/assets/jCanvas/jcanvas.min.js');

        if(name == 'logo')
        jQuery(input).parents('.layer-trigger').append('<i class="deleteText fa fa-times"></i>');

        // upload image
        var imagesrc = img.src;
        var base64ImageContent = imagesrc.replace(/^data:image\/(png|jpg|jpeg|gif|bmp|);base64,/, "");
        var blob = base64ToBlob(base64ImageContent, 'image/png');
        var formData = new FormData();
        formData.append('picture', blob);
        formData.append('imgName', imgName);
        formData.append('width', this.width);
        formData.append('height', this.height);
        var ratioo = 200 / logoAspectRatio;

        jQuery.ajax({
          beforeSend: function() { jQuery('.loaderSec').show(); },
          complete: function() { jQuery('.loaderSec').hide() },
          url: site_url+'/buyer/uploadSingleImage',
          type: 'POST',
          dataType: 'html',
          data: formData,
          processData: false,
          contentType: false,
          headers     : {
            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
          },
          success: function (response)
          {
            jQuery(input).next().attr('rel', response);
            canvasData[key]['source'] = response;
            // canvasData[key]['x']      = 150;
            // canvasData[key]['y']      = 150;
            // canvasData[key]['width']  = 200;
            canvasData[key]['aspectRat']  = logoAspectRatio;
            // canvasData[key]['height'] = canvasData[key]['width'] / logoAspectRatio;
            // console.log(key);
            setTimeout(function(){
              jQuery('canvas').setLayer(name,canvasData[key]).drawLayers();
              jQuery(".loaderSec").css('display','none');
            },200);
            console.log('canvasData'+JSON.stringify(canvasData));
          },
          error: function (e) {
            alert('Error while request..');
          }
        });
      }
    }
    reader.readAsDataURL(input.files[0]);
  }
}
</script>
@endsection
