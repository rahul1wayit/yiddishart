<table class="table">
  <thead>
    <tr>
      <th>No.</th>
      <th style="width:130px;">Design</th>
      <th style="font-size:0;width: 161px;">Name</th>
      <th>Price</th>
      <th>Downloaded</th>
      <th style="width:200px;">Category</th>
      <th style="font-size:0;">Action</th>
    </tr>
  </thead>
  <tbody>
    @if(!$result->isEmpty())
    @foreach($result as $index => $list)
    <tr class="favTr">
      <td>{{ $result->perPage() * ($result->currentPage() -1 ) + $index+1 }}</td>
      <td><img src="{{ asset("assets/images/seller-design/".$list->designimage) }}" alt="{{ $list->name }}" /></td>
      <td><a href="{{ URL('/template/'.Crypt::encrypt($list->id)) }}" >{{ ucfirst($list->name) }}</a></td>
      <td>{{ Helper::numberFormat($list->price) }}</td>
      <td>{{ Helper::dateFormat($list->ratings->updated_at,'d.m.Y') }}</td>
      <td>{{ $list->business_category->name }}</td>
      <td><a class="edit" href="{{ URL('/buyer/edit-template/'.Crypt::encrypt($list->id)) }}"><i class="fa fa-edit"></i></a></td>
    </tr>
    @endforeach
    @else
    <tr class="noFound">
      <td colspan="7" style="text-align: center;">
        No Record Found
      </td>
    </tr>
    @endif
  </tbody>
</table>
<div class="pull-left">
  {{ $result->links('pagination.pagination_links') }}
  @if($result->count() > 10)
  <div class="per_Page">
    Entries per page <select class="entriesperpage">
      <option value="10" selected>10</option>
      <option value="15">15</option>
      <option value="20">20</option>
      <option value="100">100</option>
    </select>
  </div>
  @endif
</div>
