@extends('layouts.admin')
@section('content')
<div id="wrapper">
	<div class="sellers">
		<div class="page_title">
			<div class="row">
				<div class="col-md-6">
					<h1>{{ $title }}</h1>
				</div>
				<div class="col-md-6">
					<div class="search_box">
						<form class="searchForm" action="" method="get">
							<input type="text" name="search" class="searchBox" placeholder="Search">
						</form>
						<a href="#"><img src="{{ asset('assets/images/search_icon.png') }}"></a>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<div id="paginationData">
						<table class="table table-hover">
							<div class="alert alert-success" style="display: none;">
								<span id="successMsg"></span>
							</div>
							<thead>
								<tr>
									<th>No</th>
									<th>Pic</th>
									<th>Username</th>
									<th class="email">Email ID</th>
									<th>Design</th>
									<th class="actions">Actions</th>
								</tr>
							</thead>
							<tbody>
								@if(!$sellers->isEmpty())
								@foreach($sellers as $index => $seller)
								<tr id="removeTr<?php echo $seller->id; ?>" style="<?php if($seller->status == 0){ echo 'background-color: #f7c7c7'; } ?>">
									<td>{{ $sellers->perPage() * ($sellers->currentPage() -1 ) + $index+1 }}</td>
									<td>
										<span class="img_box">
											@if($seller->image != '')
											<img src="<?php echo asset('assets/images/profile'); ?>/{{ $seller->image }}">
											@else
											@if($seller->gender != 'Male')
											<img src="{{ asset('assets/images/dummy-female.png') }}">
											@else
											<img src="{{ asset('assets/images/dummy-male.png') }}">
											@endif
											@endif
										</span>
									</td>
									<td>
										{{ ucwords($seller->name) }}
									</td>
									<td>{{ $seller->email }}</td>
									<td><a href="{{ (count($seller->design) != 0)?$url.'/seller-design/'.encrypt($seller->id):'' }}">{{ count($seller->design) }}</a></td>
									<td class="actions">
										<a href="javascript:void(0)" ref="{{ $seller->id }}" class="buyersShowBtn" id="sellerShowBtn">View Details</a>
										<a href="javascript:void(0)" ref="{{ $seller->id }}" class="buyersEditBtn" id="sellerEditBtn"><i class="fa fa-edit"></i></a>
										<a href="javascript:void(0)" ref="{{ $seller->id }}" class="deleteBuyersBtn" user-type="seller" data-type="delete"><i class="fa fa-trash"></i></a>
									</td>
								</tr>
								@endforeach
								@else
								<tr class="noFound">
									<td colspan="9" style="text-align: center;">
										No Record Found
									</td>
								</tr>
								@endif
							</tbody>
						</table>
						<div class="pull-right">
							{{ $sellers->links('pagination.pagination_links') }}
							@if($sellers->count() > 10)
							<div class="per_Page">
								Entries per page <select class="entriesperpage">
									<option value="10" selected>10</option>
									<option value="15">15</option>
									<option value="20">20</option>
									<option value="100">100</option>
								</select>
							</div>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- show -->
	<div class="show" style="display: none;">
		<div class="page_title">
			<div class="row">
				<div class="col-md-2">
					<a class="back_to" href="javascript:void(0)" id="backToSellerShow"><< back to sellers</a>
				</div>
				<div class="col-md-10">
					<h1>Seller details</h1>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2 text-center">
				<div class="seller_img">
					<img id="seller_img" src="">
				</div>
			</div>
			<div class="col-md-6">
				<div class="alert alert-success" style="display: none;">
					<span id="successMsg"></span>
				</div>
				<div class="seller_details" id="sellers_view">
					<div class="form-group">
						<label>Name</label><span id="first_name"></span>
					</div>
					<div class="form-group">
						<label>Last name</label><span id="last_name"></span>
					</div>
					<div class="form-group">
						<label>User name</label><span id="user_name"></span>
					</div>
					<div class="form-group">
						<label>E-mail</label><span id="email"></span>
					</div>
					<div class="form-group">
						<label>Mobile No</label><span id="phone"></span>
					</div>
					<div class="form-group">
						<label>Business name</label><span id="businessName"></span>
					</div>
					<!-- <div class="form-group">
					<label>Business adress</label><span id="businessAddress"></span>
				</div> -->
				<div class="form-group">
					<label>City</label><span id="city"></span>
				</div>
				<div class="form-group">
					<label>Zipcode</label><span id="zipcode"></span>
				</div>
				<div class="form-group">
					<label>State</label><span id="state"></span>
				</div>
				<div class="form-group">
					<label>Contact no</label><span id="contactNo"></span>
				</div>
			</div>
		</div>
		<?php /* <div class="col-md-4">
			<div class="seller_details">
				<div class="form-group">
					<span><label>Active plan</label></span>
					<div class="subscribe_panel entry-level">
						<div class="plan_head">
							<img class="plan_img" src="{{ asset('assets/images/entry-level.png') }}">
							<h4 class="plan_name1">Intermediate</h4>
							<h4 class="plan_name2">Level</h4>
						</div>
						<div class="plan_content">
							<ul>
								<li><span class="plan_upload"></span><span> uploads/month</span></li>
								<li><span class="plan_storage">storage</span></li>
							</ul>
							<h3 class="plan_price">$49</h3>
						</div>
					</div>
				</div>
			</div>
		</div> */ ?>
	</div>
	<div class="Main_sold_panel last_order_panel">
		<div class="row">
			<div class="col-md-10 offset-2">
				<div class="seller_details">
					<div class="sold_seller_panel">
						<label>Last sold item</label>
						<div class="sold_img">
							<img width="100px" height="100px" class="last_order_design_img myImg" src="{{ asset('assets/images/sold_img.jpg') }}"/>
						</div>
						<div class="seller_sold_item_detail">
							<h4 class="last_order_design">Raging bull</h4>
							<label class="last_order_design_cat">Postcard</label>
							<label>Price<span class="last_order_design_price">$500</span></label>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="Revenu_panel">
		<div class="row">
			<div class="col-md-9 offset-2">
				<div class="total_revenue_boxes">
					<div class="row">
						<div class="col-md-4">
							<div class="revenue_box">
								<h4>Total sales</h4>
								<h2 class="total_sales">$4389</h2>
							</div>
						</div>
						<div class="col-md-4">
							<div class="revenue_box">
								<h4>Total designs </h4>
								<h2 class="total_design_price">$4389</h2>
							</div>
						</div>
						<div class="col-md-4">
							<div class="revenue_box Today">
								<h4>Total earnings</h4>
								<h2 class="total_sales">$39</h2>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="Rd_button text-right deactBtn">
		<a href="javascript:void(0)" class="btn btn-danger deleteBuyersBtn" user-type="seller" data-type="deactive" id="deactId">Deactivate account</a>
	</div>
</div>
<!-- show -->

<!-- edit -->
<div class="edit" style="display: none;">
	<div class="page_title">
		<div class="row">
			<div class="col-md-2">
				<a class="back_to" href="javascript:void(0)" id="backToSellerEdit"><< back to sellers</a>
			</div>
			<div class="col-md-10">
				<h1>Edit seller details</h1>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="become_seller-panel">
				<form method="POST" id="update-buyer-form" class="sellers-update" action="" enctype="multipart/form-data" novalidate="novalidate">
					<div class="alert alert-success" style="display: none;">
						<span id="successMsg"></span>
					</div>
					@method('put')
					@csrf
					<div class="row">
						<div class="col-md-3">
							<div class="upload_img">
								<a href="javascript:void(0);" class="image-upload">
									<span class="dynamic-img">
										<img src="" class="view-profile">
									</span>
									<div class="clearfix"></div>
									<i class="fa fa-plus"></i>
									<em>upload image</em>
								</a>
								<input type="file" style="display:none;" class="profile-img" name="profileimage">
							</div>
							<span class="error-image" style="color: #ff0000;"></span>
						</div>
						<div class="col-md-6" id="editDetails">
							<h5>Registration Process Update</h5>
							<div class="form-group">
								<input type="text" id="fname" class="form-control" name="first_name" value="" placeholder="First Name" autofocus="">
							</div>
							<div class="form-group">
								<input type="text" id="lname" class="form-control" name="last_name" value="" placeholder="Last Name" autofocus="">
							</div>
							<div class="form-group">
								<input type="text" id="uname" class="form-control" name="user_name" value="" placeholder="User Name" autofocus="">
							</div>
							<div class="form-group">
								<input type="email" id="bemail" class="form-control" name="email" value="" readonly placeholder="Email">
							</div>
							<div class="form-group">
								<input type="text" id="phone" class="form-control numberonly" placeholder="Mobile Number" name="phone" value="">
							</div>
							<div class="spacer15"></div>
							<h5>Business Information</h5>
							<div class="form-group">
								<input type="text" name="business_name" id="businessName" class="form-control" placeholder="Business Name" value="">
							</div>
							<div class="form-group">
								@php
								$business_categories = Helper::getTablaData('business_categories');
								@endphp
								<select class="form-control business_categories" name="cat" id="category">
									<option value="">Select Category for Business</option>
									@if(!empty($business_categories))
									@foreach($business_categories as $category)
									<option value="{{ $category->id }}">{{ $category->name }}</option>
									@endforeach
									<option value="other">Other</option>
									@endif
								</select>
							</div>
							<div class="form-group business_categories_other" style="display:none;">
								<input id="business_categories_other" type="text" class="form-control{{ $errors->has('business_categories_other') ? ' is-invalid' : '' }}" placeholder="Other category Name" name="business_categories_other" value="{{ old('business_categories_other') }}"/>
								@if ($errors->has('business_categories_other'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('business_categories_other') }}</strong>
								</span>
								@endif
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-6">
										<input type="text" id="city" class="form-control" placeholder="City" name="city" value="">
									</div>
									<div class="col-md-6">
										<input type="text" id="state" class="form-control" placeholder="State" name="state" value="">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-6">
										<input type="text" id="zipcode" class="form-control" placeholder="Zip code" name="zip" value="">
									</div>
									<div class="col-md-6">
										<input type="text" id="contactNo" class="form-control numberonly" placeholder="Contact no" name="contact" value="">
									</div>
								</div>
							</div>
							<div class="form-group">
								<textarea name="description" id="description" class="form-control" placeholder="Tell us about your business" value=""></textarea>
							</div>
						</div>
						<div class="col-md-3">
							<div class="gender" id="gender">
								<h5>Gender</h5>
								<label class="radio">Male
									<input type="radio" name="gender" id="male" value="Male">
									<span class="checkround"></span>
								</label>
								<label class="radio">Female
									<input type="radio" name="gender" id="female" value="Female">
									<span class="checkround"></span>
								</label>
								<label class="check" style="display:none;">Buying
									<input type="checkbox" checked="checked" name="is_name">
									<span class="checkmark"></span>
								</label>
							</div>
						</div>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-primary pull-right updateInfo">Update</button>
						<input type="hidden" name="hiddenId" id="hiddenId" value="">
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- edit -->

</div>
@include('includes.admin.delete-modal')
<script type="text/javascript">
var get_data_url 				= url+'/sellers/show';
var post_data_url 			= url+'/sellers';
var confirm_delete			= url+'/sellers';
var confirm_deactivate	= url+'/deactivate-seller';
var searchUrl						= url+"/search-sellers";
</script>

@endsection
