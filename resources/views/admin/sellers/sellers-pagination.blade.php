<table class="table table-hover">
	<div class="alert alert-success" style="display: none;">
		<span id="successMsg"></span>
	</div>
	<thead>
		<tr>
			<th>No</th>
			<th>Pic</th>
			<th>Username</th>
			<th class="email">Email ID</th>
			<th class="actions">Actions</th>
		</tr>
	</thead>
	<tbody>
		@if(!$sellers->isEmpty())
		@foreach($sellers as $index => $seller)
		<tr id="removeTr<?php echo $seller->id; ?>" style="<?php if($seller->status == 0){ echo 'background-color: #f7c7c7'; } ?>">
			<td>{{ $sellers->perPage() * ($sellers->currentPage() -1 ) + $index+1 }}</td>
			<td>
				<span class="img_box">
					@if($seller->image != '')
					<img src="<?php echo asset('assets/images/profile'); ?>/{{ $seller->image }}">
					@else
					@if($seller->gender != 'Male')
					<img src="{{ asset('assets/images/dummy-female.png') }}">
					@else
					<img src="{{ asset('assets/images/dummy-male.png') }}">
					@endif
					@endif
				</span>
			</td>
			<td>
				{{ ucwords($seller->user_name) }}
			</td>
			<td>{{ $seller->email }}</td>
			<td class="actions">
				<a href="javascript:void(0)" ref="{{ $seller->id }}" class="buyersShowBtn" id="sellerShowBtn">View Details</a>
				<a href="javascript:void(0)" ref="{{ $seller->id }}" class="buyersEditBtn" id="sellerEditBtn"><i class="fa fa-edit"></i></a>
				<a href="javascript:void(0)" ref="{{ $seller->id }}" class="deleteBuyersBtn" data-type="delete"><i class="fa fa-trash"></i></a>
			</td>
		</tr>
		@endforeach
		@else
		<tr class="noFound">
			<td colspan="9" style="text-align: center;">
				No Record Found
			</td>
		</tr>
		@endif
	</tbody>
</table>
<div class="pull-right">
	{{ $sellers->links('pagination.pagination_links') }}
	@if($sellers->count() > 10)
	<div class="per_Page">
		Entries per page <select class="entriesperpage">
			<option value="10" selected>10</option>
			<option value="15">15</option>
			<option value="20">20</option>
			<option value="100">100</option>
		</select>
	</div>
	@endif

</div>
