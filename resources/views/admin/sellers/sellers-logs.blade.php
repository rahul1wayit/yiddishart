@extends('layouts.admin')
@section('content')
<div id="wrapper">
	<div class="sellers">
		<div class="page_title">
			<div class="row">
				<div class="col-md-2">
					<a href="{{ $url.'/seller-design/'.Request::segment(3) }}" class="back_to"><< back to designs</a>
				</div>
				<div class="col-md-10">
					<h1>Seller Logs</h1>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="table-responsive">
						<div id="paginationData">
							<table class="table table-hover">
								<div class="alert alert-success" style="display: none;">
									<span id="successMsg"></span>
								</div>
								<thead>
									<tr>
										<th>No</th>
										<th style="width:130px;">Design</th>
										<th style="font-size:0;width: 161px;">Name</th>
										<th>Status</th>
										<th>Updated Date</th>
									</tr>
								</thead>
								<tbody>
									@php
									$stat = [
									'0' => 'Waiting for approval',
									'1' => 'Approved',
									'2' => 'Rejected',
									];
									@endphp
									@if(!$sellerLogs->isEmpty())
									@foreach($sellerLogs as $index => $list)
									<tr id="removeTr<?php echo $list->id; ?>">
										<td>{{ $sellerLogs->perPage() * ($sellerLogs->currentPage() -1 ) + $index+1 }}</td>
										<td><img class="myImg" width="80" src="{{ asset("assets/images/seller-design/".$list->log_image) }}" alt="{{ $list->name }}" /></td>
										<td></td>
										<td style="color: @if($list->status == '2' || $list->status == '0') red; @else green; @endif">{{ $stat[$list->status] }}</td>
										<td>{{ Helper::dateFormat($list->created_at,"d.m.Y H:i:s") }}</td>
									</tr>
									@endforeach
									@endif
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('includes.admin.delete-modal')
	@endsection
