@extends('layouts.admin')
@section('content')
	<div id="wrapper">
		<div class="page_title">
			<div class="row">
            	<div class="col-md-2">
					<a class="back_to" href="{{ $url.'/sellers' }}">< back to sellers</a>
				</div>
				<div class="col-md-10">
					<h1>Edit seller details</h1>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2 text-center">
            	<div class="seller_img">
	            	<img src="{{ asset('assets/images/img1.png') }}"/>
                </div>
            </div>
            <div class="col-md-6">
            	<div class="seller_details">
                    <div class="form-group">
                        <span><label>Name</label>Kate</span>
                    </div>
                    <div class="form-group">
                        <span><label>Last name</label>Upton</span>
                    </div>
                    <div class="form-group">
                        <span><label>E-mail</label>Kate</span>
                    </div>
                    <div class="form-group">
                        <span><label>Mobile No</label>123 456 789</span>
                    </div>
                    <div class="form-group">
                        <span><label>Business name</label>Kate design company</span>
                    </div>
                    <div class="form-group">
                        <span><label>Business adress</label>C204</span>
                    </div>
                    <div class="form-group">
                        <span><label>City</label>New York</span>
                    </div>
                    <div class="form-group">
                        <span><label>Zipcode</label>New York</span>
                    </div>
                    <div class="form-group">
                        <span><label>State</label>12345</span>
                    </div>
                    <div class="form-group">
                        <span><label>Contact no</label>123 456 789</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
            	<div class="seller_details">
                	<div class="form-group">
                    	<span><label>Active plan</label></span>
                        <div class="subscribe_panel entry-level">
                          <div class="plan_head">
                            <img src="{{ asset('assets/images/entry-level.png') }}">
                            <h4>Entry <br>Level</h4>
                          </div>
                          <div class="plan_content">
                            <ul>
                              <li>50<span>uploads/month</span></li>
                              <li>100MB<span>storage</span></li>
                            </ul>
                            <h3>$49</h3>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
        <div class="Main_sold_panel">
            <div class="row">
                <div class="col-md-10 offset-2">
                    <div class="seller_details">
                        <div class="sold_seller_panel">
                            <label>Last sold item</label>
                            <div class="sold_img">
                                <img src="{{ asset('assets/images/sold_img.jpg') }}"/>
                            </div>
                            <div class="seller_sold_item_detail">
                                <h4>Raging bull</h4>
                                <label>Postcard</label>
                                <label>Price<span>$500</span></label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="Rd_button text-right">
        	<a href="#" class="btn btn-danger">Deactivate account</a>
        </div>
	</div>
@endsection
