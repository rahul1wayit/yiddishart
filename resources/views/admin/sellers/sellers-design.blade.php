@extends('layouts.admin')
@section('content')
<div id="wrapper">
	<div class="sellers">
		<div class="page_title">
			<div class="row">
				<div class="col-md-2">
					<a href="{{ $url.'/sellers' }}" class="back_to"><< back to sellers</a>
				</div>
				<div class="col-md-10">
					<h1>Seller Designs</h1>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="table-responsive">
						<div id="paginationData">
							<table class="table table-hover">
								<div class="alert alert-success" style="display: none;">
									<span id="successMsg"></span>
								</div>
								<thead>
									<tr>
										<th>No</th>
										<th style="width:130px;">Design</th>
										<th style="font-size:0;width: 161px;">Name</th>
										<th style="min-width:100px;">Price</th>
										<th>Uploaded</th>
										<th style="width:200px;">Category</th>
										<th style="min-width:120px;">Status</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									@if(!$design->isEmpty())
									@foreach($design as $index => $list)
									<tr id="removeTr<?php echo $list->id; ?>">
										<td>{{ $design->perPage() * ($design->currentPage() -1 ) + $index+1 }}</td>
										<td><img class="myImg" width="80" src="{{ asset("assets/images/seller-design/".$list->designimage) }}" alt="{{ $list->name }}" /></td>
										<td>{{ ucfirst($list->name) }}</td>
										<td>{{ Helper::numberFormat($list->price) }}</td>
										<td>{{ Helper::dateFormat($list->created_at,"d.m.Y") }}</td>
										<td>{{ $list->business_category->name }}</td>
										<td>
											@if( $list->status == 1)
											<a href="javascript:void(0);" class="btn btn-success comfirm_status_btn btnStatus{{ $list->id }}" data-type="category" data-status="2" data-id="{{ $list->id }}" data-status="{{ $list->status }}">Approved</a>
											@elseif( $list->status == 2)
											<a href="javascript:void(0);" class="btn btn-danger comfirm_status_btn btnStatus{{ $list->id }}" data-type="category" data-status="1" data-id="{{ $list->id }}" data-status="{{ $list->status }}">Rejected</a>
											@else
											<a href="javascript:void(0);" class="btn btn-danger comfirm_status_btn btnStatus{{ $list->id }}" data-type="category" data-status="1" data-id="{{ $list->id }}" data-status="{{ $list->status }}">Waiting for approval</a>
											@endif
										</td>
										<td>
											<a class="btn btn-secondary" href="{{ $url.'/seller-logs/'.encrypt($list->added_by).'/'.encrypt($list->id) }}">Logs</a>
										</td>
									</tr>
									@endforeach
									@else
									<tr class="noFound">
										<td colspan="9" style="text-align: center;">
											No Record Found
										</td>
									</tr>
									@endif
								</tbody>
							</table>
							<div class="pull-right">
								{{ $design->links('pagination.pagination_links') }}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
@include('includes.admin.delete-modal')
<script type="text/javascript">
var confirm_status	= url+'/change-status';
</script>
@endsection
