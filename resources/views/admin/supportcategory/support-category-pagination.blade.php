<table class="table table-hover">
	<div class="alert alert-success" style="display: none;">
		<span id="successMsg"></span>
	</div>
	<thead>
		<tr>
			<th>No</th>
			<th class="">Category Name</th>
			<th class="">Status</th>
			<th class="actions">Actions</th>
		</tr>
	</thead>
	@if(!$getSupportCat->isEmpty())
		@foreach($getSupportCat as $index => $list)
			<tr class="favTr">
			  <td>{{ $getSupportCat->perPage() * ($getSupportCat->currentPage() -1 ) + $index+1 }}</td>
			  <td>{{ $list->name }}</td>
			  <td>
					@if($list->status=="1")
						Active
					@else
						Inactive
					@endif
			  </td>
			  <td class="actions">
				<!-- <a href="{{ URL('/admin/faq/'.Crypt::encrypt($list->id)) }}">view details</a> -->
				<a href="javascript:void(0)" ref="{{ Crypt::encrypt($list->id) }}" class="support-category-modal" ><i class="fa fa-edit"></i></a>
				<a href="javascript:void(0)" ref="{{ Crypt::encrypt($list->id) }}" class="deleteBuyersBtn" user-type="faq" data-type="delete"><i class="fa fa-trash"></i></a>
			  </td>
			</tr>
		@endforeach
	@else
	<tr class="noFound">
	  <td colspan="4" style="text-align: center;">
		No Record Found
	  </td>
	</tr>
	@endif
</table>
<div class="pull-right">
	{{ $getSupportCat->links('pagination.pagination_links') }}
	<div class="per_Page">
		Entries per page <select class="entriesperpage">
			<option value="10" selected="selected">10</option>
			<option value="15">15</option>
			<option value="20">20</option>
			<option value="100">100</option>
		</select>
	</div>
</div>
