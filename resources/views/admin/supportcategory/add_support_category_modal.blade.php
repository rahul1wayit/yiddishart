{{ Form::model($result, array('action' => 'Admin\SupportCategoryController@store', 'id' => 'supportCatFormModal')) }}
    <div class="form-group">
        <label>Category Name</label>
        {{ Form::text('name', null, array("class" => "form-control", "placeholder" => "Enter Category Name")) }}
    </div>
    <div class="form-group">
        <label>Category Description</label>
        {{ Form::text('description', null, array("class" => "form-control", "placeholder" => "Enter Description")) }}
    </div>    
    <div class="checkbox">
        <?php
           $status = true;
           if(isset($result->status)) {
               if($result->status==1)
                    $status = true;
               else
                    $status = false;
           }
         ?>
        <label>{{ Form::checkbox('status', '1', $status) }} Status</label>
    </div>
    <div class="form-group text-right">
        <?php
           $catId = null;
           if(isset($result->id)) {
               $catId = Crypt::encrypt($result->id);
           }
         ?>
        {{ Form::hidden('id', $catId) }}
        {{ Form::submit('Save', array('class' => 'btn btn-primary button-disabled')) }}
    </div>
{{ Form::close() }}
