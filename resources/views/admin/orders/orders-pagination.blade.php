<table class="table table-hover">
  <thead>
    <tr>
      <th>No.</th>
      <th style="width:130px;">Order Number</th>
      <th>Transaction Amount</th>
      <th>Order Date</th>
      <th>Actions</th>
      </tr>
    </thead>
    <tbody>
      @if(!$result->isEmpty())
      @foreach($result as $index => $list)
      <tr class="favTr">
        <td>{{ $result->perPage() * ($result->currentPage() -1 ) + $index+1 }}</td>
        <td>{{ $list->order_number }}</td>
        <td>{{ Helper::numberFormat($list->transaction_amount) }}</td>
        <td>{{ Helper::dateFormat($list->created_at,'d.m.Y') }}</td>
        <td class="actions">
          <a href="{{ URL('/admin/order-details/'.Crypt::encrypt($list->id)) }}">view details</a>
        </td>
      </tr>
      @endforeach
      @else
      <tr class="noFound">
        <td colspan="7" style="text-align: center;">
          No Record Found
        </td>
      </tr>
      @endif
    </tbody>
  </table>
  <div class="pull-right">
    {{ $result->links('pagination.pagination_links') }}
    @if($result->count() > 10)
    <div class="per_Page">
      Entries per page <select class="entriesperpage">
        <option value="10" selected>10</option>
        <option value="15">15</option>
        <option value="20">20</option>
        <option value="100">100</option>
      </select>
    </div>
    @endif
  </div>
