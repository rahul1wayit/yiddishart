@extends('layouts.admin')
@section('content')
<div id="wrapper">
	<div class="buyers" id="faq_panel">
		<div class="page_title">
			<div class="row">
				<div class="col-md-3">
					<a class="back_to" href="{{url($url.'/faq')}}" id="backToBuyersShow">&lt;&lt; back to FAQ</a>
				</div>
			</div>
			<div class="row">
				<div class="col-md-9">
					<h1>{{ $title }} <span id="successMsgs"></span> </h1>
				</div>
			</div>
		</div> 
		<div class="row">
			<div class="col-md-12">
				<div class="qst">
					<h4><span>Q. </span>{{ ucfirst($result->question) }}</h4>
		
				<div class="ans"><span>A.</span>
					<p>{!! $result->answer !!}</p>
				</div></div>
				
			</div>
		</div>
	</div>
</div>
@endsection
