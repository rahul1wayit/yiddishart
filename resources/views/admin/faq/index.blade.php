@extends('layouts.admin')
@section('content')
<div id="wrapper">
	<div class="buyers">
		<div class="page_title">
			<div class="row">
				<div class="col-md-9">
					<h1>{{ $title }} <span id="successMsgs"></span> </h1>
				</div>
				<div class="col-md-3 text-right">
				<!-- <a class="New_plan" href="http://localhost/yiddishart/admin/new-subscription">New Plan</a> -->
				<a href="javascript:void(0);" class="New_plan text-white edit-faq-modal" ref="{{ Crypt::encrypt('add') }}">Add FAQ</a>
			</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<div id="paginationData">
						<table class="table table-hover">
							<div class="alert alert-success" style="display: none;">
								<span id="successMsg"></span>
							</div>
							<thead>
								<tr>
									<th>No</th>
									<th class="">Questions</th>
									<th class="">Answers</th>
									<th class="">Status</th>
									<th class="actions">Actions</th>
								</tr>
							</thead>
							@if(!$getQues->isEmpty())
				                @foreach($getQues as $index => $list)
					                <tr class="favTr">
					                  <td>{{ $getQues->perPage() * ($getQues->currentPage() -1 ) + $index+1 }}</td>
					                  <td>{{ ucfirst($list->question) }}</td>
					                  <td>{{ str_limit(strip_tags(($list->answer)), $limit = 50, $end = '...') }}</td>
									  <td>
										  	@if($list->status=="1")
										  		Active
										  	@else
												Inactive
								   			@endif
									  </td>
					                  <td class="actions">
					                    <a href="{{ URL('/admin/faq/'.Crypt::encrypt($list->id)) }}">view details</a>
										<a href="javascript:void(0)" ref="{{ Crypt::encrypt($list->id) }}" class="edit-faq-modal" ><i class="fa fa-edit"></i></a>
										<a href="javascript:void(0)" ref="{{ Crypt::encrypt($list->id) }}" class="deleteBuyersBtn" user-type="faq" data-type="delete"><i class="fa fa-trash"></i></a>
					                  </td>
					                </tr>
				                @endforeach
			                @else
			                <tr class="noFound">
			                  <td colspan="4" style="text-align: center;">
			                    No Record Found
			                  </td>
			                </tr>
			                @endif
						</table>
						<div class="pull-right">
							{{ $getQues->links('pagination.pagination_links') }}
							@if($getQues->count() >= 10)
							<div class="per_Page">
								Entries per page <select class="entriesperpage">
									<option value="10" selected="selected">10</option>
									<option value="15">15</option>
									<option value="20">20</option>
									<option value="100">100</option>
								</select>
							</div>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@include('includes.admin.delete-modal')
<script type="text/javascript" src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>
<div id="Add_FAQ" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">FAQ</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="faq-modal-body">
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
	var get_data_url 		= url+'/faq/show';
	var post_data_url 		= url+'/faq';
	var confirm_delete		= url+'/faq';
	var confirm_deactivate	= url+'/deactivate-buyer';
	var searchUrl			= url+"/faq";
	var entriesperpage		= url+"/entriesperpage";
</script>

@endsection
