{{ Form::open(array('action' => 'Admin\FaqController@store', 'id' => 'faqFormModal')) }}
    <div class="form-group">
        <label>Question</label>
        <input type="text" class="form-control" name="question" placeholder="Enter your question here...">
    </div>
    <div class="form-group">
        <label>Answer</label>
        <br>
        <textarea class="form-control" name="answer" id="answer"></textarea>
    </div>
    <div class="checkbox">
        <label>{{ Form::checkbox('status', '1', true) }} Status</label>
    </div>
    <div class="form-group text-right">
        {{ Form::submit('Save', array('class' => 'btn btn-primary button-disabled')) }}
    </div>
{{ Form::close() }}
<script type="text/javascript">
CKEDITOR.replace( 'answer' );
CKEDITOR.instances['answer'].on('key', function () {
    var self = this;
    setTimeout(function() {
        var ckValue = GetTextFromHtml(self.getData()).replace(/<[^>]*>/gi, '').trim();
        if (ckValue.length === 0) {
            $("#answer-error").show();
        } else {
            $("#answer-error").hide();
        }
    }, 10);
});
</script>
