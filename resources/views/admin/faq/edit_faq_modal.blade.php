<!-- Modal -->
{{ Form::open(array('action' => 'Admin\FaqController@store', 'id' => 'faqFormModal')) }}
    <div class="form-group">
        <label>Question</label>
        <input type="text" class="form-control" name="question" placeholder="Enter your question here..." value="{{ $result->question }}">
    </div>
    <div class="form-group">
        <label>Answer</label>
        <br>
        <textarea class="form-control" name="answer" id="answer">{{ $result->answer }}</textarea>
    </div>
    <div class="checkbox">
        <?php
        $checkBox = false;
        if($result->status==1)
            $checkBox = true;
        ?>
        <label>{{ Form::checkbox('status', '1', $checkBox) }} Status</label>
    </div>
    <div class="form-group text-right">
        <input type="hidden" name="faqid" value="{{ Crypt::encrypt($result->id) }}">
        {{ Form::submit('Save', array('class' => 'btn btn-primary button-disabled')) }}
    </div>
{{ Form::close() }}

<script type="text/javascript">
CKEDITOR.replace( 'answer' );
CKEDITOR.instances['answer'].on('key', function () {
    var self = this;
    setTimeout(function() {
        var ckValue = GetTextFromHtml(self.getData()).replace(/<[^>]*>/gi, '').trim();
        if (ckValue.length === 0) {
            $("#answer-error").show();
        } else {
            $("#answer-error").hide();
        }
    }, 10);
});
</script>
