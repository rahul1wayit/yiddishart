<table class="table table-hover">
	<div class="alert alert-success" style="display: none;">
		<span id="successMsg"></span>
	</div>
	<thead>
		<tr>
			<th>No</th>
			<th class="">Questions</th>
			<th class="">Answers</th>
			<th class="">Status</th>
			<th class="actions">Actions</th>
		</tr>
	</thead>
	@if(!$getQues->isEmpty())
        @foreach($getQues as $index => $list)
            <tr class="favTr">
              <td>{{ $getQues->perPage() * ($getQues->currentPage() -1 ) + $index+1 }}</td>
              <td>{{ $list->question }}</td>
              <td>{{ str_limit(strip_tags($list->answer), $limit = 50, $end = '...') }}</td>
			  <td>
				  	@if($list->status=="1")
				  		Active
				  	@else
						Inactive
		   			@endif
			  </td>
              <td class="actions">
				  <a href="{{ URL('/admin/faq/'.Crypt::encrypt($list->id)) }}">view details</a>
  				<a href="javascript:void(0)" ref="{{ Crypt::encrypt($list->id) }}" class="edit-faq-modal" ><i class="fa fa-edit"></i></a>
  				<a href="javascript:void(0)" ref="{{ Crypt::encrypt($list->id) }}" class="deleteBuyersBtn" user-type="faq" data-type="delete"><i class="fa fa-trash"></i></a>
              </td>
            </tr>
        @endforeach
    @else
    <tr class="noFound">
      <td colspan="4" style="text-align: center;">
        No Record Found
      </td>
    </tr>
    @endif
</table>
<div class="pull-right">
	{{ $getQues->links('pagination.pagination_links') }}
	<div class="per_Page">
		Entries per page <select class="entriesperpage">
			<option value="10" selected="selected">10</option>
			<option value="15">15</option>
			<option value="20">20</option>
			<option value="100">100</option>
		</select>
	</div>
</div>
