<div id="paginationData">
	<table class="table table-hover">
		<thead>
			<tr>
				<th>No</th>
				<th></th>
				<th>Name</th>
				<th>Date</th>
				<th class="actions">Actions</th>
			</tr>
		</thead>
		<tbody>
			@if(!$events->isEmpty())
			@foreach($events as $index => $event)
			<tr class="userId<?php echo $event->id; ?>">
				<td>{{ $events->perPage() * ($events->currentPage() -1 ) + $index+1 }}</td>
				<td>
					<span class="img_box">
						@if($event->image != '')
						<img class="myImg" width="80" src="<?php echo asset('assets/images/'); ?>/{{ $event->image }}">
						@else
						<img class="myImg" width="80" src="{{ asset('assets/images/category_default.png') }}">
						@endif
					</span>
				</td>
				<td>{{ $event->name }}</td>
				<td>{{ Helper::dateFormat($event->date,'d.m.Y') }}</td>
				<td class="actions">
					<a href="javascript:void(0)" data-user="{{ $event->id }}" data-type="events" data-id="{{ $event->id }}" class="ajax_update_btn" id="sellerEditBtn"><i class="fa fa-edit"></i></a>
					<a href="javascript:void(0)" data-id="{{ $event->id }}" class="ajax_delete_btn" user-type="seller" data-type="delete"><i class="fa fa-trash"></i></a>
				</td>
			</tr>
			@endforeach
			@else
			<tr class="noFound">
				<td colspan="5" style="text-align: center;">
					No Record Found
				</td>
			</tr>
			@endif
		</tbody>
	</table>
	<div class="pull-right">
		{{ $events->links('pagination.pagination_links') }}
	</div>
</div>
</div>
