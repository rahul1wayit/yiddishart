@extends('layouts.admin')
@section('content')
<div id="wrapper" class="editShow">
	<div class="events">
		<div class="page_title">
			<div class="row">
				<div class="col-md-9">
					<h1>{{ $title }}</h1>
				</div>
				<div class="col-md-3 text-right">
					<a class="New_plan" href="{{ route('events.create') }}">Add Event</a>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<div id="paginationData">
						<table class="table table-hover">
							<thead>
								<tr>
									<th>No</th>
									<th></th>
									<th>Name</th>
									<th>Date</th>
									<th class="actions">Actions</th>
								</tr>
							</thead>
							<tbody>
								@if(!$events->isEmpty())
								@foreach($events as $index => $event)
								<tr class="userId<?php echo $event->id; ?>">
									<td>{{ $events->perPage() * ($events->currentPage() -1 ) + $index+1 }}</td>
									<td>
										<span class="img_box">
											@if($event->image != '')
											<img class="myImg" width="80" src="<?php echo asset('assets/images/event/'); ?>/{{ $event->image }}">
											@else
											<img class="myImg" width="80" src="{{ asset('assets/images/category_default.png') }}">
											@endif
										</span>
									</td>
									<td>{{ ucfirst($event->name) }}</td>
									<td>{{ Helper::dateFormat($event->date,'d.m.Y') }}</td>
									<td class="actions">
										<a href="javascript:void(0)" data-user="{{ $event->id }}" data-type="events" data-id="{{ $event->id }}" class="edit-event-plan" id="sellerEditBtn"><i class="fa fa-edit"></i></a>
										<a href="javascript:void(0)" data-id="{{ $event->id }}" class="ajax_delete_btn" user-type="seller" data-type="delete"><i class="fa fa-trash"></i></a>
									</td>
								</tr>
								@endforeach
								@else
								<tr class="noFound">
									<td colspan="5" style="text-align: center;">
										No Record Found
									</td>
								</tr>
								@endif
							</tbody>
						</table>
						<div class="pull-right">
							{{ $events->links('pagination.pagination_links') }}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- <div class="modal" id="edit-model">
<div class="modal-dialog modal-lg">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title">
<span class="header-text modelTitle"></span>
</h4>
<button type="button" class="close" data-dismiss="modal">&times;</button>
</div>
<div class="modal-body ajax-update-view">
</div>
</div>
</div>
</div> -->

<div class="modal designModal" id="confirm_delete_user" data-id="">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">
					<span class="header-text commonDeleteMsg">   </span> 	</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<span class="body-text deleteText">Are you sure you want to delete this <span class="sellerText">plan</span>?</span>
					</div>
					@csrf
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
						<button type="button" class="btn btn-primary confirm_delete_user" id="yes">Yes</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
	var main_url = url+'/events/'
	var formaction = 'post';
	var header_name = 'Event';
</script>
@endsection
