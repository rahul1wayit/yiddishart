@extends('layouts.admin')
@section('content')
<div id="wrapper">
	<div class="page_title">
		<div class="row">
			<div class="col-md-2">
				<a class="back_to" href="{{ $url.'/events' }}"><< Back to {{ $title }}</a>
			</div>
			<div class="col-md-10">
				<h1>Add Event</h1>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xl-6 col-md-10 col-12 offset-md-2">
			<div class="seller_details Subscriptions_Plans">
				{!! Form::model($data,['action' => 'Admin\EventController@store', 'method' => 'post', 'files' => true,'id'=>'event-form']) !!}
				<div class="form-group">
					<div class="row">
						<div class="col-md-3">
							<label>Event name*</label>
						</div>
						<div class="col-md-9">
							{{ Form::text('name', null, array_merge(['class' => 'form-control', 'id' => 'name', 'placeholder' => 'Enter event name'])) }}
							@if ($errors->has('name'))
							<label class="error">{{ $errors->first('name') }}</label>
							@endif
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-3">
							<label>Event Date*</label>
						</div>
						<div class="col-md-9">
							<div class="row">
								<div class="col-md-8">
									{{ Form::text('date', null, array_merge(['class' => 'form-control validNumber datepicker','readonly'=>'true','id' => 'datepicker', 'placeholder' => 'Select date'])) }}
								</div>
								@if ($errors->has('date'))
								<label class="error">{{ $errors->first('date') }}</label>
								@endif
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-3">
							<label>Description</label>
						</div>
						<div class="col-md-9">
							<div class="row">
								<div class="col-md-12">
									{{ Form::textarea('description', null, array_merge(['class' => 'form-control', 'id' => 'description', 'placeholder' => 'Add description'])) }}
								</div>
								@if ($errors->has('description'))
								<label class="error">{{ $errors->first('description') }}</label>
								@endif
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-3">
							<label>Image</label>
						</div>
						<div class="col-md-9">
							<div class="row">
								<div class="col-md-8">
									{{ Form::file('image', array_merge(['class' => 'custom-file-input', 'id' => 'event_image'])) }}
								</div>
								@if ($errors->has('image'))
								<label class="error">{{ $errors->first('image') }}</label>
								@endif
							</div>
						</div>
					</div>
				</div>
				<div class="form-group" id="Subscription_btns">
					<div class="row">
						<div class="col-md-6">
							{!! Form::hidden('id', $id) !!}
							{{ Form::submit('Add', array_merge(['class' => 'btn btn-primary', 'id' => 'submit'])) }}
						</div>
						<div class="col-md-6 text-right">
							<a href="{{ $url.'/events' }}" id="dissmiss" class="btn btn-danger">Dismiss</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
