@extends('layouts.admin')
@section('content')
	<div id="wrapper">
    	<div class="dashboard_slots">
            <div class="row">
                 <div class="col-md-4">
									<a href="<?php echo $url.'/sellers'; ?>">
                 	<div class="record_box seller_bx">
                    	<div class="row">
                        	<div class="col-md-6 col-6">
                            	<h4>Sellers</h4>
                                <h3>{{ $userCount['seller'] }}</h3>
                            </div>
                            <div class="col-md-6 col-6 text-center">
                            	<img src="{{ asset('assets/images/seller_img.png') }}"/>
                            </div>
                        </div>
                    </div>
									</a>
                 </div>
                 <div class="col-md-4">
								<a href="<?php echo $url.'/buyers'; ?>">
                 	<div class="record_box user_bx">
                    	<div class="row">
                        	<div class="col-md-6 col-6">
                            	<h4>Users</h4>
                                <h3>{{ $userCount['buyer'] }}</h3>
                            </div>
                            <div class="col-md-6 col-6 text-center">
                            	<img src="{{ asset('assets/images/Users_img.png') }}"/>
                            </div>
                        </div>
                    </div>
									</a>
                 </div>
                 <div class="col-md-4">
									 <a href="<?php echo $url.'/orders'; ?>">
                 	<div class="record_box orders_bx">
                    	<div class="row">
                        	<div class="col-md-6 col-6">
                            	<h4>Orders</h4>
                                <h3>{{ $userCount['orders'] }}</h3>
                            </div>
                            <div class="col-md-6 col-6 text-center">
                            	<img src="{{ asset('assets/images/Orders_img.png') }}"/>
                            </div>
                        </div>
                    </div>
									</a>
                 </div>
            </div>
            <div class="row">
            	<div class="col-md-12">
                	<div class="graph_panel">
                    	<div class="row">
            				<div class="col-md-6">
                            	<div class="Graph">
                                	<div class="graph_img_panel">
	                                	<img src="{{ asset('assets/images/graph1.png') }}"/>
                                    </div>
                                    <div class="graph_img_text">
	                                	<h4>Users activity</h4>
                                        <ul>
                                        	<li><span class="seller_color">&nbsp;</span>Sellers</li>
                                            <li><span class="users_color">&nbsp;</span>Users</li>
                                            <li><span class="admin_color">&nbsp;</span>Admin login</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                            	<div class="Graph">
                                	<div class="graph_img_panel text-center">
	                                	<img src="{{ asset('assets/images/graph2.png') }}"/>
                                    </div>
                                    <div class="graph_img_text">
	                                	<h4>Seller transactions</h4>
                                        <h5>change in % this month</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>
@endsection
