@extends('layouts.admin')
@section('content')
<div id="wrapper">
	<div class="page_title">
		<div class="row">
			<div class="col-md-12">
				<h1>Settings</h1>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="setting_panel">
				<div class="form-group">
					<label>Username</label><span>{{ Auth::user()->name }}</span>
				</div>
				<div class="form-group">
					<label>Role</label><span>Master admin </span>
				</div>
				<div class="form-group">
					<label>Password</label><span><input value="54564565" readonly type="password"/>
						<a id="change-admin-password" href="javascript:void(0)">Change password</a></span>
					</div>
					<div class="form-group">
						<label>Profile image</label>
						<span>
							<span class="Profile_image_box">
								<img src="{{ asset('assets/images/profile/'.Auth::user()->image) }}"/>
							</span>
							<a id="changeAdminImage" href="javascript:void(0)">Change profile image</a>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal designModal" id="changeAdminImageModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">
						<span class="header-text deleteHead"> Change <span class="sellerHeadName">Image</span> </span>
					</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<form method="post" enctype="multipart/form-data" action="{{ $url.'/updateimage' }}" class="col-lg-12 changeImageForm">
						<!-- <form method="post" name="single_upload_form" id="single_upload_form" enctype="multipart/form-data" action=" singleSalonUploadImage"> -->
						<input type="hidden" name="image_form_submit" value="1"/>
						<label>Select Image</label>
						<span class="clearfix"></span>
						<span id="singleImageCheck"></span>
						<span class="add-multiple-images">
							<span class="upload-img btn btn-file singleSalon" id="mainImagesnew">
								<!-- <span class="fileupload-exists">Upload Image</span> -->
								<input type="button" name="mainImages" id="mainImages1" accept="image/x-png, image/gif, image/jpeg" value="Upload Image">
							</span>
						</span>
						<div id="images_preview">
						</div>
						<!-- </form> -->
						<label class="error" style="display:none;" id="user_photo-error">Please Select Image</label>
						<label class="error" style="display:none;" id="user_photo-valierror">Invalid File Upload</label>

						@csrf
						<div class="modal-footer">
							{{Form::submit('Update', ['class' => 'btn btn-primary updateImage','disabled'=>'disabled'])}}
						</div>
						{{Form::close()}}
					</div>
				</div>
			</div>
		</div>
		<div class="modal designModal" id="changeAdminPasswordModal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">
							<span class="header-text deleteHead"> Change <span class="sellerHeadName">Password</span> </span>
						</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<div class="modal-body">
						<form method="post" action="{{ $url.'/postCredentials' }}" class="col-lg-12" id="changePassword-form">

							<div class="form-group">
								<input type="password" name="password" id="password" class="form-control" value="" placeholder="Old Password">
							</div>
							<div class="form-group">
								<input type="password" name="new_password" id="new_password" class="form-control" value="" placeholder="New Password">
							</div>
							<div class="form-group">
								<input type="password" name="confirm_password" id="confirm_password" class="form-control" value="" placeholder="Confirm Password">
							</div>
							@csrf
							<div class="modal-footer">
								<button type="submit" class="btn btn-primary">Update</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

		<div class="imagesNew" style="display:none;">
			@include('imgedit2')
		</div>
		@endsection
