@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Buyers</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <pre>
                        <?php // print_r($getAllBuyers); ?>
                    </pre>
                     <table class="table table-bordered">
                        <tbody>
                            @foreach ($getAllBuyers as $getBuyer)
                                <tr>
                                    <td>{{ $getBuyer->name }}</td>
                                    <td>{{ $getBuyer->email }}</td>
                                    <td><img src="{{ url('/assets/images').'/'.$getBuyer->image }}" alt="{{ $getBuyer->name }}" class="img-thumbnail" style="width: 80px;height: auto;" /></td>
                                    <td>
                                        <a href="{{ route('subscription_plan.edit', Crypt::encryptString($getBuyer->id)) }}">Edit</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                      </table>
                      {{ $getAllBuyers->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
