<table class="table table-hover">
	<div class="alert alert-success" style="display: none;">
		<span id="successMsg"></span>
	</div>
	<thead>
		<tr>
			<th>No</th>
			<th>Username</th>
			<th class="email">Email ID</th>
			<th class="actions">Actions</th>
		</tr>
	</thead>
	<tbody>
		@if(!$buyers->isEmpty())
		<?php $i = 1; ?>
		@foreach($buyers as $index => $buyer)
		<tr id="removeTr<?php echo $buyer->id; ?>" style="<?php if($buyer->status == 0){ echo 'background-color: #f7c7c7'; } ?>">
			<td>{{ $buyers->perPage() * ($buyers->currentPage() -1 ) + $index+1 }}</td>
			<td>{{ $buyer->user_name }}</td>
			<td>{{ $buyer->email }}</td>
			<td class="actions">
				<a href="javascript:void(0)" ref="{{ $buyer->id }}" class="buyersShowBtn" id="buyersShowBtn">View Details</a>
				<a href="javascript:void(0)" ref="{{ $buyer->id }}" class="buyersEditBtn" id="buyersEditBtn"><i class="fa fa-edit"></i></a>
				<a href="javascript:void(0)" ref="{{ $buyer->id }}" class="deleteBuyersBtn" data-type="delete"><i class="fa fa-trash"></i></a>
			</td>
		</tr>
		@endforeach
		@else
		<tr class="noFound">
			<td colspan="9" style="text-align: center;">
				No Record Found
			</td>
		</tr>
		@endif
	</tbody>
</table>
<div class="pull-right">
	{{ $buyers->links('pagination.pagination_links') }}
	@if($buyers->count() > 10)
	<div class="per_Page">
		Entries per page <select class="entriesperpage">
			<option value="10" selected>10</option>
			<option value="15">15</option>
			<option value="20">20</option>
			<option value="100">100</option>
		</select>
	</div>
	@endif
</div>
