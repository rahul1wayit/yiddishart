@extends('layouts.admin')
@section('content')
<div id="wrapper">
	<div class="buyers">
		<div class="page_title">
			<div class="row">
				<div class="col-md-6">
					<h1>{{ $title }} <span id="successMsgs"></span> </h1>
				</div>
				<div class="col-md-6">
					<div class="search_box">
						<form class="searchForm" action="" method="get">
							<input type="text" name="search" class="searchBox" placeholder="Search">
						</form>
						<a href="#"><img src="{{ asset('assets/images/search_icon.png') }}"></a>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<div id="paginationData">
						<table class="table table-hover">
							<div class="alert alert-success" style="display: none;">
								<span id="successMsg"></span>
							</div>
							<thead>
								<tr>
									<th>No</th>
									<th>Username</th>
									<th class="email">Email ID</th>
									<th class="actions">Actions</th>
								</tr>
							</thead>
							<tbody>
								@if(!$buyers->isEmpty())
								<?php $i = 1; ?>
								@foreach($buyers as $index => $buyer)
								<tr id="removeTr<?php echo $buyer->id; ?>" style="<?php if($buyer->status == 0){ echo 'background-color: #f7c7c7'; } ?>">
									<td>{{ $buyers->perPage() * ($buyers->currentPage() -1 ) + $index+1 }}</td>
									<td>{{ ucwords($buyer->user_name) }}</td>
									<td>{{ $buyer->email }}</td>
									<td class="actions">
										<a href="javascript:void(0)" ref="{{ $buyer->id }}" class="buyersShowBtn" id="buyersShowBtn">View Details</a>
										<a href="javascript:void(0)" ref="{{ $buyer->id }}" class="buyersEditBtn" id="buyersEditBtn"><i class="fa fa-edit"></i></a>
										<a href="javascript:void(0)" ref="{{ $buyer->id }}" class="deleteBuyersBtn" data-type="delete"><i class="fa fa-trash"></i></a>
									</td>
								</tr>
								@endforeach
								@else
								<tr class="noFound">
									<td colspan="9" style="text-align: center;">
										No Record Found
									</td>
								</tr>
								@endif
							</tbody>
						</table>
						<div class="pull-right">
							{{ $buyers->links('pagination.pagination_links') }}
							@if($buyers->count() > 10)
							<div class="per_Page">
								Entries per page <select class="entriesperpage">
									<option value="10" selected>10</option>
									<option value="15">15</option>
									<option value="20">20</option>
									<option value="100">100</option>
								</select>
							</div>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- show -->
	<div class="show" style="display: none;">
		<div class="page_title">
			<div class="row">
				<div class="col-md-2">
					<a class="back_to" href="javascript:void(0)" id="backToBuyersShow"><< back to buyers</a>
				</div>
				<div class="col-md-10">
					<h1>Buyer details</h1>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2 text-center">
				<div class="seller_img">
					<!-- <img src="{{ asset('assets/images/img1.png') }}"/> -->
				</div>
			</div>
			<div class="col-md-6">
				<div class="alert alert-success" style="display: none;">
					<span id="successMsg"></span>
				</div>
				<div class="seller_details" id="sellers_view">
					<div class="form-group">
						<label>User Name</label><span id="user_name"></span>
					</div>
					<div class="form-group">
						<label>First Name</label><span id="first_name"></span>
					</div>
					<div class="form-group">
						<label>Last name</label><span id="last_name"></span>
					</div>
					<div class="form-group">
						<label>E-mail</label><span id="email"></span>
					</div>
				</div>
			</div>
		</div>
		<div class="Rd_button text-right deactBtn">
			<a href="javascript:void(0)" class="btn btn-danger deleteBuyersBtn" data-type="deactive" id="deactId">Deactivate account</a>
		</div>
	</div>
	<!-- show -->

	<!-- edit -->
	<div class="edit" style="display: none;">
		<div class="page_title">
			<div class="row">
				<div class="col-md-2">
					<a class="back_to" href="javascript:void(0)" id="backToBuyersEdit"><< back to buyers</a>
				</div>
				<div class="col-md-10">
					<h1>Edit buyer details</h1>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-2 text-center">
				<div class="seller_img">
					<!-- <img src="{{ asset('assets/images/img1.png') }}"/> -->
				</div>
			</div>
			<div class="col-md-6">
				<form method="post" id="update-buyer-form" class="sellers-update" action="" novalidate="novalidate">
					<div class="alert alert-success" style="display: none;">
						<span id="successMsg"></span>
					</div>
					<input type="hidden" name="csrf-token" value="{{ csrf_token() }}">
					<input name="_method" type="hidden" value="PUT">
					<div class="seller_details" id="editDetails">
						<div class="form-group">
							<label>User Name</label>
							<input type="text" name="user_name" id="uname" class="form-control" value="" placeholder="User Name">
						</div>
						<div class="form-group">
							<label>First Name</label>
							<input type="text" name="first_name" id="fname" class="form-control" value="" placeholder="First Name">
						</div>
						<div class="form-group">
							<label>Last name</label>
							<input type="text" name="last_name" id="lname" class="form-control" value="" placeholder="Last Name">
						</div>
						<div class="form-group">
							<label>E-mail</label>
							<input type="text" name="email" id="bemail" class="form-control" value="" placeholder="E-mail">
						</div>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-primary pull-right updateInfo">Update</button>
						<input type="hidden" name="hiddenId" id="hiddenId" value="">
					</div>
				</form>
			</div>
			<!-- <div class="col-md-7">
			<img src="{{ asset('assets/images/signup-img.png') }}" class="img-responsive">
		</div> -->
	</div>

</div>
<!-- edit -->

</div>
@include('includes.admin.delete-modal')
<script type="text/javascript">
var get_data_url 		= url+'/buyers/show';
var post_data_url 		= url+'/buyers';
var confirm_delete	= url+'/buyers';
var confirm_deactivate	= url+'/deactivate-buyer';
var searchUrl				=url+"/search-buyers";
var entriesperpage				=url+"/entriesperpage";
</script>

@endsection
