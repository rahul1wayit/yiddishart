@extends('layouts.admin')
@section('content')
<div id="wrapper">
  <div class="page_title">
    <div class="row">
      <div class="col-md-12">
        <h1>Transactions overview</h1>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="total_revenue_boxes">
        <div class="row">
          <div class="col-md-3">
            <div class="revenue_box TotalBalance">
              <h4>Total balance</h4>
              <h2>{{ Helper::getTransactionRevenue('total') }}</h2>
            </div>
          </div>
          <div class="col-md-3">
            <div class="revenue_box">
              <h4>Revenue this month</h4>
              <h2>{{ Helper::getTransactionRevenue('month') }}</h2>
            </div>
          </div>
          <div class="col-md-3">
            <div class="revenue_box">
              <h4>Revenue this week</h4>
              <h2>{{ Helper::getTransactionRevenue('week') }}</h2>
            </div>
          </div>
          <div class="col-md-3">
            <div class="revenue_box Today">
              <h4>Revenue today</h4>
              <h2>{{ Helper::getTransactionRevenue('today') }}</h2>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-4 offset-md-3">
      <div class="Select_date">
        <span class="datePi" id="datepickerm">Select date
          <input type="hidden" class="searchBox"></span>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div id="paginationData">
          <div class="table-responsive ">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Buyer</th>
                  <th class="email">Email ID</th>
                  <th class="actions">Amount</th>
                </tr>
              </thead>
              <tbody>
                @if(!$result->isEmpty())
                @foreach($result as $index => $list)
                <tr>
                  <td>{{ $result->perPage() * ($result->currentPage() -1 ) + $index+1 }}</td>
                  <td>
                    <span class="img_box">
                      @if($list->buyer->image != '')
                      <img class="myImg" src="<?php echo asset('assets/images/profile'); ?>/{{ $list->buyer->image }}">
                      @else
                      @if($list->buyer->gender != 'Male')
                      <img class="myImg" src="{{ asset('assets/images/dummy-female.png') }}">
                      @else
                      <img class="myImg" src="{{ asset('assets/images/dummy-male.png') }}">
                      @endif
                      @endif
                    </span>
                    {{ $list->buyer->full_name }}
                  </td>
                  <td>{{ $list->buyer->email }}</td>
                  <td class="actions">
                    {{ Helper::numberFormat($list->sum) }}
                  </td>
                </tr>
                @endforeach
                @else
                <tr class="noFound">
                  <td colspan="4" style="text-align: center;">
                    No Record Found
                  </td>
                </tr>
                @endif
              </tbody>
            </table>
            <div class="pull-right">
              {{ $result->links('pagination.pagination_links') }}
              @if($result->count() > 10)
              <div class="per_Page">
                Entries per page <select class="entriesperpage">
                  <option value="10" selected>10</option>
                  <option value="15">15</option>
                  <option value="20">20</option>
                  <option value="100">100</option>
                </select>
              </div>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
  var searchUrl	= url+"/search-transaction";
</script>
@endsection
