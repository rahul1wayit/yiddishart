<table class="table table-hover">
  <thead>
    <tr>
      <th>No</th>
      <th>Buyer</th>
      <th class="email">Email ID</th>
      <th class="actions">Amount</th>
    </tr>
  </thead>
  <tbody>
    @if(!$result->isEmpty())
    @foreach($result as $index => $list)
    <tr>
      <td>{{ $result->perPage() * ($result->currentPage() -1 ) + $index+1 }}</td>
      <td>
        <span class="img_box">
          @if($list->buyer->image != '')
          <img class="myImg" src="<?php echo asset('assets/images/profile'); ?>/{{ $list->buyer->image }}">
          @else
          @if($list->buyer->gender != 'Male')
          <img class="myImg" src="{{ asset('assets/images/dummy-female.png') }}">
          @else
          <img class="myImg" src="{{ asset('assets/images/dummy-male.png') }}">
          @endif
          @endif
        </span>
        {{ $list->buyer->full_name }}
      </td>
      <td>{{ $list->buyer->email }}</td>
      <td class="actions">
        {{ Helper::numberFormat($list->sum) }}
      </td>
    </tr>
    @endforeach
    @else
    <tr class="noFound">
      <td colspan="4" style="text-align: center;">
        No Record Found
      </td>
    </tr>
    @endif
  </tbody>
</table>
<div class="pull-right">
  {{ $result->links('pagination.pagination_links') }}
  @if($result->count() > 10)
  <div class="per_Page">
    Entries per page <select class="entriesperpage">
      <option value="10" selected>10</option>
      <option value="15">15</option>
      <option value="20">20</option>
      <option value="100">100</option>
    </select>
  </div>
  @endif
</div>
