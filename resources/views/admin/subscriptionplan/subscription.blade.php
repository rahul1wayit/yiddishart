@extends('layouts.admin')
@section('content')
<div id="wrapper">
		<div class="page_title">
			<div class="row">
				<div class="col-md-9">
					<h1>Subscription plans</h1>
				</div>
            <div class="col-md-3 text-right">
					<a class="New_plan" href="{{ $url.'/new-subscription' }}">New Plan</a>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="subscription_boxes">
                	<div class="plan_row">
                        <div class="row">
                            <div class="col-md-10">
                                <div class="Plan_box">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="revenue_box TotalBalance">
                                                <div class="Plan_img">
                                                    <img src="{{ asset('assets/images/free-plan.png') }}"/>
                                                </div>
                                                <div class="plan_name">
                                                    Free<br/>trial
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="revenue_box">
                                                <h4><span>5</span>uploads available</h4>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="revenue_box">
                                                <h4><span>20MB</span>storage</h4>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="revenue_box Today">
                                                <h3>free</h3>
                                            </div>
                                        </div>
                                     </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="actions">
                                    <a href="#"><i class="fa fa-edit"></i></a>
                                    <a href="#"><i class="fa fa-trash"></i></a>
                                </div>
                            </div>
                        </div>
                     </div>
                     <div class="plan_row">
                        <div class="row">
                            <div class="col-md-10">
                                <div class="Plan_box">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="revenue_box EntryBalance">
                                                <div class="Plan_img">
                                                    <img src="{{ asset('assets/images/entry-level.png') }}"/>
                                                </div>
                                                <div class="plan_name">
                                                    Entry<br/>Level
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="revenue_box">
                                                <h4><span>50</span>uploads / month</h4>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="revenue_box">
                                                <h4><span>100MB</span>storage</h4>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="revenue_box Today">
                                                <h3>$49</h3>
                                            </div>
                                        </div>
                                     </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="actions">
                                    <a href="#"><i class="fa fa-edit"></i></a>
                                    <a href="#"><i class="fa fa-trash"></i></a>
                                </div>
                            </div>
                        </div>
                     </div>
                     <div class="plan_row">
                        <div class="row">
                            <div class="col-md-10">
                                <div class="Plan_box">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="revenue_box IntermediateBalance">
                                                <div class="Plan_img">
                                                    <img src="{{ asset('assets/images/intermediate-plan.png') }}"/>
                                                </div>
                                                <div class="plan_name">
                                                    Intermediate<br/>Level
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="revenue_box">
                                                <h4><span>150</span>uploads available</h4>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="revenue_box">
                                                <h4><span>500MB</span>storage</h4>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="revenue_box">
                                                <h3>$79</h3>
                                            </div>
                                        </div>
                                     </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="actions">
                                    <a href="#"><i class="fa fa-edit"></i></a>
                                    <a href="#"><i class="fa fa-trash"></i></a>
                                </div>
                            </div>
                        </div>
                     </div>
                     <div class="plan_row">
                        <div class="row">
                            <div class="col-md-10">
                                <div class="Plan_box">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="revenue_box ExpertBalance">
                                                <div class="Plan_img">
                                                    <img src="{{ asset('assets/images/expert-level.png') }}"/>
                                                </div>
                                                <div class="plan_name">
                                                    Expert<br/>level
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="revenue_box">
                                                <h4><span>250</span>uploads / month</h4>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="revenue_box">
                                                <h4><span>1GB</span>storage</h4>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="revenue_box">
                                                <h3>$99</h3>
                                            </div>
                                        </div>
                                     </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="actions">
                                    <a href="#"><i class="fa fa-edit"></i></a>
                                    <a href="#"><i class="fa fa-trash"></i></a>
                                </div>
                            </div>
                        </div>
                     </div>
                </div>
			</div>
		</div>
	</div>
@endsection
