@extends('layouts.admin')
@section('content')
	<div id="wrapper">
		<div class="page_title">
			<div class="row">
            	<div class="col-md-2">
					<!-- <a class="back_to" href="seller.html">< back to sellers</a> -->
				</div>
				<div class="col-md-10">
					<h1>New Subscription Plan</h1>
				</div>
			</div>
		</div>
		<div class="row">
            <div class="col-md-6 offset-md-2">
            	<div class="seller_details Subscriptions_Plans">
                    <div class="form-group">
                    	<div class="row">
                        	<div class="col-md-3">
                            	<label>Plan name</label>
                            </div>
                            <div class="col-md-9">
                            	<input type="text" class="form-control"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                    	<div class="row">
                        	<div class="col-md-3">
                            	<label>Uploads</label>
                            </div>
                            <div class="col-md-9">
                            	<div class="row">
                            		<div class="col-md-5">
                                    	<select class="form-control">
                                        	<option>7897</option>
                                            <option>973</option>
                                            <option>9777</option>
                                            <option>79778</option>
                                        </select>
                                    </div>
                                    <div class="col-md-7">
                                    	<div class="row">
                                        	<div class="col-md-5 text-right">
                                                <label style="min-width:inherit;">Storage</label>
                                            </div>
                                            <div class="col-md-7">
                                                <select class="form-control">
                                                    <option>241</option>
                                                    <option>784</option>
                                                    <option>7745</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                    	<div class="row">
                        	<div class="col-md-3">
                            	<label>Uploads Icon</label>
                            </div>
                            <div class="col-md-9">
                            	<div class="row">
                            		<div class="col-md-5">
                                      <div class="form-group">
                                        <input type="file" class="form-control-file" id="exampleFormControlFile1">
                                      </div>
                                    </div>
                                    <div class="col-md-7">
                                    	<a href="#" class="Choose_plan_color">Choose plan color</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                    	<div class="row">
                        	<div class="col-md-3">
                            	<label>Plan price</label>
                            </div>
                            <div class="col-md-9">
                            	<div class="row">
                            		<div class="col-md-5">
                                    	<input type="text" class="form-control"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" id="Subscription_btns">
                    	<div class="row">
                        	<div class="col-md-6">
                            	<a href="#" class="btn btn-primary">Save new plan</a>
                            </div>
                            <div class="col-md-6 text-right">
                            	<a href="#" class="btn btn-danger">Dissmiss</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</div>
  @endsection
