@extends('layouts.admin')
@section('content')
<div id="wrapper">
	<div class="page_title">
		<div class="row">
			<div class="col-md-2">
				<a class="back_to" href="{{ $url.'/subscription_plan' }}"><< Back to Plans</a>
			</div>
			<div class="col-md-10">
				<h1>{{ $type }} Subscription Plan</h1>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xl-6 col-md-10 col-12 offset-md-2">
			<div class="seller_details Subscriptions_Plans">
				{!! Form::model($data,['action' => 'Admin\SubscriptionplanController@store', 'method' => 'post', 'files' => true,'id'=>'subscription-plan-form']) !!}
				<div class="form-group">
					<div class="row">

						<div class="col-md-3">
							<label>Plan name</label>
						</div>
						<div class="col-md-9">
							{{ Form::text('title', null, array_merge(['class' => 'form-control', 'id' => 'title', 'placeholder' => 'Enter plan name'])) }}
							@if ($errors->has('title'))

							<label class="error">{{ $errors->first('title') }}</label>

							@endif
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-3">
							<label>Uploads</label>
						</div>
						<div class="col-md-9">
							<div class="row">
								<div class="col-md-4">
									<?php //print_r($data['storage']); ?>
									{{ Form::select('uploads', $subUploads, 'S', ['id'=>'uploads','class'=>'form-control'. ( $errors->has('uploads') ? ' is-invalid' : '') ]) }}
									@if ($errors->has('uploads'))
									<label class="error">{{ $errors->first('uploads') }}</label>
									@endif
								</div>
								<div class="col-md-8">
									<div class="row">
										<div class="col-md-3 text-right">
											<label style="min-width:inherit;">Storage</label>
										</div>
										<div class="col-md-4">
											{{ Form::select('storage', $subUploads, 'S', ['id'=>'uploads','class'=>'form-control'. ( $errors->has('storage') ? ' is-invalid' : '') ]) }}
											@if ($errors->has('storage'))
											<label class="error">{{ $errors->first('storage') }}</label>
										</span>
										@endif
									</div>
									<div class="col-md-5">
										{{ Form::select('storagelimit', $uploadSize, 'S', ['id'=>'uploads','class'=>'form-control'. ( $errors->has('storagelimit') ? ' is-invalid' : '') ]) }}
										@if ($errors->has('storagelimit'))
										<label class="error">{{ $errors->first('storagelimit') }}</label>
									</span>
									@endif
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-3">
					<label style="color:#009ab1;">Upload Icon</label>
				</div>
				<div class="col-md-9">
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<button style="display:block;width:100px; height:30px;" onclick="document.getElementById('user_photo').click()" type="button">Choose File</button>
								<label style="display:none;" id="user_photo-name">Please Select Image</label>
								{{ Form::file('image', array_merge(['class' => 'custom-file-input', 'id' => 'user_photo','style'=>"visibility:hidden;"])) }}
								<label class="error" style="display:none;" id="user_photo-error">Please Select Image</label>
								<label class="error" style="display:none;" id="user_photo-valierror">Invalid File Upload</label>
							</div>
						</div>
						<div class="col-md-8">
							<a href="#" id="hidden-input" class="Choose_plan_color demo">Choose plan color</a>
							{{ Form::hidden('color', null, array_merge(['id' => 'color'])) }}
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-3">
					<label>Plan price</label>
				</div>
				<div class="col-md-9">
					<div class="row">
						<div class="col-md-8">
							{{ Form::text('price', null, array_merge(['class' => 'form-control validNumber', 'id' => 'price', 'placeholder' => 'Enter price'])) }}
						</div>
						@if ($errors->has('price'))
						<label class="error">{{ $errors->first('price') }}</label>
						@endif
					</div>
				</div>
			</div>
		</div>
		<!-- <div class="form-group">
		<div class="row">

		<div class="col-md-3">
		<label>Description</label>
	</div>
	<div class="col-md-9">
	{{-- Form::textarea('description', null, array_merge(['class' => 'form-control', 'id' => 'description', 'placeholder' => 'Enter description', 'rows' => 4, 'cols' => 54])) --}}
</div>
</div>
</div> -->
<div class="form-group">
	<div class="row">

		<div class="col-md-3">
			<label>Status</label>
		</div>
		<div class="col-md-9">
			<div class="row">
				<div class="col-md-8">
					{{ Form::select('status', array('1' => 'Active', '0' => 'Inactive'), null, ['class' => 'form-control'] ) }}
				</div>
			</div>
		</div>
	</div>
</div>
<div class="form-group" id="Subscription_btns">
	<div class="row">
		<div class="col-md-6">
			{!! Form::hidden('id', $id) !!}
			{{ Form::submit($button_name, array_merge(['class' => 'btn btn-primary updateImage', 'id' => 'submit'])) }}
		</div>
		<div class="col-md-6 text-right">
			<a href="{{ $url.'/subscription_plan' }}" id="dissmiss" class="btn btn-danger">Dismiss</a>
		</div>
	</div>
</div>
</div>
</div>
</div>
</div>
@endsection
