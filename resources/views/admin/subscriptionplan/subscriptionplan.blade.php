@extends('layouts.admin')
@section('content')
<div id="wrapper" class="editShow">
	<div class="page_title">
		<div class="row">
			<div class="col-md-9">
				<h1>Subscription plans</h1>
			</div>
			<div class="col-md-3 text-right">
				<!-- <a class="New_plan" href="{{ $url.'/new-subscription' }}">New Plan</a> -->
				<a class="New_plan" href="{{ route('subscription_plan.create') }}">Add Plan</a>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="subscription_boxes">
				<?php
				function color_blend_by_opacity( $foreground, $opacity, $background=null )
				{
					static $colors_rgb=array(); // stores colour values already passed through the hexdec() functions below.

					if( is_null($background) )
					$background = 'FFFFFF'; // default background.

					$pattern = '~^[a-f0-9]{6,6}$~i'; // accept only valid hexadecimal colour values.
					if( !@preg_match($pattern, $foreground)  or  !@preg_match($pattern, $background) )
					{
						trigger_error( "Invalid hexadecimal colour value(s) found", E_USER_WARNING );
						return false;
					}

					$opacity = intval( $opacity ); // validate opacity data/number.
					// print_r($opacity);die;
					if( $opacity>100  || $opacity<0 )
					{
						trigger_error( "Opacity percentage error, valid numbers are between 0 - 100", E_USER_WARNING );
						return false;
					}

					if( $opacity==100 )    // $transparency == 0
					return strtoupper( $foreground );
					if( $opacity==0 )    // $transparency == 100
					return strtoupper( $background );
					// calculate $transparency value.
					$transparency = 100-$opacity;

					if( !isset($colors_rgb[$foreground]) )
					{ // do this only ONCE per script, for each unique colour.
						$f = array(  'r'=>hexdec($foreground[0].$foreground[1]),
						'g'=>hexdec($foreground[2].$foreground[3]),
						'b'=>hexdec($foreground[4].$foreground[5])    );
						$colors_rgb[$foreground] = $f;
					}
					else
					{ // if this function is used 100 times in a script, this block is run 99 times.  Efficient.
						$f = $colors_rgb[$foreground];
					}

					if( !isset($colors_rgb[$background]) )
					{ // do this only ONCE per script, for each unique colour.
						$b = array(  'r'=>hexdec($background[0].$background[1]),
						'g'=>hexdec($background[2].$background[3]),
						'b'=>hexdec($background[4].$background[5])    );
						$colors_rgb[$background] = $b;
					}
					else
					{ // if this FUNCTION is used 100 times in a SCRIPT, this block will run 99 times.  Efficient.
						$b = $colors_rgb[$background];
					}

					$add = array(    'r'=>( $b['r']-$f['r'] ) / 100,
					'g'=>( $b['g']-$f['g'] ) / 100,
					'b'=>( $b['b']-$f['b'] ) / 100    );

					$f['r'] += intval( $add['r'] * $transparency );
					$f['g'] += intval( $add['g'] * $transparency );
					$f['b'] += intval( $add['b'] * $transparency );

					return sprintf( '%02X%02X%02X', $f['r'], $f['g'], $f['b'] );
				}
				?>
				@foreach ($getAllSubscriptionplan as $getSubscriptionplan)
				<div class="plan_row">
					<div class="row">
						<div class="col-md-10">
							<div class="Plan_box">
								<div class="row">
									<div class="col-md-3">
										<?php
										$foreground = str_replace("#","",$getSubscriptionplan['color']);
										$background = '0040A4';
										$o_color=@color_blend_by_opacity($foreground, 50, $background);
										// echo $o_color;
										?>
										<div class="revenue_box TotalBalance" style="background: {{ $getSubscriptionplan['color'] }};background: -webkit-linear-gradient(left, {{ $getSubscriptionplan['color'] }} 0%,{{ '#'.$o_color }} 100%);">
											<div class="Plan_img">
												@if($getSubscriptionplan['image'] == '')
												<img src="{{ asset('assets/images/free-plan.png') }}"/>
												@else
												<img src="{{ asset('assets/images/').'/'.$getSubscriptionplan['image'] }}"/>
												@endif
											</div>
											<div class="plan_name">
												{{ ucwords($getSubscriptionplan['title']) }}
											</div>
										</div>
									</div>
									<div class="col-md-3">
										<div class="revenue_box">
											<h4><span>{{ $getSubscriptionplan['uploads'] }}</span>uploads / month</h4>
										</div>
									</div>
									<div class="col-md-3">
										<div class="revenue_box">
											<h4><span>{{ $getSubscriptionplan['storage'].$getSubscriptionplan['storagelimit'] }}</span>storage</h4>
										</div>
									</div>
									<div class="col-md-3">
										<div class="revenue_box Today">
											@if($getSubscriptionplan['price'] != 0)
											<h3>{{ '$'.$getSubscriptionplan['price'] }}</h3>
											@else
											<h3>Free</h3>
											@endif
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-2">
							<div class="actions">
								<a href="javascript:void(0);" ref="{{ route('subscription_plan.edit', Crypt::encryptString($getSubscriptionplan['id'])) }}" id="edit-subscription-plan"><i class="fa fa-edit"></i></a>
								<a id="subscription-delete-plan" ref="{{ $getSubscriptionplan['id'] }}" href="javascript:void(0)"><i class="fa fa-trash"></i></a>
							</div>
						</div>
					</div>
				</div>
				@endforeach
			</div>
		</div>
	</div>
</div>
<div class="modal designModal" id="deleteSubscriptionPlan">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">
					<span class="header-text deleteHead"> Delete <span class="sellerHeadName">Subscription Plan</span> </span>
					<!-- <span class="header-text deactivateHead" style="display:none;"> Deactivate <span class="sellerHeadName">Buyer</span> </span>
					<span class="header-text activateHead" style="display:none;"> Activate <span class="sellerHeadName">Buyer</span> </span> -->
				</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<span class="body-text deleteText">Are you sure you want to delete this <span class="sellerText">plan</span>?</span>
				</div>
				@csrf
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
					<input type="hidden" id="sub-plan-id">
					<button type="button" class="btn btn-primary deleteSubscription">Yes</button>
					<input type="hidden" name="buyerId" id="buyerId">
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
