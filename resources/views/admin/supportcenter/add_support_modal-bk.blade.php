<script type="text/javascript" src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>
<!-- Modal -->
<div id="Add_FAQ" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add New FAQ</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <?php /* <div class="form-group add_faq_html">
                <div class="row">
                <div class="col-md-8">
                <select class="form-control">
                <option>Choose Category</option>
                <option>1</option>
                <option>2</option>
                </select>
                </div>
                <div class="col-md-4">
                <a href="javascript:void(0);" class="btn_link new_faq_cat">Add New Category</a>
                </div>
                </div>
                </div>
                <div class="form-group">
                <label><input type="checkbox" name=""> Common Question</label>
                </div> */ ?>
                {{ Form::open(array('action' => 'Admin\FaqController@store', 'id' => 'faqFormModal')) }}
                    <div class="form-group">
                        <label>Question</label>
                        <input type="text" class="form-control" name="question" placeholder="Enter your question here...">
                    </div>
                    <div class="form-group">
                        <label>Answer</label>
                        <br>
                        <textarea class="form-control" name="answer" id="answer"></textarea>
                    </div>
                    <div class="checkbox">
                        <label>{{ Form::checkbox('status', '1', true) }} Status</label>
                    </div>
                    <div class="form-group text-right">
                        {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
CKEDITOR.replace( 'answer' );
CKEDITOR.instances['answer'].on('key', function () {
    var self = this;
    setTimeout(function() {
        var ckValue = GetTextFromHtml(self.getData()).replace(/<[^>]*>/gi, '').trim();
        if (ckValue.length === 0) {
            $("#answer-error").show();
        } else {
            $("#answer-error").hide();
        }
    }, 10);
});
</script>
