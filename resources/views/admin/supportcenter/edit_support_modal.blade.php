<!-- Modal -->
{{ Form::open(array('action' => 'Admin\SupportCenterController@store', 'id' => 'supportFormModal')) }}
    <div class="form-group">
        <label>Support Category</label>
        <select class="form-control" name="category_id">
            
            @if( isset($supportCategory) && sizeOf($supportCategory) > 0 )
            <option value="">Choose category</option> 
             @foreach( $supportCategory as $key => $value )
            <option value="{{$value->id}}" <?php if( isset($result->category_id) && $result->category_id == $value->id) {echo 'selected="selected"';} ?> >  {{$value->name}}
            </option> 
            @endforeach
            @else
            <option value="">No category found</option>
            @endif
        </select>
    </div> 
    <div class="form-group">
        <label>Question</label>
        <input type="text" class="form-control" name="question" placeholder="Enter your question here..." value="{{ $result->question }}">
    </div>
    <div class="form-group">
        <label>Answer</label>
        <br>
        <textarea class="form-control" name="answer" id="answer">{{ $result->answer }}</textarea>
    </div> 
    <div class="checkbox">
        <?php
        $checkBox = false;
        $commonCheckBox = false;
        if($result->status==1)
            $checkBox = true;
        if($result->common_status == 1)
            $commonCheckBox = true;
        ?>
        <label>{{ Form::checkbox('common_status', '1', $commonCheckBox) }} Common question</label>
    </div>
    <div class="checkbox">
        <label>{{ Form::checkbox('status', '1', $checkBox) }} Status</label>
    </div>    
    <div class="form-group text-right">
        <input type="hidden" name="supportid" value="{{ Crypt::encrypt($result->id) }}">
        {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
    </div>
{{ Form::close() }}

<script type="text/javascript">
CKEDITOR.replace( 'answer' );
CKEDITOR.instances['answer'].on('key', function () {
    var self = this;
    setTimeout(function() {
        var ckValue = GetTextFromHtml(self.getData()).replace(/<[^>]*>/gi, '').trim();
        if (ckValue.length === 0) {
            $("#answer-error").show();
        } else {
            $("#answer-error").hide();
        }
    }, 10);
});
</script>
