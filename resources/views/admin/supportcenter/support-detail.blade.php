@extends('layouts.admin')
@section('content')
<div id="wrapper">
	<div class="buyers">
		<div class="page_title">
			<div class="row">
				<div class="col-md-3">
					<a class="back_to" href="{{url($url.'/support-center')}}" id="backToBuyersShow">&lt;&lt; back to support center</a>
				</div>
			</div>
			<div class="row">
				<div class="col-md-9">
					<h1>{{ $title }} <span id="successMsgs"></span> </h1>

				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="qst">
					<h4><span>Q. </span>{{ ucfirst($result->question) }}</h4>
					<div class="float-right">
						<button class="btn btn-<?php echo ($result->status == 1) ? 'primary':'danger' ?>" disabled="true"><i class="fa fa-<?php echo ($result->status == 1) ? 'check':'times' ?>"></i>Status</button>
						<button class="btn btn-<?php echo ($result->common_status == 1) ? 'primary':'danger' ?>" disabled="true"> <i class="fa fa-<?php echo ($result->common_status == 1) ? 'check':'times' ?>"></i>Common question</button>
					</div>
					<div class="ans"><span>A.</span>
<p>{!! $result->answer !!}</p>
</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
