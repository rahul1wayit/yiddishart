-- MySQL dump 10.13  Distrib 5.7.30, for Linux (x86_64)
--
-- Host: localhost    Database: yiddishart
-- ------------------------------------------------------
-- Server version	5.7.30-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `business_categories`
--

DROP TABLE IF EXISTS `business_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `business_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `business_categories_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `business_categories`
--

LOCK TABLES `business_categories` WRITE;
/*!40000 ALTER TABLE `business_categories` DISABLE KEYS */;
INSERT INTO `business_categories` VALUES (1,'School','School_icon.png','school','2019-08-28 16:03:33','2019-08-28 16:03:33'),(2,'Simcha','Simcha_icon.png','simcha','2019-08-28 16:03:33','2019-08-28 16:03:33'),(3,'Annual','Annual_icon.png','annual','2019-08-28 16:03:33','2019-08-28 16:03:33'),(4,'Holiday','Holiday_icon.png','holiday','2019-08-28 16:03:33','2019-08-28 16:03:33'),(5,'Shul','Shul_icon.png','shul','2019-08-28 16:03:33','2019-08-28 16:03:33');
/*!40000 ALTER TABLE `business_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `businesses`
--

DROP TABLE IF EXISTS `businesses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `businesses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `businessname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `businesses`
--

LOCK TABLES `businesses` WRITE;
/*!40000 ALTER TABLE `businesses` DISABLE KEYS */;
INSERT INTO `businesses` VALUES (1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2019-08-28 16:03:33','2019-08-28 16:03:33'),(2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2,'2019-08-28 16:03:33','2019-08-28 16:03:33'),(3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,3,'2019-08-28 16:03:33','2019-08-28 16:03:33'),(4,'1way IT','2','Mohali','Punjab',NULL,'160071','989898989898','Testing',5,'2019-08-29 12:43:29','2019-08-29 12:43:29'),(5,'qqq','1','cgb','fghb',NULL,'fgh','65464564564','vgndfhg dfhg dfgh',6,'2019-10-16 12:03:13','2019-10-16 12:03:13'),(6,'Software','4','Mandi','H.P.',NULL,'175003','9418162116','Due to corona virus holiday going on',12,'2020-05-26 12:18:25','2020-05-26 12:18:25'),(7,'gfb','1','mohali','punjab',NULL,'16007','8888888888','tfhftg',13,'2020-05-26 15:15:04','2020-05-27 09:25:51');
/*!40000 ALTER TABLE `businesses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cart_items`
--

DROP TABLE IF EXISTS `cart_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cart_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `design_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `design_img` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `design_data` longtext COLLATE utf8mb4_unicode_ci,
  `price` double(8,2) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cart_items_design_id_foreign` (`design_id`),
  KEY `cart_items_user_id_foreign` (`user_id`),
  CONSTRAINT `cart_items_design_id_foreign` FOREIGN KEY (`design_id`) REFERENCES `designs` (`id`),
  CONSTRAINT `cart_items_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cart_items`
--

LOCK TABLES `cart_items` WRITE;
/*!40000 ALTER TABLE `cart_items` DISABLE KEYS */;
INSERT INTO `cart_items` VALUES (1,2,14,'1590655606.jpeg','s:758:\"[{\"layer\":true,\"customtype\":\"image\",\"name\":\"default-image\",\"source\":\"http://139.59.11.192/yiddishart/assets/images/seller-design/Ok2JDXl0AXeP.png\",\"x\":0,\"y\":0,\"width\":570,\"height\":320.46852122986826,\"fromCenter\":false,\"draggable\":\"\"},{\"customtype\":\"image\",\"name\":\"logo\",\"source\":\"http://139.59.11.192/yiddishart/assets/images/white.jpg\",\"draggable\":\"\",\"x\":150,\"y\":150,\"rotate\":0,\"width\":1,\"aspectRat\":0,\"height\":1,\"layer\":true},{\"customtype\":\"text\",\"name\":\"text\",\"fillStyle\":\"#f0f0f0\",\"draggable\":\"\",\"strokeWidth\":2,\"x\":397,\"y\":70,\"lineHeight\":1,\"rotate\":0,\"language\":\"qwerty\",\"char_limit\":\"\",\"letterSpacing\":\"\",\"maxWidth\":300,\"align\":\"left\",\"fontSize\":32,\"layer\":true,\"fontStyle\":\"normal\",\"fontFamily\":\"Serif\",\"text\":\"asdfsdffa fdgdf dfgdf fff fgr fdgdfg\"}]\";',500.00,1,'0','2020-05-28 14:16:46','2020-05-28 14:16:46');
/*!40000 ALTER TABLE `cart_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `designs`
--

DROP TABLE IF EXISTS `designs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `designs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` double(8,2) DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `keyword` longtext COLLATE utf8mb4_unicode_ci,
  `category` int(11) DEFAULT NULL,
  `size` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actualHeight` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actualWidth` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aspectRatio` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `canvasData` longtext COLLATE utf8mb4_unicode_ci,
  `designimage` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `status` enum('0','1','2') COLLATE utf8mb4_unicode_ci DEFAULT '0' COMMENT '0 = waiting for approval, 1 = Approved, 2 = Reject',
  `added_by` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `designs_added_by_foreign` (`added_by`),
  CONSTRAINT `designs_added_by_foreign` FOREIGN KEY (`added_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `designs`
--

LOCK TABLES `designs` WRITE;
/*!40000 ALTER TABLE `designs` DISABLE KEYS */;
INSERT INTO `designs` VALUES (1,'Welcome',2.00,'Test','Test',3,'Small','0','0','0','s:2002:\"[{\"layer\":true,\"customtype\":\"image\",\"name\":\"default-image\",\"source\":\"http://139.59.11.192/yiddishart/assets/images/seller-design/iZQWv5JaceF2.jpg\",\"x\":0,\"y\":0,\"width\":570,\"height\":320.625,\"fromCenter\":false},{\"customtype\":\"image\",\"name\":\"logo\",\"source\":\"http://139.59.11.192/yiddishart/assets/images/seller-design/AumepGY74o3V.jpg\",\"draggable\":true,\"x\":489,\"y\":52,\"rotate\":31,\"width\":200,\"aspectRat\":3.09,\"height\":64.72491909385114,\"layer\":true},{\"customtype\":\"text\",\"name\":\"text\",\"fillStyle\":\"#000\",\"draggable\":true,\"strokeWidth\":2,\"x\":150,\"y\":54,\"lineHeight\":1,\"rotate\":0,\"language\":\"qwerty\",\"char_limit\":\"\",\"letterSpacing\":\"\",\"maxWidth\":570,\"align\":\"left\",\"fontSize\":32,\"layer\":true,\"fontStyle\":\"bold\",\"fontFamily\":\"Courier\",\"text\":\"Dear Daughter,\"},{\"customtype\":\"text\",\"name\":\"text3\",\"fillStyle\":\"#000\",\"draggable\":true,\"strokeWidth\":2,\"x\":129,\"y\":103,\"lineHeight\":1,\"rotate\":0,\"maxWidth\":570,\"language\":\"qwerty\",\"char_limit\":\"\",\"letterSpacing\":\"\",\"align\":\"left\",\"fontSize\":32,\"layer\":true,\"fontFamily\":\"Serif\",\"fontStyle\":\"normal\",\"text\":\"we are so happy to\"},{\"customtype\":\"text\",\"name\":\"text4\",\"fillStyle\":\"#000\",\"draggable\":true,\"strokeWidth\":2,\"x\":166,\"y\":147,\"lineHeight\":1,\"rotate\":0,\"maxWidth\":570,\"language\":\"qwerty\",\"char_limit\":\"\",\"letterSpacing\":\"\",\"align\":\"left\",\"fontSize\":32,\"layer\":true,\"fontFamily\":\"Serif\",\"fontStyle\":\"normal\",\"text\":\"welcome a new member\"},{\"customtype\":\"text\",\"name\":\"text5\",\"fillStyle\":\"#000\",\"draggable\":true,\"strokeWidth\":2,\"x\":94,\"y\":195,\"lineHeight\":1,\"rotate\":0,\"maxWidth\":570,\"language\":\"qwerty\",\"char_limit\":\"\",\"letterSpacing\":\"\",\"align\":\"left\",\"fontSize\":32,\"layer\":true,\"fontFamily\":\"Serif\",\"fontStyle\":\"normal\",\"text\":\" into our herd.\"},{\"customtype\":\"text\",\"name\":\"text6\",\"fillStyle\":\"#000\",\"draggable\":true,\"strokeWidth\":2,\"x\":190,\"y\":263,\"lineHeight\":1,\"rotate\":0,\"maxWidth\":570,\"language\":\"qwerty\",\"char_limit\":\"\",\"letterSpacing\":\"\",\"align\":\"left\",\"fontSize\":32,\"layer\":true,\"fontFamily\":\"Impact\",\"fontStyle\":\"italic\",\"text\":\"Congratulations!\"}]\";','1567074735.jpeg',1,'1',5,'2019-08-29 15:20:46','2019-08-29 16:03:07'),(2,'Test',500.00,'regrwegerherh','regrwegerherh',4,'Small','768','1366','1.7786458333333333','s:747:\"[{\"layer\":true,\"customtype\":\"image\",\"name\":\"default-image\",\"source\":\"http://139.59.11.192/yiddishart/assets/images/seller-design/Ok2JDXl0AXeP.png\",\"x\":0,\"y\":0,\"width\":570,\"height\":320.46852122986826,\"fromCenter\":false},{\"customtype\":\"image\",\"name\":\"logo\",\"source\":\"http://139.59.11.192/yiddishart/assets/images/white.jpg\",\"draggable\":true,\"x\":150,\"y\":150,\"rotate\":0,\"width\":1,\"aspectRat\":0,\"height\":1,\"layer\":true},{\"customtype\":\"text\",\"name\":\"text\",\"fillStyle\":\"#f0f0f0\",\"draggable\":true,\"strokeWidth\":2,\"x\":397,\"y\":70,\"lineHeight\":1,\"rotate\":0,\"language\":\"qwerty\",\"char_limit\":\"\",\"letterSpacing\":\"\",\"maxWidth\":300,\"align\":\"left\",\"fontSize\":32,\"layer\":true,\"fontStyle\":\"normal\",\"fontFamily\":\"Serif\",\"text\":\"asdfsdffa fdgdf dfgdf fff fgr fdgdfg\"}]\";','1568614569.jpeg',1,'1',2,'2019-09-16 11:46:09','2019-09-16 11:46:34'),(3,'Sun',2.90,'test description','test description',4,'Medium','0','0','0','s:1014:\"[{\"layer\":true,\"customtype\":\"image\",\"name\":\"default-image\",\"source\":\"http://134.209.151.198/yiddishart/assets/images/seller-design/3jPXLkh3zXN5.jpg\",\"x\":0,\"y\":0,\"width\":570,\"height\":331.53061224489795,\"fromCenter\":false},{\"customtype\":\"image\",\"name\":\"logo\",\"source\":\"http://134.209.151.198/yiddishart/assets/images/white.jpg\",\"draggable\":true,\"x\":150,\"y\":150,\"rotate\":0,\"width\":1,\"aspectRat\":0,\"height\":1,\"layer\":true},{\"customtype\":\"text\",\"name\":\"text\",\"fillStyle\":\"#f2f2f2\",\"draggable\":true,\"strokeWidth\":2,\"x\":281,\"y\":29,\"lineHeight\":1,\"rotate\":0,\"language\":\"qwerty\",\"char_limit\":\"\",\"letterSpacing\":\"\",\"maxWidth\":570,\"align\":\"left\",\"fontSize\":36,\"layer\":true,\"fontStyle\":\"bold\",\"fontFamily\":\"Tahoma\",\"text\":\"Title\"},{\"customtype\":\"text\",\"name\":\"text3\",\"fillStyle\":\"#000\",\"draggable\":true,\"strokeWidth\":2,\"x\":150,\"y\":100,\"lineHeight\":1,\"rotate\":0,\"maxWidth\":570,\"language\":\"qwerty\",\"char_limit\":\"\",\"letterSpacing\":\"\",\"align\":\"left\",\"fontSize\":32,\"layer\":true,\"fontFamily\":\"Serif\",\"fontStyle\":\"normal\",\"text\":\"\"}]\";','1590553352.jpeg',1,'1',13,'2020-05-27 09:52:04','2020-05-27 09:53:42');
/*!40000 ALTER TABLE `designs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `date` date DEFAULT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci DEFAULT '0' COMMENT '0 = Inactive, 1 = Active',
  `added_by` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `events_added_by_foreign` (`added_by`),
  CONSTRAINT `events_added_by_foreign` FOREIGN KEY (`added_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
INSERT INTO `events` VALUES (1,'gfjd','1567667685_download.jpg','dfyt ryt dr dftry','2019-09-11','0',1,'2019-09-05 12:43:54','2019-09-05 12:44:45');
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `faq_category`
--

DROP TABLE IF EXISTS `faq_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `faq_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `faq_category`
--

LOCK TABLES `faq_category` WRITE;
/*!40000 ALTER TABLE `faq_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `faq_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `faq_qus_ans`
--

DROP TABLE IF EXISTS `faq_qus_ans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `faq_qus_ans` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question` text COLLATE utf8mb4_unicode_ci,
  `answer` longtext COLLATE utf8mb4_unicode_ci,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `added_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `faq_qus_ans_added_by_foreign` (`added_by`),
  CONSTRAINT `faq_qus_ans_added_by_foreign` FOREIGN KEY (`added_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `faq_qus_ans`
--

LOCK TABLES `faq_qus_ans` WRITE;
/*!40000 ALTER TABLE `faq_qus_ans` DISABLE KEYS */;
INSERT INTO `faq_qus_ans` VALUES (5,'thbs','<p>thstrhbz</p>','1',1,'2020-06-05 14:24:22','2020-06-05 14:24:22');
/*!40000 ALTER TABLE `faq_qus_ans` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fonts`
--

DROP TABLE IF EXISTS `fonts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fonts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci DEFAULT '0' COMMENT '0 = Inactive, 1 = Active',
  `added_by` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fonts_added_by_foreign` (`added_by`),
  CONSTRAINT `fonts_added_by_foreign` FOREIGN KEY (`added_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fonts`
--

LOCK TABLES `fonts` WRITE;
/*!40000 ALTER TABLE `fonts` DISABLE KEYS */;
INSERT INTO `fonts` VALUES (1,'Serif','1',1,'2019-08-28 16:03:33','2019-08-28 16:03:33'),(2,'Arial','1',1,'2019-08-28 16:03:33','2019-08-28 16:03:33'),(3,'Arial Black','1',1,'2019-08-28 16:03:33','2019-08-28 16:03:33'),(4,'Courier','1',1,'2019-08-28 16:03:33','2019-08-28 16:03:33'),(5,'Courier New','1',1,'2019-08-28 16:03:33','2019-08-28 16:03:33'),(6,'Comic Sans MS','1',1,'2019-08-28 16:03:33','2019-08-28 16:03:33'),(7,'Helvetica','1',1,'2019-08-28 16:03:33','2019-08-28 16:03:33'),(8,'Impact','1',1,'2019-08-28 16:03:33','2019-08-28 16:03:33'),(9,'Lucida Grande','1',1,'2019-08-28 16:03:33','2019-08-28 16:03:33'),(10,'Lucida Sans','1',1,'2019-08-28 16:03:33','2019-08-28 16:03:33'),(11,'Tahoma','1',1,'2019-08-28 16:03:33','2019-08-28 16:03:33'),(12,'Times','1',1,'2019-08-28 16:03:33','2019-08-28 16:03:33'),(13,'Times New Roman','1',1,'2019-08-28 16:03:33','2019-08-28 16:03:33'),(14,'Verdana','1',1,'2019-08-28 16:03:33','2019-08-28 16:03:33');
/*!40000 ALTER TABLE `fonts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logs`
--

DROP TABLE IF EXISTS `logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `log_id` int(11) DEFAULT NULL,
  `action` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `added_date` date NOT NULL,
  `added_time` time NOT NULL,
  `log_data` longtext COLLATE utf8mb4_unicode_ci,
  `log_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('0','1','2') COLLATE utf8mb4_unicode_ci DEFAULT '0' COMMENT '0 = waiting for approval, 1 = Approved, 2 = Reject',
  `added_by` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `logs_added_by_foreign` (`added_by`),
  CONSTRAINT `logs_added_by_foreign` FOREIGN KEY (`added_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logs`
--

LOCK TABLES `logs` WRITE;
/*!40000 ALTER TABLE `logs` DISABLE KEYS */;
INSERT INTO `logs` VALUES (1,1,'added','design','2019-08-29','15:20:51','s:1960:\"[{\"layer\":true,\"customtype\":\"image\",\"name\":\"default-image\",\"source\":\"http://139.59.11.192/yiddishart/assets/images/seller-design/iZQWv5JaceF2.jpg\",\"x\":0,\"y\":0,\"width\":570,\"height\":320.625,\"fromCenter\":false},{\"customtype\":\"image\",\"name\":\"logo\",\"source\":\"http://139.59.11.192/yiddishart/assets/images/white.jpg\",\"draggable\":true,\"x\":150,\"y\":150,\"rotate\":0,\"width\":1,\"aspectRat\":0,\"height\":1,\"layer\":true},{\"customtype\":\"text\",\"name\":\"text\",\"fillStyle\":\"#000\",\"draggable\":true,\"strokeWidth\":2,\"x\":150,\"y\":54,\"lineHeight\":1,\"rotate\":0,\"language\":\"qwerty\",\"char_limit\":\"\",\"letterSpacing\":\"\",\"maxWidth\":570,\"align\":\"left\",\"fontSize\":32,\"layer\":true,\"fontStyle\":\"bold\",\"fontFamily\":\"Courier\",\"text\":\"Dear Daughter,\"},{\"customtype\":\"text\",\"name\":\"text3\",\"fillStyle\":\"#000\",\"draggable\":true,\"strokeWidth\":2,\"x\":129,\"y\":103,\"lineHeight\":1,\"rotate\":0,\"maxWidth\":570,\"language\":\"qwerty\",\"char_limit\":\"\",\"letterSpacing\":\"\",\"align\":\"left\",\"fontSize\":32,\"layer\":true,\"fontFamily\":\"Serif\",\"fontStyle\":\"normal\",\"text\":\"we are so happy to\"},{\"customtype\":\"text\",\"name\":\"text4\",\"fillStyle\":\"#000\",\"draggable\":true,\"strokeWidth\":2,\"x\":166,\"y\":147,\"lineHeight\":1,\"rotate\":0,\"maxWidth\":570,\"language\":\"qwerty\",\"char_limit\":\"\",\"letterSpacing\":\"\",\"align\":\"left\",\"fontSize\":32,\"layer\":true,\"fontFamily\":\"Serif\",\"fontStyle\":\"normal\",\"text\":\"welcome a new member\"},{\"customtype\":\"text\",\"name\":\"text5\",\"fillStyle\":\"#000\",\"draggable\":true,\"strokeWidth\":2,\"x\":94,\"y\":195,\"lineHeight\":1,\"rotate\":0,\"maxWidth\":570,\"language\":\"qwerty\",\"char_limit\":\"\",\"letterSpacing\":\"\",\"align\":\"left\",\"fontSize\":32,\"layer\":true,\"fontFamily\":\"Serif\",\"fontStyle\":\"normal\",\"text\":\" into our herd.\"},{\"customtype\":\"text\",\"name\":\"text6\",\"fillStyle\":\"#000\",\"draggable\":true,\"strokeWidth\":2,\"x\":190,\"y\":263,\"lineHeight\":1,\"rotate\":0,\"maxWidth\":570,\"language\":\"qwerty\",\"char_limit\":\"\",\"letterSpacing\":\"\",\"align\":\"left\",\"fontSize\":32,\"layer\":true,\"fontFamily\":\"Impact\",\"fontStyle\":\"italic\",\"text\":\"Congratulations!\"}]\";','1567072246.jpeg','1',5,'2019-08-29 15:20:51','2019-08-29 15:57:45'),(2,1,'updated','design','2019-08-29','16:02:15','s:2002:\"[{\"layer\":true,\"customtype\":\"image\",\"name\":\"default-image\",\"source\":\"http://139.59.11.192/yiddishart/assets/images/seller-design/iZQWv5JaceF2.jpg\",\"x\":0,\"y\":0,\"width\":570,\"height\":320.625,\"fromCenter\":false},{\"customtype\":\"image\",\"name\":\"logo\",\"source\":\"http://139.59.11.192/yiddishart/assets/images/seller-design/AumepGY74o3V.jpg\",\"draggable\":true,\"x\":489,\"y\":52,\"rotate\":31,\"width\":200,\"aspectRat\":3.09,\"height\":64.72491909385114,\"layer\":true},{\"customtype\":\"text\",\"name\":\"text\",\"fillStyle\":\"#000\",\"draggable\":true,\"strokeWidth\":2,\"x\":150,\"y\":54,\"lineHeight\":1,\"rotate\":0,\"language\":\"qwerty\",\"char_limit\":\"\",\"letterSpacing\":\"\",\"maxWidth\":570,\"align\":\"left\",\"fontSize\":32,\"layer\":true,\"fontStyle\":\"bold\",\"fontFamily\":\"Courier\",\"text\":\"Dear Daughter,\"},{\"customtype\":\"text\",\"name\":\"text3\",\"fillStyle\":\"#000\",\"draggable\":true,\"strokeWidth\":2,\"x\":129,\"y\":103,\"lineHeight\":1,\"rotate\":0,\"maxWidth\":570,\"language\":\"qwerty\",\"char_limit\":\"\",\"letterSpacing\":\"\",\"align\":\"left\",\"fontSize\":32,\"layer\":true,\"fontFamily\":\"Serif\",\"fontStyle\":\"normal\",\"text\":\"we are so happy to\"},{\"customtype\":\"text\",\"name\":\"text4\",\"fillStyle\":\"#000\",\"draggable\":true,\"strokeWidth\":2,\"x\":166,\"y\":147,\"lineHeight\":1,\"rotate\":0,\"maxWidth\":570,\"language\":\"qwerty\",\"char_limit\":\"\",\"letterSpacing\":\"\",\"align\":\"left\",\"fontSize\":32,\"layer\":true,\"fontFamily\":\"Serif\",\"fontStyle\":\"normal\",\"text\":\"welcome a new member\"},{\"customtype\":\"text\",\"name\":\"text5\",\"fillStyle\":\"#000\",\"draggable\":true,\"strokeWidth\":2,\"x\":94,\"y\":195,\"lineHeight\":1,\"rotate\":0,\"maxWidth\":570,\"language\":\"qwerty\",\"char_limit\":\"\",\"letterSpacing\":\"\",\"align\":\"left\",\"fontSize\":32,\"layer\":true,\"fontFamily\":\"Serif\",\"fontStyle\":\"normal\",\"text\":\" into our herd.\"},{\"customtype\":\"text\",\"name\":\"text6\",\"fillStyle\":\"#000\",\"draggable\":true,\"strokeWidth\":2,\"x\":190,\"y\":263,\"lineHeight\":1,\"rotate\":0,\"maxWidth\":570,\"language\":\"qwerty\",\"char_limit\":\"\",\"letterSpacing\":\"\",\"align\":\"left\",\"fontSize\":32,\"layer\":true,\"fontFamily\":\"Impact\",\"fontStyle\":\"italic\",\"text\":\"Congratulations!\"}]\";','1567074735.jpeg','1',5,'2019-08-29 16:02:15','2019-08-29 16:03:07'),(3,2,'added','design','2019-09-16','11:46:13','s:747:\"[{\"layer\":true,\"customtype\":\"image\",\"name\":\"default-image\",\"source\":\"http://139.59.11.192/yiddishart/assets/images/seller-design/Ok2JDXl0AXeP.png\",\"x\":0,\"y\":0,\"width\":570,\"height\":320.46852122986826,\"fromCenter\":false},{\"customtype\":\"image\",\"name\":\"logo\",\"source\":\"http://139.59.11.192/yiddishart/assets/images/white.jpg\",\"draggable\":true,\"x\":150,\"y\":150,\"rotate\":0,\"width\":1,\"aspectRat\":0,\"height\":1,\"layer\":true},{\"customtype\":\"text\",\"name\":\"text\",\"fillStyle\":\"#f0f0f0\",\"draggable\":true,\"strokeWidth\":2,\"x\":397,\"y\":70,\"lineHeight\":1,\"rotate\":0,\"language\":\"qwerty\",\"char_limit\":\"\",\"letterSpacing\":\"\",\"maxWidth\":300,\"align\":\"left\",\"fontSize\":32,\"layer\":true,\"fontStyle\":\"normal\",\"fontFamily\":\"Serif\",\"text\":\"asdfsdffa fdgdf dfgdf fff fgr fdgdfg\"}]\";','1568614569.jpeg','1',2,'2019-09-16 11:46:13','2019-09-16 11:46:34'),(4,3,'added','design','2020-05-27','09:52:09','s:1014:\"[{\"layer\":true,\"customtype\":\"image\",\"name\":\"default-image\",\"source\":\"http://134.209.151.198/yiddishart/assets/images/seller-design/3jPXLkh3zXN5.jpg\",\"x\":0,\"y\":0,\"width\":570,\"height\":331.53061224489795,\"fromCenter\":false},{\"customtype\":\"image\",\"name\":\"logo\",\"source\":\"http://134.209.151.198/yiddishart/assets/images/white.jpg\",\"draggable\":true,\"x\":150,\"y\":150,\"rotate\":0,\"width\":1,\"aspectRat\":0,\"height\":1,\"layer\":true},{\"customtype\":\"text\",\"name\":\"text\",\"fillStyle\":\"#f2f2f2\",\"draggable\":true,\"strokeWidth\":2,\"x\":281,\"y\":29,\"lineHeight\":1,\"rotate\":0,\"language\":\"qwerty\",\"char_limit\":\"\",\"letterSpacing\":\"\",\"maxWidth\":570,\"align\":\"left\",\"fontSize\":36,\"layer\":true,\"fontStyle\":\"bold\",\"fontFamily\":\"Tahoma\",\"text\":\"Title\"},{\"customtype\":\"text\",\"name\":\"text3\",\"fillStyle\":\"#000\",\"draggable\":true,\"strokeWidth\":2,\"x\":150,\"y\":100,\"lineHeight\":1,\"rotate\":0,\"maxWidth\":570,\"language\":\"qwerty\",\"char_limit\":\"\",\"letterSpacing\":\"\",\"align\":\"left\",\"fontSize\":32,\"layer\":true,\"fontFamily\":\"Serif\",\"fontStyle\":\"normal\",\"text\":\"\"}]\";','1590553324.jpeg','0',13,'2020-05-27 09:52:09','2020-05-27 09:52:09'),(5,3,'updated','design','2020-05-27','09:52:32','s:1014:\"[{\"layer\":true,\"customtype\":\"image\",\"name\":\"default-image\",\"source\":\"http://134.209.151.198/yiddishart/assets/images/seller-design/3jPXLkh3zXN5.jpg\",\"x\":0,\"y\":0,\"width\":570,\"height\":331.53061224489795,\"fromCenter\":false},{\"customtype\":\"image\",\"name\":\"logo\",\"source\":\"http://134.209.151.198/yiddishart/assets/images/white.jpg\",\"draggable\":true,\"x\":150,\"y\":150,\"rotate\":0,\"width\":1,\"aspectRat\":0,\"height\":1,\"layer\":true},{\"customtype\":\"text\",\"name\":\"text\",\"fillStyle\":\"#f2f2f2\",\"draggable\":true,\"strokeWidth\":2,\"x\":281,\"y\":29,\"lineHeight\":1,\"rotate\":0,\"language\":\"qwerty\",\"char_limit\":\"\",\"letterSpacing\":\"\",\"maxWidth\":570,\"align\":\"left\",\"fontSize\":36,\"layer\":true,\"fontStyle\":\"bold\",\"fontFamily\":\"Tahoma\",\"text\":\"Title\"},{\"customtype\":\"text\",\"name\":\"text3\",\"fillStyle\":\"#000\",\"draggable\":true,\"strokeWidth\":2,\"x\":150,\"y\":100,\"lineHeight\":1,\"rotate\":0,\"maxWidth\":570,\"language\":\"qwerty\",\"char_limit\":\"\",\"letterSpacing\":\"\",\"align\":\"left\",\"fontSize\":32,\"layer\":true,\"fontFamily\":\"Serif\",\"fontStyle\":\"normal\",\"text\":\"\"}]\";','1590553352.jpeg','1',13,'2020-05-27 09:52:32','2020-05-27 09:53:42');
/*!40000 ALTER TABLE `logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (56,'2014_10_12_000000_create_users_table',1),(57,'2014_10_12_100000_create_password_resets_table',1),(58,'2018_12_17_121201_create_roles_table',1),(59,'2018_12_17_121258_create_role_user_table',1),(60,'2018_12_19_090859_create_subscription_plan_table',1),(61,'2019_01_09_110104_create_businesses_table',1),(62,'2019_01_10_063748_create_userplans_table',1),(63,'2019_01_14_051909_create_designs_table',1),(64,'2019_03_14_055715_create_business_categories_table',1),(65,'2019_05_10_105116_create_logs_table',1),(66,'2019_05_17_173345_create_ratings_table',1),(67,'2019_05_21_180841_create_fonts_table',1),(68,'2019_06_03_131441_create_events_table',1),(69,'2019_06_17_172418_create_carts_table',1),(70,'2019_06_19_182905_create_orders_table',1),(71,'2019_06_19_183119_create_order_items_table',1),(72,'2019_06_20_104203_create_order_details_table',1),(73,'2019_08_18_132143_create_faq_qus_ans',1),(74,'2019_08_18_132250_create_faq_category',1),(75,'2019_08_21_150832_create_support_categories_table',1),(76,'2019_08_21_151634_create_support_qus_ans_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_details`
--

DROP TABLE IF EXISTS `order_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned NOT NULL,
  `shipping_first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_town` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_method` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_method` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_details_order_id_foreign` (`order_id`),
  CONSTRAINT `order_details_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_details`
--

LOCK TABLES `order_details` WRITE;
/*!40000 ALTER TABLE `order_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_items`
--

DROP TABLE IF EXISTS `order_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned NOT NULL,
  `design_id` int(10) unsigned NOT NULL,
  `design_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `design_img` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `design_data` longtext COLLATE utf8mb4_unicode_ci,
  `price` double(8,2) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `total_price` double(8,2) DEFAULT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_items_design_id_foreign` (`design_id`),
  KEY `order_items_order_id_foreign` (`order_id`),
  CONSTRAINT `order_items_design_id_foreign` FOREIGN KEY (`design_id`) REFERENCES `designs` (`id`),
  CONSTRAINT `order_items_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_items`
--

LOCK TABLES `order_items` WRITE;
/*!40000 ALTER TABLE `order_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `transaction_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subtotal` double(8,2) DEFAULT NULL,
  `shipping` double(8,2) DEFAULT NULL,
  `total_amount` double(8,2) DEFAULT NULL,
  `transaction_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payer_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transaction_amount` double(8,2) DEFAULT NULL,
  `transaction_method` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transaction_state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transaction_time` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `orders_user_id_foreign` (`user_id`),
  CONSTRAINT `orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
INSERT INTO `password_resets` VALUES ('manish@yopmail.com','$2y$10$4e9Cl6Qp.K46GdG8je9I9euzZ2pEMRs7V9MhBQ45kzBrexOj.blvu','2020-06-05 17:53:12'),('anil@yopmail.com','$2y$10$6Je.ZyHNDfDgL82UTKpkhewiK6kBFlQFDaVeV2v1lnlD77zzvyAzq','2020-06-06 12:40:57');
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ratings`
--

DROP TABLE IF EXISTS `ratings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ratings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `design_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `favourite` int(11) DEFAULT NULL,
  `download` int(11) DEFAULT NULL,
  `rating` int(11) DEFAULT NULL,
  `review` longtext COLLATE utf8mb4_unicode_ci,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ratings_design_id_foreign` (`design_id`),
  KEY `ratings_user_id_foreign` (`user_id`),
  CONSTRAINT `ratings_design_id_foreign` FOREIGN KEY (`design_id`) REFERENCES `designs` (`id`),
  CONSTRAINT `ratings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ratings`
--

LOCK TABLES `ratings` WRITE;
/*!40000 ALTER TABLE `ratings` DISABLE KEYS */;
INSERT INTO `ratings` VALUES (1,1,4,NULL,1,NULL,NULL,'0','2019-08-29 16:01:20','2019-08-29 16:01:20'),(2,2,14,1,NULL,NULL,NULL,'0','2020-05-28 15:41:57','2020-05-28 15:41:57');
/*!40000 ALTER TABLE `ratings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_user`
--

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
INSERT INTO `role_user` VALUES (1,1,1,'2019-08-28 10:33:33','2019-08-28 10:33:33'),(2,3,2,'2019-08-28 10:33:33','2019-08-28 10:33:33'),(4,2,4,'2019-08-29 12:32:28','2019-08-29 12:32:28'),(5,3,5,'2019-08-29 12:43:29','2019-08-29 12:43:29'),(6,3,6,'2019-10-16 12:03:13','2019-10-16 12:03:13'),(7,2,7,'2019-10-16 12:05:11','2019-10-16 12:05:11'),(8,2,8,'2020-02-02 11:31:35','2020-02-02 11:31:35'),(9,2,9,'2020-05-26 11:36:03','2020-05-26 11:36:03'),(10,2,10,'2020-05-26 11:42:24','2020-05-26 11:42:24'),(11,2,11,'2020-05-26 11:49:12','2020-05-26 11:49:12'),(12,3,12,'2020-05-26 12:18:25','2020-05-26 12:18:25'),(13,3,13,'2020-05-26 15:15:04','2020-05-26 15:15:04'),(14,2,14,'2020-05-26 15:16:24','2020-05-26 15:16:24'),(15,2,15,'2020-06-05 17:47:53','2020-06-05 17:47:53');
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'admin','2019-08-28 16:03:33','2019-08-28 16:03:33'),(2,'buyer','2019-08-28 16:03:33','2019-08-28 16:03:33'),(3,'seller','2019-08-28 16:03:33','2019-08-28 16:03:33');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscription_plan`
--

DROP TABLE IF EXISTS `subscription_plan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscription_plan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `offerprice` int(11) DEFAULT NULL,
  `uploads` int(11) DEFAULT NULL,
  `storage` int(11) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `storagelimit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` longtext COLLATE utf8mb4_unicode_ci,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `addedby` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscription_plan`
--

LOCK TABLES `subscription_plan` WRITE;
/*!40000 ALTER TABLE `subscription_plan` DISABLE KEYS */;
INSERT INTO `subscription_plan` VALUES (1,'Free Trial',0,0,5,20,NULL,'MB','free-plan.png',NULL,'1',1,'2019-08-28 16:03:33','2019-08-28 16:03:33'),(2,'Entry Level',49,0,50,100,NULL,'MB','entry-level.png',NULL,'1',1,'2019-08-28 16:03:33','2019-08-28 16:03:33'),(3,'Intermediate Plan',79,0,150,500,NULL,'MB','intermediate-plan.png',NULL,'1',1,'2019-08-28 16:03:33','2019-08-28 16:03:33'),(4,'Expert Level',99,NULL,250,5,NULL,'GB','expert-level.png',NULL,'1',1,'2019-08-28 16:03:33','2019-09-04 16:15:55');
/*!40000 ALTER TABLE `subscription_plan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `support_categories`
--

DROP TABLE IF EXISTS `support_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `support_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `added_by` int(10) unsigned DEFAULT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `support_categories_added_by_foreign` (`added_by`),
  CONSTRAINT `support_categories_added_by_foreign` FOREIGN KEY (`added_by`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `support_categories`
--

LOCK TABLES `support_categories` WRITE;
/*!40000 ALTER TABLE `support_categories` DISABLE KEYS */;
INSERT INTO `support_categories` VALUES (4,'test','testing purpose',1,'1','2019-09-05 12:34:08','2019-09-05 12:34:08');
/*!40000 ALTER TABLE `support_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `support_qus_ans`
--

DROP TABLE IF EXISTS `support_qus_ans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `support_qus_ans` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question` text COLLATE utf8mb4_unicode_ci,
  `answer` longtext COLLATE utf8mb4_unicode_ci,
  `added_by` int(10) unsigned DEFAULT NULL,
  `common_status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `category_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `support_qus_ans`
--

LOCK TABLES `support_qus_ans` WRITE;
/*!40000 ALTER TABLE `support_qus_ans` DISABLE KEYS */;
/*!40000 ALTER TABLE `support_qus_ans` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_plan`
--

DROP TABLE IF EXISTS `user_plan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_plan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `plan_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_plan`
--

LOCK TABLES `user_plan` WRITE;
/*!40000 ALTER TABLE `user_plan` DISABLE KEYS */;
INSERT INTO `user_plan` VALUES (1,1,2,'2019-08-28 16:58:16','2019-08-28 16:58:16'),(2,1,5,'2019-08-29 12:50:36','2019-08-29 12:50:36');
/*!40000 ALTER TABLE `user_plan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` longtext COLLATE utf8mb4_unicode_ci,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT '1 for Active, 0 for Inactive',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Admin',NULL,NULL,NULL,'admin12@yopmail.com','QU2ezYW68fsT.jpg',NULL,NULL,'2019-08-28 00:00:00','$2y$10$4DNTNGkztCkG3wUh0/C9seNNyUNdBsrj3KfwXrhmoiLPHUcCNyGzC','1','JlNA4gZeM7gWXu20TmVLVGIfw94T1qX8QkB4OSSeE4LK9Is3rYzsGSxNPs0N','2019-08-28 16:03:33','2020-06-06 07:34:32'),(2,'seller',NULL,NULL,NULL,'seller@yopmail.com',NULL,NULL,NULL,'2019-08-28 00:00:00','$2y$10$4ZOZEOVenwYJ7ySf1/3Un.gI.n69w6Cf93kU3tVZAnL9nGmkwPmSS','1','sHt5QDmlC5Dr0jraV2hzIO4EsENH5wIpibKDtF7nwEzIi8bsEcknSLwY5ZlG','2019-08-28 16:03:33','2019-09-16 06:17:44'),(4,'bhumika buyer','bhumika','buyer','bhumika','bhumika@1wayit.com',NULL,NULL,NULL,'2019-08-29 00:00:00','$2y$10$TVCJgb4DAUbATMK8wtAuiOja73bbo7.wKkx.M/Ns0Xuzu0iQcH3F2','1','3V6tqBMLrOQhcuvVhwB4vcLiNMZcNzhWQoKVqvCBDA96nFiktH5geYr34neq','2019-08-29 12:32:28','2019-09-05 07:17:47'),(5,'bhumi seller','bhumi','seller','bhumi','bhumika1wayit@gmail.com','','989898989898','Female','2019-08-29 00:00:00','$2y$10$Jir5s10535igNyI0c2wc.Ok39Sl7zh/g75PCfU3f63dlJXdWtkwgO','1','OeMOegMG8tUjqxd6z6ximNRBeTTST8p3QH8UD34MPtQnHIadFqne750vX9I7','2019-08-29 12:43:29','2019-09-11 10:29:02'),(6,'test user','test','user','test123','test123@yopmail.com','','1212121212','Male','2019-10-16 00:00:00','$2y$10$MSVc57xDjoW.SNBFkhrcXO4p5i5Fhpa9tMxTZngJLc30ATqyo8ObK','1','SWN7ozx05W3LiOyo8RBqxtMX6YDHWUEA6wE8cPKhy33rY3oGFaRn9mhAL4Pt','2019-10-16 12:03:13','2019-10-16 06:34:40'),(7,'buyer user','buyer','user','buyer123','buyer123@yopmail.com',NULL,NULL,NULL,NULL,'$2y$10$KSe6KBczQi57nJwDf.UKkOMHgqRCUfM.JxkJux8KBe7gUxtIt.4L2','1','PGZzXxOK4NhQ02Gpx1HGTsKwkQlmDSjVDpIzTYcpk6nNLC0n3xVINWiwG7cd','2019-10-16 12:05:11','2020-05-28 08:34:31'),(8,'JamesSePVG JamesSePVG','JamesSePVG','JamesSePVG','JamesSeP','s.z.y.m.anskiashley5@gmail.com',NULL,NULL,NULL,'2020-02-11 00:00:00','$2y$10$gZzbjWiQ1EhyF4XHM4nDE.lGRgEQRH557uuXQ3uy6wZ7LRM12sl6u','1','1w7PJJNH5MqS2fQyFzXSjIKre8DEhPuR9jkFGJJLRJ5CuC9XgmwsA3fF0mhf','2020-02-02 11:31:34','2020-02-11 02:58:02'),(11,'manish kumar','manish','kumar','manish27','manish@yopmail.com',NULL,NULL,NULL,'2020-05-26 00:00:00','$2y$10$Kfb4GEHomVAgHYi4TIkcbumw5xsdNpOujj065sLZp/vpWusVSPXPO','1','4b4peZuikA6fLBD5LCxJGClyud0YrrMC8s0yDtmKyJuOaFxcdxHcezqUi3FL','2020-05-26 11:49:12','2020-06-05 12:06:40'),(12,'Anil Thakur','Anil','Thakur','anil27','anil@yopmail.com','','7986726321','Male','2020-05-26 00:00:00','$2y$10$2HGZPcuGVN/.Z2iQ5K9XMuum2/DiICr5rkJ7/7OTclu8AK.3EKreu','1','bmEwSXXLhK4fV9EaCa3HK0TNSn6fU9f6OBErybQslPVLIWz4VDNjusBIcdy1','2020-05-26 12:18:25','2020-06-06 07:34:09'),(13,'Bhumika rajput','Bhumika','rajput','buyer','bhumika123@yopmail.com','','1111111111','Female','2020-05-26 00:00:00','$2y$10$fwd0VXYt0UrYaY1Jk7rd8e1Pr8WJgo3VmQpgcX73uWr.pH8UDIcdi','1','U3SpBnF4MnagkVFRMw5lsUfnzdKedlLlXSST711ysab4nR1R55gULiCJ7Y3w','2020-05-26 15:15:04','2020-06-05 09:39:14'),(14,'Bhumika rajput','Bhumika','rajput','buyer','bhumika11@yopmail.com',NULL,'8988774455','Female','2020-05-26 00:00:00','$2y$10$nPfUXltFoD8FPn5kQVHGbeObss6R1COyOJlEs1XAAsFk9DwpL4Wru','1','reRgVvsDwHSuKvoqN0HFpoAsraChX3OxazEqoTh9DNA8s4YQq2M3uQT98NCM','2020-05-26 15:16:24','2020-06-05 09:08:20'),(15,'Abhay Thakur','Abhay','Thakur','abhay','abhay@yopmail.com',NULL,NULL,NULL,'2020-06-05 00:00:00','$2y$10$DZRZezcxTmkBssvW31aGNe4vctlqxtNFT0E9tElSpzaDu4vWPBXEy','1','VJbBLu3lHDrEC4bL0jLU1YGY8CoJizHF2DLeCPOJvioEzttPJOr74nm3DLm5','2020-06-05 17:47:53','2020-06-06 05:22:58');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-06  9:28:08
