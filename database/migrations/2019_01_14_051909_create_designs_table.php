<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDesignsTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::create('designs', function (Blueprint $table) {
      $table->increments('id');
      $table->string('name')->nullable();
      $table->float('price', 8, 2)->nullable();
      $table->longText('description')->nullable();
      $table->longText('keyword')->nullable();
      $table->integer('category')->nullable();
      $table->string('size')->nullable();
      $table->string('actualHeight')->nullable();
      $table->string('actualWidth')->nullable();
      $table->string('aspectRatio')->nullable();
      $table->longText('canvasData')->nullable();
      $table->string('designimage')->nullable();
      $table->boolean('is_active')->default(1)->nullable();
      $table->enum('status', ['0', '1', '2'])->default(0)->nullable()->comment('0 = waiting for approval, 1 = Approved, 2 = Reject');
      $table->integer('added_by')->unsigned();
      $table->timestamps();
    });
    Schema::table('designs', function($table) {
      $table->foreign('added_by')->references('id')->on('users');
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::dropIfExists('designs');
  }
}
