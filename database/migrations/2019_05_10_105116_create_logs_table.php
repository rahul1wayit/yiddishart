<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('log_id')->nullable();
            $table->string('action');
            $table->string('type');
            $table->date('added_date');
            $table->time('added_time');
            $table->longText('log_data')->nullable();
            $table->string('log_image')->nullable();
            $table->enum('status', ['0', '1', '2'])->default(0)->nullable()->comment('0 = waiting for approval, 1 = Approved, 2 = Reject');
            $table->integer('added_by')->unsigned();
            $table->timestamps();
        });
        Schema::table('logs', function($table) {
          $table->foreign('added_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logs');
    }
}
