<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('design_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('design_img')->nullable();
            $table->longText('design_data')->nullable();
            $table->float('price')->nullable();
            $table->integer('qty')->nullable();
            $table->enum('status', ['0', '1'])->default(0)->nullable();
            $table->timestamps();
        });
        Schema::table('cart_items', function($table) {
          $table->foreign('design_id')->references('id')->on('designs');
        });
        Schema::table('cart_items', function($table) {
          $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart_items');
    }
}
