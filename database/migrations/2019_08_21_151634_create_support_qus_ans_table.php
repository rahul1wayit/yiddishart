<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupportQusAnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('support_qus_ans', function (Blueprint $table) {
            $table->increments('id');
            $table->text('question')->nullable();
            $table->longText('answer')->nullable();
            $table->integer('added_by')->unsigned()->nullable();
            $table->enum('common_status', ['0', '1'])->default(0);
            $table->enum('status', ['0', '1'])->default(0);
            $table->integer('category_id')->nullable();
            $table->timestamps();
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('support_qus_ans');
    }
}
