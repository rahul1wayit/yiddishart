<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionPlanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscription_plan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->integer('price')->nullable();
            $table->integer('offerprice')->nullable();
            $table->integer('uploads')->nullable();
            $table->integer('storage')->nullable();
            $table->integer('size')->nullable();
            $table->string('storagelimit')->nullable();
            $table->longText('image')->nullable();
            $table->string('color')->nullable();
            $table->string('status')->default(1);
            $table->integer('addedby')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscription_plan');
    }
}
