<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_number');
            $table->integer('user_id')->unsigned();
            $table->string('transaction_id');
            $table->float('subtotal')->nullable();
            $table->float('shipping')->nullable();
            $table->float('total_amount')->nullable();
            $table->string('transaction_token')->nullable();
            $table->string('payer_id')->nullable();
            $table->float('transaction_amount')->nullable();
            $table->string('transaction_method')->nullable();
            $table->string('transaction_state')->nullable();
            $table->string('transaction_time')->nullable();
            $table->enum('status', ['0', '1'])->default(1)->nullable();
            $table->timestamps();
        });

        Schema::table('orders', function($table) {
          $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
