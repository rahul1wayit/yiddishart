<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaqQusAns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('faq_qus_ans', function (Blueprint $table) {
            $table->increments('id');
            $table->text('question')->nullable();
            $table->longText('answer')->nullable();
            $table->enum('status', ['0', '1'])->default(0);
            $table->integer('added_by')->unsigned()->nullable();
            $table->timestamps();
            $table->foreign('added_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('faq_qus_ans');
    }
}
