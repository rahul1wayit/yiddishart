<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ratings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('design_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('favourite')->nullable();
            $table->integer('download')->nullable();
            $table->integer('rating')->nullable();
            $table->longText('review')->nullable();
            $table->enum('status', ['0', '1'])->default(0)->nullable();
            $table->timestamps();
        });
        Schema::table('ratings', function($table) {
          $table->foreign('design_id')->references('id')->on('designs');
        });
        Schema::table('ratings', function($table) {
          $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ratings');
    }
}
