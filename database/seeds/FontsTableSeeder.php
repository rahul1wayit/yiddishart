<?php

use Illuminate\Database\Seeder;
use App\Models\Fonts;

class FontsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $font = new Fonts();
      $font->name = 'Serif';
      $font->status = '1';
      $font->added_by = '1';
      $font->save();

      $font = new Fonts();
      $font->name = 'Arial';
      $font->status = '1';
      $font->added_by = '1';
      $font->save();

      $font = new Fonts();
      $font->name = 'Arial Black';
      $font->status = '1';
      $font->added_by = '1';
      $font->save();

      $font = new Fonts();
      $font->name = 'Courier';
      $font->status = '1';
      $font->added_by = '1';
      $font->save();

      $font = new Fonts();
      $font->name = 'Courier New';
      $font->status = '1';
      $font->added_by = '1';
      $font->save();

      $font = new Fonts();
      $font->name = 'Comic Sans MS';
      $font->status = '1';
      $font->added_by = '1';
      $font->save();

      $font = new Fonts();
      $font->name = 'Helvetica';
      $font->status = '1';
      $font->added_by = '1';
      $font->save();

      $font = new Fonts();
      $font->name = 'Impact';
      $font->status = '1';
      $font->added_by = '1';
      $font->save();

      $font = new Fonts();
      $font->name = 'Lucida Grande';
      $font->status = '1';
      $font->added_by = '1';
      $font->save();

      $font = new Fonts();
      $font->name = 'Lucida Sans';
      $font->status = '1';
      $font->added_by = '1';
      $font->save();

      $font = new Fonts();
      $font->name = 'Tahoma';
      $font->status = '1';
      $font->added_by = '1';
      $font->save();

      $font = new Fonts();
      $font->name = 'Times';
      $font->status = '1';
      $font->added_by = '1';
      $font->save();

      $font = new Fonts();
      $font->name = 'Times New Roman';
      $font->status = '1';
      $font->added_by = '1';
      $font->save();

      $font = new Fonts();
      $font->name = 'Verdana';
      $font->status = '1';
      $font->added_by = '1';
      $font->save();
    }
}
