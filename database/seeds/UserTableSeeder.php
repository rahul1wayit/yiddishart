<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_admin = Role::where('name', 'admin')->first();
        $admin = new User();
        $admin->name = 'Admin';
        $admin->email = 'admin12@yopmail.com';
        $admin->password = bcrypt('admin786');
        $admin->email_verified_at = date('Y-m-d');
        $admin->save();
        $admin->roles()->attach($role_admin);

        $role_seller = Role::where('name', 'seller')->first();
        $seller = new User();
        $seller->name = 'seller';
        $seller->email = 'seller@yopmail.com';
        $seller->password = bcrypt('admin786');
        $seller->email_verified_at = date('Y-m-d');
        $seller->save();
        $seller->roles()->attach($role_seller);

        $role_buyer = Role::where('name', 'buyer')->first();
        $buyer = new User();
        $buyer->name = 'buyer';
        $buyer->email = 'buyer@yopmail.com';
        $buyer->password = bcrypt('admin786');
        $buyer->email_verified_at = date('Y-m-d');
        $buyer->save();
        $buyer->roles()->attach($role_buyer);
    }
}
