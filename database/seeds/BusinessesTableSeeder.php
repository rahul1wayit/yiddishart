<?php

use Illuminate\Database\Seeder;
use App\Models\Business;


class BusinessesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $admin = new Business();
      $admin->user_id = 1;
      $admin->save();
      $admin = new Business();
      $admin->user_id = 2;
      $admin->save();
      $admin = new Business();
      $admin->user_id = 3;
      $admin->save();
    }
}
