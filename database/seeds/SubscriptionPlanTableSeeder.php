<?php

use Illuminate\Database\Seeder;
use App\Models\Subscriptionplan;

class SubscriptionPlanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subscriptionplanFree = new Subscriptionplan();
        $subscriptionplanFree->title = 'Free Trial';
        $subscriptionplanFree->price = '0';
        $subscriptionplanFree->offerprice = '0';
        $subscriptionplanFree->uploads = '5';
        $subscriptionplanFree->storage = '20';
        $subscriptionplanFree->storagelimit = 'MB';
        // $subscriptionplanFree->description = '';
        $subscriptionplanFree->image = 'free-plan.png';
        $subscriptionplanFree->addedby = '1';
        $subscriptionplanFree->save();

        $subscriptionplanEntryLevel = new Subscriptionplan();
        $subscriptionplanEntryLevel->title = 'Entry Level';
        $subscriptionplanEntryLevel->price = '49';
        $subscriptionplanEntryLevel->offerprice = '0';
        $subscriptionplanEntryLevel->uploads = '50';
        $subscriptionplanEntryLevel->storage = '100';
        $subscriptionplanEntryLevel->storagelimit = 'MB';
        // $subscriptionplanEntryLevel->description = '';
        $subscriptionplanEntryLevel->image = 'entry-level.png';
        $subscriptionplanEntryLevel->addedby = '1';
        $subscriptionplanEntryLevel->save();

        $subscriptionplanIntermediatePlan = new Subscriptionplan();
        $subscriptionplanIntermediatePlan->title = 'Intermediate Plan';
        $subscriptionplanIntermediatePlan->price = '79';
        $subscriptionplanIntermediatePlan->offerprice = '0';
        $subscriptionplanIntermediatePlan->uploads = '150';
        $subscriptionplanIntermediatePlan->storage = '500';
        $subscriptionplanIntermediatePlan->storagelimit = 'MB';
        // $subscriptionplanIntermediatePlan->description = '';
        $subscriptionplanIntermediatePlan->image = 'intermediate-plan.png';
        $subscriptionplanIntermediatePlan->addedby = '1';
        $subscriptionplanIntermediatePlan->save();

        $subscriptionplanExpertLevel = new Subscriptionplan();
        $subscriptionplanExpertLevel->title = 'Expert Level';
        $subscriptionplanExpertLevel->price = '99';
        $subscriptionplanExpertLevel->offerprice = '0';
        $subscriptionplanExpertLevel->uploads = '250';
        $subscriptionplanExpertLevel->storage = '1';
        $subscriptionplanExpertLevel->storagelimit = 'GB';
        // $subscriptionplanExpertLevel->description = '';
        $subscriptionplanExpertLevel->image = 'expert-level.png';
        $subscriptionplanExpertLevel->addedby = '1';
        $subscriptionplanExpertLevel->save();
    }
}
