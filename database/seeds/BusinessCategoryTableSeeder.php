<?php

use Illuminate\Database\Seeder;
use App\Models\Business_category;

class BusinessCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $role_admin = new Business_category();
      $role_admin->name = 'School';
      $role_admin->image = 'School_icon.png';
      $role_admin->slug = 'school';
      $role_admin->save();
      $role_buyer = new Business_category();
      $role_buyer->name = 'Simcha';
      $role_buyer->image = 'Simcha_icon.png';
      $role_buyer->slug = 'simcha';
      $role_buyer->save();
      $role_seller = new Business_category();
      $role_seller->name = 'Annual';
      $role_seller->image = 'Annual_icon.png';
      $role_seller->slug = 'annual';
      $role_seller->save();
      $role_seller = new Business_category();
      $role_seller->name = 'Holiday';
      $role_seller->image = 'Holiday_icon.png';
      $role_seller->slug = 'holiday';
      $role_seller->save();
      $role_seller = new Business_category();
      $role_seller->name = 'Shul';
      $role_seller->image = 'Shul_icon.png';
      $role_seller->slug = 'shul';
      $role_seller->save();
    }
}
