<?php
namespace App\Helpers;
use DB;
use App\Role;
use App\Models\Logs;
use App\Models\OrderItem;
use Mail;
use Auth;
use App\Models\Order;

class GlobalFunctions
{
  public static function getRoleWithoutAdmin()
  {
    $getRoleClass = Role::where('id', '!=' , 1);
    if ($getRoleClass->count()>0)
    {
      $returnData["status"] = 1;
      $returnData["getRole"] = $getRoleClass->get()->toArray();
    }
    else
    {
      $returnData["status"] = 0;
      $returnData["getRole"] = "";
    }
    return $returnData;
  }

  public static function numberFormat($foo){
    return '$ '.number_format((float)$foo, 2, '.', ',');
  }

  public static function dateFormat($date, $format){
    return date($format,strtotime($date));
  }

  public static function getTablaData($table){
    $arr = DB::table($table)->orderBy('name','ASC')->get();
    return $arr;
  }

  public static function getTablaDataOrderBy($table,$orderBy_col,$orderBy,$whereCol=null,$whereVal=null){
    if($whereCol != '')
    $arr = DB::table($table)->where($whereCol,$whereVal)->orderBy($orderBy_col,$orderBy)->get();
    else
    $arr = DB::table($table)->orderBy($orderBy_col,$orderBy)->get();
    return $arr;
  }

  public static function getFonts(){
    $arr = DB::table('fonts')->whereIN('added_by',['1',Auth::user()->id])->orderBy('id','asc')->get();
    return $arr;
  }

  //function to send mail
  public static function sendmail($from,$fromname,$to,$toname,$subject,$data,$mailview,$attachment=null){
    $response =Mail::send($mailview, $data, function($message) use ($from,$fromname,$to,$toname,$subject,$attachment){
      $message->from($from,$fromname);
      $message->to($to,$toname);
      $message->subject($subject);
      if($attachment != '')
      $message->attach($attachment);
    });

    if(Mail::failures()){
      $response = 0;
    }else
    {
      $response = 1;
    }
    return $response;
  }

  //function to get logs
  public static function get_logs($added_by,$designid){
    $response = Logs::where(['added_by'=>$added_by,'log_id'=>$designid])->orderBy('created_at','desc')->paginate('100');
    return $response;
  }

  public static function order_number()
  {
    $latestOrder = Order::orderBy('created_at','DESC')->first();
    if(!empty($latestOrder))
    {
      $orderIdd = $latestOrder->id;
    }else
    {
      $orderIdd = 0;
    }
    $invoiceId = 'ORDER'.date('Y').date('m').str_pad($orderIdd + 1, 4, "0", STR_PAD_LEFT);
    return $invoiceId;
  }

  //function to get logs
  public static function get_order_details($designid, $userId){
    $response = Order::with('order_details','order_items')->whereHas('order_items', function($q) use ($designid){
      $q->where('design_id', $designid);
    })->where('user_id', $userId)
    ->orderBy('id','desc')->get();
    return $response;
  }

  //function to seller dashboard logs
  public static function get_seller_logs($added_by){
    $response = Logs::where(['action'=> 'added','added_by' => $added_by])->orderBy('created_at','desc')->limit('2')->get();
    return $response;
  }

  // get transaction Revenue
  public static function getTransactionRevenue($type){

    if($type == 'total')
    $arr = Order::sum('total_amount');
    if($type == 'month')
    {
      $start_date = date('Y-m-01');
      $end_date  = date('Y-m-t 23:59:59');
      $arr = Order::whereBetween('created_at', [$start_date,$end_date])->sum('total_amount');
    }
    if($type == 'week')
    {
      $ts = strtotime(date('Y-m-d'));
      $start = (date('w', $ts) == 0) ? $ts : strtotime('last monday', $ts);
      $start_date = date('Y-m-d', $start);
      $end_date = date('Y-m-d', strtotime('next sunday', $start));
      $arr = Order::whereBetween('created_at', [$start_date,$end_date])->sum('total_amount');
    }
    if($type == 'today')
    {
      $arr = Order::where('created_at','LIKE',date('Y-m-d').'%')->sum('total_amount');
    }
    return self::numberFormat($arr);
  }

  // after some string add ...
  public static function substrwords($text, $maxchar, $end='...') {
    if (strlen($text) > $maxchar || $text == '') {
        $words = preg_split('/\s/', $text);
        $output = '';
        $i      = 0;
        while (1) {
            $length = strlen($output)+strlen($words[$i]);
            if ($length > $maxchar) {
                break;
            }
            else {
                $output .= " " . $words[$i];
                ++$i;
            }
        }
        $output .= $end;
    }
    else {
        $output = $text;
    }
    return $output;
  }

 public static function changeSupportUrl( $string ){
    return str_replace(' ', '-', strtolower($string));
 }
}
?>
