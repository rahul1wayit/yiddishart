<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Console\GeneratorCommand;
class TraitMakeCommand extends GeneratorCommand
{
  /**
  * The name and signature of the console command.
  *
  * @var string
  */
  // protected $signature = 'command:name';

  protected $name = 'make:trait';
  /**
  * The console command description.
  *
  * @var string
  */
  protected $description = 'Create a new trait';

  protected function getStub()
  {
    return __DIR__.'/stubs/trait.stub';
  }
  protected function getDefaultNamespace($rootNamespace)
  {
    return $rootNamespace.'\Traits';
  }
}
