<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubscriptionplanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'price' => 'required|integer',
            // 'image' => 'mimes:jpeg,jpg,png,gif|required',
        ];
    }
    public function messages()
    {
        return [
            'title.required' => 'The name is required.',
        ];
    }
}
