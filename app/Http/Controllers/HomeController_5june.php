<?php

namespace App\Http\Controllers;
use App\Models\Design;
use App\Models\Rating;
use Illuminate\Http\Request;
use Auth;
use DB;
use App\User;
use Helper;
use Validator;
use App\Traits\CategoryTrait;
use App\Models\Faq_qus_ans;
use App\Models\SupportCategory;
use App\Models\SupportQusAns;

class HomeController extends Controller
{
    use CategoryTrait;
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        //$this->middleware('auth');
        // $this->middleware(['auth', 'verified']);
    }

    /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
    public function index()
    {
        $categories =  $this->categoryAll();
        return view('home',compact('categories'));
    }
    //search
    public function frontSearch(Request $request)
    {
        if($request->ajax())
        {
            $search = trim($request->input('searchName'));
            $searchCat = $request->input('searchCat');
            $column = $request->input('column');
            $order = $request->input('order');
            if(isset($column))
            $column = $column;
            else
            $column = 'id';

            if(isset($order))
            $order = $order;
            else
            $order = 'desc';

            if($searchCat)
            {
                $searchCat = $searchCat;
            }else {
                $searchCat = '';
            }
            // echo Auth()->user()->id; die;
            if (false != Auth::check())
            {
                $result = Design::where('status','1')->where('name','LIKE','%'.$search.'%');
                $result->with('business_category','totalRating');
                $result->with(['ratings'=>function($r){
                    $r->where('user_id',Auth()->user()->id);
                }]);
                if($searchCat != '')
                $result->where('category',$searchCat);
                $result->orderBy($column,$order);
                $result = $result->paginate('12');
            }
            else
            {
                $result = Design::with('business_category','totalRating')
                ->where(function($q) use ($search,$searchCat){
                    $q->where('name','LIKE','%'.$search.'%');
                    if($searchCat)
                    $q->where('category',$searchCat);
                })
                ->where('status','1')
                ->orderBy($column,$order)
                ->paginate('12');
            }
            if($order == 'desc')
            $order = 'asc';
            else
            $order = 'desc';
        }
        // echo '<pre>';   print_r($result->ratings); die;
        return view('search-result',compact('result','order','column'));
    }

    public function categoryWise(Request $request,$slug){
            $column = 'id';
            $order  = 'asc';
            if (false != Auth::check())
            {
                //DB::enableQueryLog();
                $result = Design::with('business_category','totalRating')
                            ->with(['ratings'=>function($r){
                                    $r->where('user_id',Auth()->user()->id);
                                }])
                            ->whereHas('business_category',function($q) use ($slug){
                                $q->where('slug',$slug);
                            })
                            ->where('status','1')
                            ->orderBy($column,$order)
                            ->paginate('12');
                // $queries = DB::getQueryLog();
                // print_r($queries);
            }
            else
            {
                $result = Design::with('business_category','totalRating')
                            ->whereHas('business_category',function($q) use ($slug){
                                $q->where('slug',$slug);
                            })
                            ->where('status','1')
                            ->orderBy($column,$order)
                            ->paginate('12');
            }
            if($order == 'desc')
            $order = 'asc';
            else
            $order = 'desc';;
            return view('search-result-category',compact('result','order','column'));
    }

    public function contactUs(Request $request)
    {
        if($request->ajax())
        {
            {
                $name = $request->name;
                $email      = User::select('email')->where('id',1)->first()->email;
                // $email = $request->email;;
                $phone = $request->phone;
                $mailmessage = 'New contact message received.';
                $maildata	= array('data'=>$request);
                $mailresult = Helper::sendmail('noreply@domain.com', 'Yiddishart', $email, 'User', 'New contact message', $maildata, 'mail.contact');

                $res['success']         = true;
                $res['url']             = URL('contact-us');
                $res['delayTime']       = '3000';
                $res['success_message'] = 'Your request for contact is sent.';
            }
            echo json_encode($res); die;
        }
        return view('footer-pages/contact-us');
    }

    public function supportCatoryWise(Request $request ,$id){
        $supportData = SupportCategory::with('support_qus_ans')
                                ->whereHas('support_qus_ans',function($q){
                                    $q->where('status', '1');
                                    })
                                ->where('id', $id)
                                ->where('status', '1')
                                ->orderBy('id','desc')
                                 ->get();
        return view('footer-pages/support-details', ["supportData" => $supportData]);
    }

    public function supportCenter(Request $request)
    {
        if( $request->has('search') && !empty($request->input('search'))){
            $search = $request->search;
            $supportData = SupportCategory::whereHas('support_qus_ans',
                                    function($q) use ($search){
                                            $q->whereRaw( "( question like '%$search%' OR answer like '%$search%' )" );
                                            $q->where('status', '1');
                                        })
                                    ->where('status', '1')
                                    ->orderBy('id','desc')
                                    ->get();
        }else{
            $search="";
            $supportData = SupportCategory::where('status', '1')
                                        ->orderBy('id','desc')
                                        ->get();
        }
        return view('footer-pages/support-center',["supportData"=>$supportData,'search'=>$search]);
    }

    public function supportCenterAnswer(Request $request, $id)
    {
     $supportData = SupportQusAns::with('support_category')
                                    ->whereHas('support_category',function($q){
                                        $q->where('status', '1');
                                        })
                                    ->where('status', '1')
                                    ->where('id', $id)
                                    ->orderBy('id','desc')
                                    ->first();
        return view('footer-pages/support-answer',["supportData"=>$supportData]);
    }

    public function faq()
    {
        $getQues = Faq_qus_ans::where('status', '1')->orderBy('id','desc')->get()->toArray();
        return view('footer-pages/faq', ["getQues" => $getQues]);
    }
    public function aboutUs()
    {
        return view('footer-pages/about-us');
    }
    public function events()
    {
        $events =  $this->eventAll();
        return view('footer-pages/events',compact('events'));
    }
    public function careers()
    {
        return view('footer-pages/careers');
    }
    public function categories()
    {
        $categories =  $this->categoryAll();
        return view('footer-pages/categories',compact('categories'));
    }
    public function template(Request $request)
    {
        $design_id =  \Crypt::decrypt($request->id);
        $design = Design::with('business_category','totalRating')->find($design_id);
        // echo '<pre>'; print_r($design->toArray()); die;
        return view('template',['design'=>$design ]);
    }
}
