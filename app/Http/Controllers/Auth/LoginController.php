<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Redirect;
use Session;
use Validator;


class LoginController extends Controller
{
  /*
  |--------------------------------------------------------------------------
  | Login Controller
  |--------------------------------------------------------------------------
  |
  | This controller handles authenticating users for the application and
  | redirecting them to your home screen. The controller uses a trait
  | to conveniently provide its functionality to your applications.
  |
  */

  use AuthenticatesUsers;

  /**
  * Where to redirect users after login.
  *
  * @var string
  */
  protected $redirectTo = '/admin/dashboard';

  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    $this->middleware('guest')->except('logout');
  }

  // public function redirectTo() {
  //     if(Auth::user()->roles()->count()!=0)
  //     {
  //       $status = Auth::user()->status;
  //       if($status == 0)
  //       {
  //         Redirect::to('/login')->send();
  //       }else{
  //         $role_name = Auth::user()->roles()->first()->name;
  //         return $role_name.'/dashboard';
  //         }
  //     }
  //     else {
  //         return '/';
  //     }
  // }
  public function login(Request $request)
  {
    $rules = array(
      'email' => 'required', // make sure the email is an actual email
      'password' => 'required'
    );
    $remember = ($request->has('remember')) ? true : false;
    // password has to be greater than 3 characters and can only be alphanumeric and);
    // checking all field
    $validator = Validator::make($request->all() , $rules);
    // if the validator fails, redirect back to the form
    if ($validator->fails())
    {
      $errors             	 = $validator->errors();
      $response['success']     = false;
      $response['formErrors']  = true;
      $response['errors']      = $errors;
    }
    else
    {
      $credentials = $request->only($this->username(), 'password');
      $authSuccess = Auth::attempt($credentials, $remember);
      if($authSuccess) {
        $email_verified_at = Auth::user()->email_verified_at;
        // echo 'sss'.$email_verified_at; die;
        if($email_verified_at == '')
        {
          $response['success'] = false;
          $response['error_message'] = 'Please verify your email.';
          Auth::logout();
          return response($response);
        }
        else
        {
          $role = Auth::user()->roles()->first()->name;
          // dd($role);
          if ($request->has('previous') && $role == 'buyer') {
            $redirectUrl = $request->get('previous').'?design_id='.$request->get('design_id');
          }else {
            $redirectUrl = 'dashboard';
          }

          if($request->has('login_as') && $request->login_as != ''){
            if($role != 'admin'){
              // dd("d");
              if($request->login_as == '1'){
                $role = 'seller';
              }
              if($request->login_as == '2'){
                $role = 'buyer';
              }
            }
          }
          \Session::put('role',$role);
          $response['success']  = true;
          $response['type']  = 'login';
          $response['delayTime']     = '2000';
          $response['url']  = $role.'/'.$redirectUrl;
          $response['success_message'] = 'Login successfully.';
          $request->session()->regenerate();
          return response($response);
        }
      }
      else
      {
        $response['success'] = false;
        $response['delayTime']     = '3000';
        $response['error_message'] = 'Incorrect Email or password. Please try again.';
      }
    }
    // echo json_encode($response); die();

    return response($response);
  }
}
