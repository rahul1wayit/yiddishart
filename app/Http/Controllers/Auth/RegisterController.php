<?php

namespace App\Http\Controllers\Auth;
use Auth;
use Crypt;
use Helper;
use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Models\Business;
use App\Role_user;

class RegisterController extends Controller
{
  /*
  |--------------------------------------------------------------------------
  | Register Controller
  |--------------------------------------------------------------------------
  |
  | This controller handles the registration of new users as well as their
  | validation and creation. By default this controller uses a trait to
  | provide this functionality without requiring any additional code.
  |
  */

  use RegistersUsers;

  /**
  * Where to redirect users after registration.
  *
  * @var string
  */
  protected $redirectTo = '/admin/dashboard';

  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    $this->middleware('guest');
  }

  /**
  * Get a validator for an incoming registration request.
  *
  * @param  array  $data
  * @return \Illuminate\Contracts\Validation\Validator
  */
  protected function validator(array $data)
  {
    $messages = [
      'role.in' => 'The :attribute field does not exist.',
    ];
    if ($data["role"]==3) {
      return Validator::make($data, [
        'first_name'      => ['required', 'string', 'max:255'],
        'last_name'       => ['required', 'string', 'max:255'],
        'user_name'       => ['required', 'string', 'max:255'],
        'email'           => ['required', 'string', 'email', 'max:255', 'unique:users'],
        'password'        => ['required', 'string', 'min:6', 'confirmed'],
        'role'            => ['required', 'in:2,3'],
        'businessname'    => ['required', 'string', 'max:255'],
        'cat'             => ['required', 'string', 'max:255'],
        'city'            => ['required', 'string', 'max:255'],
        'state'           => ['required', 'string', 'max:255'],
        'zip'             => ['required', 'string', 'max:255'],
        'phone'           => ['required', 'string', 'min:10','max:12'],
        'contact'         => ['required', 'string', 'min:10','max:12'],
        'description'     => ['required', 'string', 'max:5000'],
        'profileimage'    => ['mimes:jpeg,bmp,png'],
      ], $messages);
    } else {
      return Validator::make($data, [
        'first_name' => ['required', 'string', 'max:255'],
        'last_name' => ['required', 'string', 'max:255'],
        'user_name' => ['required', 'string', 'max:255'],
        'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        'password' => ['required', 'string', 'min:6', 'confirmed'],
        'role' => ['required', 'in:2,3'],
      ], $messages);
    }
  }
  /**
  * Create a new user instance after a valid registration.
  *
  * @param  array  $data
  * @return \App\User
  */
  protected function create(array $data)
  {
    if ($data["role"]==3) {
      $imageName = "";
      if(!empty($data["profileimage"])) {
        $image = $data["profileimage"];
        if($image != '') {
          $fullName = $image->getClientOriginalName();
          $name = pathinfo($fullName, PATHINFO_FILENAME);
          $imageName  =  $name.'_'.time().'.'.$image->getClientOriginalExtension();
          $image->move('assets/images/profile/', $imageName);
        }
      }
      $user = User::create([
        'first_name'  => $data['first_name'],
        'last_name'   => $data['last_name'],
        'user_name'   => $data['user_name'],
        'name'        => $data['first_name'].' '.$data['last_name'],
        'email'       => $data['email'],
        'gender'      => $data['gender'] ?? null,
        'phone'       => $data['phone'],
        'image'       => $imageName,
        'password'    => bcrypt($data['password'])
      ]);
      if($data['cat'] == 'other')
      {
        $category = $data['business_categories_other'];
      }else {
        $category = $data['cat'];
      }
      Business::create([
        'businessname'  => $data['businessname'],
        'category'      => $category,
        'city'          => $data['city'],
        'state'         => $data['state'],
        'zip'           => $data['zip'],
        'description'   => $data['description'],
        'phone'         => $data['contact'],
        'user_id'       => $user->id
      ]);
    } else {
      $user = User::create([
        'first_name'  => $data['first_name'],
        'last_name'   => $data['last_name'],
        'user_name'   => $data['user_name'],
        'name'        => $data['first_name'].' '.$data['last_name'],
        'email'       => $data['email'],
        'password'    => bcrypt($data['password'])
      ]);
    }
    if($user && $data['email'] != '')
    {
      $idd = null;
      // add role
      $createRole = array(
        'role_id' => $data['role'],
        'user_id' => $user->id
      );
      Role_user::updateOrCreate(['id'=>$idd],$createRole);

      $name = $data['first_name'].' '.$data['last_name'];
      $email = $data['email'];
      $mailmessage = 'Your account has been successfully created with us. Please click on below link to verify your Email.';
      $passlink 	= URL('/').'/confirm-account/'.Crypt::encrypt($user->id);
      $maildata	= array('name'=>$name,'setlink'=>$passlink,'mailmessage'=>$mailmessage);
      $mailresult = Helper::sendmail('noreply@domain.com', 'Yiddishart', $email, 'User', 'New account register', $maildata, 'mail.new-register');
      // echo URL('verify/').'/'.Crypt::encrypt($user->id); die;
      $res['success']         = true;
      $res['url']             = URL('verify/').'/'.Crypt::encrypt($user->id);
      $res['delayTime']       = '3000';
      $res['success_message'] = 'A link to confirm your account, has been sent to your email address.';
    }
    echo json_encode($res); die;
  }

  public function confirmAccount($id)
  {
    if($id != '')
    {
      $id 			= Crypt::decrypt($id);
      $user  = User::where('id', $id)->first();
      if($user->email_verified_at == '')
      {
        $data = array(
          'email_verified_at'		=> date('Y-m-d'),
        );
        if($id){
          $passes = User::where('id', $id)->update($data);
          return redirect('login')->with('success', 'Your account is activated. Please login here');
        }
      }else {
        return redirect('login')->with('error', 'Link expired.');
      }
    }
  }

  public function resendRegisterEmail($id)
  {
    if($id != '')
    {
      $id 			= Crypt::decrypt($id);
      $user  = User::where('id', $id)->first();
      if($user->id != '')
      {
        $name = $user->first_name.' '.$user->last_name;
        $email = $user->email;
        $mailmessage = 'Your account has been successfully created with us. Please click on below link to verify your Email.';
        $passlink 	= URL('/').'/confirm-account/'.Crypt::encrypt($user->id);
        $maildata	= array('name'=>$name,'setlink'=>$passlink,'mailmessage'=>$mailmessage);
        $mailresult = Helper::sendmail('noreply@domain.com', 'Yiddishart', $email, 'User', 'New account register', $maildata, 'mail.new-register');

        if($mailresult){
          return redirect('login')->with('success', 'A link to confirm your account, has been sent to your email address.');
        }
      }
    }
  }
  public function verifyPage($id)
  {
    $id 			= Crypt::decrypt($id);
    $user  = User::where('id', $id)->first()->toArray();
    return view('auth.verify',['data'=>[$user]]);
  }
}
