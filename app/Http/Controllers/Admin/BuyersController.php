<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Role;
use App\Role_user;
use App\User;
use DB;
use Validator;
use Illuminate\Support\Facades\Crypt;

class BuyersController extends Controller
{
  public $url;
  public function __construct()
  {
    $this->url    = url(request()->route()->getPrefix());
    $this->prefix = 'admin/buyers';
    $this->title  = 'Buyers';
  }
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */

  public function index(Request $request)
  {
    $data = array();
    $buyers = User::whereHas('roles',function($q){
      $q->where('role_id', '2');
    })->orderBy('id', 'desc')->paginate(10);
    if($request->ajax())
    {
      $entriesperpage = trim($request->input('entriesperpage'));
      $buyers = User::whereHas('roles',function($q){
        $q->where('role_id', '2');
      })->orderBy('id', 'desc')->paginate($entriesperpage);
      return view($this->prefix.'/buyers-pagination', ['url'=>$this->url, 'title'=>$this->title,'buyers'=>$buyers]);
    }
    else
    {
      return view($this->prefix.'/buyers', ['url'=>$this->url, 'title'=>$this->title,'buyers'=>$buyers]);
    }
  }
  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    //
  }
  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    //
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show(Request $request)
  {
    try {
      DB::beginTransaction();
      $id = $request->input('ref');
      $result = User::whereHas('roles',function($q) use ($id){
        $q->where('role_id','2');
      })->where('id',$id)->first();
      if($result)
      {
        DB::commit();
        $data['success']          = true;
        $data['buyer']            = $result;
        $data['delayTime']        = '3000';
        $data['success_message']  = 'View buyer details.';
      }
      else
      {
        DB::rollback();
        $data['success']          = false;
        // $data['url']              = 'buyers';
        $data['delayTime']        = '3000';
        $data['success_message']  = 'Something went wrong.';
      }
      echo json_encode($data); die();
    }
    catch (\Exception $e)
    {
      DB::rollback();
      $errors             = $e->getMessage();
      $data['success']    = false;
      $data['formErrors'] = true;
      $data['errors']     = $errors;
      echo json_encode($data); die();
    }
  }
  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    //
  }
  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    try {
      DB::beginTransaction();
      $val = Validator::make($request->all(),[
        'user_name'  => 'required',
        'first_name' => 'required',
        'last_name'  => 'required',
        'email'      => 'required|unique:users,email,'.$id
      ]);
      if($val->fails())
      {
        // return response()->json(['formErrors'=>$val->errors()->all()]);
        $errors             = $val->errors();
        $data['success']    = false;
        $data['formErrors'] = true;
        $data['errors']     = $errors;
        echo json_encode($data); die();
      }
      else
      {
        $update = array(
          'user_name'   => $request->input('user_name'),
          'first_name'  => $request->input('first_name'),
          'last_name'   => $request->input('last_name'),
          'email'       => $request->input('email')
        );
        $postResult = User::where('id',$id)->update($update);
        if($postResult)
        {
          DB::commit();
          $data['success']          = true;
          $data['url']              = 'buyers';
          $data['delayTime']        = '3000';
          $data['success_message']  = 'Updated Successfully.';
        }
        else
        {
          DB::rollback();
          $data['success']          = false;
          // $data['url']              = 'buyers';
          $data['delayTime']        = '3000';
          $data['success_message']  = 'Something went wrong.';
        }
        echo json_encode($data); die();
      }
    }
    catch (\Exception $e)
    {
      DB::rollback();
      $errors             = $e->getMessage();
      $data['success']    = false;
      $data['formErrors'] = true;
      $data['errors']     = $errors;
      echo json_encode($data); die();
    }
  }
  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy(Request $request, $id)
  {
    try {
      DB::beginTransaction();
      $result['user']       = User::find($id)->delete();
      $result['user_role']  = DB::table('role_user')->where('user_id',$id)->delete();
      if($result)
      {
        DB::commit();
        $data['success']          = true;
        // $data['buyer']            = $result;
        $data['delayTime']        = '3000';
        $data['success_message']  = 'Buyer deleted Successfully.';
      }
      else
      {
        DB::rollback();
        $data['success']          = false;
        // $data['url']              = 'buyers';
        $data['delayTime']        = '3000';
        $data['success_message']  = 'Something went wrong.';
      }
      echo json_encode($data); die();
    }
    catch (\Exception $e)
    {
      DB::rollback();
      $errors             = $e->getMessage();
      $data['success']    = false;
      $data['formErrors'] = true;
      $data['errors']     = $errors;
      echo json_encode($data); die();
    }
  }
  //search buyers
  public function searchBuyers(Request $request)
  {
    $search = trim($request->input('search'));
    $entriesperpage = trim($request->input('entriesperpage'));

    // $user = '';
    $user = User::whereHas('roles',function($q) use ($search){
      $q->where('role_id', '2');
    })
    ->orderBy('id','desc')
    ->paginate($entriesperpage);
    if($search == 'empty')
    {
      $user = User::whereHas('roles',function($q) use ($search){
        $q->where('role_id', '2');
      })
      ->orderBy('id','desc')
      ->paginate($entriesperpage);
    }
    else if($search != '')
    {
      $user = User::whereHas('roles',function($q) use ($search){
        $q->where('role_id', '2');
      })
      ->where(function($q) use ($search){
        $q->where('user_name','LIKE','%'.$search.'%');
        $q->orWhere('first_name', 'LIKE', '%'.$search.'%');
        $q->orWhere('last_name', 'LIKE', '%'.$search.'%');
        $q->orWhere('email', 'LIKE', '%'.$search.'%');
      })
      ->orderBy('id','desc')
      ->paginate($entriesperpage);
    }
    // $query = DB::getQueryLog();
    // print_r($query); die;
    return view($this->prefix.'/buyers-pagination', ['url'=>$this->url, 'title'=>$this->title,'buyers'=>$user]);
  }
  //activate and deactivate buyer account
  public function deactivateBuyer(Request $request)
  {
    try {
      DB::beginTransaction();
      $id   = $request->input('buyerId');
      $type = $request->input('type');
      if($type == 'deactive')
      {
        $result = DB::table('users')->where('id',$id)->update(['status'=> '0']);
      }
      else if($type == 'activate')
      {
        $result = DB::table('users')->where('id',$id)->update(['status'=> '1']);
      }
      if($result)
      {
        DB::commit();
        $data['success']          = true;
        // $data['buyer']            = $result;
        $data['delayTime']        = '3000';
        if($type == 'activate'){
          $data['success_message']  = 'Buyer account activated Successfully.';
        }else{
          $data['success_message']  = 'Buyer account deactivated Successfully.';
        }
      }
      else
      {
        DB::rollback();
        $data['success']          = false;
        // $data['url']              = 'buyers';
        $data['delayTime']        = '3000';
        $data['success_message']  = 'Something went wrong.';
      }
      echo json_encode($data); die();
    }
    catch (\Exception $e)
    {
      DB::rollback();
      $errors             = $e->getMessage();
      $data['success']    = false;
      $data['formErrors'] = true;
      $data['errors']     = $errors;
      echo json_encode($data); die();
    }
  }

  //search buyers
  public function entriesperpage(Request $request)
  {
    $search = trim($request->input('search'));
    $entriesperpage = trim($request->input('entriesperpage'));
    // $user = '';
    $user = User::whereHas('roles',function($q) use ($search){
      $q->where('role_id', '2');
    })
    ->orderBy('id','desc')
    ->paginate($entriesperpage);
    if($search == 'empty')
    {
      $user = User::whereHas('roles',function($q) use ($search){
        $q->where('role_id', '2');
      })
      ->orderBy('id','desc')
      ->paginate($entriesperpage);
    }
    else if($search != '')
    {
      $user = User::whereHas('roles',function($q) use ($search){
        $q->where('role_id', '2');
      })
      ->where(function($q) use ($search){
        $q->where('user_name','LIKE','%'.$search.'%');
        $q->orWhere('first_name', 'LIKE', '%'.$search.'%');
        $q->orWhere('last_name', 'LIKE', '%'.$search.'%');
        $q->orWhere('email', 'LIKE', '%'.$search.'%');
      })
      ->orderBy('id','desc')
      ->paginate($entriesperpage);
    }
    // $query = DB::getQueryLog();
    // print_r($query); die;
    return view($this->prefix.'/buyers-pagination', ['url'=>$this->url, 'title'=>$this->title,'buyers'=>$user]);
  }

}
