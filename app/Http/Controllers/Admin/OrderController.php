<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrderItem;

class OrderController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public $url;
  public function __construct()
  {
    $this->url    = url(request()->route()->getPrefix());
    $this->prefix = 'admin/orders';
    $this->title  = 'Orders';
  }

  public function index(Request $request)
  {
    $result = Order::with('order_details','order_items')->orderBy('id','desc')->paginate(10);
    if($request->ajax())
    {
      $entriesperpage = trim($request->input('entriesperpage'));
      $result = Order::with('order_details','order_items')->orderBy('id','desc')->paginate($entriesperpage);
      return view($this->prefix.'/orders-pagination', ['url'=>$this->url, 'title'=>$this->title, 'result'=>$result]);
    }
    else
    {
      return view($this->prefix.'/orders', ['url'=>$this->url, 'title'=>$this->title,'result' => $result]);
    }
  }

  //search orders
  public function searchOrders(Request $request)
  {
    $entriesperpage = trim($request->input('entriesperpage'));
    $search = trim($request->input('search'));
    $orders = Order::with('order_details','order_items')->orderBy('id','desc')->paginate($entriesperpage);
    if($search == 'empty')
    {
      $orders = Order::with('order_details','order_items')->orderBy('id','desc')->paginate($entriesperpage);
    }
    else if($search != '')
    {
      $orders = Order::with('order_details','order_items')
      ->where(function($q) use ($search){
        $q->where('order_number','LIKE','%'.$search.'%');
      })
      ->orderBy('id','desc')
      ->paginate($entriesperpage);
    }
    return view($this->prefix.'/orders-pagination', ['url'=>$this->url, 'title'=>$this->title,'result'=>$orders]);
  }


  public function orderDetail(Request $request)
  {
    $orderid = decrypt($request->id);
    $result = Order::with('order_details','order_items')->where(['id' => $orderid])->first();
    return view($this->prefix.'/order-detail',['url'=>$this->url, 'title'=> 'Order Details','result'=>$result]);
  }
}
