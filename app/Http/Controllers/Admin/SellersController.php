<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Helper;
use App\User;
use App\Models\Business;
use App\Models\Design;
use App\Models\OrderItem;
use App\Models\Order;
use App\Models\Logs;
use App\Models\Subscriptionplan;
use App\Models\Userplan;
use DB;
use Auth;
use App\Role_user;
use Validator;
class SellersController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */

  public $url;
  public function __construct()
  {
    $this->url    = url(request()->route()->getPrefix());
    $this->prefix = 'admin/sellers';
    $this->title  = 'Sellers';
  }

  public function index(Request $request)
  {
    $data = array();
    $seller = User::with('design')->whereHas('roles',function($q){
      $q->where('role_id', '3');
    })->orderBy('id','desc')->paginate(10);
    // echo '<pre>'; print_r($seller->toArray()); die;
    if($request->ajax())
    {
      $entriesperpage = trim($request->input('entriesperpage'));
      $seller = User::with('design')->whereHas('roles',function($q){
        $q->where('role_id', '3');
      })->orderBy('id','desc')->paginate($entriesperpage);
      return view($this->prefix.'/sellers-pagination', ['url'=>$this->url, 'title'=>$this->title, 'sellers'=>$seller]);
    }
    else
    {
      return view($this->prefix.'/sellers', ['url'=>$this->url, 'title'=>$this->title, 'sellers'=>$seller]);
    }
  }
  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    //return view('/');
  }
  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    //
  }
  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show(Request $request,$id)
  {
    try {
      DB::beginTransaction();
      $id = $request->input('ref');
      $result = User::whereHas('roles',function($q) use ($id){
        $q->where('role_id','3');
      })->with('business')->where('id',$id)->first();
      $last_order = OrderItem::with('design')->whereHas('design',function($q) use ($id){
        $q->where('added_by',$id);
      })->orderBy('id','desc')->first();
      $design = Design::where(['added_by'=>$id,'status'=>'1'])->get()->sum('price');
      $all_order = OrderItem::with('design')->whereHas('design',function($q) use ($id){
        $q->where('added_by',$id);
      })->get()->sum('total_price');
      $user_plan = Userplan::with('plan')->where(['user_id'=>$id])->first();

      if($last_order)
      $last_order['category_name'] = $last_order->design->business_category->name;

      if($result)
      {
        DB::commit();
        $data['success']    = true;
        $data['buyer']      = $result;
        $data['last_order'] = isset($last_order)?$last_order:'';
        $data['design']     = isset($design)?$design:'';
        $data['all_order']  = isset($all_order)?$all_order:'';
        $data['user_plan']  = isset($user_plan)?$user_plan:'';
        $data['delayTime']  = '3000';
        // $data['success_message']  = 'View seller details.';
      }
      else
      {
        DB::rollback();
        $data['success']          = false;
        $data['delayTime']        = '3000';
        $data['success_message']  = 'Something went wrong.';
      }
      echo json_encode($data); die();
    }
    catch (\Exception $e)
    {
      DB::rollback();
      $errors             = $e->getMessage();
      $data['success']    = false;
      $data['formErrors'] = true;
      $data['errors']     = $errors;
      echo json_encode($data); die();
    }
  }
  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    //
  }
  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    try {
      DB::beginTransaction();
      $val = Validator::make($request->all(),[
        'user_name'    => 'required',
        'first_name'   => 'required',
        'last_name'    => 'required',
        'email'        => 'required',
        'phone'        => 'required',
        'business_name' => 'required',
        'city'         => 'required',
        'state'        => 'required',
        'zip'          => 'required',
        'description'  => 'required',
        'contact'      => 'required'
      ]);
      if($val->fails())
      {
        // return response()->json(['formErrors'=>$val->errors()->all()]);
        $errors             = $val->errors();
        $data['success']    = false;
        $data['formErrors'] = true;
        $data['errors']     = $errors;
        echo json_encode($data); die();
      }
      else
      {
        if($request->hasfile('profileimage'))
        {
          $image = $request->file('profileimage');
          $image_name = time().'_'.$image->getClientOriginalName();
          $img_path = base_path().'/assets/images/profile';
          $image->move($img_path, $image_name);
          $image_name = $image_name;

          $user = array(
            'first_name'  => $request->input('first_name'),
            'last_name'   => $request->input('last_name'),
            'user_name'   => $request->input('user_name'),
            'name'        => $request->input('first_name').' '.$request->input('last_name'),
            'email'       => $request->input('email'),
            'gender'      => $request->input('gender'),
            'phone'       => $request->input('phone'),
            'image'       => $image_name
          );
          $update = User::updateOrCreate(['id'=>$id],$user);
        }else{
          $user = array(
            'first_name'  => $request->input('first_name'),
            'last_name'   => $request->input('last_name'),
            'user_name'   => $request->input('user_name'),
            'name'        => $request->input('first_name').' '.$request->input('last_name'),
            'email'       => $request->input('email'),
            'gender'      => $request->input('gender'),
            'phone'       => $request->input('phone'),
          );
          $update = User::updateOrCreate(['id'=>$id],$user);
        }
        if($request->input('cat') == 'other')
        {
          $category = $request->input('business_categories_other');
        }else {
          $category = $request->input('cat');
        }
        $business = array(
          'businessname'  => $request->input('business_name'),
          'category'      => $category,
          'city'          => $request->input('city'),
          'state'         => $request->input('state'),
          'zip'           => $request->input('zip'),
          'description'   => $request->input('description'),
          'phone'         => $request->input('contact'),
          'user_id'       => $id
        );
        $update = Business::updateOrCreate(['user_id'=>$id],$business);
        if($update)
        {
          DB::commit();
          $data['success']          = true;
          $data['url']              = 'sellers';
          $data['delayTime']        = '2000';
          $data['success_message']  = 'Updated Successfully.';
        }
        else
        {
          DB::rollback();
          $data['success']          = false;
          $data['delayTime']        = '3000';
          $data['success_message']  = 'Something went wrong.';
        }
        echo json_encode($data); die();
      }
    }
    catch (\Exception $e)
    {
      DB::rollback();
      $errors             = $e->getMessage();
      $data['success']    = false;
      $data['formErrors'] = true;
      $data['errors']     = $errors;
      echo json_encode($data); die();
    }
  }
  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    try {
      DB::beginTransaction();
      $result['user']       = User::find($id)->delete();
      $result['user_role']  = DB::table('role_user')->where('user_id',$id)->delete();
      if($result)
      {
        DB::commit();
        $data['success']          = true;
        // $data['buyer']            = $result;
        $data['delayTime']        = '2000';
        $data['success_message']  = 'Seller deleted Successfully.';
      }
      else
      {
        DB::rollback();
        $data['success']          = false;
        // $data['url']              = 'buyers';
        $data['delayTime']        = '3000';
        $data['success_message']  = 'Something went wrong.';
      }
      echo json_encode($data); die();
    }
    catch (\Exception $e)
    {
      DB::rollback();
      $errors             = $e->getMessage();
      $data['success']    = false;
      $data['formErrors'] = true;
      $data['errors']     = $errors;
      echo json_encode($data); die();
    }
  }
  //search sellers
  public function searchSellers(Request $request)
  {
    $entriesperpage = trim($request->input('entriesperpage'));

    $search = trim($request->input('search'));
    $user = User::whereHas('roles',function($q) use ($search){
      $q->where('role_id', '3');
    })
    ->orderBy('id','desc')
    ->paginate($entriesperpage);
    if($search == 'empty')
    {
      $user = User::whereHas('roles',function($q) use ($search){
        $q->where('role_id', '3');
      })
      ->orderBy('id','desc')
      ->paginate($entriesperpage);
    }
    else if($search != '')
    {
      $user = User::whereHas('roles',function($q) use ($search){
        $q->where('role_id', '3');
      })
      ->where(function($q) use ($search){
        $q->where('user_name','LIKE','%'.$search.'%');
        // $q->orWhere('first_name', 'LIKE', '%'.$search.'%');
        // $q->orWhere('last_name', 'LIKE', '%'.$search.'%');
        $q->orWhere('email', 'LIKE', '%'.$search.'%');
      })
      ->orderBy('id','desc')
      ->paginate($entriesperpage);
    }
    // $query = DB::getQueryLog();
    // print_r($query); die;
    return view($this->prefix.'/sellers-pagination', ['url'=>$this->url, 'title'=>$this->title,'sellers'=>$user]);
  }


  //activate and deactivate seller account
  public function deactivateSeller(Request $request)
  {
    try {
      DB::beginTransaction();
      $id   = $request->input('buyerId');
      $type = $request->input('type');
      if($type == 'deactive')
      {
        $result = DB::table('users')->where('id',$id)->update(['status'=> '0']);
      }
      else if($type == 'activate')
      {
        $result = DB::table('users')->where('id',$id)->update(['status'=> '1']);
      }
      if($result)
      {
        DB::commit();
        $data['success']          = true;
        // $data['buyer']            = $result;
        $data['delayTime']        = '3000';
        if($type == 'activate'){
          $data['success_message']  = 'Seller account activated Successfully.';
        }else{
          $data['success_message']  = 'Seller account deactivated Successfully.';
        }
      }
      else
      {
        DB::rollback();
        $data['success']          = false;
        // $data['url']              = 'buyers';
        $data['delayTime']        = '3000';
        $data['success_message']  = 'Something went wrong.';
      }
      echo json_encode($data); die();
    }
    catch (\Exception $e)
    {
      DB::rollback();
      $errors             = $e->getMessage();
      $data['success']    = false;
      $data['formErrors'] = true;
      $data['errors']     = $errors;
      echo json_encode($data); die();
    }
  }
  public function entriesperpage(Request $request)
  {
    $search = trim($request->input('search'));
    $entriesperpage = trim($request->input('entriesperpage'));
    // $user = '';
    $user = User::whereHas('roles',function($q) use ($search){
      $q->where('role_id', '3');
    })
    ->orderBy('id','desc')
    ->paginate($entriesperpage);
    if($search == 'empty')
    {
      $user = User::whereHas('roles',function($q) use ($search){
        $q->where('role_id', '3');
      })
      ->orderBy('id','desc')
      ->paginate($entriesperpage);
    }
    else if($search != '')
    {
      $user = User::whereHas('roles',function($q) use ($search){
        $q->where('role_id', '3');
      })
      ->where(function($q) use ($search){
        $q->where('user_name','LIKE','%'.$search.'%');
        $q->orWhere('first_name', 'LIKE', '%'.$search.'%');
        $q->orWhere('last_name', 'LIKE', '%'.$search.'%');
        $q->orWhere('email', 'LIKE', '%'.$search.'%');
      })
      ->orderBy('id','desc')
      ->paginate($entriesperpage);
    }
    // $query = DB::getQueryLog();
    return view($this->prefix.'/sellers-pagination', ['url'=>$this->url, 'title'=>$this->title,'sellers'=>$user]);
  }
  // seller Design
  public function sellerDesign(Request $request)
  {
    $id = decrypt($request->id);
    $sellerDesign = Design::where('added_by', $id)->orderBy('id','desc')->paginate(10);
    if($request->ajax())
    {
      $sellerDesign = Design::where('added_by', $id)->orderBy('id','desc')->paginate(10);
      return view($this->prefix.'/sellers-design-pagination', ['url'=>$this->url, 'title'=>$this->title, 'design'=>$sellerDesign]);
    }
    else
    {
      return view($this->prefix.'/sellers-design', ['url'=>$this->url, 'title'=>$this->title, 'design'=>$sellerDesign]);
    }
  }

  public function changeStatus(Request $request)
  {
    try{
      $id = $request->id;
      $status = $request->status;

      DB::beginTransaction();
      $Design = Design::where('id',$id)->update(array('status'=>$status));
      $lastDesignId = Logs::where(['log_id'=>$id,'type'=>'design'])->orderBy('id','DESC')->first();
      $log_data = array(
        'status' => $status,
      );
      Logs::where('id',$lastDesignId->id)->update($log_data);
      if($Design)
      {
        DB::commit();
        $res['success']         = true;
        $res['delayTime']       = '2000';
        $res['success_message']   = 'Design status has been updated.';
      }
      else
      {
        DB::rollback();
        $res['success']         = false;
        $res['url']             = 'category';
        $res['delayTime']       = '2000';
        $res['error_message']   = 'Something went wrong.';
      }
      echo json_encode($res);
      die();
    }
    catch (\Exception $e)
    {
      DB::rollback();
      $res['success']         = false;
      $res['url']             = 'sellers';
      $res['delayTime']       = '2000';
      $res['error_message']   = 'Something went wrong.';
      echo json_encode($res);
      die();
    }
  }
  // seller Logs
  public function sellerLogs(Request $request)
  {
    $id = decrypt($request->id);
    $designid = decrypt($request->designid);
    $sellerLogs = Helper::get_logs($id,$designid);
    $designMain = Design::where('id',$designid)->first();
    return view($this->prefix.'/sellers-logs', ['url'=>$this->url, 'title'=>$this->title, 'sellerLogs'=>$sellerLogs,'designMain'=>$designMain]);
  }
}
