<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\SupportCategory;

class SupportCategoryController extends Controller
{
    public $url;
    public function __construct()
    {
        $this->url    = url(request()->route()->getPrefix());
        $this->prefix = 'admin/supportcategory';
        $this->title  = 'Support Category';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $getSupportCat = SupportCategory::orderBy('id','desc')->paginate(10);
        if($request->ajax())
        {
          $entriesperpage = trim($request->input('entriesperpage'));
          $getSupportCat  = SupportCategory::orderBy('id','desc')->paginate($entriesperpage);
          return view($this->prefix.'/support-category-pagination', ['url'=>$this->url, 'title'=>$this->title,'getSupportCat' => $getSupportCat]);
        } else {
          return view($this->prefix.'/index', ['url'=>$this->url, 'title'=>$this->title,'getSupportCat' => $getSupportCat]);
        }

    }

    public function supportCategoryModal(Request $request) {
        try {
            if ( $request->ajax() ) {
                $getResult = array();
                if ($request->input('catId') && ""!=trim($request->input('catId'))) {
                    $faqId     = decrypt($request->input('catId'));
                    $getResult = SupportCategory::where(['id' => $faqId])->first();
                    // echo "<pre>";
                    // print_r($getResult);
                }
                // $faqId = decrypt($request->input('catId'));
                // if ($faqId=="add") {
                //     return view($this->prefix.'/add_faq_modal');
                // }
                // $getResult = SupportCategory::where(['id' => $faqId])->first();
                return view($this->prefix.'/add_support_category_modal', ['result'=>$getResult]);
            }
        } catch (\Exception $e) {
            $errors             = $e->getMessage();
            $data['success']    = false;
            $data['formErrors'] = true;
            $data['errors']     = $errors;
            return response($data);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
     {
         try {
             DB::beginTransaction();
             if ( $request->ajax() ) {
                 $val = Validator::make($request->all(),[
                     'name'    => 'required|unique:support_categories,name' . ($request->id ? ",".decrypt($request->id) : ''),
                 ]);

                 if($val->fails()) {
                     $errors             = $val->errors();
                     $data['success']    = false;
                     $data['formErrors'] = true;
                     $data['errors']     = $errors;
                     $data['errorShow']  = false;
                 } else {
                     $id = null;
                     if ($request->input('id')) {
                         $id = decrypt($request->input('id'));
                     }
                     $insertArray = array();
                     $insertArray["name"]     = (!empty($request->input('name')) && ""!=trim($request->input('name'))) ? $request->input('name') : "";
                     $insertArray["description"]   = (!empty($request->input('description')) && ""!=trim($request->input('description'))) ? $request->input('description') : "";
                     $insertArray["status"]   = ($request->input('status')) ? $request->input('status') : 0;
                     $insertArray["added_by"] = Auth::id();
                     $checkData = SupportCategory::updateOrCreate(['id'=>$id],$insertArray);
                     if($checkData)
                     {
                         DB::commit();
                         if ($request->input('id')) {
                             $data['success']          = true;
                             $data['url']              = 'support-category';
                             $data['delayTime']        = '2000';
                             $data['success_message']  = 'Update Successfully.';
                         } else {
                             $data['success']          = true;
                             $data['url']              = 'support-category';
                             $data['delayTime']        = '2000';
                             $data['success_message']  = 'Insert Successfully.';
                         }

                     }
                     else
                     {
                         DB::rollback();
                         $data['success']          = false;
                         $data['delayTime']        = '3000';
                         $data['success_message']  = 'Something went wrong.';
                     }
                 }
                 return response($data);
             }
         } catch (\Exception $e) {
             DB::rollback();
             $errors             = $e->getMessage();
             $data['success']    = false;
             $data['formErrors'] = true;
             $data['errors']     = $errors;
             return response($data);
         }
     }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $id = decrypt($id);
            $result['user'] = SupportCategory::find($id)->delete();
            if($result)
            {
                DB::commit();
                $data['success']          = true;
                $data['delayTime']        = '3000';
                $data['success_message']  = 'Support Category deleted Successfully.';
            } else {
                DB::rollback();
                $data['success']          = false;
                $data['delayTime']        = '3000';
                $data['success_message']  = 'Something went wrong.';
            }
            return response($data);
        } catch (\Exception $e) {
            DB::rollback();
            $errors             = $e->getMessage();
            $data['success']    = false;
            $data['formErrors'] = true;
            $data['errors']     = $errors;
            return response($data);
        }
    }
}
