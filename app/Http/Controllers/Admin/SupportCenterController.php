<?php

namespace App\Http\Controllers\Admin;

use DB;
use Hash;
use Helper;
use Validator;
use Illuminate\Http\Request;
use App\Models\SupportQusAns;
use App\Models\SupportCategory;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class SupportCenterController extends Controller
{
    public $url;
    public function __construct()
    {
        $this->url    = url(request()->route()->getPrefix());
        $this->prefix = 'admin/supportcenter';
        $this->title  = 'Support Center';
    } 

    public function index(Request $request) {
        $getQues = SupportQusAns::with('support_category')->orderBy('id','desc')->paginate(10);
        if($request->ajax())
        {
          $entriesperpage = trim($request->input('entriesperpage'));
          $getQues = SupportQusAns::with('support_category')->orderBy('id','desc')->paginate($entriesperpage);
          return view($this->prefix.'/support-pagination', ['url'=>$this->url, 'title'=>$this->title, 'getQues'=>$getQues]);
        }
        else
        {
          return view($this->prefix.'/index', ['url'=>$this->url, 'title'=>$this->title,'getQues' => $getQues]);
        }
    } 

    public function show($id)
    {
        $faqId = decrypt($id);
        $result = SupportQusAns::with('support_category')->where(['id' => $faqId])->first();
        return view($this->prefix.'/support-detail',['url'=>$this->url, 'title'=> 'Support question & answer details','result'=>$result]);
    }

    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            if ( $request->ajax() ) {
                $val = Validator::make($request->all(),[
                    'category_id'    => 'required',
                    'answer'    => 'required',
                    'question'   => 'required'
                ]);

                if($val->fails()) {
                    $errors             = $val->errors();
                    $data['success']    = false;
                    $data['formErrors'] = true;
                    $data['errors']     = $errors;
                } else {
                    $id = null;
                    if ($request->has('supportid')) {
                        $id = decrypt($request->input('supportid'));
                    }
                    $insertArray = array();
                    $insertArray["category_id"]   = (int)$request->input('category_id');
                    $insertArray["question"]   = (!empty($request->input('question')) && ""!=trim($request->input('question'))) ? $request->input('question') : "";
                    $insertArray["answer"]     = (!empty($request->input('answer')) && ""!=trim($request->input('answer'))) ? $request->input('answer') : "";
                    $insertArray["common_status"]     = ($request->input('common_status')) ? $request->input('status') : 0;                    
                    $insertArray["status"]     = ($request->input('status')) ? $request->input('status') : 0;
                    $insertArray["added_by"]   = Auth::id();
                    $checkData = SupportQusAns::updateOrCreate(['id'=>$id],$insertArray);
                    if($checkData)
                    {
                        DB::commit();
                        if ($request->has('supportid')) {
                            $data['success']          = true;
                            $data['url']              = 'support-center';
                            $data['delayTime']        = '2000';
                            $data['success_message']  = 'Update Successfully.';
                        } else {
                            $data['success']          = true;
                            $data['url']              = 'support-center';
                            $data['delayTime']        = '2000';
                            $data['success_message']  = 'Insert Successfully.';
                        }

                    }
                    else
                    {
                        DB::rollback();
                        $data['success']          = false;
                        $data['delayTime']        = '3000';
                        $data['success_message']  = 'Something went wrong.';
                    }
                }
                return response($data);
            }
        } catch (\Exception $e) {
            
            DB::rollback();
            $errors             = $e->getMessage();
            $data['success']    = false;
            $data['formErrors'] = true;
            $data['errors']     = $errors;
            return response($data);
        }
    }

    public function editModal(Request $request) {
        try {
            if ( $request->ajax() ) {
                $faqId = decrypt($request->input('id'));
               	$supportCategory = SupportCategory::where('status','1')->get();
                if ($faqId=="add") {
                    return view($this->prefix.'/add_support_modal',['supportCategory'=>$supportCategory]);
                }
                $getResult = SupportQusAns::where(['id' => $faqId])->first();
                return view($this->prefix.'/edit_support_modal', ['result'=>$getResult,'supportCategory'=>$supportCategory]);
            }
        } catch (\Exception $e) {
            $errors             = $e->getMessage();
            $data['success']    = false;
            $data['formErrors'] = true;
            $data['errors']     = $errors;
            return response($data);
        }
    }

    public function destroy(Request $request, $id)
    {
        try {
                DB::beginTransaction();
                $id = decrypt($id);
                $result['user'] = SupportQusAns::find($id)->delete();
                if($result)
                {
                    DB::commit();
                    $data['success']          = true;
                    $data['delayTime']        = '3000';
                    $data['success_message']  = 'Support question deleted Successfully.';
                } else {
                    DB::rollback();
                    $data['success']          = false;
                    $data['delayTime']        = '3000';
                    $data['success_message']  = 'Something went wrong.';
                }
                return response($data);
            } catch (\Exception $e) {
                DB::rollback();
                $errors             = $e->getMessage();
                $data['success']    = false;
                $data['formErrors'] = true;
                $data['errors']     = $errors;
                return response($data);
            }
    }
}
