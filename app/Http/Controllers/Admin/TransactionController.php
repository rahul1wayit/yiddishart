<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrderItem;

class TransactionController extends Controller
{
  public $url;
  public function __construct()
  {
    $this->url    = url(request()->route()->getPrefix());
    $this->prefix = 'admin/transactions';
    $this->title  = 'Transactions';
  }

  public function index(Request $request)
  {
    $result = Order::selectRaw('*, sum(total_amount) as sum')->groupBy('user_id')->orderBy('id','desc')->paginate(10);
    if($request->ajax())
    {
      $entriesperpage = trim($request->input('entriesperpage'));
      $result = Order::selectRaw('*, sum(total_amount) as sum')->groupBy('user_id')->orderBy('id','desc')->paginate($entriesperpage);
      return view($this->prefix.'/transactions-pagination', ['url'=>$this->url, 'title'=>$this->title, 'result'=>$result]);
    }
    else
    {
      return view($this->prefix.'/transactions', ['url'=>$this->url, 'title'=>$this->title,'result' => $result]);
    }
  }

  //search Transaction
  public function searchTransaction(Request $request)
  {
    $entriesperpage = trim($request->input('entriesperpage'));
    $search = trim($request->input('search'));
    $result = Order::selectRaw('*, sum(total_amount) as sum')->groupBy('user_id')->orderBy('id','desc')->paginate($entriesperpage);
    if($search == 'empty')
    {
      $result = Order::selectRaw('*, sum(total_amount) as sum')->groupBy('user_id')->orderBy('id','desc')->paginate($entriesperpage);
    }
    else if($search != '')
    {
      $result = Order::selectRaw('*, sum(total_amount) as sum')
      ->where(function($q) use ($search){
        $q->where('created_at','LIKE',$search.'%');
      })
      ->groupBy('user_id')
      ->orderBy('id','desc')
      ->paginate($entriesperpage);
    }
    return view($this->prefix.'/transactions-pagination', ['url'=>$this->url, 'title'=>$this->title,'result'=>$result]);
  }
}
