<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Hash;
use DB;
use Validator;

use App\User;
use App\Role_user;
use App\Models\Order;

class DashboardController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public $url;
  public function __construct()
  {
    $this->url    = url(request()->route()->getPrefix());
    $this->prefix = 'admin/dashboard';
    $this->title  = 'Dashboard';
  }
  public function index()
  {
    $data = array();
    $buyer = User::whereHas('roles', function($q){
      $q->where('role_id','2');
    })->get()->count();
    $seller = User::whereHas('roles', function($q){
      $q->where('role_id','3');
    })->get()->count();
    $orders = Order::get()->count();
    $userCount = array(
      'buyer'   => $buyer,
      'seller'  => $seller,
      'orders'  => $orders,
    );
    // print_r($userCount); die;
    return view($this->prefix, ['url'=>$this->url, 'title'=>$this->title,'userCount'=>$userCount]);
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */

  public function settings()
  {
    $this->title  = 'Settings';
    return view('admin/settings',['url'=>$this->url, 'title'=>$this->title]);
  }

  public function checkPassword(Request $request)
  {

    $oldPassword = $request['oldPassword'];
    // print_r($oldPassword);die;
    $password = Auth::user()->password;
    if(Hash::check($oldPassword, $password)){
      $data['success']          = true;
      $data['checkPassword']    = 'checked';
      $data['success_message']  = 'password match';
    }else{
      $data['success']          = false;
      $data['checkPassword']    = 'checked';
      $data['success_message']  = 'password not match';
    }
    echo json_encode($data); die();
  }


  public function changePassword(Request $request)
  {
    if(Auth::Check())
    {
      $user_id  = Auth::User()->id;
      $obj_user = User::find($user_id);
      $obj_user->password = Hash::make($request['newPassword']);;

      if($obj_user->save()){
        $data['success']          = true;
        $data['changePassword']   = 'checked';
        $data['success_message']  = 'password change';
      }else{
        $data['success']          = false;
        $data['changePassword']   = 'checked';
        $data['success_message']  = 'password not change';
      }
      echo json_encode($data); die();

    }
    else
    {
      return redirect()->to('/');
    }
  }

  public function postCredentials(Request $request)
  {
    if(Auth::Check())
    {
      $request_data = $request->All();
      $messages = [
        'password.required' => 'Please enter current password',
        'new_password.required' => 'Please enter new password',
      ];
      $validator = Validator::make($request_data, [
        'password' => 'required',
        'new_password' => 'required|same:new_password',
        'confirm_password' => 'required|same:new_password',
      ], $messages);
      if($validator->fails())
      {
        DB::rollback();
        $errors             = $validator->errors();
        $res['success']     = false;
        $res['formErrors']  = true;
        $res['errors']      = $errors;
        echo json_encode($res);
        die();
      }
      else
      {
        $current_password = Auth::User()->password;
        if(Hash::check($request_data['password'], $current_password))
        {
          $user_id = Auth::User()->id;
          $obj_user = User::find($user_id);
          $obj_user->password = Hash::make($request_data['new_password']);;
          $obj_user->save();
          $res['success']         = true;
          $res['url']             = 'settings';
          $res['delayTime']       = '2000';
          $res['success_message']   = 'New Password has been changed.';
        }
        else
        {
          DB::rollback();
          $res['success']         = false;
          $res['delayTime']       = '2000';
          $res['error_message']   = 'Please enter correct current password.';
        }
      }
      echo json_encode($res);
      die();
    }
  }

  public function updateImage(Request $request)
  {
    try
    {
      $id=Auth::user()->id;
      DB::beginTransaction();
      $image_name = $request->input('imageName');
      $createData = array(
        'image' => $image_name
      );
      if(User::updateOrCreate(['id'=>$id],$createData))
      {
        DB::commit();
        $res['success']         = true;
        $res['url']             = 'settings';
        $res['delayTime']       = '2000';
        $res['success_message']   = 'Image has been successfully uploaded.';
      }
      else
      {
        DB::rollback();
        $res['success']         = false;
        $res['url']             = 'settings';
        $res['delayTime']       = '2000';
        $res['error_message']   = 'Something went wrong.';
      }
      echo json_encode($res);
      die();
    }
    catch (\Exception $e)
    {
      DB::rollback();
      $errors             =$e->getMessage();
      $res['success']     = false;
      $res['formErrors']  = true;
      $res['errors']      = $errors;

      // return response()->json(['errors'=>['message'=>$e->getMessage()]],400);
      echo json_encode($res);
      die();
    }
  }


  public function create()
  {
    //
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    //
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    //
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    //
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    //
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    //
  }
  public function uploadSingleImage(Request $request) {
    if ( !empty($_FILES) && $_FILES['picture']['name'] )
    {
      $length = 6;
      $image = $request->file('picture');
      $realName = $request->input('realName');
      $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
      $randomString1 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
      $randomString = $randomString . $randomString1;
      $fileParts1 = explode(".", $realName);
      $ext1 = $fileParts1[count($fileParts1) - 1];
      $image_name = $randomString . '.' . $ext1;
      $img_path = base_path().'/assets/images/profile';
      $image->move($img_path, $image_name);
      ?>
      <div class="single-im bx">
        <span class="pro-imgs">
          <img class="uploadProductImages previewMainImagePro" style="transform:rotate(<?php echo $request->input('rotateAngle') ?>deg)" width="70" height="60" src="<?php echo URL('/') . '/assets/images/profile/' . $image_name; ?>" alt="">
          <a href="javascript:void(0)" class="delete-gallery-img singleDeleteImg pro-img" rel="<?php echo $image_name; ?>" urlLink="/deleteSingleImage" rotateangle="<?php echo $request->input('rotateAngle') ?>">Delete</a>
          <input type="hidden" name="imageName" value="<?php echo $image_name; ?>">
        </span>
      </div>
      <?php
    }
  }

  //delete file or images
  public function deleteSingleImage(Request $request)
  {
    if(file_exists(base_path('assets/images/profile/'.$request->input('currentVal')))) {
      unlink(base_path('assets/images/profile/'.$request->input('currentVal')));
      User::where('id',Auth::user()->id)->update(['image'=>'']);
      echo 1; die;
    }
  }
}
