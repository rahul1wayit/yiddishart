<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Event;
use Redirect;
use Helper;
use DB;

class EventController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public $prefix='';
  public function __construct()
  {
    $this->url    =  url(request()->route()->getPrefix());
    $this->prefix = 'admin/events';
    $this->title  = 'Events';
  }
  public function index(Request $request)
  {
    $data = array();
    $events = Event::orderBy('id','DESC')->paginate('20');
    if($request->ajax())
    {
      $events = Event::orderBy('id','DESC')->paginate('20');
      return view($this->prefix.'/pagination', ['url'=>$this->url, 'title'=>$this->title, 'events'=>$events]);
    }
    return view($this->prefix.'/list',['url'=>$this->url,'title'=>$this->title,'events'=>$events]);
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    return view($this->prefix.'/add',['url'=>$this->url,'title'=>$this->title,'data'=>[],'id'=>null]);
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    try
    {
      $id=null;
      if(!empty($request->id))
      {
        $id = \Crypt::decryptString($request->id);
        $action = 'updated';
      }else {
        $action = 'added';
      }
      DB::beginTransaction();
      $request->name=trim($request->name);

      if($request->hasfile('image'))
      {
        $image = $request->file('image');
        $image_name = time().'_'.$image->getClientOriginalName();
        $img_path = base_path().'/assets/images/event/';
        $image->move($img_path, $image_name);
        $image_name = $image_name;
      }else {
        $image_name = '';
      }
      $createData = array(
        'name' => $request->name,
        'date' => Helper::dateFormat($request->date,'Y-m-d'),
        'description' => $request->description,
        'status' => 1,
        'added_by' => 1,
        'image' => $image_name,
      );
      if(Event::updateOrCreate(['id'=>$id],$createData))
      {
        DB::commit();
        $res['success']         = true;
        $res['url']             = $this->url.'/events';
        $res['delayTime']       = '2000';
        $res['success_message']   = 'Event has been '.$action.' successfully.';
      }
      else
      {
        DB::rollback();
        $res['success']         = false;
        $res['url']             = $this->url.'/events/create';
        $res['delayTime']       = '2000';
        $res['error_message']   = 'Something went wrong.';
      }
      echo json_encode($res);
      die();
    }
    catch (\Exception $e)
    {
      DB::rollback();
      $errors             =$e->getMessage();
      $res['success']     = false;
      $res['formErrors']  = true;
      $res['errors']      = $errors;
      echo json_encode($res);
      die();
    }
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    //
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    try
    {
      $pass_id=$id;
      $data = Event::where('id',$pass_id)->firstOrFail();
      // echo '<pre>'; print_r($data->toArray()); die;
      return view($this->prefix.'/edit',['url'=>$this->url,'title'=>$this->title,'id'=>$id,'data'=>$data]);
    }
    catch (\Exception $e)
    {
      DB::rollback();
      $res['success']         = false;
      $res['delayTime']       = '2000';
      $res['error_message']   = 'Something went wrong.';
      echo json_encode($res);
      die();
    }
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    try
    {
      DB::beginTransaction();
      if($request->hasfile('image'))
      {
        $image = $request->file('image');
        $image_name = time().'_'.$image->getClientOriginalName();
        $img_path = base_path().'/assets/images/event/';
        $image->move($img_path, $image_name);
        $image_name = $image_name;
        $createData = array(
          'name' => $request->name,
          'date' => Helper::dateFormat($request->date,'Y-m-d'),
          'description' => $request->description,
          'status' => 1,
          'added_by' => 1,
          'image' => $image_name
        );
      }
      else
      {
        $createData = array(
          'name' => $request->name,
          'date' => Helper::dateFormat($request->date,'Y-m-d'),
          'description' => $request->description,
          'status' => 1,
          'added_by' => 1
        );
      }
      if(Event::updateOrCreate(['id'=>$id],$createData))
      {
        DB::commit();
        $res['success']         = true;
        $res['url']             = $this->url.'/events';
        $res['delayTime']       = '2000';
        $res['success_message']   = 'Event has been updated successfully.';
      }
      else
      {
        DB::rollback();
        $res['success']         = false;
        $res['url']             = $this->url.'/events/create';
        $res['delayTime']       = '2000';
        $res['error_message']   = 'Something went wrong.';
      }
      echo json_encode($res);
      die();
    }
    catch (\Exception $e)
    {
      DB::rollback();
      $errors             =$e->getMessage();
      $res['success']     = false;
      $res['formErrors']  = true;
      $res['errors']      = $errors;
      echo json_encode($res);
      die();
    }
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    $event = Event::find($id);
    if($event->delete())
    {
      $data['success']          = true;
      $data['Subscriptionplan'] = 'deleted';
      $data['success_message']  = 'Event deleted Successfully.';
    }
    else
    {
      $data['success']          = false;
      $data['Subscriptionplan'] = 'not delete';
      $data['success_message']  = 'Something went wrong.';
    }
    echo json_encode($data); die();
  }
}
