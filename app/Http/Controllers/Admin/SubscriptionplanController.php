<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\SubscriptionplanRequest;
use App\Models\Subscriptionplan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Redirect;
use DB;

class SubscriptionplanController extends Controller
{
  public $prefix='';
  public function __construct()
  {
    $this->url    =  url(request()->route()->getPrefix());
    $this->prefix = 'admin/subscriptionplan';
    $this->title  = 'Subscriptionplan';
  }
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {

    $data = array();
    $getAllSubscriptionplan = Subscriptionplan::orderBy('id','DESC')->get()->toArray();
    return view($this->prefix.'/subscriptionplan',['url'=>$this->url,'title'=>$this->title,'getAllSubscriptionplan'=>$getAllSubscriptionplan]);
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    try {
      $title = "Add Subscription Plan";
      $button_name = "Save new plan";
      $subUploads = array('5'=>'5','50'=>'50','150'=>'150','250'=>'250',);
      $uploadSize = array('MB'=>'MB','GB'=>'GB','TB'=>'TB',);
      $type = "New";
      return view($this->prefix.'/add',['url'=>$this->url,'type'=>$type, 'title'=>$title,'uploadSize'=>$uploadSize,'subUploads'=>$subUploads, 'button_name'=>$button_name,'data'=>[],'id'=>null]);
    } catch (\Exception $e) {
      DB::rollback();
      return response()->json(['errors'=>['message'=>$e->getMessage()]],400);
    }
  }

  public function store(Request $request)
  {
    try
    {
      $id=null;
      if(!empty($request->id))
      {
        $id = \Crypt::decryptString($request->id);
        $action = 'updated';
      }else {
        $action = 'added';
      }
      DB::beginTransaction();
      $request->title=trim($request->title);

      if($request->hasfile('image'))
      {
        $image = $request->file('image');
        $image_name = time().'_'.$image->getClientOriginalName();
        $img_path = base_path().'/assets/images/';
        $image->move($img_path, $image_name);
        $image_name = $image_name;
      }else {
        $image_name = '';
      }
      // echo $image_name; die;
      $createData = array(
        'title' => $request->title,
        'price' => $request->price,
        'offerprice' => $request->offerprice,
        'uploads' => $request->uploads,
        'storage' => $request->storage,
        'storagelimit' => $request->storagelimit,
        'color' => $request->color,
        'status' => $request->status,
        'addedby' => Auth::user()->id,
        'image' => $image_name,
      );
      // echo '<pre>'.$id;
      // print_r($createData); die;
      if(Subscriptionplan::updateOrCreate(['id'=>$id],$createData))
      {
        DB::commit();
        $res['success']         = true;
        $res['url']             = $this->url.'/subscription_plan';
        $res['delayTime']       = '2000';
        $res['success_message']   = 'Plan has been '.$action.' successfully.';
      }
      else
      {
        DB::rollback();
        $res['success']         = false;
        $res['url']             = $this->url.'/subscription_plan/create';
        $res['delayTime']       = '2000';
        $res['error_message']   = 'Something went wrong.';
      }
      echo json_encode($res);
      die();
    }
    catch (\Exception $e)
    {
      DB::rollback();
      $errors             =$e->getMessage();
      $res['success']     = false;
      $res['formErrors']  = true;
      $res['errors']      = $errors;
      echo json_encode($res);
      die();
    }
  }



  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    //
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    try {
      $title = "Edit Subscription Plan";
      $button_name = "Edit Plan";
      $pass_id = $id;
      $subUploads = array('5'=>'5','50'=>'50','150'=>'150','250'=>'250',);
      $uploadSize = array('MB'=>'MB','GB'=>'GB','TB'=>'TB');
      $id = Crypt::decryptString($id);
      $data = Subscriptionplan::where('id',$id)->first();
      $type = "Edit";
      return view($this->prefix.'/edit',['url'=>$this->url,'type'=>$type, 'title'=>$title,'uploadSize'=>$uploadSize, 'subUploads'=>$subUploads, 'button_name'=>$button_name,'data'=>$data,'id'=>$pass_id]);
    } catch (\Exception $e) {
      DB::rollback();
      return response()->json(['errors'=>['message'=>$e->getMessage()]],400);
    }
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    try
    {
      $id=null;
      if(!empty($request->id))
      {
        $id = \Crypt::decryptString($request->id);
        $action = 'updated';
      }else {
        $action = 'added';
      }
      DB::beginTransaction();
      $request->title=trim($request->title);

      if($request->hasfile('image'))
      {
        $image = $request->file('image');
        $image_name = time().'_'.$image->getClientOriginalName();
        $img_path = base_path().'/assets/images/';
        $image->move($img_path, $image_name);
        $image_name = $image_name;
        $createData = array(
          'title' => $request->title,
          'price' => $request->price,
          'offerprice' => $request->offerprice,
          'uploads' => $request->uploads,
          'storage' => $request->storage,
          'storagelimit' => $request->storagelimit,
          'color' => $request->color,
          'status' => $request->status,
          'addedby' => Auth::user()->id,
          'image' => $image_name,
        );
      }
      else
      {
        $createData = array(
          'title' => $request->title,
          'price' => $request->price,
          'offerprice' => $request->offerprice,
          'uploads' => $request->uploads,
          'storage' => $request->storage,
          'storagelimit' => $request->storagelimit,
          'color' => $request->color,
          'status' => $request->status,
          'addedby' => Auth::user()->id
        );
      }
      // echo '<pre>'.$id;
      // print_r($createData); die;
      if(Subscriptionplan::updateOrCreate(['id'=>$id],$createData))
      {
        DB::commit();
        $res['success']         = true;
        $res['url']             = $this->url.'/subscription_plan';
        $res['delayTime']       = '2000';
        $res['success_message']   = 'Plan has been '.$action.' successfully.';
      }
      else
      {
        DB::rollback();
        $res['success']         = false;
        $res['url']             = $this->url.'/subscription_plan/create';
        $res['delayTime']       = '2000';
        $res['error_message']   = 'Something went wrong.';
      }
      echo json_encode($res);
      die();
    }
    catch (\Exception $e)
    {
      DB::rollback();
      $errors             =$e->getMessage();
      $res['success']     = false;
      $res['formErrors']  = true;
      $res['errors']      = $errors;
      echo json_encode($res);
      die();
    }
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    $user = Subscriptionplan::find($id);
    if($user->delete())
    {
      $data['success']          = true;
      $data['Subscriptionplan'] = 'deleted';
      $data['success_message']  = 'Subscription plan delete Successfully.';
    }
    else
    {
      $data['success']          = false;
      $data['Subscriptionplan'] = 'not delete';
      $data['success_message']  = 'Subscription plan delete failed.';
    }
    echo json_encode($data); die();
  }
  public function subscription()
  {
    return view('admin/subscriptionplan/subscription', ['url'=>$this->url]);
  }

  public function newSubscription()
  {
    return view('admin/subscriptionplan/new-subscription', ['url'=>$this->url]);
  }

}
