<?php

namespace App\Http\Controllers\Admin;

use DB;
use Hash;
use App\User;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Faq_qus_ans;
use Helper;
 
class FaqController extends Controller
{
    public $url;
    public function __construct()
    {
        $this->url    = url(request()->route()->getPrefix());
        $this->prefix = 'admin/faq';
        $this->title  = 'FAQ';
    }

    public function index(Request $request) {
        $getQues = Faq_qus_ans::orderBy('id','desc')->paginate(10);
        if($request->ajax())
        {
          $entriesperpage = trim($request->input('entriesperpage'));
          $getQues = Faq_qus_ans::orderBy('id','desc')->paginate($entriesperpage);
          return view($this->prefix.'/faq-pagination', ['url'=>$this->url, 'title'=>$this->title, 'getQues'=>$getQues]);
        }
        else
        {
          return view($this->prefix.'/index', ['url'=>$this->url, 'title'=>$this->title,'getQues' => $getQues]);
        }
    }

    public function show($id)
    {
        $faqId = decrypt($id);
        $result = Faq_qus_ans::where(['id' => $faqId])->first();
        return view($this->prefix.'/faq-detail',['url'=>$this->url, 'title'=> 'Faq Details','result'=>$result]);
    }

    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            if ( $request->ajax() ) {
                $val = Validator::make($request->all(),[
                    'answer'    => 'required',
                    'question'   => 'required'
                ]);

                if($val->fails()) {
                    $errors             = $val->errors();
                    $data['success']    = false;
                    $data['formErrors'] = true;
                    $data['errors']     = $errors;
                } else {
                    $id = null;
                    if ($request->input('faqid')) {
                        $id = decrypt($request->input('faqid'));
                    }
                    $insertArray = array();
                    $insertArray["question"]   = (!empty($request->input('question')) && ""!=trim($request->input('question'))) ? $request->input('question') : "";
                    $insertArray["answer"]     = (!empty($request->input('answer')) && ""!=trim($request->input('answer'))) ? $request->input('answer') : "";
                    $insertArray["status"]     = ($request->input('status')) ? $request->input('status') : 0;
                    $insertArray["added_by"]   = Auth::id();
                    $checkData = Faq_qus_ans::updateOrCreate(['id'=>$id],$insertArray);
                    if($checkData)
                    {
                        DB::commit();
                        if ($request->input('faqid')) {
                            $data['success']          = true;
                            $data['url']              = 'faq';
                            $data['delayTime']        = '2000';
                            $data['success_message']  = 'Update Successfully.';
                        } else {
                            $data['success']          = true;
                            $data['url']              = 'faq';
                            $data['delayTime']        = '2000';
                            $data['success_message']  = 'Insert Successfully.';
                        }

                    }
                    else
                    {
                        DB::rollback();
                        $data['success']          = false;
                        $data['delayTime']        = '3000';
                        $data['success_message']  = 'Something went wrong.';
                    }
                }
                return response($data);
            }
        } catch (\Exception $e) {
            DB::rollback();
            $errors             = $e->getMessage();
            $data['success']    = false;
            $data['formErrors'] = true;
            $data['errors']     = $errors;
            return response($data);
        }
    }

    public function editModal(Request $request) {
        try {
            if ( $request->ajax() ) {
                $faqId = decrypt($request->input('id'));
                if ($faqId=="add") {
                    return view($this->prefix.'/add_faq_modal');
                }
                $getResult = Faq_qus_ans::where(['id' => $faqId])->first();
                return view($this->prefix.'/edit_faq_modal', ['result'=>$getResult]);
            }
        } catch (\Exception $e) {
            $errors             = $e->getMessage();
            $data['success']    = false;
            $data['formErrors'] = true;
            $data['errors']     = $errors;
            return response($data);
        }
    }


    public function destroy(Request $request, $id)
    {
        try {
                DB::beginTransaction();
                $id = decrypt($id);
                $result['user'] = Faq_qus_ans::find($id)->delete();
                if($result)
                {
                    DB::commit();
                    $data['success']          = true;
                    $data['delayTime']        = '3000';
                    $data['success_message']  = 'Faq deleted Successfully.';
                } else {
                    DB::rollback();
                    $data['success']          = false;
                    $data['delayTime']        = '3000';
                    $data['success_message']  = 'Something went wrong.';
                }
                return response($data);
            } catch (\Exception $e) {
                DB::rollback();
                $errors             = $e->getMessage();
                $data['success']    = false;
                $data['formErrors'] = true;
                $data['errors']     = $errors;
                return response($data);
            }
    }

}
