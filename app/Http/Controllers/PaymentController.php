<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Subscriptionplan;
use App\Models\Userplan;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function paymentView($id)
    {
        $getId  = decrypt($id);
        $userId = Auth::user()->id;
        $checkPlan = Subscriptionplan::where('id',$getId)->first()->toArray();
        // if ($checkPlan["price"]==0) {
        //     echo "free plan";
            // $saveFreePlan = array();
            // $saveFreePlan["plan_id"] = $getId;
            // $saveFreePlan["user_id"] = $userId;
            // Userplan::create($saveFreePlan);
            Userplan::create([
                'plan_id' => $getId,
                'user_id' => $userId
            ]);
            return Redirect("admin/dashboard")->with('status', "Plan activated successfully");
        // } else {
        //     echo "select free plan";
        // }
        // echo "<pre>";
        // print_r($checkPlan);
        // Userplan::create();
        // echo $id;
        // echo $getId;
        // die();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
