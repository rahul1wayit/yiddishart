<?php

namespace App\Http\Controllers\Buyer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrderItem;
use Auth;

class OrderController extends Controller
{
  public $prefix='';

  public function __construct()
  {
    $this->url    =  url(request()->route()->getPrefix());
    $this->prefix = 'buyer/orders';
  }

  public function index()
  {
    $userId = Auth::user()->id;
    $result = Order::with('order_details','order_items')->where('user_id', $userId)
    ->orderBy('id','desc')->paginate(1000);
    return view('buyer.orders',compact('result'));
  }

  public function orderDetail(Request $request)
  {
    $userId = Auth::user()->id;
    $orderid = decrypt($request->id);
    $result = Order::with('order_details','order_items')->where(['user_id'=> $userId,'id' => $orderid ])->first();
    return view('buyer.order-detail',compact('result'));
  }
}
