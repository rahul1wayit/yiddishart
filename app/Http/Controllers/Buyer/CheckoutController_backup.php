<?php

namespace App\Http\Controllers\Buyer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Srmklive\PayPal\Services\ExpressCheckout;
use App\Models\Carts;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\OrderItem;
use App\Models\Logs;
use Auth;
use Helper;
use Session;

class CheckoutController extends Controller
{
  public $prefix='';
  public function __construct()
  {
    $this->url    =  url(request()->route()->getPrefix());
    $this->prefix = 'buyer/checkout';
    $this->title  = 'Checkout';
    $this->invoiceId  = Helper::order_number();
  }

  public function index()
  {
    $userId = Auth::user()->id;
    $result = Carts::with('design')->where(['user_id'=> $userId])->get();
    if(!$result->isEmpty())
    return view('buyer.checkout',compact('result'));
    else
    return redirect($this->url.'/cart');
  }

  public function paypalWithPayment(Request $request)
  {
    try{
      $provider = new ExpressCheckout;
      $invoiceId = $this->invoiceId;
      $cartData = $this->cartItems($invoiceId,$request);
      $response = $provider->setExpressCheckout($cartData);
      if($request)
      {
        $mySessionVar = $request->all();
        Session::put('shipping_details', $mySessionVar);
        Session::save();
      }
      if(!empty($response))
      {
        if($response['ACK'] == 'Success')
        {
          $data['success']     = true;
          $data['url']  = $response['paypal_link'];
          $data['success_message'] = 'Please wait. You are redirected to paypal account';
        }
        else
        {
          $data['success']     = false;
          $data['error_message'] = $response['L_SHORTMESSAGE0'];
        }
        echo json_encode($data); die;
      }
    } catch (\Exception $e) {
      DB::rollback();
      $response['success']     = false;
      $response['formErrors']  = true;
      $response['error_message'] = 'Something went wrong.';
      echo json_encode($response); die;
    }
  }

  public function paypalSuccess(Request $request)
  {
    try{
      $provider = new ExpressCheckout;
      $payerID = $request->get('PayerID');
      $token = $request->get('token');
      if (empty($token) || empty($payerID)) {
        return Redirect::route('buyer/checkout');
      }else {
        $invoiceId = $this->invoiceId;
        $data = $this->cartItems($invoiceId);
        $response = $provider->getExpressCheckoutDetails($data);

        $result = $provider->doExpressCheckoutPayment($data, $token, $payerID);

        if($result['ACK'] == "Success") { //if state = approved continue..

          //get transaction details
          $transaction_id 		= $result['PAYMENTINFO_0_TRANSACTIONID'];
          $transaction_time 		= $result['PAYMENTINFO_0_ORDERTIME'];
          $transaction_amount 	= $result['PAYMENTINFO_0_AMT'];
          $transaction_method 	= 'Paypal';
          $transaction_token 	= $token;
          $transaction_state 		= $result['PAYMENTINFO_0_ACK'];

          $session_details = Session::get('shipping_details');
          // echo '<pre>sss'; dd($session_details); die;
          $userId = Auth::user()->id;
          $cart = Carts::with('design')->where(['user_id'=> $userId])->get();
          if(!$cart->isEmpty())
          {
            $total_price = 0;
            $data['items'] = [];

            foreach ($cart as $key => $item) {
              $total_price += $item->price* $item->qty;
            }
          }
          if($session_details)
          {
            $shipping_method = $session_details['shipping_method'];
            if($shipping_method == 'express')
            $shipping = 5;
            else
            $shipping = 0;
          }

          $orderData = array(
            'order_number' => $invoiceId,
            'user_id' => Auth::user()->id,
            'transaction_id' => $transaction_id,
            'subtotal' => $total_price,
            'shipping' => $shipping,
            'total_amount' => $total_price+$shipping,
            'transaction_token' => $transaction_token,
            'payer_id' => $payerID,
            'transaction_amount' => $transaction_amount,
            'transaction_method' => $transaction_method,
            'transaction_state' => $transaction_state,
            'transaction_time' => $transaction_time,
          );
          $id = null;
          $addOrder = Order::updateOrCreate(['id'=>$id],$orderData);
          if($addOrder)
          {
            // add logs
            $idd = null;
            $log_data = array(
              'log_id' => $addOrder->id,
              'action' => 'added',
              'type' => 'order',
              'added_date' => date('Y-m-d'),
              'added_time' => date('H:i:s'),
              'log_data' => serialize($result),
              'status' => '0',
              'added_by' => Auth::user()->id
            );
            Logs::updateOrCreate(['id' => $idd], $log_data);

            // add order items
            foreach ($cart as $key => $item)
            {
              $orderDetailData = [
                'order_id' => $addOrder->id,
                'design_id' => $item->design_id,
                'design_img' => $item->design_img,
                'design_name' => $item->design->name,
                'design_data' => $item->design_data,
                'price' => $item->price,
                'qty' => $item->qty,
                'total_price' => $item->price* $item->qty,
              ];
              $data['items'][] = $orderDetailData;
            }
            OrderItem::insert($data['items']);
            // add order details
            $orderDetailData = array(
              'order_id' => $addOrder->id,
              'shipping_first_name' => isset($session_details['first_name'])?$session_details['first_name']:'',
              'shipping_last_name' => isset($session_details['last_name'])?$session_details['last_name']:'',
              'shipping_email' => isset($session_details['email'])?$session_details['email']:'',
              'shipping_phone' => isset($session_details['phone'])?$session_details['phone']:'',
              'shipping_address' => isset($session_details['address'])?$session_details['address']:'',
              'shipping_town' => isset($session_details['town'])?$session_details['town']:'',
              'shipping_state' => isset($session_details['state'])?$session_details['state']:'',
              'shipping_method' => isset($session_details['shipping_method'])?$session_details['shipping_method']:'',
              'payment_method' => isset($session_details['payment_method'])?$session_details['payment_method']:'',
            );
            $orderDetail = OrderDetail::updateOrCreate(['id'=>$id],$orderDetailData);
            if($orderDetail)
            {
              Session::put('shipping_details','');
              Session::save();
              Carts::with('design')->where(['user_id'=> $userId])->delete();
              return redirect($this->url.'/orders')->with('success','Your transaction is succesfull!');
            }
          }
        }else {
          return redirect($this->url.'/checkout')->with('warning',$result['L_SHORTMESSAGE0']);
        }
      }
    }catch(PPConnectionException $ex) {
      DB::rollback();
      $errors             = $ex->getData();
      $data['success']    = false;
      $data['formErrors'] = true;
      $data['errors']     = $errors;

      $ex->getData();
    } catch (Exception $ex) {
      echo $ex->getMessage();
    }
  }

  protected function cartItems($invoiceId,$request=null)
  {
    $data = [];
    $userId = Auth::user()->id;
    $cart = Carts::with('design')->where(['user_id'=> $userId])->get();
    if(!$cart->isEmpty())
    {
      $total_price = 0;
      $data['items'] = [];

      foreach ($cart as $key => $item) {
        $total_price += $item->price* $item->qty;
        $item_details = [
          'name' => $item->design->name,
          'qty' => $item->qty,
          'price' => $item->price
        ];
        $data['items'][] = $item_details;
      }

      $data['invoice_id'] = $invoiceId;
      $data['invoice_description'] = "Order #{$data['invoice_id']} Invoice";
      $data['return_url'] = route('payment.paypalSuccess');
      $data['cancel_url'] = url('/buyer/checkout');
      $total = 0;
      foreach($data['items'] as $item) {
        $total += $item['price']*$item['qty'];
      }
      $session_details = Session::get('shipping_details');
      // echo '<pre>qqq'; print_r($session_details); die;

      if(isset($request))
      {
        if($request->get('shipping_method') == 'express')
        $shipping = 5;
        else
        $shipping = 0;
      }else if(!empty($session_details) && isset($session_details['shipping_method']))
      {
        if($session_details['shipping_method'] == 'express' )
        $shipping = 5;
        else
        $shipping = 0;
      }
      else
      {
        $shipping = 0;
      }

      $data['subtotal'] = $total;
      $data['shipping'] = $shipping;
      $data['total'] =  $total+$shipping;
      $data['email'] = Auth::user()->email;

      return $data;
    }
    else {
      $data1['success']     = false;
      $data1['url']  = URL('/'.$this->url.'/cart');
      $data1['error_message'] = 'Something went wrong.';
      echo json_encode($data1); die;
    }
  }
}
