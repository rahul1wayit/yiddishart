<?php

namespace App\Http\Controllers\Buyer;

use Hash;
use Session;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Models\Business;



class AccountSettingsController extends Controller
{
  public $prefix='';
  public function __construct()
  {
    $this->prefix='buyer';
  }
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    $userId = Auth::user()->id;
    $businessData = User::with("business")->where('id',$userId)->first()->toArray();
    //echo "<pre>"; print_r($businessData); die;
    return view($this->prefix.'/accountsettings',['data'=>[$businessData]]);
  }


  public function changePassword(Request $request)
  {
    if(Auth::Check())
    {
      $request_data = $request->All();
      $messages = [
        'password.required' => 'Please enter current password',
        'new_password.required' => 'Please enter new password',
      ];
      $validator = \Validator::make($request_data, [
        'password' => 'required',
        'new_password' => 'required|same:new_password',
        'confirm_password' => 'required|same:new_password',
      ], $messages);
      if($validator->fails())
      {
        DB::rollback();
        $errors             = $validator->errors();
        $res['success']     = false;
        $res['formErrors']  = true;
        $res['errors']      = $errors;
        echo json_encode($res);
        die();
      }
      else
      {
        $current_password = Auth::User()->password;
        if(Hash::check($request_data['password'], $current_password))
        {
          $user_id = Auth::User()->id;
          $obj_user = User::find($user_id);
          $obj_user->password = Hash::make($request_data['new_password']);;
          $obj_user->save();
          $res['success']         = true;
          $res['url']             = 'account-settings';
          $res['delayTime']       = '2000';
          $res['success_message']   = 'New Password has been changed.';
        }
        else
        {
          DB::rollback();
          $res['success']         = false;
          $res['delayTime']       = '2000';
          $res['error_message']   = 'Please enter correct current password.';
        }
      }
      echo json_encode($res);
      die();
    }
  }
  /**
  * Update Seller Genral Info.
  *
  * @return \Illuminate\Http\Response
  */
  public function changeGenralInfo(Request $request)
  {
    $imageName = "";
    if(!empty($request->profileimage)) {
      $image = $request->profileimage;
      if($image != '') {
        $fullName = $image->getClientOriginalName();
        $name = pathinfo($fullName, PATHINFO_FILENAME);
        $imageName  =  $name.'_'.time().'.'.$image->getClientOriginalExtension();
        $image->move('assets/images/profile/', $imageName);

        $userData = [
          'first_name' => $request->first_name,
          'last_name' => $request->last_name,
          'name' => $request->first_name.' '.$request->last_name,
          'gender' => $request->gender,
          'phone' => $request->phone,
          'image' => $imageName
        ];
      }
    }else{
      $userData = [
        'first_name' => $request->first_name,
        'last_name' => $request->last_name,
        'name' => $request->first_name.' '.$request->last_name,
        'gender' => $request->gender,
        'phone' => $request->phone
      ];
    }

    $userId = Auth::user()->id;

    $userUpdate = User::where('id', $userId)->update($userData);

    $businessUpdate = Business::where('user_id', $userId)->update([
      'businessname' => $request->businessname,
      'category' => $request->cat,
      'city' => $request->city,
      'state' => $request->state,
      'zip' => $request->zip,
      'description' => $request->description,
      'phone' => $request->contact
    ]);

      if($userUpdate)
      {
        DB::commit();
        $data['success']          = true;
        $data['url']              = 'dashboard';
        $data['delayTime']        = '2000';
        $data['success_message']  = 'Updated Successfully.';
      }
      else
      {
        DB::rollback();
        $data['success']          = false;
        $data['delayTime']        = '3000';
        $data['success_message']  = 'Something went wrong.';
      }
      echo json_encode($data); die();

  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    //
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    //
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    //
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    //
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    //
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    //
  }
}
