<?php

namespace App\Http\Controllers\Buyer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use Validator;
use App\Models\Design;
use App\Models\Carts;
use App\Models\Rating;

class CartController extends Controller
{
  public function __construct()
  {
    $this->url    = url(request()->route()->getPrefix());
    $this->prefix = 'buyer/cart';
    $this->limitPerPage  = 10;
  }

  public function index(Request $request)
  {
    $userId = Auth::user()->id;
    $result = Carts::with('design')->where(['user_id'=> $userId])->orderBy('id', $request->filterBy)->paginate(100);
    return view('buyer.cart',compact('result'));
  }

  public function cartList(Request $request)
  {
    $userId = Auth::user()->id;
    $result = Carts::with('design')->where(['user_id'=> $userId])->orderBy('id', $request->filterBy)->paginate(100);
    return view('buyer.cart_list',compact('result'));
  }

  public function addToCart(Request $request)
  {
    try {
      $rules = array(
        'design_id' => 'required'
      );
      if ($request->design_id) {
        $design_id = decrypt($request->design_id);
      }
      $validator = Validator::make($request->all() , $rules);
      if ($validator->fails()) {
        $errors                = $validator->errors()->first();
        $response['success']     = false;
        $response['formErrors']  = true;
        $response['error_message']      = $errors;
        echo json_encode($response); die;
      } else {
        $design = Design::find($design_id);
        if($design)
        {
          $data = array();
          $checkExist = Carts::where(['design_id'=>$design->id,'user_id'=>Auth::user()->id])->first();
          if($checkExist)
          {
            $idd = $checkExist->id;
          }else {
            $idd = null;
          }
          DB::commit();
          if($request->design_img != '') {
            $image = $request->design_img;
            list($type, $image) = explode(';', $image);
            list(, $image)      = explode(',', $image);
            $image = base64_decode($image);
            $imageName  =  time().'.jpeg';
            $jj=  file_put_contents('assets/images/seller-design/'.$imageName, $image);
            $data["design_img"] = $imageName;
          }
          $data["design_id"] = $design->id;
          $data["user_id"] = Auth::user()->id;
          $data["design_data"] = serialize($request->design_data);
          $data["price"] = $design->price;
          $data["qty"] = 1;
          $insert_id = Carts::updateOrCreate(['id' => $idd], $data);
          // echo 'sssss'; print_r($data); die;
          if($insert_id)
          {
            $id_rat=null;
            if($request->download == 1){
              $createData = [
                'design_id'=>$design_id,
                'user_id'=>Auth::user()->id,
                'download'=>$request->download
              ];
            }
            if(Rating::updateOrCreate(['design_id'=>$design_id],$createData))
            {
              DB::commit();
             return $insert_id;
            }
            else
            {
              DB::rollback();
              return '';
            }

            
          }else {
            # code...
            return '';
          }
        }
      }
    } catch (\Exception $e) {
      DB::rollback();
      $errors             = $e->getMessage();
      $data['success']    = false;
      $data['formErrors'] = true;
      $data['errors']     = $errors;
      echo json_encode($data); die;
    }

  }

  public function removeUpdateFromCart(Request $req)
  {
    try {
      DB::beginTransaction();
      $id = $req->cartId;
      $item_count = $req->item_count;
      if($item_count != '')
      {
        $result = Carts::where('id', $id)->update(['qty' => $item_count]);
        $success_message = 'Design template quantity has been updated.';
      }
      else
      {
        $result = Carts::find($id)->delete();
        $success_message = 'Design template has been removed from cart.';
      }
      if($result)
      {
        DB::commit();
        $data['success']          = true;
        $data['delayTime']        = '1000';
        $data['success_message']  = $success_message;
      }
      else
      {
        DB::rollback();
        $data['success']          = false;
        $data['delayTime']        = '3000';
        $data['success_message']  = 'Something went wrong.';
      }
      echo json_encode($data); die();
    }
    catch (\Exception $e)
    {
      DB::rollback();
      $errors             = $e->getMessage();
      $data['success']    = false;
      $data['formErrors'] = true;
      $data['errors']     = $errors;
      echo json_encode($data); die();
    }
  }

  public function checkout(Request $request)
  {
    $userId = Auth::user()->id;
    $result = Carts::with('design')->where(['user_id'=> $userId])->paginate(100);
    return view('buyer.checkout',compact('result'));
  }
}
