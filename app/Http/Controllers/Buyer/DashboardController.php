<?php

namespace App\Http\Controllers\Buyer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Design;
use App\Models\Rating;
use Auth;
use DB;

class DashboardController extends Controller
{
  public function __construct()
  {
    $this->middleware(['auth', 'verified']);
  }
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    $userId = Auth::user()->id;
    $result = Design::with('business_category')->where('added_by', $userId)->orderBy('id', 'DESC')->get();
    return view('buyer.dashboard',compact('result'));
  }

  public function addRatingFaviorite(Request $request)
  {
    if(Auth::Check())
    {
      $checkRating = Rating::where(['design_id'=>$request->design_id,'user_id'=>Auth::user()->id])->first();
      if($checkRating)
      {
        if($request->download != '')
        {
          $updateData = [
            'download'=>$request->download
          ];
        }
        if($request->favourite != '')
        {
          $updateData = [
            'favourite'=>$request->favourite
          ];
        }
        if($request->rating != '')
        {
          $updateData = [
            'rating'=>$request->rating
          ];
        }

        $ratingUpdate = Rating::where(['design_id'=>$request->design_id,'user_id'=>Auth::user()->id])->update($updateData);
        if($ratingUpdate)
        {

          DB::commit();
          return 'true';
        }
        else
        {
          DB::rollback();
        }
      }else {
        $id = null;
        if($request->download != '')
        {
          $createData = [
            'design_id'=>$request->design_id,
            'user_id'=>Auth::user()->id,
            'download'=>$request->download
          ];
        }
        if($request->favourite != '')
        {
          $createData = [
            'design_id'=>$request->design_id,
            'user_id'=>Auth::user()->id,
            'favourite'=>$request->favourite
          ];
        }
        if($request->rating != '')
        {
          $createData = [
            'design_id'=>$request->design_id,
            'user_id'=>Auth::user()->id,
            'rating'=>$request->rating
          ];
        }
        if(Rating::updateOrCreate(['id'=>$id],$createData))
        {
          DB::commit();
          return 'true';
        }
        else
        {
          DB::rollback();
        }
      }
    }
  }
}
