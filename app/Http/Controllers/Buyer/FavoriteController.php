<?php

namespace App\Http\Controllers\Buyer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Design;
use App\Models\Rating;
use Auth;
use DB;
use Crypt;

class FavoriteController extends Controller
{
  public function __construct()
  {
    $this->middleware(['auth', 'verified']);
    $this->url    = url(request()->route()->getPrefix());
    $this->prefix = 'admin/buyer';
    $this->limitPerPage  = 10;
  }
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index(Request $request)
  {
    if ($request->has('design_id'))
    {
      $des_id = Crypt::decrypt($request->get('design_id'));
      $checkRating = Rating::where(['design_id'=>$des_id,'user_id'=>Auth::user()->id])->first();
      if($checkRating)
      {
        $updateData = [
          'favourite'=>1
        ];
        $ratingUpdate = Rating::where(['design_id'=>$des_id,'user_id'=>Auth::user()->id])->update($updateData);
      }else {
        $id = null;
        $createData = [
          'design_id'=>$des_id,
          'user_id'=>Auth::user()->id,
          'favourite'=>1
        ];
        Rating::updateOrCreate(['id'=>$id],$createData);
      }
    } 

    $userId = Auth::user()->id;
    $result = Design::with('business_category','ratings')
    ->whereHas('ratings', function($q){
      $q->where('user_id',Auth()->user()->id);
      $q->where('favourite','1');
    })
    ->orderBy('id','desc')
    ->paginate('100');
    return view('buyer.favorite',compact('result'));
  }

  public function downloads(Request $request)
  {
    if ($request->has('design_id'))
    {
      $des_id = Crypt::decrypt($request->get('design_id'));
      $checkRating = Rating::where(['design_id'=>$des_id,'user_id'=>Auth::user()->id])->first();
      if($checkRating)
      {
        $updateData = [
          'download'=>1
        ];
        $ratingUpdate = Rating::where(['design_id'=>$des_id,'user_id'=>Auth::user()->id])->update($updateData);
      }else {
        $id = null;
        $createData = [
          'design_id'=>$des_id,
          'user_id'=>Auth::user()->id,
          'download'=>1
        ];
        Rating::updateOrCreate(['id'=>$id],$createData);
      }
    }
    $userId = Auth::user()->id;
    $result = Design::with('business_category','ratings')
    ->whereHas('ratings', function($q){
      $q->where('user_id',Auth()->user()->id);
      $q->where('download','1');
    })
    ->orderBy('id','desc')
    ->paginate($this->limitPerPage);
    if($request->ajax())
    {
      return view('buyer.pagination-downloads',compact('result'))->with('i', (request()->input('page', 1) - 1) * $this->limitPerPage);
    } else {
      return view('buyer.downloads',compact('result'))->with('i', (request()->input('page', 1) - 1) * $this->limitPerPage);
    }
  }

  //search Downloads
  public function searchDownload(Request $request)
  {
    $userId = Auth::user()->id;
    $entriesperpage = trim($request->input('entriesperpage'));
    $result = Design::with('business_category','ratings')
    ->whereHas('ratings', function($q){
      $q->where('user_id',Auth()->user()->id);
      $q->where('download','1');
    })
    ->orderBy('id','desc')
    ->paginate($entriesperpage);
    return view('buyer.pagination-downloads',compact('result'))->with('i', (request()->input('page', 1) - 1) * $entriesperpage);
  }

  public function editTemplate(Request $request)
  {
    $id = decrypt($request->id);
    $result = Design::where('id', $id)->first();
     //echo '<pre>';   print_r($result); die;
    return view('buyer.edit-template',compact('result'));
  }

  public function uploadSingleImage(Request $request) {
    if ( !empty($_FILES) && $_FILES['picture']['name'] )
    {
      $length = 6;
      $image = $request->file('picture');
      $realName = $request->input('imgName');
      $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
      $randomString1 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
      $randomString = $randomString . $randomString1;
      $fileParts1 = explode(".", $realName);
      $ext1 = $fileParts1[count($fileParts1) - 1];
      $image_name = $randomString . '.' . $ext1;
      $img_path = base_path().'/assets/images/seller-design';
      $image->move($img_path, $image_name);
      $imageName = URL('/') . '/assets/images/seller-design/' . $image_name;
      return $imageName;
    }
  }

  public function addRatingReview(Request $request)
  {
    if(Auth::Check())
    {
      $checkRating = Rating::where(['design_id'=>$request->design_id,'user_id'=>Auth::user()->id])->first();
      if($checkRating)
      {
        if($request->rating != '')
        {
          $updateData = [
            'rating'=>$request->rating,
            'review'=>$request->review
          ];
        }
        $ratingUpdate = Rating::where(['design_id'=>$request->design_id,'user_id'=>Auth::user()->id])->update($updateData);
        if($ratingUpdate)
        {
          DB::commit();
          return 'true';
        }
        else
        {
          DB::rollback();
        }
      }else {
        $id = null;
        if($request->rating != '')
        {
          $createData = [
            'design_id'=>$request->design_id,
            'user_id'=>Auth::user()->id,
            'rating'=>$request->rating,
            'review'=>$request->review
          ];
        }
        if(Rating::updateOrCreate(['id'=>$id],$createData))
        {
          DB::commit();
          return 'true';
        }
        else
        {
          DB::rollback();
        }
      }
    }
  }

  //getRatingReview
  public function getRatingReview(Request $request)
  {
    if(Auth::Check())
    {
      $checkRating = Rating::select('rating','review')->where(['design_id'=>$request->design_id,'user_id'=>Auth::user()->id])->first();
      if($checkRating)
      {
        echo json_encode($checkRating); die;
      }
      else
      {
        $checkRating = [];
        echo json_encode($checkRating); die;
      }
    }
  }
}
