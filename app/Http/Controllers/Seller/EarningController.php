<?php

namespace App\Http\Controllers\Seller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Helper;
use DB;
use Auth;
use App\User;
use App\Models\Design;
use App\Models\Fonts;
use App\Models\Logs;
use App\Models\Order;
use App\Models\OrderItem;
use Maatwebsite\Excel\Facades\Excel;

class EarningController extends Controller
{
  public function __construct()
  {
    $this->url    = url(request()->route()->getPrefix());
    $this->prefix = 'admin/sellers';
    $this->limitPerPage  = 10;
  }

  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */

  public function index(Request $request)
  {
    $userId = Auth::user()->id;
    $result = Design::with('business_category','orderItems')->where(['added_by'=> $userId,'status'=> '1'])->orderBy('id', $request->filterBy)->paginate(10);
    if($request->ajax()) {
      return view('seller.earning-ajax',compact('result'));
    } else {
      return view('seller.earning',compact('result'));
    }
  }


  public function earningsExport(Request $request)
  {

      set_time_limit(0);  
      ini_set('memory_limit', '1G');
      // $validator = Validator::make($request->all(), [
      //     'from_date' => ['required', 'string', 'date_format:d-m-Y'],
      //     'to_date' => ['required', 'string', 'date_format:d-m-Y'],
      //     //'image'=> ['mimes:jpeg,png,jpg,gif,svg'],
      // ]);

      // if ($validator->fails()) {
      //   $returnData['validation'] = true;
      //   $returnData['success']=false;
      //   $returnData['error']=false;
      //   $returnData['message']=$validator->messages();
      //   return response($returnData);die();
      // }

      $from_date="";
      $to_date="";
      if($request->has('from_date')){
        $from_date = strtotime($request->from_date.' 00:00:00');
        //$from_date = date('Y-m-d',strtotime($request->from_date));
      }
      if($request->has('to_date')){
        //$to_date = date('Y-m-d',strtotime($request->to_date));
         $to_date = strtotime($request->to_date.' 23:59:59');
      }
      $data=[];

      $userId = Auth::user()->id;
      $result = Design::with('business_category','orderItems')->where(['added_by'=> $userId,'status'=> '1'])->orderBy('id', $request->filterBy)->paginate(10);
      
      
                           
      $data_array[] = [
                        'Sr. No.',
                       // 'Booking No.',
                        'Design type',
                        'Design',
                        'Price',
                        'No. of downloads',
                        
                      ];  
      if(isset($result) && sizeof($result) > 0){
        $i=0;  
        foreach ($result as $key => $value) {
          $array=[];
          $array['Sr. No.']= ++$i;
          //$array['Booking No.']= isset($value->bookingRef) ? $value->bookingRef : "";
          $array['Design type']=isset($value->business_category->name) ? $value->business_category->name: "-";
          $array['Design']= isset($value->name) ? $value->name : "-";
          $array['Price'] = isset($value->price) ? "$".number_format((float)$value->price,2) : "-";
          $array['No. of downloads'] = $value->orderItems->sum('qty');
          $data_array[] = $array;
        }
      } 
      array_splice($data_array, 0, 1);
      
        try
        {
            return Excel::create('earnings', function($excel) use ($data_array) {
                $excel->sheet('Earnings', function($sheet) use ($data_array)
                {
                    $sheet->setWidth('A', 25);
                    $sheet->setWidth('B', 25);
                    $sheet->setWidth('C', 25);
                    $sheet->setWidth('D', 25);
                    $sheet->setWidth('E', 25);
                    $sheet->fromArray($data_array);
                    $sheet->getStyle('A1:R1')->applyFromArray([
                      'font' => [
                          'name' =>'Calibri',
                          'size' =>  12,
                          'bold' => true
                      ]
                    ]);
                });
            })->export('xls');
        }
        catch (\Exception $e)
        {
            DB::rollback();
            $errors             = $e->getMessage();
            $res['success']     = false;
            $res['url']         = $this->url.'/earnings';
            $res['formErrors']  = true;
            $res['errors']      = $errors;
            echo json_encode($res);
            die();
        } 

  }

  //search Uploads
  public function searchEarnings(Request $request)
  {
    $userId = Auth::user()->id;
    $entriesperpage = trim($request->input('entriesperpage'));

    $search = trim($request->input('search'));
    $result = Design::with('business_category')->where('added_by', $userId)->orderBy('id','desc')->paginate($entriesperpage);
    if($search == 'empty')
    {
      $result = Design::with('business_category')->where('added_by', $userId)->orderBy('id','desc')->paginate($entriesperpage);
    }
    else if($search != '')
    {
      // echo 'sss'; die;
      // $result = Design::with('business_category')->where('added_by', $userId)->orderBy('id','desc')->paginate($entriesperpage);
      $result = Design::with('business_category')->where('added_by', $userId)
      ->where(function($q) use ($search){
        $q->where('name','LIKE','%'.$search.'%');
        $q->orWhere('description', 'LIKE', '%'.$search.'%');
      })
      ->orderBy('id','desc')
      ->paginate($entriesperpage);
    }
    return view('seller.earning-ajax',compact('result'))->with('i', (request()->input('page', 1) - 1) * $entriesperpage);

  }

  public function get_earning_details(Request $request)
  {
    $userId = Auth::user()->id;
    $id = decrypt($request->design_id);
    $result = Design::where('id', $id)->first();
    $last_order = OrderItem::with('design')->whereHas('design',function($q) use ($userId,$id){
        $q->where('design_id',$id);
        $q->where('added_by',$userId);
    })->orderBy('id','desc')->first();
    $all_order = Order::with('order_items')->whereHas('order_items',function($q) use ($id){
        $q->where('design_id',$id);
    })->groupBy('user_id')->limit('3')->orderBy('id','desc')->get();
    // echo '<pre>'; print_r($all_order->toArray()); die;
    return view('seller.get-earning-details',['result'=> $result,'last_order' => $last_order,'all_order' => $all_order]);
  }

  public function download_csv()
  {
    //
  }
}
