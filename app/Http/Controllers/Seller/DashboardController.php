<?php

namespace App\Http\Controllers\Seller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Design;
use App\Models\OrderItem;
use Auth;
use Helper;

class DashboardController extends Controller
{

  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    //$this->middleware('auth');
    $this->middleware(['auth', 'verified']);
    $this->limitPerPage  = 5;

  }
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    $userId = Auth::user()->id;
    $result = Design::with('business_category')->where('added_by', $userId)->orderBy('id', 'DESC')->get();
    $last_order = OrderItem::with('design')->whereHas('design',function($q) use ($userId){
        $q->where('added_by',$userId);
    })->orderBy('id','desc')->first();
    $all_order = OrderItem::with('design')->whereHas('design',function($q) use ($userId){
        $q->where('added_by',$userId);
    })->orderBy('id','desc')->get();
    $added_design = Helper::get_seller_logs($userId);
    // echo '<pre>'; print_r($order->toArray()); die;
    if($last_order == null){
      $last_order = [];
    }
    return view('seller.dashboard',['result'=> $result,'last_order' => $last_order,'all_order' => $all_order,'added_design' => $added_design]);
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    //
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    //
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    //
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    //
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    //
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    //
  }
}
