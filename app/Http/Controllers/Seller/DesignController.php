<?php

namespace App\Http\Controllers\Seller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;
use Helper;
use DB;
use App\User;
use App\Models\Design;
use App\Models\Fonts;
use App\Models\Logs;

class DesignController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */

  public function __construct()
  {
    $this->url    = url(request()->route()->getPrefix());
    $this->prefix = 'admin/sellers';
    $this->limitPerPage  = 10;
  }

  public function index(Request $request)
  {
    $userId = Auth::user()->id;
    $result = Design::with('business_category')->where(['added_by'=> $userId,'status'=> '1'])->orderBy('id', $request->filterBy)->paginate(8);
    // echo '<pre>'; print_r($result->toArray()); die;
    if($request->ajax()) {
      return view('seller.design-ajax',compact('result'));
    } else {
      return view('seller.design',compact('result'));
    }
  }

  public function getEditDesignModal(Request $request)
  {
    $id = decrypt($request->id);
    $result = Design::where('id', $id)->first();
    return view('seller.designeditmodal',compact('result'));
  }

  public function getUploads(Request $request)
  {
    $userId = Auth::user()->id;
    $result = Design::with('business_category')->where('added_by', $userId)->orderBy('id', 'DESC')->paginate(12);
    if($request->ajax()) {
      return view('seller.uploadlist',compact('result'))->with('i', (request()->input('page', 1) - 1) * $this->limitPerPage);
    } else {
      return view('seller.upload',compact('result'))->with('i', (request()->input('page', 1) - 1) * $this->limitPerPage);
    }
  }

  //search Uploads
  public function searchUploads(Request $request)
  {
    $userId = Auth::user()->id;
    $entriesperpage = trim($request->input('entriesperpage'));

    $search = trim($request->input('search'));
    $result = Design::with('business_category')->where('added_by', $userId)->orderBy('id','desc')->paginate($entriesperpage);
    if($search == 'empty')
    {
      $result = Design::with('business_category')->where('added_by', $userId)->orderBy('id','desc')->paginate($entriesperpage);
    }
    else if($search != '')
    {
      // echo 'sss'; die;
      // $result = Design::with('business_category')->where('added_by', $userId)->orderBy('id','desc')->paginate($entriesperpage);
      $result = Design::with('business_category')->where('added_by', $userId)
      ->where(function($q) use ($search){
        $q->where('name','LIKE','%'.$search.'%');
        $q->orWhere('description', 'LIKE', '%'.$search.'%');
      })
      ->orderBy('id','desc')
      ->paginate($entriesperpage);
    }
    // $query = DB::getQueryLog();
    // echo '<pre>';   print_r($result->toArray()); die;
    // return view($this->prefix.'/seller.uploadlist', ['url'=>$this->url, 'title'=>$this->title,'sellers'=>$user]);
    // echo 'sss'.$entriesperpage; die;
    return view('seller.uploadlist',compact('result'))->with('i', (request()->input('page', 1) - 1) * $entriesperpage);

  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    //
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    $response = array();
    $id = null;
    $rules = array(
      'name' => 'required',
      'price' => 'required',
      'category' => 'required',
      'size' => 'required',
    );
    if ($request->id) {
      $id = decrypt($request->id);
    }
    // echo '<pre>'; print_r($id); die;
    $validator = Validator::make($request->all() , $rules);
    if ($validator->fails()) {
      $errors                = $validator->errors()->first();
      $response['success']     = false;
      $response['formErrors']  = true;
      $response['error_message']      = $errors;
      echo json_encode($response); die;

    } else {
      $data = array();
      if($request->designimage != '') {
        $image = $request->designimage;
        list($type, $image) = explode(';', $image);
        list(, $image)      = explode(',', $image);
        $image = base64_decode($image);
        $imageName  =  time().'.jpeg';
        $jj=  file_put_contents('assets/images/seller-design/'.$imageName, $image);
        $data["designimage"] = $imageName;
      }
      $data["name"] = $request->name;
      $data["price"] = $request->price;
      $data["description"] = $request->description;
      $data["keyword"] = $request->description;
      $data["category"] = $request->category;
      $data["actualHeight"] = $request->actualHeight;
      $data["actualWidth"]  = $request->actualWidth;
      $data["aspectRatio"]  = $request->aspectRatio;
      $data["size"] = $request->size;
      $data["status"] = '0';
      $data["canvasData"] = serialize($request->canvasData);
      $data["added_by"] = Auth::user()->id;
      $insert_id = Design::updateOrCreate(['id' => $id], $data);
      if($insert_id)
      {
        $adminemail      = User::select('email')->where('id',1)->first()->email;
        if($adminemail != '' && $request->id == '')
        {
          $action = 'added';
          $msg        = '<p>New design has been added.</p>';
          $sbj        = 'New Design Added';
          $mailData   = array('msg'=>$msg,'data'=>$data,'email'=>$adminemail);
          $emailresult = Helper::sendmail('ritika1wayit@gmail.com', 'Yiddishart', $adminemail, Auth::user()->name, $sbj , $mailData, 'mail.design');
        }else {
          $action = 'updated';
        }
        $idd = null;
        $log_data = array(
          'log_id' => $insert_id->id,
          'action' => $action,
          'type' => 'design',
          'added_date' => date('Y-m-d'),
          'added_time' => date('H:i:s'),
          'log_data' => serialize($request->canvasData),
          'log_image' => $imageName,
          'status' => '0',
          'added_by' => Auth::user()->id
        );
        Logs::updateOrCreate(['id' => $idd], $log_data);
        $response['success']         = true;
        $response['url']             = action('Seller\DesignController@index');
        $response['delayTime']       = '2000';
      }
      if ($request->id) {
        $response['url']             = route('uploads');
        $response['success_message'] = 'Design updated successfully.';
      } else {
        $response['success_message'] = 'New Design added successfully.';
      }
    }
    // return response()->json($response);
    echo json_encode($response); die;

  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    //
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    //
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    //
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    //
  }

  public function getImageEditor(Request $request)
  {
    $src = $request->src;
    return view('seller.image',['data'=>$src]);
  }

  public function uploadSingleImage(Request $request) {
    if ( !empty($_FILES) && $_FILES['picture']['name'] )
    {
      $length = 6;
      $image = $request->file('picture');
      $realName = $request->input('imgName');
      $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
      $randomString1 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
      $randomString = $randomString . $randomString1;
      $fileParts1 = explode(".", $realName);
      $ext1 = $fileParts1[count($fileParts1) - 1];
      $image_name = $randomString . '.' . $ext1;
      $img_path = base_path().'/assets/images/seller-design';
      $image->move($img_path, $image_name);
      $imageName = URL('/') . '/assets/images/seller-design/' . $image_name;
      return $imageName;
    }
    //
  }

  public function addFont(Request $request)
  {
    if($request->font_name != '')
    {
      try {
        DB::beginTransaction();
        $response = array();
        $id = null;
        $checkPrev = Fonts::where('name',$request->font_name)->whereIn('added_by', ['1', Auth::user()->id])->first();
        // $checkPrev = Fonts::where(['name'=>$request->font_name,'added_by'=>'1'])->first();
        if(!empty($checkPrev))
        {
          DB::rollback();
          $response['success']          = false;
          $response['delayTime']        = '3000';
          $response['error_message']  = 'Font Name already exist.';
        }else {
          $data["name"] = $request->font_name;
          $data["status"] = '1';
          $data["added_by"] = Auth::user()->id;
          $insert_id = Fonts::updateOrCreate(['id' => $id], $data);
          if($insert_id)
          {
            DB::commit();
            $response['success']         = true;
            $response['delayTime']        = '3000';
            $response['success_message'] = 'Font uploaded successfully.';
          }
        }
        echo json_encode($response); die;
      }
      catch (\Exception $e)
      {
        DB::rollback();
        $errors             = $e->getMessage();
        $data['success']    = false;
        $data['formErrors'] = true;
        $data['errors']     = $errors;
        echo json_encode($data); die();
      }
    }
  }

}
