<?php

namespace App\Http\Middleware;

use Closure;

class kushal
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      if($request->age <= 200){
        return redirect('user');
      }
        return $next($request);
    }
}
