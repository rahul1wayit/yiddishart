<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Models\Userplan;


class Plan
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userId =  Auth::user()->id;
        $role_name = Auth::user()->roles()->first()->name;
        $getUserPlanId = Userplan::where('user_id',$userId )->first();
        if ($getUserPlanId) {
            if($getUserPlanId["plan_id"] != 0) {
                return $next($request);
            }
        }
        return redirect('/'.$role_name.'/choose_plan');
    }
}
