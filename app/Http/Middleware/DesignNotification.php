<?php

namespace App\Http\Middleware;

use Closure;

class DesignNotification
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $notification = \App\Models\Design::where('status','0')->orderBy('id','desc')->get();
        \View::share('notification', $notification);
        return $next($request);
    }
}
