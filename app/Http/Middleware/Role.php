<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $roles=null)
    {
        if(\Session::has('role')){
          $role_name = \Session::get('role');
          if($role_name == $roles)
          {
              return $next($request);
          }
        }
        if(Auth::user()->roles()->count()!=0)
        {
            $role_name = Auth::user()->roles()->first()->name;
        }
        if($role_name==$roles)
        {
            return $next($request);
        }
        return redirect('/'.$role_name.'/');

        // if(Auth::user()->roles()->count()!=0)
        // {
        //     $role_name = Auth::user()->roles()->first()->name;
        // }
        // if($role_name==$roles)
        // {
        //     return $next($request);
        // }
        // return redirect('/'.$role_name.'/');
    }
}
