<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\MustVerifyEmail as MustVerifyEmailContract;

class User extends Authenticatable implements MustVerifyEmailContract
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'name', 'user_name', 'email', 'password', 'image', 'phone', 'gender','status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
      return $this->belongsToMany(Role::class);
    }

    public function business()
    {
      return $this->hasOne('App\Models\Business', 'user_id','id');
    }

    public function design()
    {
      return $this->hasMany('App\Models\Design', 'added_by','id');
    }

    public function getFullNameAttribute($value)
    {
      return ucfirst($this->first_name) . ' ' . ucfirst($this->last_name);
    }
}
