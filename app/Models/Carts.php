<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Carts extends Model
{
  protected $table 	= 'cart_items';
  protected $guarded = [];
  public $timestamps  = true;

  public function design()
  {
    return $this->hasOne('App\Models\Design','id','design_id');
  }
}
