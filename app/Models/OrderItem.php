<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
  protected $guarded = [];
  public $timestamps  = true;

  public function design()
  {
    return $this->hasOne('App\Models\Design', 'id','design_id');
  }

  public function order()
  {
    return $this->hasOne('App\Models\Order', 'id','order_id');
  }

  public function getTotalPricesAttribute()
  {
    return '$ '.number_format((float)$this->total_price, 2, '.', ',');
  }
}
