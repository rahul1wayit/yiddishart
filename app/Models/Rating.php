<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
  protected $guarded = [];
  public $timestamps  = true;

  public function design()
  {
    return $this->belongsTo('App\Models\Design', 'design_id','id');
  }
  
  public function user()
  {
    return $this->belongsTo('App\User', 'user_id','id');
  }
}
