<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Faq_qus_ans extends Model
{
    protected $guarded = [];
    public $timestamps  = true;
}
