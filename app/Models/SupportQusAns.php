<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SupportQusAns extends Model
{

  	protected $fillable = ['question', 'answer', 'addedby', 'common_status', 'status', 'category_id'];
	public $timestamps  = true;

    protected $guarded = [];
	
	public function support_category()
	{
	    return $this->belongsTo('App\Models\SupportCategory','category_id','id');
	}  


}
