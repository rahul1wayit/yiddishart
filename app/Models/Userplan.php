<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Userplan extends Model
{
  protected $table = 'user_plan';
  protected $fillable = ['id', 'plan_id', 'user_id'];
  public $timestamps  = true;

  public function plan()
  {
    return $this->hasOne('App\Models\Subscriptionplan','id','plan_id');
  }

}
