<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
  protected $guarded = [];
  public $timestamps  = true;

  public function getFullNameAttribute($value)
  {
    return ucfirst($this->shipping_first_name) . ' ' . ucfirst($this->shipping_last_name);
  }
}
