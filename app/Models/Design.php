<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Design extends Model
{
  protected $guarded = [];
  public $timestamps  = true;

  public function business_category()
  {
    return $this->hasOne('App\Models\Business_category', 'id','category');
  }

  public function ratings()
  {
    return $this->hasOne('App\Models\Rating', 'design_id','id');
  }

  public function totalRating()
  {
    return $this->hasMany('App\Models\Rating', 'design_id','id');
  }

  public function getNameAttribute($value)
  {
    return ucfirst($value);
  }

  public function orderItems()
  {
    return $this->hasMany('App\Models\OrderItem', 'design_id','id');
  }

  public function seller()
  {
    return $this->hasOne('App\User','id','added_by');
  }
}
