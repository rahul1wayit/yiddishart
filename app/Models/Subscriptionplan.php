<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subscriptionplan extends Model
{
  protected $table 	= 'subscription_plan';
  protected $fillable = ['id', 'title', 'color', 'price', 'offerprice', 'uploads', 'storage', 'storagelimit', 'description', 'image', 'status', 'addedby'];
  public $timestamps  = true;
}
