<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
  protected $guarded = [];
  public $timestamps  = true;

  public function order_items()
  {
    return $this->hasMany('App\Models\OrderItem','order_id','id');
  }

  public function buyer()
  {
    return $this->hasOne('App\User','id','user_id');
  }

  public function order_details()
  {
    return $this->hasOne('App\Models\OrderDetail','order_id','id');
  }

  public function getFullNameAttribute($value)
  {
    return ucfirst($this->shipping_first_name) . ' ' . ucfirst($this->shipping_last_name);
  }

}
