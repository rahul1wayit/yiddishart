<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Business extends Model
{
    protected $fillable = [
        'businessname', 'category', 'city', 'state', 'country', 'zip', 'phone', 'description', 'user_id',
    ];
    public $timestamps = true;

    public function users()
    {
      return $this->belongsToMany(User::class);
    }
}
