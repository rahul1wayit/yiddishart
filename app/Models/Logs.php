<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Logs extends Model
{
  protected $guarded = [];
  public $timestamps  = true;

  public function design()
  {
    return $this->hasOne('App\Models\Design', 'id','log_id');
  }

}
