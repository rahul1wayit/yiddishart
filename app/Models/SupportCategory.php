<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SupportCategory extends Model
{

    protected $guarded = [];
    public $timestamps  = true;

    public function support_qus_ans()
	{
	    return $this->hasMany('App\Models\SupportQusAns','category_id');
	} 

}
