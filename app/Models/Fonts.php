<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fonts extends Model
{
  protected $guarded = [];
  public $timestamps  = true;
}
