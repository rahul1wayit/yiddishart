<?php

namespace App\Traits;
use App\Models\Business_category;
use App\Models\Event;

trait CategoryTrait
{
  public function categoryAll() {
    // Get all the brands from the Business_category Table.
    $brands = Business_category::all();
    return $brands;
  }
  public function eventAll() {
    $brands = Event::orderBy('date','desc')->get();
    return $brands;
  }
}
