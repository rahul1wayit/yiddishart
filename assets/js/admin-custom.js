$(document).ready( function() {
  var site_url = $("#site_url").val();

  $(document).on("click", "#subscription-delete-plan", function() {
    var deleteId = $(this).attr('ref');
    $('#sub-plan-id').val(deleteId);
    jQuery('#deleteSubscriptionPlan').modal('show');
  });

  $(document).on("click", ".app-sidebar__toggle", function() {
    $('.main_wrapper').toggleClass('active');
  });

  $(document).on("click", ".deleteSubscription", function() {
    var deleteID = $('#sub-plan-id').val();
    // alert(site_url);
    jQuery.ajax({
      type    : 'DELETE',
      cache   : false,
      url     : site_url+'/admin/subscription_plan/'+deleteID,
      data    : {id:deleteID},
      headers : {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      success:function(res){
        var res = jQuery.parseJSON(res);
        if(res.success){
          jQuery.toast({
            heading             : 'Success',
            text                : "Subscription Plan Deleted.",
            loader              : true,
            loaderBg            : '#fff',
            showHideTransition  : 'fade',
            icon                : 'success',
            hideAfter           : 3000,
            position            : 'top-right'
          });
          setTimeout(function(){
            location.reload();
          },3000);
        }

      }
    });
  });

  $(document).on("click", "#changeAdminImage", function() {
    jQuery('#changeAdminImageModal').modal('show');
  });

  $(document).on("click", "#change-admin-password", function() {
    jQuery('#changeAdminPasswordModal').modal('show');
  });

  $(document).on("keyup", "#oldPassword", function() {
    var oldPassword = $('#oldPassword').val();

    jQuery.ajax({
      type    : 'POST',
      cache   : false,
      url     : site_url+'/admin/checkpassword',
      data    : {oldPassword:oldPassword},
      headers : {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      success:function(res){
        var res = jQuery.parseJSON(res);
        if(res.success == 1){
          $('#pass-match').show();
          $('#pass-not-match').hide();
          $(".updatePassword").prop('disabled', false);
        }else{
          $('#pass-not-match').show();
          $('#pass-match').hide();
          $('#oldPassword-error').hide();
          $(".updatePassword").prop('disabled', true);
        }

      }
    });
  });

  $(document).on("change", "#user_photo", function() {
    var user_photo = $(this).val();
    if(user_photo == ""){
      $('#user_photo-error').show();
      $('.updateImage').attr('disabled','disabled');
      return false;
    }else{
      var ext = $(this).val().split('.').pop().toLowerCase();
      if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
        $('#user_photo-error').hide();
        $('#user_photo-valierror').show();
        $('.updateImage').attr('disabled','disabled');
        return false;
      }else{
        $('#user_photo-error').hide();
        $('#user_photo-valierror').hide();
        $('.updateImage').prop("disabled", false);
        return true;
      }
    }
  });

  $(document).on("click", "#dissmiss", function() {
    $(".form-control").val('');
  });

  $(document).on("keyup", "#confirmPassword", function() {
    $(".updatePassword").prop('disabled', false);
  });
  $(document).on("keyup", "#newPassword", function() {
    $(".updatePassword").prop('disabled', false);
  });
  $(document).on("click", ".updatePassword", function() {
    if($('#oldPassword').val() == ""){
      $('#oldPassword-error').show();
    }else{
      $('#oldPassword-error').hide();
    }

    if($('#newPassword').val() == ""){
      $('#newPassword-error').show();
    }else{
      $('#newPassword-error').hide();
    }
    if($('#confirmPassword').val() == ""){
      $('#confirmPassword-error').show();
    }else{
      $('#confirmPassword-error').hide();
      if($('#confirmPassword').val() != $('#newPassword').val()){
        $('#confirmPassword-match-error').show();
        $(".updatePassword").prop('disabled', true);
      }else{
        $('#confirmPassword-match-error').hide();
        $(".updatePassword").prop('disabled', false);
        var newPassword = $('#newPassword').val();
        jQuery.ajax({
          type    : 'POST',
          cache   : false,
          url     : site_url+'/admin/changepassword',
          data    : {newPassword:newPassword},
          headers : {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          success:function(res){
            var res = jQuery.parseJSON(res);
            if(res.success == 1){
              jQuery('#changeAdminPasswordModal').modal('hide');
              jQuery.toast({
                heading             : 'Success',
                text                : "Password change successfully.",
                loader              : true,
                loaderBg            : '#fff',
                showHideTransition  : 'fade',
                icon                : 'success',
                hideAfter           : 3000,
                position            : 'top-right'
              });
            }
          }
        });
      }
    }
  });

  $('.demo').each( function() {
    $(this).minicolors({
      control: $(this).attr('data-control') || 'hue',
      defaultValue: $(this).attr('data-defaultValue') || '',
      format: $(this).attr('data-format') || 'hex',
      keywords: $(this).attr('data-keywords') || '',
      inline: $(this).attr('data-inline') === 'true',
      letterCase: $(this).attr('data-letterCase') || 'lowercase',
      opacity: $(this).attr('data-opacity'),
      position: $(this).attr('data-position') || 'bottom',
      swatches: $(this).attr('data-swatches') ? $(this).attr('data-swatches').split('|') : [],
      change: function(value, opacity) {
        if( !value ) return;
        if( opacity ) value += ', ' + opacity;
        if( typeof console === 'object' ) {
          // color
          var array = value.split(",");
          $('#color').val(array[0]);
          return false;
        }
      },
      theme: 'bootstrap'
    });
  });

  $(document).on("click", ".image-upload", function() {
    $(".profile-img").click();
  });
  function uploadImage(input) {
    if (input.files && input.files[0]) {
      var formData = new FormData();
      var file = input.files[0];
      formData.append("Filedata", file);
      var t = file.type.split('/').pop().toLowerCase();
      if (t != "jpeg" && t != "jpg" && t != "png" && t != "bmp" && t != "gif") {
        $(".error-image").html('Please select a valid image file');
        document.getElementsByClassName("profile-img").value = '';
        return false;
      }
      // if (file.size > 1024000) {
      //   $(".error-image").html('Max Upload size is 1MB only');
      //   document.getElementsByClassName("profile-img").value = '';
      //   return false;
      // }
      var reader = new FileReader();
      reader.onload = function (e) {
        $('.view-profile').attr('src', e.target.result);
        $(".error-image").html('');
      }
      reader.readAsDataURL(input.files[0]);
    }
  }
  $(".profile-img").change(function(){
    uploadImage(this);

  });

  //sellers jQuery
  jQuery(document).on('click', '#sellerShowBtn', function(){
    jQuery('.sellers').css('display','none');
    jQuery('.show').css('display','block');
  });
  jQuery(document).on('click', '#sellerEditBtn', function(){
    jQuery('.sellers').css('display','none');
    jQuery('.edit').css('display','block');
  });
  jQuery(document).on('click', '#backToSellerEdit', function(){
    jQuery('.sellers').css('display','block');
    jQuery('.edit').css('display','none');
    jQuery('.show').css('display','none');
  });
  jQuery(document).on('click', '#backToEventEdit', function(){
    jQuery('.events').css('display','block');
    jQuery('.edit').css('display','none');
    jQuery('.show').css('display','none');
  });
  jQuery(document).on('click', '#backToSellerShow', function(){
    jQuery('.sellers').css('display','block');
    jQuery('.edit').css('display','none');
    jQuery('.show').css('display','none');
  });

  jQuery(document).on('click', '.deleteSellersBtn', function(){
    jQuery('#deleteSellersModal').modal('show');
  });


  //buyers jQuery
  jQuery(document).on('click', '#buyersShowBtn', function(){
    jQuery('.buyers').css('display','none');
    jQuery('.show').css('display','block');
  });
  jQuery(document).on('click', '#buyersEditBtn', function(){
    jQuery('.buyers').css('display','none');
    jQuery('.edit').css('display','block');
  });
  jQuery(document).on('click', '#backToBuyersEdit', function(){
    jQuery('.buyers').css('display','block');
    jQuery('.edit').css('display','none');
    jQuery('.show').css('display','none');
  });
  jQuery(document).on('click', '#backToBuyersShow', function(){
    jQuery('.buyers').css('display','block');
    jQuery('.edit').css('display','none');
    jQuery('.show').css('display','none');
  });
  /*===============delete buyers=============*/
  jQuery(document).on('click', '.deleteBuyersBtn', function(){
    var userType = jQuery(this).attr('user-type');
    jQuery('.sellerHeadName').html(userType);
    jQuery('.sellerText').html(userType);
    var ref = jQuery(this).attr('ref');
    jQuery('#buyerId').val(ref);
    var type = jQuery(this).attr('data-type');
    jQuery('.deleteBuyers').attr('data-type',type);
    if(type == 'deactive'){
      jQuery('.deleteHead').css('display','none');
      jQuery('.deleteText').css('display','none');
      jQuery('.deactivateHead').css('display','block');
      jQuery('.deactivateText').css('display','block');
    }else if (type == 'activate'){
      jQuery('.deleteHead').css('display','none');
      jQuery('.deleteText').css('display','none');
      jQuery('.activateHead').css('display','block');
      jQuery('.activateText').css('display','block');
      jQuery('.activateText').css('display','block');
    }
    jQuery('#deleteBuyersModal').modal('show');
  });
  jQuery(document).on('click', '.deleteBuyers', function(){
    var buyerId = jQuery('#buyerId').val();
    var type = jQuery(this).attr('data-type');
    if(type == 'delete')
    {
      jQuery.ajax({
        type      : 'DELETE',
        dataType  : 'json',
        cache:    false,
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url       : confirm_delete+'/'+buyerId,
        success:function(res){
            $.toast({
              heading             : 'Success',
              text                : res.success_message,
              loader              : true,
              loaderBg            : '#fff',
              showHideTransition  : 'fade',
              icon                : 'success',
              hideAfter           : 2000,
              position            : 'top-right'
            });
            jQuery('#deleteBuyersModal').modal('hide');
            setTimeout(function(){
                // jQuery('#deleteBuyersModal').modal('hide');
                location.reload();
            }, 2000);
          // jQuery('.alert-success #successMsg').text(res.success_message);
          // jQuery('.alert-success').css('display','block');
          // setTimeout(function(){
          //   jQuery('#successMsg').fadeOut();
          //   location.reload();
          // },3000);
        }
      });
    }
    if(type == "deactive" || type == "activate")
    {
      jQuery.ajax({
        type      : 'POST',
        dataType  : 'json',
        cache:    false,
        data      : {buyerId:buyerId,type:type},
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url       : confirm_deactivate,
        success:function(res){
          setTimeout(function(){
            jQuery('#removeTr'+buyerId).remove();
            jQuery('#deleteBuyersModal').modal('hide');
          }, 200);
          jQuery('.alert-success #successMsg').text(res.success_message);
          jQuery('.alert-success').css('display','block');
          setTimeout(function(){
            jQuery('#successMsg').fadeOut();
            location.reload();
          },3000);
        }
      });
    }
  });
  /*==============buyer details show==============*/
  jQuery(document).on('click','.buyersShowBtn', function(){
    var ref = jQuery(this).attr('ref');
    jQuery.ajax({
      type:     'get',
      url:      get_data_url,
      cache:    false,
      data:     {ref:ref},
      dataType: 'json',
      success:function(res)
      {
        if(res.buyer.status == 0)
        {
          jQuery(document).find('#deactId').removeClass('btn btn-danger');
          jQuery(document).find('#deactId').addClass('btn btn-success');
          jQuery(document).find('#deactId').text('Activate account');
          jQuery(document).find('#deactId').attr('data-type','activate');
        }
        if(res.last_order)
        {
          jQuery(document).find('.last_order_panel').show();
          jQuery(document).find('.Revenu_panel').show();
          jQuery(document).find('.last_order_design').html(res.last_order.design_name);
          jQuery(document).find('.last_order_design_img').attr('src',site_url+'/assets/images/seller-design/'+res.last_order.design_img);
          jQuery(document).find('.last_order_design_price').html('$ '+res.last_order.total_price);
          jQuery(document).find('.last_order_design_cat').html(res.last_order.category_name);
          jQuery(document).find('.total_sales').html('$ '+res.all_order.toFixed(2));
          jQuery(document).find('.total_design_price').html('$ '+res.design.toFixed(2));
        }
        else
        {
          jQuery(document).find('.total_sales').html('$ 0.00');
          jQuery(document).find('.total_design_price').html('$ 0.00');
          jQuery(document).find('.last_order_panel').hide();
        }
        if(res != '')
        {
          // plan
          if(res.user_plan)
          {
            if(res.user_plan.plan.price != null)
            var prc = '$ '+res.user_plan.plan.price;
            else
            var prc = 'FREE';

            var plan_name = res.user_plan.plan.title.split(" ");
            jQuery(document).find('.plan_img').attr('src',site_url+'/assets/images/'+res.user_plan.plan.image);
            jQuery(document).find('.plan_name1').text(plan_name[0]);
            jQuery(document).find('.plan_name2').text(plan_name[1]);
            jQuery(document).find('.plan_upload').text(res.user_plan.plan.uploads);
            jQuery(document).find('.plan_storage').text(res.user_plan.plan.storage+'MB storage');
            jQuery(document).find('.plan_price').text(prc);
          }
          // plan end
          jQuery(document).find('#sellers_view #user_name').text(capitalizeFirstLetter(res.buyer.user_name));
          jQuery(document).find('#sellers_view #first_name').text(capitalizeFirstLetter(res.buyer.first_name));
          jQuery(document).find('#sellers_view #last_name').text(capitalizeFirstLetter(res.buyer.last_name));
          jQuery(document).find('#sellers_view #email').text(res.buyer.email);
          jQuery(document).find('#sellers_view #phone').text(res.buyer.phone);
          if(res.buyer.image != '' && res.buyer.image != null)
          {
            var img = res.buyer.image;
            var ii = site_url+'/'+"assets/images/profile/"+img;
          }else {
            if(res.buyer.gender == 'Male')
            var ii = site_url+'/'+"assets/images/dummy-male.png";
            else
            var ii = site_url+'/'+"assets/images/dummy-female.png";
          }
          jQuery(document).find('#seller_img').attr('src',ii);
          if(res.buyer.business != '' && res.buyer.business != null){
            jQuery(document).find('#sellers_view #businessName').text(res.buyer.business.businessname);
            // jQuery(document).find('#sellers_view #businessAddress').text(res.buyer.business.businessAddress);
            jQuery(document).find('#sellers_view #city').text(res.buyer.business.city);
            jQuery(document).find('#sellers_view #zipcode').text(res.buyer.business.zip);
            jQuery(document).find('#sellers_view #state').text(res.buyer.business.state);
            jQuery(document).find('#sellers_view #contactNo').text(res.buyer.business.phone);
          }
          jQuery(document).find('.deactBtn #deactId').attr('ref',res.buyer.id);
        }else {
          alert('No Record Found.');
        }
      }
    });
  });
  /*=============get data for efit process===============*/
  jQuery(document).on('click','.buyersEditBtn', function(){
    var ref = jQuery(this).attr('ref');
    jQuery('#hiddenId').val(ref);
    jQuery.ajax({
      type:     'get',
      url:      get_data_url,
      cache:    false,
      data:     {ref:ref},
      dataType: 'json',
      success:function(res){
        if(res != ''){
          jQuery('#update-buyer-form').attr('action',post_data_url+'/'+ref);
          jQuery('#editDetails #uname').val(res.buyer.user_name);
          jQuery('#editDetails #fname').val(res.buyer.first_name);
          jQuery('#editDetails #lname').val(res.buyer.last_name);
          jQuery('#editDetails #bemail').val(res.buyer.email);
          jQuery('#editDetails #phone').val(res.buyer.phone);
          if(res.buyer.gender == 'Male'){
            jQuery('#gender, #male').attr('checked','checked');
          }else if(res.buyer.gender == 'Female'){
            jQuery('#gender, #female').attr('checked','checked');
          }else{
          }
          if(res.buyer.image != '' && res.buyer.image != null)
          {
            var img = res.buyer.image;
            var ii = site_url+'/'+"assets/images/profile/"+img;
          }else {
            if(res.buyer.gender == 'Male')
            var ii = site_url+'/'+"assets/images/dummy-male.png";
            else
            var ii = site_url+'/'+"assets/images/dummy-female.png";
          }
          jQuery('.view-profile').attr('src',ii);
          if(res.buyer.business != '' && res.buyer.business != null)
          {
            var numeric = jQuery.isNumeric(res.buyer.business.category);
            if(numeric == false)
            {
              var catVal = "other";
              jQuery('.business_categories_other').css('display','block');
              jQuery('#editDetails #business_categories_other').val(res.buyer.business.category);
            }else
            {
              jQuery('#editDetails #business_categories_other').val('');
              jQuery('.business_categories_other').css('display','none');
              var catVal = res.buyer.business.category;
            }
            jQuery('#editDetails #businessName').val(res.buyer.business.businessname);
            jQuery('#editDetails #category').val(catVal);
            jQuery('#editDetails #description').val(res.buyer.business.description);
            jQuery('#editDetails #city').val(res.buyer.business.city);
            jQuery('#editDetails #zipcode').val(res.buyer.business.zip);
            jQuery('#editDetails #state').val(res.buyer.business.state);
            jQuery('#editDetails #contactNo').val(res.buyer.business.phone);
          }
        }else {
          alert('No Record Found.');
        }
      }
    });
  });
  /*===============after get data and update (post)=============*/
  jQuery(document).on("click", "#edit-subscription-plan", function() {
    var getId = jQuery(this).attr("ref");
    ajaxHit('edit-subscription-plan','get',getId);
  });

  jQuery(document).on("click", ".edit-event-plan", function() {
    var getId = jQuery(this).attr("data-id");
    ajaxHit('edit-event','get',main_url+getId+'/edit');
  });

  jQuery('body').on('click', '#mainImagesnew', function () {
    jQuery('.imagesNew').css('display', 'table-cell');
  });

  jQuery('body').on('click', '.cancelupload', function () {
    jQuery('.imagesNew').css('display', 'none');
    var aTag = $("#mainImagesnew");
    jQuery('html,body').animate({scrollTop: aTag.offset().top}, '2000');
  });

  jQuery('body').on('click', '.uploadfiles', function () {
    var uploadfiles = jQuery('.editor').children('img').attr('src');
    var url1 = jQuery(this).attr('rel');
    var realName = jQuery('.editor').children('img').attr('alt');
    var imgName = jQuery('.singleDeleteImg').attr('rel');
    var checkdiv = jQuery('.cropper-canvas').html();
    if (checkdiv != undefined) {
      var obj = jQuery('.cropper-canvas').children('img');
      var matrix = obj.css("-webkit-transform") ||
      obj.css("-moz-transform") ||
      obj.css("-ms-transform") ||
      obj.css("-o-transform") ||
      obj.css("transform");
      if (matrix !== 'none') {
        var values = matrix.split('(')[1].split(')')[0].split(',');
        var a = values[0];
        var b = values[1];
        var angle = Math.round(Math.atan2(b, a) * (180 / Math.PI));
      } else {
        var angle = 0;
      }
    } else {
      angle = 0;
    }
    if (imgName == undefined) {
      imgName = 0;
      var imgexist = 0;
    } else {
      var imgexist = 1;
    }
    jQuery('.loaderSec').show();

    var image = jQuery('.editor').children('img').attr('src');
    var vall = jQuery('.editor').children('img').attr('alt');
    var imageExtention = vall.substr(vall.indexOf(".") + 1);
    var base64ImageContent = image.replace(/^data:image\/(png|jpg|jpeg|gif|bmp|);base64,/, "");
    var blob = base64ToBlob(base64ImageContent, 'image/png');
    var formData = new FormData();
    formData.append('picture', blob);
    formData.append('imgName', imgName);
    formData.append('imgexist', imgexist);
    formData.append('realName', realName);
    formData.append('rotateAngle', angle);
    formData.append('imageExtention', imageExtention);
    jQuery.ajax({
      url: url +'/'+url1,
      type: "POST",
      cache: false,
      contentType: false,
      processData: false,
      data: formData,
      dataType    : "html",
      headers     : {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      success: function (data) {
        try {
          jQuery('.updateImage').removeAttr('disabled');

          jQuery("#removeData").trigger("click");
          jQuery('#imageupdate-holder').html(data);
          jQuery('#images_preview').html(data);
          jQuery('.imagesNew').css('display', 'none');
          jQuery('.loaderSec').hide();
          var aTag = $("#mainImagesnew");
          jQuery('html,body').animate({scrollTop: aTag.offset().top}, '2000');
        } catch (e) {

          jQuery('#imageupdate-holder').html(data);
          if (url == 'uploadProductSubImage' || url == 'uploadServiceSubImage' || url == 'uploadSalonSubImage') {
            if (jQuery('#images_preview').text() == 'No image uploaded') {
              jQuery('#images_preview').html(' ');
            }
            jQuery('#images_preview1').append(data);
          } else {
            jQuery('#images_preview1').html(data);
          }
          jQuery("#removeData").trigger("click");
          jQuery('.productAddPage').css('display', 'block');
          jQuery('.imagesNew').css('display', 'none');
          jQuery('.loaderSec').hide();
          var aTag = $("#mainImagesnew");
          jQuery('html,body').animate({scrollTop: aTag.offset().top}, '2000');
        }
      },
      error: function (data) {
        jQuery('#imageupdate-holder').html(data);
        if (url == 'uploadProductSubImage' || url == 'uploadServiceSubImage' || url == 'uploadSalonSubImage') {
          if (jQuery('#images_preview').text() == 'No image uploaded') {
            jQuery('#images_preview').html(' ');
          }
          jQuery('#images_preview1').append(data);
        } else {
          jQuery('#images_preview1').html(data);
        }
        jQuery("#removeData").trigger("click");
        jQuery('.productAddPage').css('display', 'block');
        jQuery('.imagesNew').css('display', 'none');
        jQuery('.loaderSec').hide();
        var aTag = $("#mainImagesnew");
        jQuery('html,body').animate({scrollTop: aTag.offset().top}, '2000');
      }
    });
  });

  jQuery('body').on('click', '.singleDeleteImg', function () {
    var currentVal = jQuery(this).attr('rel');
    var urlLink = jQuery(this).attr('urlLink');
    jQuery('#images_preview1').html('No image uploaded <input type="hidden" class="personalInfo" name="ownerImage" value="">');

    jQuery(this).parent('span').remove();
    jQuery.ajax({
      type: "post",
      url: url + urlLink,
      beforeSend: function () {
        $('.loaderSec').show();
      },
      complete: function () {
        $('.loaderSec').hide();
      },
      cache: false,
      data: {currentVal: currentVal},
      dataType: "json",
      headers     : {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      success: function (data) {
        jQuery('.imageName').val('');
        jQuery(".updateImage").attr('disabled', 'disabled');
      },
      error: function () {
      }
    });
  });

  jQuery('body').on('click', '.singleSalon', function () {
    jQuery('.uploadfiles').attr('rel', 'uploadSingleImage');
  });
});

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

function base64ToBlob(base64, mime)
{
  mime = mime || '';
  var sliceSize = 1024;
  var byteChars = window.atob(base64);
  var byteArrays = [];
  for (var offset = 0, len = byteChars.length; offset < len; offset += sliceSize) {
    var slice = byteChars.slice(offset, offset + sliceSize);
    var byteNumbers = new Array(slice.length);
    for (var i = 0; i < slice.length; i++) {
      byteNumbers[i] = slice.charCodeAt(i);
    }
    var byteArray = new Uint8Array(byteNumbers);
    byteArrays.push(byteArray);
  }
  return new Blob(byteArrays, {type: mime});
}

/* Harjeet code 18-aug-2019 */
jQuery('.new_faq_cat').click(function(){
  var a =this;
  if(jQuery(this).is('[disabled=disabled]')){
    return false;
  }
  jQuery(this).attr('disabled','disabled');
  var html='<div class="form-group" id="new_cat_form"><div class="row"><div class="col-md-8"><input  type="text" class="form-control" name="new_category"></div><div class="col-md-4"><button class=" btn btn-primary faq_cat_save">Save</button></div></div></div>';
  jQuery('.add_faq_html').after(html) ;
});

jQuery('body').on('click','.faq_cat_save',function(){
  var a=jQuery('input[name="new_category"]');
  var val = a.val();
  if(val==""){
    console.log("asd")
    //jQuery('input[name="new_category"]').css({'border':'1px solid red'});
    a.addClass('error-category');
    a.css({'border':'1px solid red'});
  }
})
jQuery('body').on('keydown','input[name="new_category"]',function(){
  console.log("fffss");
  var a=jQuery('input[name="new_category"]');
  var val = a.val();
    if(val!=""){

    a.css({'border':'1px solid #ced4da'});
    a.removeClass('error-category');
    }
})
