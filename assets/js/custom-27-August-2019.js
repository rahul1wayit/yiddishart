var aspectRatio   = 0;
var actualWidth   = 0;
var actualHeight  = 0;
var canvasData = [];
var site_url = jQuery("#site_url").val();

$(document).ready( function() {
  $(document).on("click", ".image-upload", function() {
    $(".profile-img").click();
  });


  function uploadImage(input) {
    if (input.files && input.files[0]) {
      var formData = new FormData();
      var file = input.files[0];
      formData.append("Filedata", file);
      var t = file.type.split('/').pop().toLowerCase();
      if (t != "jpeg" && t != "jpg" && t != "png" && t != "bmp" && t != "gif") {
        $(".error-image").html('Please select a valid image file');
        document.getElementsByClassName("profile-img").value = '';
        return false;
      }
      var reader = new FileReader();
      reader.onload = function (e) {
        $('.view-profile').attr('src', e.target.result);
        $(".error-image").html('');
      }
      reader.readAsDataURL(input.files[0]);
    }
  }

  jQuery(".profile-img").change(function(){
    uploadImage(this);
  });

  jQuery(".numberonly").keypress(function (e) {
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
      jQuery(this).parent('div').find('.onlynumbererror').remove();
      return false;
    }
  });

  jQuery(document).on("change", ".business_categories", function() {
    var optionValue = jQuery(".business_categories option:selected").val();
    if(optionValue == 'other')
    {
      jQuery('.business_categories_other').css('display','block');
    }else {
      jQuery('.business_categories_other').css('display','none');
    }
  });
  // search button click
  jQuery(document).on("click", ".frontSearch", function(e) {
    var searchName = jQuery(this).parents('.input-group').find(".searchName").val();
    jQuery('.searchName').val(searchName);
    var role = jQuery(this).attr('ref');
    var homePage = jQuery(this).attr('rel');
    if(searchName != '')
    {
      if(homePage != '')
      {
        var drpDwn = '<a href="'+site_url+'/'+role+'/dashboard" class="dropdown-item">Home</a>';
        if(role == 'buyer')
        {
          drpDwn += '<a href="'+site_url+'/'+role+'/downloads" class="dropdown-item">Downloads</a><a href="'+site_url+'/'+role+'/favourite" class="dropdown-item">Favourite</a><a href="'+site_url+'/'+role+'/cart" class="dropdown-item">Cart</a>';
        }else {
          drpDwn += '<a href="'+site_url+'/'+role+'/portfolio" class="dropdown-item">Portfolio</a><a href="'+site_url+'/'+role+'/uploads" class="dropdown-item">Uploads</a><a href="'+site_url+'/'+role+'/earnings" class="dropdown-item">Earnings</a>';

        }
        drpDwn += '<a href="'+site_url+'/'+role+'/account-settings" class="dropdown-item">Settings</a>';
        jQuery('.drpDwnAdd').html(drpDwn);
      }
      var formData = new FormData();
      formData.append('searchName', searchName);
      setTimeout(function(){
        jQuery('.searchNameOn').html(searchName);
      },200);
      ajaxHit('frontSearch','POST',site_url+'/front-search',formData);
    }
  });


  // search by filter
  jQuery(document).on("change", ".catFilter", function() {
    jQuery('.selecCat').html($(this).children("option:selected").text());
  });

  jQuery(document).on("click", ".frontCategorySearch", function() {
    var searchName = jQuery(this).parents('.input-group').find(".searchName").val();
    var searchCat = jQuery(this).parents('.input-group').find(".catFilter").val();
    jQuery('.searchName').val(searchName);
    var role = jQuery(this).attr('ref');
    var homePage = jQuery(this).attr('rel');
    // if(searchName != '')
    // {
    if(homePage != '')
    {
      var drpDwn = '<a href="'+site_url+'/'+role+'/dashboard" class="dropdown-item">Home</a>';
      if(role == 'buyer')
      {
        drpDwn += '<a href="'+site_url+'/'+role+'/downloads" class="dropdown-item">Downloads</a><a href="'+site_url+'/'+role+'/favourite" class="dropdown-item">Favourite</a><a href="'+site_url+'/'+role+'/cart" class="dropdown-item">Cart</a>';
      }else {
        drpDwn += '<a href="'+site_url+'/'+role+'/portfolio" class="dropdown-item">Portfolio</a><a href="'+site_url+'/'+role+'/uploads" class="dropdown-item">Uploads</a><a href="'+site_url+'/'+role+'/earnings" class="dropdown-item">Earnings</a>';

      }
      drpDwn += '<a href="'+site_url+'/'+role+'/account-settings" class="dropdown-item">Settings</a>';
      jQuery('.drpDwnAdd').html(drpDwn);
    }
    var formData = new FormData();
    formData.append('searchName', searchName);
    formData.append('searchCat', searchCat);
    setTimeout(function(){
      jQuery('.searchNameOn').html(searchName);
    },200);
    ajaxHit('frontSearch','POST',site_url+'/front-search',formData);
    // }
  });
  // search by category wise design
  jQuery(document).on("click", ".category_wise_design", function() {
    var searchName = '';
    var searchCat = jQuery(this).attr('rel');
    var role = jQuery(this).attr('ref');
    var homePage = jQuery(this).attr('rel');
    var formData = new FormData();
    formData.append('searchName', searchName);
    formData.append('searchCat', searchCat);
    setTimeout(function(){
      jQuery('.searchNameOn').html(searchName);
    },200);
    ajaxHit('frontSearch','POST',site_url+'/front-search',formData);
  });


  // search input
  jQuery(document).on("keypress", ".searchName", function(event) {
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
      var searchName = jQuery(this).val();
      jQuery('.searchName').val(searchName);
      var role = jQuery(this).attr('ref');
      var homePage = jQuery(this).attr('rel');
      if(searchName != '')
      {
        if(homePage != '' || homePage == 'template')
        {
          var drpDwn = '<a href="'+site_url+'/'+role+'/dashboard" class="dropdown-item">Home</a>';
          if(role == 'buyer')
          {
            drpDwn += '<a href="'+site_url+'/'+role+'/downloads" class="dropdown-item">Downloads</a><a href="'+site_url+'/'+role+'/favourite" class="dropdown-item">Favourite</a><a href="'+site_url+'/'+role+'/cart" class="dropdown-item">Cart</a>';
          }else {
            drpDwn += '<a href="'+site_url+'/'+role+'/portfolio" class="dropdown-item">Portfolio</a><a href="'+site_url+'/'+role+'/uploads" class="dropdown-item">Uploads</a><a href="'+site_url+'/'+role+'/earnings" class="dropdown-item">Earnings</a>';
          }
          drpDwn += '<a href="'+site_url+'/'+role+'/account-settings" class="dropdown-item">Settings</a>';
          jQuery('.drpDwnAdd').html(drpDwn);
        }
        var formData = new FormData();
        formData.append('searchName', searchName);
        setTimeout(function(){
          jQuery('.searchNameOn').html(searchName);
        },200);
        ajaxHit('frontSearch','POST',site_url+'/front-search',formData);
      }
    }
  });

  jQuery(document).on("click", ".favouriteBtn", function() {
    var design_id = jQuery(this).attr('data-id');
    var design_encid = jQuery(this).attr('data-encid');
    var redirectTo = jQuery(this).hasClass('redirectTo');
    if(redirectTo)
    {
      window.location.href=site_url+'/login?previous=favourite&design_id='+design_encid;
    }
    var act = jQuery(this).hasClass('active');
    if(act)
    {
      jQuery(this).removeClass('active');
    }else {
      jQuery(this).addClass('active');
    }
    var act = jQuery(this).hasClass('active');
    if(act)
    {
      jQuery(this).html('<i class="fa fa-heart"></i>');
      var formData = new FormData();
      formData.append('design_id', design_id);
      formData.append('favourite', 1);
      ajaxHit('addRatingFaviorite','POST',site_url+'/buyer/addRatingFaviorite',formData);
    }
    else
    {
      jQuery(this).html('<i class="fa fa-heart-o"></i>');
      var formData = new FormData();
      formData.append('design_id', design_id);
      formData.append('favourite', 0);
      ajaxHit('addRatingFaviorite','POST',site_url+'/buyer/addRatingFaviorite',formData);
    }
  });

  jQuery(document).on("click", ".downloadBtn", function() {
    var design_id = jQuery(this).attr('data-id');
    var design_encid = jQuery(this).attr('data-encid');
    if(design_id)
    {
      var formData = new FormData();
      formData.append('design_id', design_id);
      formData.append('download', 1);
      ajaxHit('addDownload','POST',site_url+'/buyer/addRatingFaviorite',formData);
    }
  });

  jQuery(document).on('click','.edit_Design_btn',function(event) {
    event.preventDefault();
    var getId = jQuery(this).attr("data-designid");
    jQuery.ajax({
      url         : site_url + '/seller/geteditdesignmodal',
      type        : 'POST',
      data        : { id : getId },
      headers     : {
        'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
      },
      dataType    : "html",
      beforeSend  : function () {
        jQuery(".loaderSec").css('display','block');
      },
      complete: function () {
        jQuery(".loaderSec").css('display','none');
      },
      success: function (response) {
        jQuery(".loaderSec").css('display','block');

        jQuery("#edit-design-body").html(response);
        bootbox.dialog({
          title: 'Edit Design',
          size: 'large',
          className: "my-popup-design",
          message: jQuery('div#Edit_Design').html(),
        });
        var json = jQuery.parseJSON(dbCanvasData);
        setTimeout(function(){
          if(json[0] && json[0]['name'] == 'default-image')
          {
            jQuery('canvas').attr('height', json[0]['height']);
            jQuery('canvas').attr('width' , json[0]['width']);
          }
          jQuery(json).each(function (i, val) {
            json[i]['dragstart'] = function(layer) {
              showTextDivWithCanvas(layer);
            };
            json[i]['dragstop'] = function(layer) {
              if(json[i]['customtype'] == 'image' && json[i]['name'] != 'default-image')
              updateCanvasAxis(layer,'image');
              else
              updateCanvasAxis(layer);
            };
            canvasData.push(val);
            if(val.customtype == 'image')
            jQuery('canvas').drawImage(val);
            setTimeout(function(){
              if(val.customtype == 'text')
              jQuery('canvas').drawText(val);
            },100);
          });
        },200);
        jQuery(".loaderSec").css('display','none');
      },
      error:function(response){
      }
    });
  });

  jQuery(document).on("click", ".changePwdBtn", function() {
    jQuery('#changePassword-form').css('display','block');
    jQuery('.changePwdDiv').css('display','none');
  });
  jQuery('body').on("click", ".fontBtn", function() {
    jQuery(".fontDropDwn").toggle();
  });

  jQuery(document).on("change", ".imgPreviewInp", function() {
    readURL(this);
  });

  jQuery(document).on("change", ".addNewImgInp", function() {
    jQuery(".loaderSec").css('display','block');
    readMultiImageURL(this);
  });

  jQuery(document).on("click", ".sec-page2-back", function() {
    jQuery('.Add_designhide').css('display','block');
    jQuery('.add_design_image').css('display','none');
    jQuery('.text-truncate').html(' ');
  });


  jQuery(document).on("click", ".sec-page3-back", function(ev) {
    ev.preventDefault();
    jQuery('.add_design_image').css('display','block');
    jQuery('.user_customization').css('display','none');

    for (var key in canvasData) {
      jQuery('canvas').removeLayer(canvasData[key].name).drawLayers();
    }

    jQuery('.bootbox').find('.text-settings').each(function(){
      var setField = jQuery(this).parents(".layer-trigger-wrapper");
      var checkDelete = setField.find('.deleteText').length;
      if(checkDelete == 1)
      {
        setTimeout(function(){
          jQuery('[data-name="text"]').attr('data-key',2);
        },500);
      }
      jQuery(this).next('.deleteImage').trigger('click');
      jQuery(this).next('.deleteText').trigger('click');
      setField.find(".keyBord1").css('display','block');
      setField.find(".keyBord2").css('display','none');
      setField.find(".text_1st").val("");
      setField.find(".char_limit_1st").val("");
      setField.find('input[name="x_1st"]').val("150");
      setField.find('input[name="y_1st"]').val("150");
      setField.find('input[name="angle_1st"]').val("0");
      setField.find('select[name="language_1st"]').val("qwerty");
      setField.find('select[name="font_1st"]').val("Serif");
      setField.find('select[name="font_weight_1st"]').val("normal");
      setField.find('select[name="font_size_1st"]').val("32");
      setField.find('select[name="leading_1st"]').val("1");
      setField.find('input[name="languageName"]').val("qwerty");
      setField.find('input[name="color_1st"]').val("");

    });

    jQuery('canvas').clearCanvas();
    canvasData = [];
    jQuery('.modal-title').text('Add design');

  });

  jQuery(document).on("click", ".sec-page3-next", function() {
    jQuery('.modal-title').text('Design info');
    jQuery('.canvasDataCls').val(JSON.stringify(canvasData));
    jQuery('.design_information').css('display','block');
    jQuery('.user_customization').css('display','none');
    setTimeout(function() {
      var img = jQuery('canvas').getCanvasImage('jpeg');
      jQuery('.designimage').val(img);
      jQuery.getScript(site_url+'/assets/js/lib/jquery/jquery.validate.min.js');
      jQuery.getScript(site_url+'/assets/js/form-validate.js');
    }, 100);
  });

  jQuery(document).on("click", ".sec-page4-back", function() {
    jQuery('.modal-title').text('User customization');
    jQuery('.design_information').css('display','none');
    jQuery('.user_customization').css('display','block');
  });

  //angle text limit
  jQuery(document).on("keyup", ".angle_1st", function() {
    var maxNumber = jQuery(this).val();
    if(maxNumber > 360)
    {
      var inputString = $(this).val();
      var shortenedString = inputString.substr(0,(inputString.length -1));
      jQuery(this).val(shortenedString);
    }
  });

  //text limit
  jQuery(document).on("blur change", ".char_limit_1st", function(e) {

    var ele = e.target;
    var key = $(ele).parents('.text-settings').attr('data-key');
    var name = $(ele).parents('.text-settings').attr('data-name');
    var language = $(ele).parents('.layer-trigger-wrapper').find('.language').val();
    if(language == 'qwerty')
    var keyBord = 'keyBord1';
    else
    var keyBord = 'keyBord2';

    var charLimit = jQuery(this).val();
    var inputText = jQuery(this).parents('.layer-trigger-wrapper').find('.'+keyBord).val();
    if(inputText.length >= charLimit && charLimit > 0)
    {
      var kk = inputText.substring(0,charLimit);
      jQuery(this).parents('.layer-trigger-wrapper').find('.text_1st').val(kk);

      if(ele.name == 'char_limit_1st')
      {
        canvasData[key]['char_limit'] = charLimit;
        canvasData[key]['text'] = kk;
        jQuery('canvas').setLayer(name,canvasData[key]).drawLayers();
      }
    }
  });

  jQuery(document).on("click", "#testt", function() {
    var l = jQuery('canvas').getLayers();
    console.log(l);
  });
  // draw image in canvas
  jQuery(document).on("click", ".sec-page2-next", function() {
    jQuery('canvas').clearCanvas();
    jQuery(".loaderSec").css('display','block');
    jQuery('.modal-title').text('User customization');
    var imgsrc = jQuery('.mainImageCls').attr('src');
    console.log('imgsrc');
    console.log(imgsrc);

    canvasData.push({
      layer: true,
      customtype: 'image',
      name: 'default-image',
      source: imgsrc,
      x: 0, y: 0,
      width: 570,
      height: 570 / aspectRatio,
      fromCenter: false
    });

    // canvasData.push({
    //   layer: true,
    //   customtype: 'image',
    //   name: 'default-image',
    //   source: imgsrc,
    //   x: 0, y: 0,
    //   width: actualWidth,
    //   height: actualHeight,
    //   fromCenter: false
    // });

    canvasData.push({
      customtype: 'image',
      name: 'logo',
      source: site_url+'/assets/images/white.jpg',
      draggable: true,
      x: 150, y: 150,
      rotate: 0,
      width: 1,
      aspectRat: 0,
      height: 1,
      layer: true,
      dragstart: function(layer) {
        showTextDivWithCanvas(layer);
      },
      dragstop: function(layer) {
        updateCanvasAxis(layer,'image');
      }
    });

    canvasData.push({
      customtype: 'text',
      name: 'text',
      fillStyle: '#000',
      draggable: true,
      strokeWidth: 2,
      x: 150, y: 100,
      lineHeight: 1,
      rotate: 0,
      language: 'qwerty',
      char_limit: '',
      letterSpacing: '',
      maxWidth: 570,
      align: 'left',
      fontSize: 32,
      layer: true,
      fontStyle: 'normal',
      fontFamily: 'Serif',
      text: '',
      dragstart: function(layer) {
        showTextDivWithCanvas(layer);
      },
      dragstop: function(layer) {
        updateCanvasAxis(layer);
      }
    });

    // console.log('canvasData1111'+JSON.stringify(canvasData));
    setTimeout(function(){
      drawCanvas();
    },200);

    jQuery('.add_design_image').css('display','none');
    jQuery('.user_customization').css('display','block');
  });

  // change effect in canvas on all text fields
  jQuery(document).on("input change", ".text-settings input, .text-settings select", function(e) {
    // e.preventDefault();
    var ele = e.target;
    var key = jQuery(ele).parents('.text-settings').attr('data-key');
    var name = jQuery(ele).parents('.text-settings').attr('data-name');
    var char_limit = jQuery(ele).parents('.text-settings').find('.char_limit_1st').val();
    var textVal = jQuery(ele).parents('.text-settings').find('.text_1st');
    if(ele.name == 'text_1st')
    {
      if(char_limit > 0)
      {
        var inputString = ele.value;
        var shortenedString = inputString.substr(0,(char_limit));
        textVal.val(shortenedString);
        canvasData[key]['text'] = ele.value;
      }else{
        canvasData[key]['text'] = ele.value;
      }
    }

    else if(ele.name == 'x_1st')
    canvasData[key]['x'] = ele.value;

    else if(ele.name == 'y_1st')
    canvasData[key]['y'] = ele.value;

    else if(ele.name == 'width_1st')
    {
      canvasData[key]['width'] = ele.value;
      var aspectRat = canvasData[key]['aspectRat'];
      var heigth_new = (ele.value/aspectRat).toFixed(2);
      jQuery(ele).parents('.text-settings').find("input[name='height_1st']").val(heigth_new);
      canvasData[key]['height'] = heigth_new;
    }
    else if(ele.name == 'height_1st')
    {
      canvasData[key]['height'] = ele.value;
      var aspectRat = canvasData[key]['aspectRat'];
      var width_new = (ele.value*aspectRat).toFixed(2);
      jQuery(ele).parents('.text-settings').find("input[name='width_1st']").val(width_new);
      canvasData[key]['width'] = width_new;
    }
    else if(ele.name == 'angle_1st')
    {
      if(ele.value.length < 4)
      {
        canvasData[key]['rotate'] = ele.value;
      }
    }

    else if(ele.name == 'font_size_1st')
    canvasData[key]['fontSize'] = ele.value;

    else if(ele.name == 'font_1st')
    canvasData[key]['fontFamily'] = ele.value;

    else if(ele.name == 'font_weight_1st')
    canvasData[key]['fontStyle'] = ele.value;

    else if(ele.name == 'color_1st')
    canvasData[key]['fillStyle'] = ele.value;

    else if(ele.name == 'leading_1st')
    canvasData[key]['lineHeight'] = ele.value;

    else if(ele.name == 'tracking_1st')
    {
      textVal.css('letter-spacing',ele.value+'px');
      canvasData[key]['text'] = textVal.val().replace(/\s/g,'');
      // console.log('aa '+canvasData[key]['text']);
      if(ele.value == 0)
      var ha = canvasData[key]['text'].split('').join('');
      if(ele.value == 1)
      var ha = canvasData[key]['text'].split('').join(' ');
      if(ele.value == 2)
      var ha = canvasData[key]['text'].split('').join('  ');
      if(ele.value == 3)
      var ha = canvasData[key]['text'].split('').join('   ');
      if(ele.value == 4)
      var ha = canvasData[key]['text'].split('').join('    ');
      if(ele.value == 5)
      var ha = canvasData[key]['text'].split('').join('     ');
      canvasData[key]['text'] = ha;
      canvasData[key]['letterSpacing'] = ele.value;
    }

    jQuery('canvas').setLayer(name,canvasData[key]).drawLayers();

    // console.log('Keyup '+key+name+JSON.stringify(canvasData));
  });


  // paragraph setting
  jQuery(document).on("click", ".text-settings a", function(e) {
    var ele = e.target;
    var key = $(ele).parents('.text-settings').attr('data-key');
    var name = $(ele).parents('.text-settings').attr('data-name');

    if(ele.name == 'left_align')
    canvasData[key]['align'] = 'left';

    else if(ele.name == 'center_align')
    canvasData[key]['align'] = 'center';

    else if(ele.name == 'right_align')
    canvasData[key]['align'] = 'right';

    jQuery('canvas').setLayer(name,canvasData[key]).drawLayers();
  });

  // add new text in jcanvas
  jQuery(document).on("click", "#add-more", function() {
    var ele =  jQuery(this);
    var cloneVar = ele.closest('span').prev(".layer-trigger-wrapper").clone();
    var layerss = jQuery('canvas').getLayers();
    var findDiv = jQuery(this).parents('.form-group').find('.textDiv');
    var newname  = findDiv.length+1;

    var newkey  = layerss.length;
    cloneVar.find(".select_textfield").attr("data-target","#textfields"+newkey);
    cloneVar.find(".select_textfield").text("Text field "+newname);
    cloneVar.find(".text-settings").attr("id","textfields"+newkey);
    cloneVar.find(".text-settings").attr("data-key",newkey);
    cloneVar.find(".text-settings").attr("data-name",'text'+newkey);
    cloneVar.find(".keyBord1").css('display','block');
    cloneVar.find(".keyBord2").css('display','none');
    cloneVar.find(".text_1st").attr("placeholder","Text field "+newname);
    cloneVar.find(".text_1st").val("");
    cloneVar.find(".char_limit_1st").val("");
    cloneVar.find(".layer-trigger").append('<i class="deleteText fa fa-times"></i>');
    cloneVar.find('input[name="x_1st"]').val("150");
    cloneVar.find('input[name="y_1st"]').val("150");
    cloneVar.find('input[name="angle_1st"]').val("0");
    cloneVar.find('select[name="language_1st"]').val("qwerty");
    cloneVar.find('select[name="tracking_1st"]').val("0");
    cloneVar.find('select[name="font_1st"]').val("Serif");
    cloneVar.find('select[name="font_weight_1st"]').val("normal");
    cloneVar.find('select[name="font_size_1st"]').val("32");
    cloneVar.find('select[name="leading_1st"]').val("1");
    cloneVar.find('input[name="languageName"]').val("qwerty");
    cloneVar.find('input[name="color_1st"]').val("");
    cloneVar.insertBefore(".add_more_text_cls");

    var totalText = 0;
    for (canvas of canvasData) {
      if(canvas.customtype == 'text')
      totalText++;
    }
    newname = totalText  + 1;
    var newobj = {
      customtype: 'text',
      name: 'text'+newkey,
      fillStyle: '#000',
      draggable: true,
      strokeWidth: 2,
      x: 150, y: 100,
      lineHeight: 1,
      rotate: 0,
      maxWidth: 570,
      language: 'qwerty',
      char_limit: '',
      letterSpacing: '',
      align: 'left',
      fontSize: 32,
      layer: true,
      fontFamily: 'Serif',
      fontStyle: 'normal',
      text: '',
      dragstart: function(layer) {
        showTextDivWithCanvas(layer);
      },
      dragstop: function(layer) {
        updateCanvasAxis(layer);
      }
    }
    canvasData.push(newobj);
    jQuery('canvas').drawText(newobj);
    // var l = jQuery('canvas').getLayers();
    console.log('canvasData'+JSON.stringify(canvasData));
  });

  // add img in canvas clone
  jQuery(document).on("click", "#add-more-image", function() {
    var ele =  jQuery(this);
    var findDiv = jQuery(this).parents('.form-group').find('.imageDiv');
    var cloneVar = ele.closest('span').prev(".layer-trigger-wrapper").clone();
    var layerss = jQuery('canvas').getLayers();
    var newkey2  = layerss.length;
    var newname  = findDiv.length+1;

    if(findDiv.length == 0)
    {
      var htt = imageHtml(newkey2);
      jQuery('.logoDiv').after(htt);
    }else {
      cloneVar.find(".select_textfield").attr("data-target","#imagefields"+newkey2);
      cloneVar.find(".select_textfield").text("Logo "+newname);
      cloneVar.find(".text-settings").attr("id","imagefields"+newkey2);
      cloneVar.find(".text-settings").attr("data-key",newkey2);
      cloneVar.find(".text-settings").attr("data-name",'image'+newkey2);
      cloneVar.find('input[name="x_1st"]').val("150");
      cloneVar.find('input[name="y_1st"]').val("150");
      cloneVar.find('input[name="width_1st"]').val("200");
      cloneVar.find('input[name="height_1st"]').val("200");
      cloneVar.find('input[name="angle_1st"]').val("0");
      cloneVar.find('.error-image').css("display","none");
      cloneVar.find('.editPatientView').val(" ");
      cloneVar.insertBefore(".add_more_image_cls");
    }

    var newobj = {
      customtype: 'image',
      name: 'image'+newkey2,
      source: site_url+'/assets/images/white.jpg',
      draggable: true,
      x: 150, y: 150,
      width: 1,
      rotate: 0,
      aspectRat: 0,
      height: 1,
      layer: true,
      dragstart: function(layer) {
        showTextDivWithCanvas(layer);
      },
      dragstop: function(layer) {
        updateCanvasAxis(layer,'image');
      }
    }
    canvasData.push(newobj);
    jQuery('canvas').drawImage(newobj);
    var sorto = {
      name:"asc"
    };
    canvasData.keySort(sorto);
    for (var key in canvasData) {
      var name = canvasData[key]['name'];
      jQuery('[data-name="'+name+'"]').attr('data-key',key);
      jQuery('canvas').moveLayer(name, key);
    }
    // console.log('canvasData'+JSON.stringify(canvasData));
  });

  jQuery(document).on("click", ".select_textfield", function(e) {
    var closeD = jQuery(this).hasClass('collapsed');
    if(closeD == false)
    {
      jQuery(this).parent('.layer-trigger').addClass('openWrap');
    }
    if(closeD == true)
    {
      jQuery(this).parent('.layer-trigger').removeClass('openWrap');
    }
  });

  jQuery(document).on("click", ".deleteText", function(e) {
    var dataKey = jQuery(this).parent('.layer-trigger').find('.text-settings').attr('data-key');
    var dataName = jQuery(this).parent('.layer-trigger').find('.text-settings').attr('data-name');
    deleteCanvasAxis(this,dataKey,dataName);
    countClone('textDiv',dataKey);
  });

  jQuery(document).on("click", ".deleteImage", function(e) {
    var dataKey = jQuery(this).parent('.layer-trigger').find('.text-settings').attr('data-key');
    var dataName = jQuery(this).parent('.layer-trigger').find('.text-settings').attr('data-name');
    deleteCanvasAxis(this,dataKey,dataName);
    countClone('imageDiv',dataKey,dataName);
  });

  jQuery('body').on('change', '.language', function() {
    jQuery(this).parents('.layer-trigger').find('.languageSelectInput').val(jQuery(this).val());
    var valu12 =  jQuery(this).parents('.layer-trigger').find('.languageSelectInput').val();
    var keys = jQuery(this).parents('.layer-trigger').find('.text-settings').attr('data-key');
    // console.log(keys);
    if(valu12 == 'hebrew-qwerty')
    {
      jQuery(this).parents('.layer-trigger').find('.keyBord2').css('display','block');
      jQuery(this).parents('.layer-trigger').find('.keyBord1').css('display','none');
      canvasData[keys]['language'] = 'hebrew-qwerty';
    }else {
      jQuery(this).parents('.layer-trigger').find('.keyBord1').css('display','block');
      jQuery(this).parents('.layer-trigger').find('.keyBord2').css('display','none');
      canvasData[keys]['language'] = 'qwerty';
    }
  });

  jQuery('button#dialogButton').on('click', function() {
    bootbox.dialog({
      title: 'Add design',
      size: 'large',
      className: "my-popup-design",
      message: jQuery('div#Add_Design').html(),
    });
    jQuery('.Add_designhide').css('display','block');
    jQuery('.add_design_image').css('display','none');
    jQuery('.user_customization').css('display','none');
    jQuery('.design_information').css('display','none');
    setTimeout(function() {
      jQuery.getScript(site_url+'/assets/js/form-validate.js');
    }, 200);
  });

  // keyboard english
  jQuery(document).on('mouseenter', '.keyBord1', function() {
    setTimeout(function(){
      jQuery('.keyBord1').keyboard({
        layout: 'qwerty',
        css: {
          input: 'form-control input-sm',
          container: 'center-block dropdown-menu',
          buttonDefault: 'btn btn-default',
          buttonHover: 'btn-primary',
          buttonAction: 'active',
          buttonDisabled: 'disabled'
        },
        usePreview: false
      })
      .addTyping({
        showTyping: true,
        delay: 50
      }).previewKeyset();
    },200);
  });

  // keyboard hebrew
  jQuery(document).on('mouseenter', '.keyBord2', function() {
    jQuery('.keyBord2').keyboard({
      layout: 'hebrew-qwerty',
      css: {
        input: 'form-control input-sm rtl',
        container: 'center-block dropdown-menu',
        buttonDefault: 'btn btn-default',
        buttonHover: 'btn-primary',
        buttonAction: 'active',
        buttonDisabled: 'disabled',
        direction: 'rtl'
      },
      restrictInput: true
    })
    .addTyping({
      showTyping: true,
      delay: 50
    });
  });

  jQuery(document).on('change', '.fileUp', function(){
    jQuery(this).parent().find('.form-control').val(jQuery(this).val().replace(/C:\\fakepath\\/i, ''));
    jQuery('.editPatientView').val(jQuery(this).val().replace(/C:\\fakepath\\/i, ''));
    jQuery('.upload_bx').removeClass('success');
    jQuery('.upload_bx').css('border', '1px solid #00acb9');
  });

  jQuery(document).on('click', '.browse', function(){
    var file = jQuery(this).parent().parent().parent().find('.fileUp');
    file.trigger('click');
    jQuery('.upload_bx').removeClass('success');
  });

  /* submit button validation */
  jQuery(document).on('submit', '#addDesignForm', function(e){
    e.preventDefault();
    var count = 0;
    jQuery(".success").each(function () {
      var text = jQuery(this).val();
      text = text.trim();
      if (text == '') {
        var textPl = jQuery(this).attr('placeholder');
        jQuery(this).attr('placeholder', textPl);
        jQuery(this).css('border', '1.8px solid #a94442');
        count = parseInt(count) + parseInt(1);
        jQuery(this).addClass('error');
        jQuery(this).focus();
      } else
      {
        count = parseInt(count) - parseInt(1);
        jQuery(this).removeClass('error');
      }
    });
    if (count > 0) {
      return false;
    }else {
      var type = 'json';
      var form = jQuery(this)[0];
      // console.log('form '+JSON.stringify(form));
      var formData = new FormData(form);
      formData.append('actualHeight', actualHeight);
      formData.append('actualWidth', actualWidth);
      formData.append('aspectRatio', aspectRatio);
      // console.log(JSON.stringify(formData));
      var url =jQuery(this).attr('action');
      $.ajax({
        beforeSend: function() { $('.loaderSec').show(); },
        complete: function() { $('.loaderSec').hide() },
        url: url,
        type: 'POST',
        dataType: type,
        data: formData,
        processData: false,
        contentType: false,
        headers     : {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (res) {
          toster(res);
        },
        error: function (e) {
          console.log(e);
          alert('Error while request..');
        }
      });
    }
  });

  /* add font validation */
  jQuery(document).on('submit', '#add_font_form', function(e){
    e.preventDefault();
    var count = 0;
    jQuery(".success2").each(function () {
      var text = jQuery(this).val();
      text = text.trim();
      if (text == '') {
        var textPl = jQuery(this).attr('placeholder');
        jQuery(this).attr('placeholder', textPl);
        jQuery(this).css('border', '1.8px solid #a94442');
        count = parseInt(count) + parseInt(1);
        jQuery(this).addClass('error');
        jQuery(this).focus();
      } else
      {
        count = parseInt(count) - parseInt(1);
        jQuery(this).removeClass('error');
      }
    });
    if (count > 0) {
      return false;
    }else {
      var type = 'json';
      var form = jQuery(this)[0];
      var font_name = jQuery('.font_name').val();
      var formData = new FormData(form);
      var url =jQuery(this).attr('action');
      $.ajax({
        beforeSend: function() { $('.loaderSec').show(); },
        complete: function() { $('.loaderSec').hide() },
        url: url,
        type: 'POST',
        dataType: type,
        data: formData,
        processData: false,
        contentType: false,
        headers     : {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (res) {
          setTimeout(function(){
            if(res.success)
            {
              jQuery('#addFontModel').addClass('displayNone');
              jQuery('#addFontModel').removeClass('show');
              jQuery('.font_1st').append('<option value="'+font_name+'">'+font_name+'</option>');
            }
          },3000);
          toster(res);
        },
        error: function (e) {
          alert('Error while request..');
        }
      });
    }
  });

  jQuery(document).on('click','.uploadFontBtn', function() {
    jQuery('#addFontModel').removeClass('displayNone');
    jQuery('.font_name').val('');
    setTimeout(function() {
      jQuery('#addFontModel').modal('show');
    }, 200);
  });

  // buyer favourite page start
  jQuery('.addQty').click(function (e) {
    var ele = e.target;
    if (jQuery(this).prev().val() < 10) {
      var fav = jQuery(this).attr('ref');
      jQuery(this).prev().val(+jQuery(this).prev().val() + 1);
      var total = jQuery(this).prev().val()*jQuery(this).parents('.favTr').find('.total_price').attr('ref');
      var total_price = '$ '+total.toFixed(2);
      jQuery(this).parents('.favTr').find('.total_price').text(total_price);
      if(fav != 'fav')
      totalPriceCount(ele);
    }
  });

  jQuery('.subQty').click(function (e) {
    var ele = e.target;
    if (jQuery(this).next().val() > 1) {
      var fav = jQuery(this).attr('ref');
      jQuery(this).next().val(+jQuery(this).next().val() - 1);
      var total_price = '$ '+jQuery(this).next().val()*jQuery(this).parents('.favTr').find('.total_price').attr('ref')+'.00';
      jQuery(this).parents('.favTr').find('.total_price').text(total_price);
      if(fav != 'fav')
      totalPriceCount(ele);
    }
  });

  jQuery('.qty').change(function () {
    var total_price = '$ '+jQuery(this).val()*jQuery(this).parents('.favTr').find('.total_price').attr('ref')+'.00';
    jQuery(this).parents('.favTr').find('.total_price').text(total_price);
  });

  jQuery('.design_qty').change(function (e) {
    var ele = e.target;
    if (jQuery(this).val() >= 1)
    {
      var total_price = '$ '+jQuery(this).val()*jQuery(this).parents('.favTr').find('.total_price').attr('ref')+'.00';
      jQuery(this).parents('.favTr').find('.total_price').text(total_price);
      totalPriceCount(ele);
    }
  });
  // buyer favourite page end

  jQuery(document).on('click','.bootbox-close-button',function(e){
    e.preventDefault();

    var conf = confirm('Are you sure you want to close this window?');
    if(conf)
    {
      jQuery('.modal').modal('hide');
      jQuery('.mainImageCls').attr('src','');

      for (var key in canvasData) {
        jQuery('canvas').removeLayer(canvasData[key].name).drawLayers();
      }
      jQuery('canvas').clearCanvas();
      canvasData = [];
    }
    else {
      return false;
    }
  });

  jQuery(document).on('click','.remove-design',function(e){
    var ele = e.target;
    var cartId = jQuery(this).attr('ref');
    bootbox.confirm({
      title: "Remove design?",
      message: "Do you want to remove this design from cart?",
      buttons: {
        cancel: {
          label: '<i class="fa fa-times"></i> Cancel'
        },
        confirm: {
          label: '<i class="fa fa-check"></i> Confirm'
        }
      },
      callback: function (result) {
        if(result == true)
        {
          jQuery('.cartTr'+cartId).remove();
          var trCount = jQuery('.favTr').length;
          if(trCount == 0)
          {
            jQuery('.noFound1').hide();
            jQuery('.noFound').show();
          }
          totalPriceCount(ele,'remove');
        }
      }
    });
    jQuery('.close').removeClass('bootbox-close-button');
    jQuery('.close').addClass('closeBootBox');
  });

  jQuery(document).on('click','.shipping_method',function(e){
    var shipping = jQuery(this).val();
    var total = jQuery('.total_order').attr('ref');
    if(shipping == 'regular')
    {
      jQuery('.shipping_class').html('FREE');
      var new_total = parseFloat(total);
      jQuery('.total_order').html('$ '+new_total.toFixed(2));
    }
    if(shipping == 'express')
    {
      jQuery('.shipping_class').html('$ 5.00');
      var new_total = parseFloat(total)+5;
      jQuery('.total_order').html('$ '+new_total.toFixed(2));
    }
  });

  jQuery(document).on("click", ".view_earning_detail", function() {
    var design_id = jQuery(this).attr('data-id');
    if(design_id)
    {
      var formData = new FormData();
      formData.append('design_id', design_id);
      ajaxHit('get_earning_details','POST',site_url+'/seller/get_earning_details',formData);
    }
  });

  jQuery('.add_rating_btn').on('click', function() {
    var design_id = jQuery(this).attr('ref');
    if(design_id != '')
    {
      jQuery('.design_id_inpt').val(design_id);
      var formData = new FormData();
      formData.append('design_id', design_id);
      ajaxHit('getRatingReview','POST',site_url+'/buyer/getRatingReview',formData);
    }
  });

  jQuery('.add_rating_submit_btn').on('click', function() {
    var design_id = jQuery(this).parents('.add_rating_form').find('.design_id_inpt').val();
    var rating = jQuery(this).parents('.add_rating_form').find('.rating_inpt').val();
    var review = jQuery(this).parents('.add_rating_form').find('.review_input').val();
    if(rating != '' || review !='')
    {
      var formData = new FormData();
      formData.append('design_id', design_id);
      formData.append('rating', rating);
      formData.append('review', review);
      ajaxHit('addRatingReview','POST',site_url+'/buyer/addRatingReview',formData);
    }
  });

  function totalPriceCount(ele,type = null)
  {
    var over_total = 0;
    jQuery('.total_price').each(function(){
      var value = parseFloat(jQuery(this).parents('.favTr').find('.qty').val()*jQuery(this).parents('.favTr').find('.total_price').attr('ref'));
      if (!isNaN(value)) {
        over_total += value;
      }
    });
    jQuery('.over_total').html('$ '+over_total.toFixed(2));
    var cartId = jQuery(ele).parents('.favTr').find('.remove-design').attr('ref');
    if(cartId != '')
    {
      var item_count = jQuery(ele).parents('.favTr').find('.qty').val();
      console.log(item_count+cartId);
      var formData = new FormData();
      formData.append('cartId', cartId);
      if(type != 'remove')
      formData.append('item_count', item_count);
      ajaxHit('delete-update-cart-item','POST',site_url+'/buyer/delete-update-cart-item',formData);
    }
  }

  function drawCanvas()
  {
    jQuery('canvas').clearCanvas();
    for (canvas of canvasData)
    {
      if(canvas.customtype == 'image')
      jQuery('canvas').drawImage(canvas);
      else if(canvas.customtype == 'text')
      {
        jQuery('canvas').drawText(canvas);
      }
    }
    jQuery(".loaderSec").css('display','none');
  }

  function showTextDivWithCanvas(layer)
  {
    jQuery('[data-name="'+layer.name+'"]').prev('.select_textfield').addClass('showSelectedPanel');
  }

  function updateCanvasAxis(layer,type=null)
  {
    console.log(layer);
    jQuery('[data-name="'+layer.name+'"]').find('input[name="x_1st"]').val(layer.x);
    jQuery('[data-name="'+layer.name+'"]').find('input[name="y_1st"]').val(layer.y);
    if(type == 'image')
    {
      jQuery('[data-name="'+layer.name+'"]').find('input[name="width_1st"]').val(layer.width);
      jQuery('[data-name="'+layer.name+'"]').find('input[name="height_1st"]').val(layer.height);
    }
    for (var key in canvasData) {
      if(canvasData[key].name == layer.name)
      {
        canvasData[key]['x'] = layer.x;
        canvasData[key]['y'] = layer.y;
        if(type == 'image')
        {
          canvasData[key]['width'] = layer.width;
          canvasData[key]['height'] = layer.height;
        }
        jQuery('canvas').setLayer(layer.name,canvasData[key]).drawLayers();
      }
    }

    // console.log('updateCanvasAxis '+JSON.stringify(canvasData));
    jQuery('[data-name="'+layer.name+'"]').prev('.select_textfield').removeClass('showSelectedPanel');
  }

  function deleteCanvasAxis(ele,dataKey,dataName)
  {
    if(dataKey != '')
    {
      if(dataName != 'logo')
      {
        canvasData.splice(dataKey,1);
        jQuery('canvas').removeLayer(dataName).drawLayers();
        jQuery(ele).parents('.layer-trigger-wrapper').remove();
      }
      else
      {
        canvasData[dataKey]['x'] = 1;
        canvasData[dataKey]['y'] = 1;
        canvasData[dataKey]['width'] = 1;
        canvasData[dataKey]['height'] = 1;
        canvasData[dataKey]['rotate'] = 0;
        jQuery(ele).parents('.layer-trigger-wrapper').find('.consent_dateClass').val("");
        jQuery(ele).parents('.layer-trigger-wrapper').find('.deleteText').remove();
        setTimeout(function(){
          jQuery('canvas').setLayer(dataName,canvasData[dataKey]).drawLayers();
        },100);
      }
    }
  }

  function readMultiImageURL(input)
  {
    var logoAspectRatio;
    if (input.files && input.files[0])
    {
      var formData = new FormData();
      var file = input.files[0];
      var imgName = input.files[0].name;
      formData.append("Filedata", file);
      var t = file.type.split('/').pop().toLowerCase();
      if (t != "jpeg" && t != "jpg" && t != "png" && t != "bmp" && t != "gif") {
        jQuery(".error-image").html('Please select a valid image file');
        document.getElementsByClassName("profile-img").value = '';
        jQuery('.loaderSec').hide();
        return false;
      }
      var reader = new FileReader();
      reader.onload = function (e) {
        var img = new Image();
        img.src = e.target.result;
        jQuery(".error-image").html('');
        img.onload = function () {
          logoAspectRatio  = (this.width / this.height).toFixed(2);
          var imagesrc = img.src;
          var key = jQuery(input).parents('.text-settings').attr('data-key');
          var name = jQuery(input).parents('.text-settings').attr('data-name');
          jQuery('canvas').clearCanvas();
          jQuery.getScript(site_url+'/assets/jCanvas/jcanvas.min.js');

          if(name == 'logo')
          jQuery(input).parents('.layer-trigger').append('<i class="deleteImage fa fa-times"></i>');

          // upload image
          var imagesrc = img.src;
          var base64ImageContent = imagesrc.replace(/^data:image\/(png|jpg|jpeg|gif|bmp|);base64,/, "");
          var blob = base64ToBlob(base64ImageContent, 'image/png');
          var formData = new FormData();
          formData.append('picture', blob);
          formData.append('imgName', imgName);
          formData.append('width', this.width);
          formData.append('height', this.height);
          var ratioo = 200 / logoAspectRatio;
          jQuery(input).parents('.text-settings').find(jQuery('input[name="height_1st"]')).val(ratioo.toFixed(2));

          jQuery.ajax({
            beforeSend: function() { jQuery('.loaderSec').show(); },
            complete: function() { jQuery('.loaderSec').hide(); },
            url: site_url+'/seller/uploadSingleImage',
            type: 'POST',
            dataType: 'html',
            data: formData,
            processData: false,
            contentType: false,
            headers     : {
              'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            },
            success: function (response)
            {
              jQuery(input).next().attr('rel', response);
              canvasData[key]['source'] = response;
              canvasData[key]['x']      = 150;
              canvasData[key]['y']      = 150;
              canvasData[key]['width']  = 200;
              canvasData[key]['aspectRat']  = logoAspectRatio;
              canvasData[key]['height'] = 200 / logoAspectRatio;
              console.log(key);
              setTimeout(function(){
                jQuery('canvas').setLayer(name,canvasData[key]).drawLayers();
                jQuery(".loaderSec").css('display','none');
              },200);
              console.log('canvasData'+JSON.stringify(canvasData));
            },
            error: function (e) {
              alert('Error while request..');
            }
          });
        }
      }
      reader.readAsDataURL(input.files[0]);
    }
  }

  function countClone(className,datakey,dataName = null)
  {
    var count = 0;
    jQuery('.bootbox').find('.'+className).each(function(){
      var cloneName = jQuery(this).find('.layer-trigger');
      if(className == 'textDiv')
      var name = 'Text field ';
      if(className == 'imageDiv')
      var name = 'Logo ';
      jQuery(cloneName).each(function()
      {
        count = parseInt(count) + parseInt(1);
        jQuery(this).find('.select_textfield').html(name+count);
      });
    });
    if(dataName != 'logo')
    {
      // change array key
      jQuery('.bootbox').find('.layer-trigger-wrapper').each(function(){
        var cloneName = jQuery(this).find('.layer-trigger');
        jQuery(cloneName).each(function()
        {
          // change array key
          var canvasArrKey = jQuery(this).find('.text-settings').attr('data-key');
          if(canvasArrKey > datakey)
          {
            var newCanvasArrKey = canvasArrKey-1;
            jQuery(this).find('.text-settings').attr('data-key',newCanvasArrKey);
          }
        });
      });
    }
  }

  //array short by name
  Array.prototype.keySort = function(keys) {

    keys = keys || {};

    var obLen = function(obj) {
      var size = 0, key;
      for (key in obj) {
        if (obj.hasOwnProperty(key))
        size++;
      }
      return size;
    };

    // avoiding using Object.keys because I guess did it have IE8 issues?
    // else var obIx = function(obj, ix){ return Object.keys(obj)[ix]; } or
    // whatever
    var obIx = function(obj, ix) {
      var size = 0, key;
      for (key in obj) {
        if (obj.hasOwnProperty(key)) {
          if (size == ix)
          return key;
          size++;
        }
      }
      return false;
    };

    var keySort = function(a, b, d) {
      d = d !== null ? d : 1;
      // a = a.toLowerCase(); // this breaks numbers
      // b = b.toLowerCase();
      if (a == b)
      return 0;
      return a > b ? 1 * d : -1 * d;
    };

    var KL = obLen(keys);

    if (!KL)
    return this.sort(keySort);

    for ( var k in keys) {
      // asc unless desc or skip
      keys[k] =
      keys[k] == 'desc' || keys[k] == -1  ? -1
      : (keys[k] == 'skip' || keys[k] === 0 ? 0
      : 1);
    }

    this.sort(function(a, b) {
      var sorted = 0, ix = 0;

      while (sorted === 0 && ix < KL) {
        var k = obIx(keys, ix);
        if (k) {
          var dir = keys[k];
          sorted = keySort(a[k], b[k], dir);
          ix++;
        }
      }
      return sorted;
    });
    return this;
  };



  jQuery(document).on('click','#fullscreenBtn',function(e) {
    $(this).parents('.user_customization').toggleClass('fullscreen');
  });

});


function readURL(input)
{
  if (input.files && input.files[0])
  {
    var formData = new FormData();
    var file = input.files[0];
    var imgName = input.files[0].name;
    formData.append("Filedata", file);
    var t = file.type.split('/').pop().toLowerCase();
    if (t != "jpeg" && t != "jpg" && t != "png" && t != "bmp" && t != "gif") {
      jQuery(".error-image").html('Please select a valid image file');
      document.getElementsByClassName("profile-img").value = '';
      return false;
    }
    jQuery('.Add_designhide').css('display','none');
    var reader = new FileReader();
    reader.onload = function (e) {
      var img = new Image();
      img.src = e.target.result;
      jQuery('.mainImageCls').attr('src', img.src);
      jQuery(".error-image").html('');
      img.onload = function () {
        var w = this.width*0.010416666666819;
        var h = this.height*0.010416666666819;
        var sizee = w.toFixed(2) +' X '+ h.toFixed(2) +' inch';
        // console.log('sizee');
        // console.log(this.width);
        // console.log(this.height);
        aspectRatio  = this.width / this.height;
        actualWidth  = this.width;
        actualHeight = this.height;
        jQuery('.sizeImg').html(sizee);

        jQuery('.canvasCls').attr('height', 570 / aspectRatio);
        jQuery('.canvasCls').attr('width' , 570);

        // jQuery('.canvasCls').attr('height', actualHeight);
        // jQuery('.canvasCls').attr('width' , actualWidth);

        var imagesrc = img.src;
        var base64ImageContent = imagesrc.replace(/^data:image\/(png|jpg|jpeg|gif|bmp|);base64,/, "");
        var blob = base64ToBlob(base64ImageContent, 'image/png');
        var formData = new FormData();
        formData.append('picture', blob);
        formData.append('imgName', imgName);
        formData.append('width', this.width);
        formData.append('height', this.height);
        ajaxHit('uploadSingleImage','POST',site_url+'/seller/uploadSingleImage',formData);
      }
    }
    jQuery('.add_design_image').css('display','block');
    reader.readAsDataURL(input.files[0]);
  }
}


function testsave()
{
  jQuery('.canvasCls').attr('height', actualHeight).attr('width' , actualWidth); // Setting Read H & W to Canvas

  var l = jQuery('canvas').getLayer('default-image');
  data = JSON.parse(JSON.stringify(canvasData[0]));

  data['height'] = actualHeight;
  data['width']  = actualWidth;

  jQuery('canvas').setLayer(data['name'],data).drawLayers();
  var layers = jQuery('canvas').getLayers();

  var dwidth  = 570;
  var dheight = 570 / aspectRatio;

  var wr      = actualWidth   / dwidth;
  var hr      = actualHeight / dheight;

  for (layer of canvasData)
  {
    if(layer.name != 'default-image' && layer.width != 1)
    {
      layer.x      = layer.x * wr;
      layer.y      = layer.y * hr;

      if(layer.customtype == 'image' && layer.width != 1)
      {
        var width    = layer.width;
        var height   = layer.height;

        layer.height = height * hr;
        layer.width  = width  * wr;
      }
      else if(layer.customtype == 'text' && layer.width != 1)
      {
        layer.fontSize = layer.fontSize * hr;
      }
      jQuery('canvas').setLayer(layer.name,layer).drawLayers();
    }
  }
}

function testsave22()
{
  jQuery('.canvasCls').attr('height', 570 / aspectRatio).attr('width' , 570);
  var l = jQuery('canvas').getLayer('default-image');
  data = JSON.parse(JSON.stringify(canvasData[0]));
  data['height'] = 570 / aspectRatio;
  data['width']  = 570;
  jQuery('canvas').setLayer(data['name'],data).drawLayers();
}


function imageHtml(newkey)
{
  var del  = site_url+"/assets/images/Angle.png";
  var html = '<div class="layer-trigger-wrapper imageDiv"> <div class="layer-trigger"> <a href="javascript:void(0);" data-toggle="collapse" class="select_textfield collapsed" data-target="#imagefields">Logo 1 <span class="icon_select"></span></a> <div id="imagefields" class="collapse text-settings" data-name="image'+newkey+'" data-key="'+newkey+'"> <div class="xy"> <div class="left"> <div class="form-group"> <label>X</label> <input type="number" class="form-control 1st_text_class validNumber" min="1" name="x_1st" value="150"/> </div> </div> <div class="right"> <div class="form-group"> <label>Width</label> <input type="number" class="form-control 1st_text_class" name="width_1st" min="1" value="200"/> </div> </div> </div> <div class="xy"> <div class="left"> <div class="form-group"> <label>Y</label> <input type="number" class="form-control 1st_text_class validNumber" min="1" name="y_1st" value="150"/> </div> </div> <div class="right"> <div class="form-group"> <label>Height</label><input type="number" class="form-control 1st_text_class" name="height_1st" min="1" value="200"/> </div> </div> </div> <div class="xy"> <div class="left"> <div class="form-group"> <label><img src="'+del+'"/></label> <input type="number" class="form-control 1st_text_class validNumber angle_1st" min="0" max="360" value="0" name="angle_1st"/> </div> </div> <div class="right"> <div class="form-group"> <input type="file" name="image" class="fileUp addNewImgInp"><input type="hidden" class="editPatientView" value=""><div class="input-group col-xs-12">  <span class="input-group-addon"><i class="glyphicon glyphicon-picture"></i></span>  <input type="text" class="form-control upload_bx consent_dateClass" disabled placeholder="Choose File">  <span class="input-group-btn">    <button class="browse btn btn-primary" type="button"><i class="fa fa-upload"></i> </button>  </span></div><span class="error-image" style="color:red; text-align:center;" ></span> <input type="hidden" class="imageUrl" rel="" name="imageName"/></div> </div> </div> </div> <i class="deleteImage fa fa-times"></i> </div></div>';
  return html;
}

jQuery(document).on("click", ".edit-faq-modal", function() {
    var id = $(this).attr('ref');
    $.ajax({
          data: { 'id': id },
          url: site_url+"/admin/edit-modal",
          type: "POST",
          headers   : {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          beforeSend  : function () {
            $(".loaderSec").css('display','block');
          },
          complete: function () {
            $(".loaderSec").css('display','none');
          },
          success: function (data) {
              // $("#question").val(data.question);
              $("#faq-modal-body").html(data);
              $("#Add_FAQ").modal('show');
              // $("#faqFormModal").valid()
          },
          error: function (data) {
              console.log('Error:', data);
          }
      });
});


jQuery(document).on("click", ".support-category-modal", function() {
    var id = $(this).attr('ref');
    $.ajax({
          data: { 'catId': id },
          url: site_url+"/admin/support-category-modal",
          type: "POST",
          headers   : {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          beforeSend  : function () {
            $(".loaderSec").css('display','block');
          },
          complete: function () {
            $(".loaderSec").css('display','none');
          },
          success: function (data) {
              // $("#question").val(data.question);
              $("#support-category-modal-body").html(data);
              $("#add_support_category").modal('show');
              // $("#faqFormModal").valid()
          },
          error: function (data) {
              console.log('Error:', data);
          }
      });
});


jQuery(document).on("click", ".edit-support-modal", function() {
    var id = $(this).attr('ref');
    $.ajax({
          data: { 'id': id },
          url: site_url+"/admin/edit-support-modal",
          type: "POST",
          headers   : {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          beforeSend  : function () {
            $(".loaderSec").css('display','block');
          },
          complete: function () {
            $(".loaderSec").css('display','none');
          },
          success: function (data) {
              // $("#question").val(data.question);
              $("#faq-modal-body").html(data);
              $("#Add_FAQ").modal('show');
              // $("#faqFormModal").valid()
          },
          error: function (data) {
              console.log('Error:', data);
          }
      });
});
