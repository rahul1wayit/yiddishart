var site_url = jQuery('#site_url').val();
jQuery(document).ready(function(){
  jQuery('[data-toggle="tooltip"]').tooltip();
  // valid number
  jQuery(document).on('keypress', '.validNumber', function (eve) {
    if (eve.which == 0) {
      return true;
    } else {
      if (eve.which == '.') {
        eve.preventDefault();
      }
      if ((eve.which != 46 || $(this).val().indexOf('.') != -1) && (eve.which < 48 || eve.which > 57)) {
        if (eve.which != 8)
        {
          eve.preventDefault();
        }
      }
      $('.validNumber').keyup(function (eve) {
        if ($(this).val().indexOf('.') == 0) {
          $(this).val($(this).val().substring(1));
        }
      });
    }
  });




  jQuery("#Add_Design").modal({
    show: false,
    backdrop: 'static'
  });

  jQuery("#addFontModel").modal({
    show: false,
    backdrop: 'static'
  });

  //valid decimal value
  jQuery(document).on('blur','.toFixed',function () {
    var num = parseFloat($(this).val());
    var cleanNum = num.toFixed(2);
    if (!isNaN(cleanNum)) {
      $(this).val(cleanNum);
    }else{
      // $(this).val(0.00);
    }
  });

  //allow only characters and hyphen in name fields
  jQuery('body').on('keypress', '.hypen', function (e) {
    var regex = new RegExp("^[0-9 \b\t\r\n\-]");
    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (!regex.test(str)) {
      e.preventDefault();
      return true;
    }
  });

  var maxLength = 13;
  jQuery('body').on('change', '#user_photo', function (e) {
    filename = this.files[0].name
    jQuery('#user_photo-name').show();
    jQuery('#user_photo-name').html(filename);
    jQuery('#user_photo-name').attr('title',filename);
    // console.log(filename);
    $('#user_photo-name').text(function(i, text) {
      if (text.length > maxLength) {
        return text.substr(0, maxLength) + '...';
      }
    });
  });

  $(function () {
    $('.hyphenNumbers').keydown(function (e) {
      var key = e.charCode || e.keyCode || 0;
      $text = $(this);
      if (key !== 8 && key !== 9) {
        if ($text.val().length === 3) {
          $text.val($text.val() + '-');
        }
        if ($text.val().length === 7){
          $text.val($text.val() + '-')
        }
        if ($text.val().length === 11) {
          $text.val($text.val() + '-');
        }
      }
      return (key == 8 || key == 9 || key == 46 || (key >= 48 && key <= 57) || (key >= 96 && key <= 105));
    })
  });

  //allow only characters in name fields
  jQuery('body').on('keypress', '.charonly', function (e) {
    var regex = new RegExp("^[a-zA-Z\s\b ]+$");
    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (!regex.test(str)) {
      e.preventDefault();
      return true;
    }
  });

  /*=============buyers pagination===============*/
  jQuery('body').on('click','.pagination a', function(){
    jQuery('.pagination li.active').removeClass('active');
    jQuery(this).parent('li').addClass('active');
    var value = jQuery(this).val();
    var url = jQuery(this).attr('href');
    var search = jQuery('.searchBox').val();
    var entriesperpage = jQuery('.entriesperpage :selected').val();
    jQuery.ajax({
      type      : 'GET',
      url       : url,
      cache:    false,
      headers   : {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      dataType  : 'html',
      data      : {value:value,search:search,entriesperpage:entriesperpage},
      success:function(data){
        jQuery('#paginationData').html(data);
        jQuery('.entriesperpage').val(entriesperpage);

      }
    });
    return false;
  });

  jQuery('body').on('click', '.datePi', function(){
    jQuery('.ui-datepicker').css('display','block');
    jQuery( this ).datepicker({
      dateFormat: 'yy-mm-dd',
      autoclose: true,
      autoHide: true,
       closeAfterSelect: true,
      onSelect: function() {
        jQuery(this).change();
        jQuery('.searchBox').val(jQuery(this).val());
        jQuery('.searchBox').trigger('keyup');
        jQuery(this).datepicker('hide');
      }
    }).on('change', function(){
      // console.log('aaaas`');
        jQuery('.ui-datepicker').css('display','none');
    });
  });

  jQuery('body').on('keyup', '.searchBox', function(){
    var entriesperpage = jQuery('.entriesperpage :selected').val();
    var value = jQuery(this).val();

    if(value == '')
    value = 'empty';
    jQuery.ajax({
      type    : 'GET',
      cache:    false,
      headers : {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url     : searchUrl,
      data    : {search:value,entriesperpage:entriesperpage},
      success:function(data){
        jQuery('#paginationData').html(data);
        jQuery('.entriesperpage').val(entriesperpage);

      }
    });
  });

  jQuery('body').on('change', '.entriesperpage', function(){
    var entriesperpage = jQuery('.entriesperpage :selected').val();
    var search = jQuery('.searchBox').val();
    jQuery.ajax({
      type    : 'GET',
      cache:    false,
      headers : {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url     : searchUrl,
      data    : {search:search,entriesperpage:entriesperpage},
      success:function(data){
        jQuery('#paginationData').html(data);
        jQuery('.entriesperpage').val(entriesperpage);

      }
    });
  });

  // table-sorting
  jQuery(document).on('click', '.table-sorting', function () {

    var keyword = $('.searchBox').val();
    var type = $('.searchBox').attr('data-type');

    var table = jQuery(this).attr('ref');
    var order = jQuery(this).attr('rel');
    var column = jQuery(this).attr('data-type');
    jQuery.ajax({
      type: "POST",
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      data: {table:table,order:order,column:column,search:keyword},
      url: sortUrl,
      dataType: "html",
      success: function (msg) {
        jQuery("#allResult").html(msg);
      }
    });
    return false;
  });

  // sorting search
  jQuery(document).on('click', '.searchShort', function () {
    var keyword = jQuery(".searchName").val();
    var searchCatByName = jQuery(".catFilter").find('option:selected').text();
    console.log('keyword'+keyword);
    var table = jQuery(this).attr('ref');
    var order = jQuery(this).attr('rel');
    var column = jQuery(this).attr('data-type');
    jQuery.ajax({
      type: "POST",
      headers: {
        'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
      },
      data: {order:order,column:column,searchName:keyword},
      url: site_url+'/front-search',
      dataType: "html",
      success: function (msg) {
        if(jQuery('.main_content_wrapper').hasClass('searchSection')){
          jQuery('.searchSection').html(msg);
        }else{
          jQuery('.main_content_wrapper').html(msg);
        }
        // jQuery('.searchNameOn').html(keyword);
        jQuery('.searchCategoryNameOn').text(searchCatByName);
        setTimeout(function(){
          jQuery.getScript(site_url+'/assets/js/rater.js');
        },200);
      }
    });
    return false;
  });


  // reset model
  jQuery('.modal').on('hidden.bs.modal', function (e) {
    jQuery('.form-control').removeClass('has-error');
    jQuery('.form-group.has-error').removeClass('has-error');
    jQuery('label.has-error').remove();
    jQuery('.form-control').removeClass('error');
    jQuery('.form-group.error').removeClass('error');
    jQuery('label.error').remove();
    jQuery('.alert').css('display','none');

    var  id  = jQuery(this).attr('id');
    jQuery('#'+id).find('input[type="text"]').val('');
    jQuery('#'+id).find('input[type="file"]').val('');
    jQuery('#'+id).find('input[type="password"]').val('');
    jQuery('#'+id).find('input[type="hidden"]').val('');
    jQuery('#'+id).find('input[type="button"]').removeAttr('disabled');
    jQuery('#'+id).find('input[type="submit"]').removeAttr('disabled');
    jQuery('#'+id).find('button[type="button"]').removeAttr('disabled');
    jQuery('#'+id).find('button[type="submit"]').removeAttr('disabled');
    jQuery('#'+id).find('textarea').val('');
    jQuery('#'+id).find('select').val('');
    jQuery('.gallery').html('');
  });

  // change data
  jQuery(document).on('click', '.ajax_update_btn', function ()
  {
    var user = jQuery(this).attr('data-user');
    var id = jQuery(this).attr('data-id');
    var action = url+'/'+formaction;
    var ajaxurl = main_url+id+'/edit';
    if(id != '')
    {
      jQuery.ajax({
        type: "GET",
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend  : function () {
          $(".loaderSec").css('display','block');
        },
        complete: function () {
          $(".loaderSec").css('display','none');
        },
        dataType: "html",
        url:ajaxurl,
        success: function (msg) {
          jQuery('.form-model-class').attr('action',action);
          jQuery('#edit-model').modal('show');
          jQuery('.modelTitle').html('Edit '+header_name);
          jQuery('.ajax-update-view').html(msg);
          // jQuery('#event-form').valid();
          setTimeout(function() {
            // $( "#datepicker" ).datepicker({
            //   inline: true,
            //   dateFormat: 'dd.mm.yy',
            //   minDate: 0
            // });
            jQuery.getScript(site_url+'/assets/js/form-validate.js');
          }, 200);

        }
      });
    }
  });

  // update status popup
  jQuery('body').on('click', '.ajax_status_btn', function ()
  {
    var id = jQuery(this).attr('data-id');
    var status = jQuery(this).attr('data-status').trim();
    var type = types;
    var action = url+'/'+formaction;
    if(id != '')
    {
      if(header_name == 'user')
      {
        jQuery('.commonDeleteMsg').css('display','block');
      }else {
        jQuery('.commonDeleteMsg').css('display','block');
      }
      jQuery('#confirm_delete').modal('show');
      jQuery("#confirm_delete").attr('data-id',id);
      jQuery("#confirm_delete").attr('data-status',status);
    }
  });

  // dlete
  jQuery('body').on('click', '.ajax_delete_btn', function ()
  {
    var id = jQuery(this).attr('data-id');
    var action = url+'/'+formaction;
    if(id != '')
    {
      jQuery('.commonDeleteMsg').text('Delete '+header_name);
      jQuery('.sellerText').text(header_name);
      jQuery('.commonDeleteMsg').css('display','block');

      jQuery('#confirm_delete_user').modal('show');
      jQuery("#confirm_delete_user").attr('data-id',id);
    }
  });

  //confirm delete
  jQuery(document).on('click', '.comfirm_delete_data', function ()
  {
    var uId = jQuery("#confirm_delete").attr('data-id');
    var status = jQuery("#confirm_delete").attr('data-status');
    if(jQuery(this).attr('id')=='yes')
    {
      jQuery.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: 'delete',
        url:main_url+'/'+jQuery("#confirm_delete").attr('data-id'),
        cache: false,
        data: {"status": jQuery("#confirm_delete").attr('data-status')},
        dataType: "json",
        beforeSend: function() {
          $(".loaderSec").css('display','block');
        },
        complete: function() {
          $(".loaderSec").css('display','none');
        },
        success: function (data) {
          if(status == 0)
          {
            jQuery('.btnStatus'+uId).removeClass('btn btn-primary');
            jQuery('.btnStatus'+uId).addClass('btn btn-default');
            jQuery('.btnStatus'+uId).attr('data-status',1);
            jQuery('.btnStatus'+uId).text('Inactive');
          }else {
            jQuery('.btnStatus'+uId).removeClass('btn btn-default');
            jQuery('.btnStatus'+uId).addClass('btn btn-primary');
            jQuery('.btnStatus'+uId).attr('data-status',0);
            jQuery('.btnStatus'+uId).text('Active');
          }
          toster(data);
        },
        error: function (response) {
          jQuery.each(response.responseJSON.errors,function(k,message){
            $.toast({
              heading             : 'Error',
              text                : message,
              loader              : true,
              loaderBg            : '#fff',
              showHideTransition  : 'fade',
              icon                : 'error',
              hideAfter           : 4000,
              position            : 'top-right'
            });
          });
        }
      });
    }
  });
  //confirm delete
  jQuery(document).on('click', '.confirm_delete_user', function ()
  {
    var uId = jQuery("#confirm_delete_user").attr('data-id');
    if(jQuery(this).attr('id')=='yes')
    {
      jQuery.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: 'post',
        url:main_url+jQuery("#confirm_delete_user").attr('data-id'),
        cache: false,
        data: {"userId": jQuery("#confirm_delete_user").attr('data-status'),'_method':'DELETE'},
        dataType: "json",
        beforeSend: function() {
          $(".loaderSec").css('display','block');
        },
        complete: function() {
          $(".loaderSec").css('display','none');
        },
        success: function (data) {
          jQuery('.userId'+uId).remove();
          toster(data);
          jQuery('#confirm_delete_user').modal('hide');
        },
        error: function (response) {
          jQuery.each(response.responseJSON.errors,function(k,message){
            $.toast({
              heading             : 'Error',
              text                : message,
              loader              : true,
              loaderBg            : '#fff',
              showHideTransition  : 'fade',
              icon                : 'error',
              hideAfter           : 4000,
              position            : 'top-right'
            });
          });
        }
      });
    }
  });

  // success
  jQuery('body').on('keyup blur change', '.error,.success', function () {
    var text = jQuery(this).val();
    text = text.trim();
    var textPl = jQuery(this).attr('placeholder');
    if (text != '') {
      jQuery(this).css('border', '1.8px solid #9d92b4');
    } else {
      jQuery(this).val('');
      jQuery(this).css('border', '1.8px solid #F16969');
      jQuery(this).attr('placeholder', 'This field is required');
    }
  });

  // status popup
  jQuery('body').on('click', '.comfirm_status_btn', function ()
  {
    var thiss = this;
    var id = jQuery(this).attr('data-id');
    bootbox.confirm({
      title: 'Change Status',
      message: "Are you sure you want to change status?",
      // closeButton: false,
      buttons: {
        confirm: {
          label: 'Approve',
          className: 'btn-success comfirm_status'
        },
        cancel: {
          label: 'Reject',
          className: 'btn-danger'
        }
      },
      callback: function (result) {
        if(result == true)
        {
          jQuery(thiss).attr('data-status',1);
          var formData = new FormData();
          formData.append('id', id);
          formData.append('status', 1);
          var action = site_url+'/admin/change-status';
          ajaxHit('changeStatus','POST',action,formData,thiss);
        }
        if(result == false)
        {
          jQuery(thiss).attr('data-status',2);
          var formData = new FormData();
          formData.append('id', id);
          formData.append('status', 2);
          var action = site_url+'/admin/change-status';
          ajaxHit('changeStatus','POST',action,formData,thiss);
        }
      }
    });
    jQuery('.close').removeClass('bootbox-close-button');
    jQuery('.close').addClass('closeBootBox');
  });

  jQuery(document).on('click', '.closeBootBox', function ()
  {
    jQuery('.bootbox').modal('hide');
  });

  //confirm delete
  jQuery(document).on('click', '.comfirm_status', function ()
  {
    var uId = jQuery(".comfirm_status").attr('data-id');
    var status = jQuery(".comfirm_status").attr('data-status');
    if(jQuery(this).attr('id')=='yes')
    {
      jQuery.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: 'delete',
        url:comfirm_status+'/'+jQuery(".comfirm_status").attr('data-id'),
        cache: false,
        data: {"status": jQuery(".comfirm_status").attr('data-status')},
        dataType: "json",
        beforeSend: function() {
          $("#loader-section").css('display','block');
        },
        success: function (data) {
          if(status == 0)
          {
            jQuery('.btnStatus'+uId).removeClass('btn btn-inactive');
            jQuery('.btnStatus'+uId).addClass('btn btn-active');
            jQuery('.btnStatus'+uId).attr('data-status',1);
            jQuery('.btnStatus'+uId).text('Active');
          }else {
            jQuery('.btnStatus'+uId).removeClass('btn btn-active');
            jQuery('.btnStatus'+uId).addClass('btn btn-inactive');
            jQuery('.btnStatus'+uId).attr('data-status',0);
            jQuery('.btnStatus'+uId).text('Inactive');
          }
          toster(data);
        },
        complete: function() {
          $("#loader-section").css('display','none');
        },
        error: function (response) {
          jQuery.each(response.responseJSON.errors,function(k,message){
            $.toast({
              heading             : 'Error',
              text                : message,
              loader              : true,
              loaderBg            : '#fff',
              showHideTransition  : 'fade',
              icon                : 'error',
              hideAfter           : 4000,
              position            : 'top-right'
            });
          });
        }
      });
    }
  });
  // image zoom in popup start
  jQuery(document).on('click','.myImg',function()
  {
    jQuery('#zoomImg').css('display','block');
    jQuery('.img01').attr('src',this.src);
  });
  jQuery(document).on('click','.closeModal',function()
  {
    jQuery('#zoomImg').css('display','none');
  });
  jQuery(document).on('click','.closeFontModel',function(e)
  {
    jQuery('#addFontModel').addClass('displayNone');
    jQuery('#addFontModel').removeClass('show');
  });
  // image zoom in popup end


  $(".accordion_head").click(function() {
    if ($('.accordion_body').is(':visible')) {
      $(".accordion_body").slideUp(300);
      $(".plusminus").text('+');
    }
    if ($(this).next(".accordion_body").is(':visible')) {
      $(this).next(".accordion_body").slideUp(300);
      $(this).children(".plusminus").text('+');
    } else {
      $(this).next(".accordion_body").slideDown(300);
      $(this).children(".plusminus").text('-');
    }
  });

});

var canvasData = [];
function ajaxHit(requestType,type,action,data=null,ele=null)
{
  if(requestType == 'getImageEditor' || requestType ==  'edit-event' || requestType == 'edit-subscription-plan' || requestType == 'uploadSingleImage' || requestType == 'uploadMultiImage' || requestType == 'frontSearch' || requestType == 'get_earning_details')
  {
    var rType = 'html';
  }else {
    var rType = 'json';
  }
  jQuery.ajax({
    url         : action,
    type        : type,
    data        : data,
    processData: false,
    contentType: false,
    cache: false,
    headers     : {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    dataType    : rType,
    beforeSend  : function () {
      jQuery(".loaderSec").css('display','block');
    },
    complete: function () {
      jQuery(".loaderSec").css('display','none');
    },
    success: function (response) {
      if(response)
      {
        if(requestType == 'getImageEditor')
        {
          var imgsrc = jQuery('.mainImageCls').attr('src');
          imgsrc =  imgsrc.split(',')[1];
          var base64 = imgsrc;
          var binary = fixBinary(atob(base64));
          var blob = new Blob([binary], {type: 'image/png'});
          jQuery('.custom-file-label').append(' ');
          setTimeout(function() {
            jQuery('.user_customization').css('display','block');
            jQuery('.add_design_image').css('display','none');
            jQuery('.user_customization').html(response);
            process(blob);
            jQuery.getScript(site_url+'/assets/js/jquery.toast.js');
            jQuery.getScript(site_url+'/assets/js/form-validate.js');
          },200);
        }

        if(requestType == 'edit-subscription-plan')
        {
          jQuery('.editShow').html(response);
          jQuery.getScript(site_url+'/assets/js/admin-custom.js');
        }

        if(requestType == 'edit-event')
        {
          jQuery('.editShow').html(response);
          setTimeout(function() {
            $( "#datepicker" ).datepicker({
              inline: true,
              dateFormat: 'dd.mm.yy',
              minDate: 0
            });
            jQuery.getScript(site_url+'/assets/js/form-validate.js');
            jQuery('#event-form').valid();
            jQuery.getScript(site_url+'/assets/js/admin-custom.js');
          }, 200);
        }

        if(requestType == 'uploadSingleImage')
        {
          var imgsrc = jQuery('.mainImageCls').attr('src',response);
          jQuery('.Add_design').css('display','none');
          jQuery('.add_design_image').css('display','block');
        }
        if(requestType == 'changeStatus')
        {
          var status = jQuery(ele).attr('data-status');
          if(status == 1)
          {
            jQuery(ele).addClass('btn-success');
            jQuery(ele).removeClass('btn-danger');
            jQuery(ele).html('Approved');
          }
          else
          {
            jQuery(ele).removeClass('btn-success');
            jQuery(ele).addClass('btn-danger');
            jQuery(ele).html('Rejected');
          }
          toster(response);
        }
        if(requestType == 'frontSearch')
        {
          if(jQuery('.main_content_wrapper').hasClass('searchSection')){
            jQuery('.searchSection').html(response);
          }else{
            jQuery('.main_content_wrapper').html(response);
          }
          setTimeout(function(){
            jQuery.getScript(site_url+'/assets/js/rater.js');
          },200);
        }
        if(requestType == 'addDownload')
        {
          $.toast({
            heading             : 'Success',
            text                : 'Design has been added into Download',
            loader              : true,
            loaderBg            : '#fff',
            showHideTransition  : 'fade',
            icon                : 'success',
            hideAfter           : 2000,
            position            : 'top-right'
          });
          setTimeout(function(){
            window.location.href= site_url+'/buyer/downloads';
          },2000);
        }
        if(requestType == 'add-to-cart')
        {
          if(response !='')
          {
            $.toast({
              heading             : 'Success',
              text                : 'Design has been added into cart.',
              loader              : true,
              loaderBg            : '#fff',
              showHideTransition  : 'fade',
              icon                : 'success',
              hideAfter           : 2000,
              position            : 'top-right'
            });
            setTimeout(function(){
              window.location.href= site_url+'/buyer/cart';
            },2000);
          }
        }
        if(requestType == 'delete-update-cart-item')
        {
          $.toast({
            heading             : 'Success',
            text                : response.success_message,
            loader              : true,
            loaderBg            : '#fff',
            showHideTransition  : 'fade',
            icon                : 'success',
            hideAfter           : 2000,
            position            : 'top-right'
          });
        }
        if(requestType == 'get_earning_details')
        {
          jQuery("#get_earning_detail").html(response);
          bootbox.dialog({
            size: 'large',
            className: "my-earning-design",
            message: jQuery('div#View_Earnings').html(),
          });
          jQuery('.close').removeClass('bootbox-close-button');
          jQuery('.close').addClass('closeBootBox');
        }
        if(requestType == 'getRatingReview')
        {
          if(response != 'false')
          {
            jQuery('.rating_inpt').val(response.rating);
            jQuery('.review_input').html(response.review);
            jQuery('.star').removeClass('selected');
            jQuery('.star').each(function(index){
              if(response.rating >= 1 && index== 0)
              jQuery(this).addClass('selected');
              if(response.rating >= 2 && index== 1)
              jQuery(this).addClass('selected');
              if(response.rating >= 3 && index== 2)
              jQuery(this).addClass('selected');
              if(response.rating >= 4 && index== 3)
              jQuery(this).addClass('selected');
              if(response.rating >= 5 && index== 4)
              jQuery(this).addClass('selected');
            });
          }
          bootbox.dialog({
            title: 'Add Rating',
            size: 'sm',
            className: "my-rating-design",
            message: jQuery('div#Add_Rating').html(),
          });
          jQuery('.close').removeClass('bootbox-close-button');
          jQuery('.close').addClass('closeBootBox');
          setTimeout(function(){
            jQuery.getScript(site_url+'/assets/js/rater.js');
            jQuery.getScript(site_url+'/assets/js/custom.js');
          },100);
        }
        if(requestType == 'addRatingReview')
        {
          $.toast({
            heading             : 'Success',
            text                : 'Rating has been updated.',
            loader              : true,
            loaderBg            : '#fff',
            showHideTransition  : 'fade',
            icon                : 'success',
            hideAfter           : 2000,
            position            : 'top-right'
          });
          setTimeout(function(){
            bootbox.hideAll();
          },200);
        }
      }
    },
    error:function(response){
      var data =[];
      console.log(response);
      jQuery.each(response.responseJSON.errors,function(k,message){
        data['error'] = true;
        data['error_message'] = message;
        alert_message(data);
      });
    }
  });
}

function fixBinary (bin) {
  var length = bin.length;
  var buf = new ArrayBuffer(length);
  var arr = new Uint8Array(buf);
  for (var i = 0; i < length; i++) {
    arr[i] = bin.charCodeAt(i);
  }
  return buf;
}

function responseMessage(msg) {
  $('.success-box').fadeIn(200);
  $('.success-box div.text-message').html("<span>" + msg + "</span>");
}
function toster(data)
{
  if (data.success) {
    $.toast({
      heading             : 'Success',
      text                : data.success_message,
      loader              : true,
      loaderBg            : '#fff',
      showHideTransition  : 'fade',
      icon                : 'success',
      hideAfter           : 2000,
      position            : 'top-right'
    });

    if(data.url)
    {
      if(data.delayTime)
      setTimeout(function() { window.location.href=data.url;}, data.delayTime);
    }

  }else{
    $.toast({
      heading             : 'Error',
      text                : data.error_message,
      loader              : true,
      loaderBg            : '#fff',
      showHideTransition  : 'fade',
      icon                : 'error',
      hideAfter           : 2000,
      position            : 'top-right'
    });
  }

}

function tost(data)
{
  $.toast({
    heading             : data.success,
    text                : data.success_message,
    loader              : true,
    loaderBg            : '#9EC600',
    showHideTransition  : 'fade',
    icon                : 'info',
    hideAfter           : 4000,
    position            : 'top-right'
  });
}

function base64ToBlob(base64, mime)
{
  mime = mime || '';
  var sliceSize = 1024;
  var byteChars = window.atob(base64);
  var byteArrays = [];
  for (var offset = 0, len = byteChars.length; offset < len; offset += sliceSize) {
    var slice = byteChars.slice(offset, offset + sliceSize);
    var byteNumbers = new Array(slice.length);
    for (var i = 0; i < slice.length; i++) {
      byteNumbers[i] = slice.charCodeAt(i);
    }
    var byteArray = new Uint8Array(byteNumbers);
    byteArrays.push(byteArray);
  }
  return new Blob(byteArrays, {type: mime});
}

function blockSpecialChar(e) {
  var k = e.keyCode;
  return ((k > 64 && k < 91) || k == 8   || (k >= 48 && k <= 57));
}

jQuery("body").on("contextmenu", "img", function(e) {
  return false;
});
