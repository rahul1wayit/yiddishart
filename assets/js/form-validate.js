$(document).ready(function ()
{
  /*====================Start login form validation================= */
  $("#login-form").validate({
    rules:
    {
      email: {
        required: true,
        email: true
      },
      password:{
        required: true,
        minlength: 6
      },
    },
    messages:
    {
      email: {
        required: "Please enter your email",
        email: "Please enter valid email address"
      },
      password: {
        required: "Please enter your password",
        minlength: "Your password must be at least 6 characters long"
      },
    },
    submitHandler: function (form)
    {
      formSubmit(form);
    }
  });
  /*====================End login form validation================= */

  /*====================Start Registration form validation================= */
  $("#register-form").validate({
    rules:
    {
      first_name: {
        required: true,
      },
      last_name: {
        required: true,
      },
      email: {
        required: true,
        email: true
      },
      user_name: {
        required: true,
      },
      password:{
        required: true,
        minlength: 6
      },
      password_confirmation:{
        required: true,
        minlength: 6,
        equalTo: "#password"
      },
    },
    messages:
    {
      first_name: {
        required: "Please enter first name"
      },
      last_name: {
        required: "Please enter last name"
      },
      email: {
        required: "Please enter email",
        email: "Please enter valid email address"
      },
      user_name: {
        required: "Please enter user name"
      },
      password: {
        required: "Please enter password",
        minlength: "Your password must be at least 6 characters long"
      },
      password_confirmation: {
        required:  "Please enter confirm password",
        minlength: "Your password must be at least 6 characters long",
        equalTo:   "Please enter the same value again."
      },
    },
    submitHandler: function (form)
    {
      formSubmit(form);
    }
  });
  /*====================End Registration form validation================= */

  /*====================Start Seller Registration form validation================= */
  $("#seller-register-form1").validate({
    rules:
    {
      first_name: {
        required: true,
      },
      last_name: {
        required: true,
      },
      email: {
        required: true,
        email: true
      },
      user_name: {
        required: true,
      },
      phone: {
        required: true,
        minlength:10,
        maxlength:12,
        number: true
      },
      password:{
        required: true,
        minlength: 6
      },
      password_confirmation:{
        required: true,
        minlength: 6,
        equalTo: "#password"
      },
      businessname: {
        required: true,
      },
      cat: {
        required: true,
      },
      city: {
        required: true,
      },
      state: {
        required: true,
      },
      zip: {
        required: true,
      },
      contact: {
        required: true,
        minlength:10,
        maxlength:12,
        number: true
      },
      description: {
        required: true,
      },
    },
    messages:
    {
      first_name: {
        required: "Please enter first name"
      },
      last_name: {
        required: "Please enter last name"
      },
      email: {
        required: "Please enter email",
        email: "Please enter valid email address"
      },
      user_name: {
        required: "Please enter user name"
      },
      phone: {
        required:  "Please enter phone",
        minlength: "Your phone must be at least 10 characters long",
        maxlength: "Your phone must be less than 12 characters long",
        number: "Your phone must be number"
      },
      password: {
        required: "Please enter password",
        minlength: "Your password must be at least 6 characters long"
      },
      password_confirmation: {
        required:  "Please enter confirm password",
        minlength: "Your password must be at least 6 characters long",
        equalTo:   "Please enter the same value again."
      },
      businessname: {
        required: "Please enter business name"
      },
      cat: {
        required: "Please select category"
      },
      city: {
        required: "Please enter city"
      },
      state: {
        required: "Please enter state"
      },
      zip: {
        required: "Please enter zip"
      },
      contact: {
        required:  "Please enter contact no",
        minlength: "Your contact must be at least 10 characters long",
        maxlength: "Your contact must be less than 12 characters long",
        number: "Your contact must be number"
      },
      description: {
        required: "Please enter description"
      }
    },
    submitHandler: function (form)
    {
      formSubmit(form);
    }
  });

 /*====================Edit Seller Registration form validation================= */
  $("#seller-register-form").validate({
    rules:
    {
      first_name: {
        required: true,
      },
      last_name: {
        required: true,
      },
      email: {
        required: true,
        email: true
      },
      phone: {
        required: true,
        minlength:10,
        maxlength:12,
        number: true
      },
      businessname: {
        required: true,
      },
      cat: {
        required: true,
      },
      city: {
        required: true,
      },
      state: {
        required: true,
      },
      zip: {
        required: true,
      },
      contact: {
        required: true,
        minlength:10,
        maxlength:12,
        number: true
      },
      description: {
        required: true,
      },
    },
    messages:
    {
      first_name: {
        required: "Please enter first name"
      },
      last_name: {
        required: "Please enter last name"
      },
      email: {
        required: "Please enter email",
        email: "Please enter valid email address"
      },
      phone: {
        required:  "Please enter phone",
        minlength: "Your phone must be at least 10 characters long",
        maxlength: "Your phone must be less than 12 characters long",
        number: "Your phone must be number"
      },
      businessname: {
        required: "Please enter business name"
      },
      cat: {
        required: "Please select category"
      },
      city: {
        required: "Please enter city"
      },
      state: {
        required: "Please enter state"
      },
      zip: {
        required: "Please enter zip"
      },
      contact: {
        required:  "Please enter contact no",
        minlength: "Your contact must be at least 10 characters long",
        maxlength: "Your contact must be less than 12 characters long",
        number: "Your contact must be number"
      },
      description: {
        required: "Please enter description"
      }
    },
    submitHandler: function (form)
    {
      formSubmit(form);
    }
  });
  $("#buyer-register-form").validate({
    rules:
    {
      first_name: {
        required: true,
      },
      last_name: {
        required: true,
      },
      email: {
        required: true,
        email: true
      },

      phone: {
        required: true,
        minlength:10,
        maxlength:12,
        number: true
      },
    },
    messages:
    {
      first_name: {
        required: "Please enter first name"
      },
      last_name: {
        required: "Please enter last name"
      },
      email: {
        required: "Please enter email",
        email: "Please enter valid email address"
      },
      phone: {
        required:  "Please enter phone",
        minlength: "Your phone must be at least 10 characters long",
        maxlength: "Your phone must be less than 12 characters long",
        number: "Your phone must be number"
      },
    },
    submitHandler: function (form)
    {
      formSubmit(form);
    }
  });
  /*====================End Seller Registration form validation================= */

  //forgot Password
  $("#forgotPassword-form").validate({
    errorClass   : "has-error",
    highlight    : function(element, errorClass) {
      $(element).parents('.form-group').addClass(errorClass);
    },
    unhighlight  : function(element, errorClass, validClass) {
      $(element).parents('.form-group').removeClass(errorClass);
    },
    rules:
    {
      email:
      {
        required: true,
        email: true
      },
    },
    messages:
    {
      email: {
        required: "Please enter your email",
        email: "Please enter valid email address"
      },
    },
    submitHandler: function (form)
    {
      formSubmit(form);
    }
  });

  $('.resetpassword').validate({
    rules:  {
      email:  {
        required: true,
        email: true
      },
      password:   {
        required: true,
        minlength: 6
      },
      password_confirmation:  {
        required: true,
        minlength: 6,
        equalTo: "#password"
      },
    },
    messages:   {
      email:  {
        required: "Please enter your email",
        email: "Please enter valid email address"
      },
      password:   {
        required: "Please enter your password",
        minlength:  "Your password must be at least 6 characters long"
      },
      password_confirmation:  {
        required:   "Please enter your confirm password",
        minlength:  "Your password must be at least 6 characters long",
        equalTo:  "Please enter the same password as above"
      },
    },
    submitHandler: function (form)
    {
      formSubmit(form);
    }
  });



  $(".reset-password").validate({
    errorClass   : "has-error",
    highlight    : function(element, errorClass)
    {
      $(element).parent('.input-group').addClass("has-error");
    },
    unhighlight  : function(element, errorClass, validClass)
    {
      $(element).parent('.input-group').removeClass("has-error");
    },
    errorPlacement: function (error, element)
    {
      error.insertAfter($(element).parent('.input-group'));
    },
    rules:
    {
      email: {
        required: true,
        email: true
      },
    },
    messages:
    {
      email: {
        required: "Please enter your email",
        email: "Please enter valid email address"
      },
    },
    submitHandler: function (form)
    {
      formSubmit(form);
    }
  });

  //change Password-form
  $("#changePassword-form").validate({
    errorClass: "has-error",
    highlight: function (element, errorClass) {
      $(element).parents('.form-group').addClass(errorClass);
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).parents('.form-group').removeClass(errorClass);
    },
    rules:
    {
      password:
      {
        required: true,
      },
      new_password:
      {
        required: true,
        minlength: 6,
        // equalTo: '#confirm_password'
      },
      confirm_password:
      {
        required: true,
        minlength: 6,
        equalTo: '#new_password'
      },
    },
    messages:
    {
      password: "Old Password is required.",
      new_password:
      {
        'required': "New Password is required.",
        'minlength': "New Password must contain at least 6 characters.",
        //'equalTo': "New Password and Confirm Password not mactched."
      },
      confirm_password:
      {
        'required': "Confirm Password is required.",
        'minlength': "Confirm Password must contain at least 6 characters.",
        'equalTo': "New Password and Confirm Password not mactched."
      },
    },

    submitHandler: function (form)
    {
      formSubmit(form);
    }
  });

  //change Password-form
  $("#subscription-plan-form").validate({
    errorClass: "has-error",
    highlight: function (element, errorClass) {
      $(element).parents('.form-group').addClass(errorClass);
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).parents('.form-group').removeClass(errorClass);
    },
    rules:
    {
      title:
      {
        required: true
      },
      price:
      {
        required: true
      },
    },
    messages:
    {
      title: "Plan Name is required.",
      price: "Price is required.",
    },

    submitHandler: function (form)
    {
      formSubmit(form);
    }
  });

  // image
  $(".changeImageForm").validate({
    submitHandler: function (form)
    {
      formSubmit(form);
    }
  });

  //event form
  $("#event-form").validate({
    errorClass: "has-error",
    highlight: function (element, errorClass) {
      $(element).parents('.form-group').addClass(errorClass);
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).parents('.form-group').removeClass(errorClass);
    },
    rules:
    {
      name:
      {
        required: true
      },
      date:
      {
        required: true
      },
      image: {
        extension: "jpg|png|jpe?g"
      },
    },
    messages:
    {
      name: "Event name is required.",
      date: "Event date is required.",
      image: {
        extension:  "Please select valid format file"
      }
    },

    submitHandler: function (form)
    {
      formSubmit(form);
    }
  });

  //contact-form
  $("#contact-form").validate({
    errorClass: "has-error",
    highlight: function (element, errorClass) {
      $(element).parents('.form-group').addClass(errorClass);
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).parents('.form-group').removeClass(errorClass);
    },
    rules:
    {
      name:
      {
        required: true
      },
      message:
      {
        required: true
      },
      phone:
      {
        required: true,
        minlength: 10,
        maxlength: 12,
      },
      email:
      {
        required: true,
        email: true,
      },

    },
    messages:
    {
      name: "Please enter name",
      message: "Please enter message",
      email: {
        required: "Please enter email",
        email: "Please enter valid email address"
      },
      phone: {
        required: "Please enter phone number",
        minlength: "Phone must be at least 10 characters long",
        maxlength: "Phone must be less than 12 characters long",
      }
    },

    submitHandler: function (form)
    {
      formSubmit(form);
    }
  });
});


$(".sellers-update").validate({
  rules:
  {
    first_name: {
      required: true,
    },
    last_name: {
      required: true,
    },
    email: {
      required: true,
      email: true
    },
    user_name: {
      required: true,
    },
    phone: {
      required: true,
      minlength:10,
      maxlength:12,
      number: true
    },
    password:{
      required: true,
      minlength: 6
    },
    password_confirmation:{
      required: true,
      minlength: 6,
      equalTo: "#password"
    },
    businessname: {
      required: true,
    },
    cat: {
      required: true,
    },
    city: {
      required: true,
    },
    state: {
      required: true,
    },
    zip: {
      required: true,
    },
    contact: {
      required: true,
      minlength:10,
      maxlength:12,
      number: true
    },
    description: {
      required: true,
    },
  },
  messages:
  {
    first_name: {
      required: "Please enter first name"
    },
    last_name: {
      required: "Please enter last name"
    },
    email: {
      required: "Please enter email",
      email: "Please enter valid email address"
    },
    user_name: {
      required: "Please enter user name"
    },
    phone: {
      required:  "Please enter phone",
      minlength: "Your phone must be at least 10 characters long",
      maxlength: "Your phone must be less than 12 characters long",
      number: "Your phone must be number"
    },
    password: {
      required: "Please enter password",
      minlength: "Your password must be at least 6 characters long"
    },
    password_confirmation: {
      required:  "Please enter confirm password",
      minlength: "Your password must be at least 6 characters long",
      equalTo:   "Please enter the same value again."
    },
    businessname: {
      required: "Please enter business name"
    },
    cat: {
      required: "Please select category"
    },
    city: {
      required: "Please enter city"
    },
    state: {
      required: "Please enter state"
    },
    zip: {
      required: "Please enter zip"
    },
    contact: {
      required:  "Please enter contact no",
      minlength: "Your contact must be at least 10 characters long",
      maxlength: "Your contact must be less than 12 characters long",
      number: "Your contact must be number"
    },
    description: {
      required: "Please enter description"
    }
  },
  submitHandler: function (form)
  {
    formSubmit(form);
  }
});
// checkout-form
$("#checkout-form").validate({
  rules:
  {
    first_name: {
      required: true,
    },
    last_name: {
      required: true,
    },
    email: {
      required: true,
      email: true
    },
    address: {
      required: true,
    },
    state: {
      required: true,
    },
    phone: {
      required: true,
      minlength:10,
      maxlength:12,
      number: true
    }
  },
  messages:
  {
    first_name: {
      required: "Please enter first name"
    },
    last_name: {
      required: "Please enter last name"
    },
    email: {
      required: "Please enter email address",
      email: "Please enter valid email address"
    },
    address: {
      required: "Please enter address"
    },
    state: {
      required: "Please select state"
    },
    phone: {
      required:  "Please enter phone",
      minlength: "Your phone must be at least 10 characters long",
      maxlength: "Your phone must be less than 12 characters long",
      number: "Your phone must be number"
    },

  },
  submitHandler: function (form)
  {
    formSubmit(form);
  }
});




/*====================Start faq form validation================= */
$( document ).ajaxComplete(function() {
    $("#faqFormModal").validate({
        ignore: [],
        rules:
        {
            question: {
                            required: true
                      },
            answer: {
                        ckeditor_required:true
                    }
        },
        messages:
        {
            question: {
                            required: "Please enter your question"
                      }
        },
        submitHandler: function (form)
        {
            formSubmit(form);
        }
    });
});

/*====================Start support form validation================= */
$( document ).ajaxComplete(function() {
    $("#supportFormModal").validate({
        ignore: [],
        rules:
        {
            category_id: {
                            required: true
                      },
            question: {
                            required: true
                      },
            answer: {
                        ckeditor_required:true
                    }
        },
        messages:
        {
            category_id: {
                            required: "Please select your support catgory"
                      },
            question: {
                            required: "Please enter your question"
                      }                      
        },
        submitHandler: function (form)
        {
            formSubmit(form);
        }
    });
});

jQuery.validator.addMethod("ckeditor_required", function (value, element) {
            var idname = $(element).attr('id');
            var editor = CKEDITOR.instances[idname];
            var ckValue = GetTextFromHtml(editor.getData()).replace(/<[^>]*>/gi, '').trim();
            if (ckValue.length === 0) {
                $(element).val(ckValue);
            } else {
                $(element).val(editor.getData());
            }
            return $(element).val().length > 0;
        }, "Please enter your answer");

function GetTextFromHtml(html) {
    var dv = document.createElement("DIV");
    dv.innerHTML = html;
    return dv.textContent || dv.innerText || "";
}

/*====================End faq form validation================= */

/*====================Start Support Category form validation================= */
$( document ).ajaxComplete(function() {
    $("#supportCatFormModal").validate({
        ignore: [],
        rules:
        {
            name: {
                            required: true
                      },
            description: {
                            required: true
                      }

        },
        messages:
        {
            name: {
                            required: "Please enter category name"
            },
            description: {
                            required: "Please enter category description"
            }                      
        },
        submitHandler: function (form)
        {
            formSubmit(form);
        }
    });
});
/*====================End Support Category form validation================= */

function formSubmit(form)
{
  $.ajax({
    url         : form.action,
    type        : form.method,
    data        : new FormData(form),
    contentType : false,
    cache       : false,
    headers     : {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    processData : false,
    dataType    : "json",
    beforeSend  : function () {
      $(".button-disabled").attr("disabled", "disabled");
      $(".loaderSec").css('display','block');
    },
    complete: function () {
      $(".loaderSec").css('display','none');
    },
    success: function (response) {
      if(response.formErrors)
      {
        $.each(response.errors, function( index, value )
        {
          $("input[name='"+index+"']").parents('.form-group').addClass('has-error');
          $("#"+index+"-error").remove();
          $("input[name='"+index+"']").after('<label id="'+index+'-error" class="has-error" for="'+index+'">'+value+'</label>');
          $("textarea[name='"+index+"']").parents('.form-group').addClass('has-error');
          $("textarea[name='"+index+"']").after('<label id="'+index+'-error" class="has-error" for="'+index+'">'+value+'</label>');
          $("select[name='"+index+"']").parents('.form-group').addClass('has-error');
          $("select[name='"+index+"']").after('<label id="'+index+'-error" class="has-error" for="'+index+'">'+value+'</label>');
        });
      }
      // if(response.success_message){
      //   jQuery('.alert-success #successMsg').text(response.success_message);
      //   jQuery('.alert-success').css('display','block');
      //   setTimeout(function(){
      //     jQuery('#successMsg').fadeOut();
      //   },3000);
      // }
      if(response.url)
      {
        if(response.delayTime)
        setTimeout(function() { window.location.href=response.url;}, response.delayTime);
        else
        window.location.href=response.url;
      }
      $.toast().reset('all');
      var delayTime = 3000;
      if(response.delayTime)
      delayTime = response.delayTime;
      if (response.success)
      {
        $.toast({
          heading             : 'Success',
          text                : response.success_message,
          loader              : true,
          loaderBg            : '#fff',
          showHideTransition  : 'fade',
          icon                : 'success',
          hideAfter           : delayTime,
          position            : 'top-right'
        });
      }
      else
      {
        $(".button-disabled").removeAttr("disabled");
        if( response.formErrors)
        { 
          if(!response.errorShow){
            return false;
          }
          $.toast({
            heading             : 'Error',
            text                : response.errors,
            loader              : true,
            loaderBg            : '#fff',
            showHideTransition  : 'fade',
            icon                : 'error',
            hideAfter           : delayTime,
            position            : 'top-right'
          });
        }
        else
        {console.log("assssd");
          jQuery('#InputEmail').val('');
          $.toast({
            heading             : 'Error',
            text                : response.error_message,
            loader              : true,
            loaderBg            : '#fff',
            showHideTransition  : 'fade',
            icon                : 'error',
            hideAfter           : delayTime,
            position            : 'top-right'
          });
        }
      }
      if(response.ajaxPageCallBack)
      {
        response.formid = form.id;
        ajaxPageCallBack(response);
      }
      if(response.resetform)
      $(form.id).resetForm();
      if(response.url)
      {
        if(response.delayTime)
        setTimeout(function() { window.location.href=response.url;}, response.delayTime);
        else
        window.location.href=response.url;
      }
    },
    error:function(response){
      // console.log(response);
      // alert('error')
      jQuery.each(response.responseJSON.errors,function(k,message){
        $.toast({
          heading             : 'Error',
          text                : message,
          loader              : true,
          loaderBg            : '#fff',
          showHideTransition  : 'fade',
          icon                : 'error',
          hideAfter           : 4000,
          position            : 'top-right'
        });
      });
    }
  });
}
