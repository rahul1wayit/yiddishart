
$(document).ready(function(){

  /* 1. Visualizing things on Hover - See next part for action on click */
  $('.stars li').on('mouseover', function(){
    var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

    // Now highlight all the stars that's not after the current hovered star
    $(this).parent().children('li.star').each(function(e){
      if (e < onStar) {
        $(this).addClass('hover');
      }
      else {
        $(this).removeClass('hover');
      }
    });

  }).on('mouseout', function(){
    $(this).parent().children('li.star').each(function(e){
      $(this).removeClass('hover');
    });
  });


  /* 2. Action to perform on click */
  $('.stars li').on('click', function(){
    var designId = jQuery(this).parent().attr('data-id');
    var onStar = parseInt($(this).data('value'), 10); // The star currently selected
    var stars = $(this).parent().children('li.star');

    for (i = 0; i < stars.length; i++) {
      $(stars[i]).removeClass('selected');
    }

    for (i = 0; i < onStar; i++) {
      $(stars[i]).addClass('selected');
    }

    // JUST RESPONSE (Not needed)
    var ratingValue = parseInt($(this).last().data('value'), 10);
    var msg = "";
    if (ratingValue >= 1) {
      jQuery(this).parents('.Rate').find('.rating_inpt').val(ratingValue);
      // var formData = new FormData();
      // formData.append('design_id', designId);
      // formData.append('rating', ratingValue);
      // ajaxHit('addRatingFaviorite','POST',site_url+'/buyer/addRatingFaviorite',formData);
    }
    else {
      msg = "We will improve ourselves. You rated this " + ratingValue + " stars.";
    }
  });

});
