$(function(){
	var layouts = [
		// 'title , file name , layout name'
		"English, arabic, qwerty",
		"Hebrew, hebrew, hebrew-qwerty"
	],

	t, o = '',

	// Change display languageuage, if the definitions are available
	showKb = function(layout){
		var kb = $('.keyBord').getkeyboard();
		kb.options.layout = layout;
		// redraw keyboard with new layout
		kb.redraw();
	};

	$.each(layouts, function(i, l){
		t = l.split(/\s*,\s*/);
		o += '<option data-filename="' + t[1] + '" value="' + t[2] + '">' + t[0] + '</option>';
	});

	// allow theme selector to set up, otherwise it pushes the page down after the
	// keyboard has opened and covers up the <h2> layout title
	setTimeout(function(){
		$('.keyBord').keyboard({
			layout: 'qwerty',
			stayOpen: false
		})
		// activate the typing extension
		.addTyping({
			showTyping: true,
			delay: 50
		})
		.previewKeyset();

		$('.language')
		.html(o)
		.change(function(){
			var kb = $('.keyBord'),
			$this = $(this),
			$opt = $this.find('option:selected'),
			layout = $this.val();
			showKb( layout );
		}).trigger('change');

	}, 100);

});
